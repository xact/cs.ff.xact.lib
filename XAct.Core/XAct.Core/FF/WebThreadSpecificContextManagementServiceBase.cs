﻿//namespace XAct
//{
//    using System;
//    using XAct.Diagnostics;
//    using XAct.Extensions;
//    using XAct.State;

//    /// <summary>
//    /// 
//    /// </summary>
//    /// <typeparam name="TContext"></typeparam>
//    public abstract class WebThreadSpecificContextManagementServiceBase<TContext> :
//        IWebThreadSpecificContextManagementServiceBase<TContext>, IHasXActLibService
//        where TContext :class
//    {
//        /// <summary>
//        /// The Type Name -- used for the SessionCacheKey.
//        /// </summary>
//        protected readonly string _cacheKey;
//        /// <summary>
//        /// </summary>
//        protected readonly ITracingService _tracingService;

//        /// <summary>
//        /// An instance of the <see cref="IContextStateService"/>
//        /// </summary>
//        protected readonly IContextStateService _stateService;

//        /// <summary>
//        /// Gets a current context instance.
//        /// </summary>
//        public TContext Current
//        {
//            get
//            {
//                //Get a web thread specific object:
//                TContext context = EnsureThreadSpecificInstance();
//                _tracingService.Trace(TraceLevel.Verbose, "Popping Thread specific Context: {0}", context);
//                return context;
//            }
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="WebThreadSpecificContextManagementServiceBase{TContext}"/> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        /// <param name="contextStateService">The context state service.</param>
//        protected WebThreadSpecificContextManagementServiceBase(ITracingService tracingService,IContextStateService contextStateService)
//        {
//            //We want the cache key for the end class -- not this base, so
//            //we set it in Constructor, not static constructor.
//            _cacheKey = this.GetType().FullName;

//            _tracingService = tracingService;
//            _stateService = contextStateService;
//        }

//        /// <summary>
//        /// Gets or Creates the Thread specific instance.
//        /// <para>
//        /// Invokes <see cref="Create"/>
//        /// if it is the first time.
//        /// </para>
//        /// </summary>
//        /// <returns></returns>
//        protected virtual TContext EnsureThreadSpecificInstance()
//        {
//            TContext result = _stateService.Items[_cacheKey] as TContext;

//            if (object.Equals(result, default(TContext)))
//            {
//                _stateService.Items[_cacheKey] = result = Create();
//            }
//            return result;
//        }





//        /// <summary>
//        /// Creates a new Context.
//        /// <para>
//        /// Invoked by <see cref="IHasCurrent{TContext}.Current"/>
//        /// to create a thread specific instance if one doesn't exist.
//        /// </para>
//        /// </summary>
//        /// <returns></returns>
//        public virtual TContext Create()
//        {

//            //Invoke protected method to 
//            //get hands on default singleton (using ServiceLocator).
//            TContext srcContext = CreateNewSourceInstance();

//            _tracingService.Trace(TraceLevel.Verbose, "Retrieved source Context: {0}", srcContext);

//                return srcContext;
//        }



//        /// <summary>
//        /// Invoked by <see cref="Create"/> to creates the source instance
//        /// </summary>
//        /// <returns></returns>
//        protected virtual TContext CreateNewSourceInstance()
//        {
//            return XAct.DependencyResolver.Current.GetInstance<TContext>();
//        }





//    }
//}