//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using Microsoft.Practices.ServiceLocation;

//#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 
//using System.Diagnostics.Contracts;
//#endif

//namespace XAct.Services
//{

//    /// <summary>
//    /// 
//    /// </summary>
//    [DebuggerStepThrough]
//    [DebuggerNonUserCode]
//    public class XXX
//    {



//        private Exception _lastError;

//        /// <summary>
//        /// Gets this instance.
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <returns></returns>
//        [DebuggerHidden]
//        [DebuggerStepThrough]
//        [DebuggerNonUserCode]
//        public T Get<T>()
//        {
//            try
//            {
//                if (ServiceLocator.Current != null)
//                {
//                    return ServiceLocator.Current.GetInstance<T>();
//                }
//            }
//            catch (Exception e)
//            {
//                _lastError = e;
//            }
//            return default(T);
//        }
//    }

//    /// <summary>
//    /// </summary>
//    /// <remarks>
//    ///   <para>
//    ///     ServiceLocator is an indirection around Ninject, Unity or other DependencyInjectionContainer.
//    ///   </para>
//    ///   <para>
//    ///     For it to work, one has to first register the DependencyInjectionContainer.
//    ///   </para>
//    /// <para>
//    /// It's already all done for you: see
//    /// <c>XAct.Services.IoC.Ninject</c>, <c>XAct.Services.IoC.Unity</c>,
//    /// <c>XAct.Services.IoC.AspMvc.Ninject</c>, <c>XAct.Services.IoC.AspMvc.Unity</c>,
//    /// <c>XAct.Services.IoC.AspNet.Ninject</c>, or <c>XAct.Services.IoC.AspNet.Unity</c>,
//    /// </para>
//    /// </remarks>
//    /// <internal>
//    ///   <para>
//    ///     A very good question is why on earth would one provide
//    ///     indirection to indirection, right? I mean, we already
//    ///     have a perfectly good wrapper (CommonServiceLocator)
//    ///     around any DependencyInjectionContainer container we prefer to use -- so why
//    ///     on earth would we then wrap a service around it?
//    ///   </para>
//    ///   <para>
//    ///     Good question. The reason is that Common ServiceLocator is
//    ///     a static factory, and any all use of it polutes the code
//    ///     with a reference to an external assembly.
//    ///   </para>
//    ///   <para>
//    /// This solution allows the rest of the library remain Vendor
//    /// agnostic, protecting it from CommonServiceLocator pattern
//    /// (which is version 1 after all, and may change).
//    ///   </para>
//    /// </internal>
//    [DefaultBindingImplementation(typeof (IServiceLocatorService))]
//    public class ServiceLocatorService : XActLibServiceBase,  IServiceLocatorService
//    {

//        //static object _lock = new object();


//        //Amazingly, of all Services as well, I have 
//        //some State in this Service. It's an abomination, I know,
//        //but I honestly, can't work around the issue  raised 
//        //by the code in the CurrentOLD method (namely, that 
//        //DebuggerHidden won't trap things in a Static Method....
//        [ThreadStatic] private static IServiceLocatorService _serviceLocatorService;
//        //Static State across all instances:
//        //static bool _registered;
//        //static readonly Object  _lock = new Object();

//        /*
//                /// <summary>
//                /// Returns the Current ServiceLocatorService.
//                /// </summary>
//                /// <remarks>
//                /// <para>
//                /// ALways tries to get the one registered in the Microsoft ServiceLocator, falling back to a 
//                /// local static singleton.
//                /// </para>
//                /// <para>
//                /// Will trigger Static Constructor if never invoked before,
//                /// ensuring somekind of DependencyInjectionContainer is back there.
//                /// </para>
//                /// </remarks>
//                /// <internal>
//                /// DAMN! DebuggerHidden doesn't work in static methods!!!
//                /// </internal>
//                [DebuggerHidden]
//                private static IServiceLocatorService CurrentOld
//                {
//                    get
//                    {
//                        //This is a static property.
//                        //So we need to switch to a more specific 
//                        //Service, one that is Thread specific, or Request
//                        //specific, depending on the settings of
//                        //XAct.Lib.DefaultServiceLifespan
                
//                        //Anyway...this is where it get's tricky.
//                        //I could make it a ThreadSpecific property, or 
//                        //even an app-wide local var -- it really shouldn't matter
//                        //as this service is stateless.
                
//                        //But it really the whole intent of delegating
//                        //Factorisation of services to an underlying DependencyInjectionContainer.

//                        //Hence not using a local ThreadStatic var
//                        //and using returning the instance of IServiceLocatorService
//                        //registed in DependencyInjectionContainer.

//                        //Except...there's a hitch.

//                        //NinjectBoostrapCommandMessageHandler invokes this service from the following line
//                        //IServiceLocatorService serviceLocatorService = ServiceLocatorService.Current;
//                        //and it turns out that the Service has not yet been registed (nor any other
//                        //service in the lib.

//                        //So it will fail to find it.


//                        //And the hitch is that As it's UnHandled, 

//                        //We always want to get the latest one:
//                        //So look in MSPP's ServiceLocator, to see if an instance
//                        //of this ServiceLocatorService was ever registered:
//                        IServiceLocatorService result;

//                        try
//                        {
//                            //It *should* try/catch/hide the error...
//                            //especially as Property has DebuggerHidden
//                            //on it (except that DebuggerHidden is not 
//                            //applied to properties)
//                            result = GetServiceLocatorService();
//                            //As it exists (no exception), 
//                            //it means it was registered in MSPP,
//                            //and we can return it:
//                            return result;
//                        }
//        // ReSharper disable EmptyGeneralCatchClause
//                        catch 
//        // ReSharper restore EmptyGeneralCatchClause
//                        {
//                        }

//                        //It wasn't found in MSPP...
//                        //So we need to teach MSPP about it:

//                        //Get the MSPP service locator:
//                        IServiceLocator serviceLocator = GetMSPPServiceLocator<IServiceLocator>();

//                        //So that we can auto register ourselves (this IServiceLocatorService) into it:
//                        RegisterServiceByReflection(
//                            serviceLocator,
//                            new ServiceBindingDescriptor(
//                                typeof (IServiceLocatorService), 
//                                typeof (ServiceLocatorService)));

//                        //Call the method again -- this time it will find
//                        //the service interface, instantiate this class
//                        //and we're done:
//                        result = GetServiceLocatorService();
//                        return result;
//                    }
//                }
//        */


//        /// <summary>
//        /// Returns the Current ServiceLocatorService.
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// ALways tries to get the one registered in the Microsoft ServiceLocator.
//        /// </para>
//        /// </remarks>
//        public static IServiceLocatorService Current
//        {
//            get { return _serviceLocatorService ?? (_serviceLocatorService = new ServiceLocatorService()); }
//        }


//        /*
//        public static TInstance Instance<TInstance>()
//        {
//            {
//                return Current.GetInstance<TInstance>();
//            }
//        }

//        public static object Instance(Type serviceType, bool raiseExceptionIfNotFound = true)
//        {
//            {
//                return Current.GetInstance(serviceType,raiseExceptionIfNotFound);
//            }
//        }

//        public static object Instance(Type serviceType, string key, bool raiseExceptionIfNotFound = true)
//        {
//            {
//                return Current.GetInstance(serviceType, key, raiseExceptionIfNotFound);
//            }
//        }

//        public static IEnumerable<object> AllInstances(Type serviceType)
//        {
//            return Current.GetAllInstances(serviceType);
//        }



//        public static TService Instance<TService>(bool raiseExceptionIfNotFound = true)
//        {
//            return Current.GetInstance<TService>(raiseExceptionIfNotFound);
//        }


//        public static TService Instance<TService>(string key, bool raiseExceptionIfNotFound = true)
//        {
//            return Current.GetInstance<TService>(string, raiseExceptionIfNotFound);
//        }

//        public static IEnumerable<TService> AllInstances<TService>()
//        {
//            return Current.GetAllInstances<TService>();
//        }
//        */



//        /// <summary>
//        /// Gets the inner DependencyInjectionContainer.
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <returns></returns>
//        public T GetInnerServiceLocator<T>()
//        {
//            return (T) ServiceLocator.Current;
//        }





//        /// <summary>
//        /// Shortcut wrapper around
//        /// <para>
//        /// <code>
//        /// <![CDATA[
//        /// DependencyResolver.Current.GetInstance<TServiceInterface>();
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </summary>
//        /// <typeparam name="TServiceInterface">The type of the service interface.</typeparam>
//        /// <returns></returns>
//        public TServiceInterface GetInstance<TServiceInterface>()
//        {
//            return Current.GetInstance<TServiceInterface>();
//        }


//        /// <summary>
//        /// Gets an instance of the given service Type.
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Usage Example:
//        /// <code>
//        /// <![CDATA[
//        /// DateTime now =
//        ///    DependencyResolver
//        ///      .Current
//        ///      .GetInstance(typeof(IEnvironmentService)).Now;
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <param name="serviceType">Type of the service.</param>
//        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
//        /// <returns>
//        /// An instance of the given Type.
//        /// </returns>
//        public object GetInstance(Type serviceType, bool raiseExceptionIfNotFound = true)
//        {
//            //Wraps the Microsoft ServiceLocator:
//            try
//            {
//                object o = ServiceLocator.Current.GetInstance(serviceType);
//                return o;

//            }
//#pragma warning disable 168
//            catch (Microsoft.Practices.ServiceLocation.ActivationException activationException)
//#pragma warning restore 168
//            {
//                if (raiseExceptionIfNotFound)
//                {
//                    throw;
//                }
//                return null;
//            }
//        }

//        /// <summary>
//        /// Gets a named instance of the given service Type.
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Usage Example:
//        /// <code>
//        /// <![CDATA[
//        /// DateTime now =
//        ///    DependencyResolver
//        ///      .Current
//        ///      .GetInstance(typeof(IEnvironmentService),"Secondary",true).Now;
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <param name="serviceType">Type of the service.</param>
//        /// <param name="key">The key.</param>
//        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
//        /// <returns>
//        /// An instance of the given Type.
//        /// </returns>
//        public object GetInstance(Type serviceType, string key, bool raiseExceptionIfNotFound = true)
//        {
//            try
//            {
//                //Wraps the Microsoft ServiceLocator:
//                object result = ServiceLocator.Current.GetInstance(serviceType, key);

//                return result;
//            }
//#pragma warning disable 168
//            catch (Microsoft.Practices.ServiceLocation.ActivationException activationException)
//#pragma warning restore 168
//            {
//                if (raiseExceptionIfNotFound)
//                {
//                    throw;
//                }
//                return null;
//            }
//        }

//        /// <summary>
//        ///   Gets an instances of the given service Type currently registered in the container.
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Usage Example:
//        /// <code>
//        /// <![CDATA[
//        /// DateTime now =
//        ///    DependencyResolver
//        ///      .Current
//        ///      .GetAllInstances(typeof(IEnvironmentService))[0].Now;
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <param name = "serviceType">Type of the service.</param>
//        /// <returns>A collection of the given Type.</returns>
//        public IEnumerable<object> GetAllInstances(Type serviceType)
//        {
//            //Wraps the Microsoft ServiceLocator:

//            // ReSharper disable LoopCanBeConvertedToQuery
//            foreach (object result in ServiceLocator.Current.GetAllInstances(serviceType))
//                // ReSharper restore LoopCanBeConvertedToQuery
//            {
//                yield return result;
//            }
//        }

//        /// <summary>
//        /// Gets all instances of the given service Type currently registered in the container.
//        /// <para>
//        /// Internally, this is just wrapping MSPP's 
//        /// ServiceLocator.Current.GetInstance() method,
//        /// with a bit of try/catch.
//        /// </para>
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Usage Example:
//        /// <code>
//        /// <![CDATA[
//        /// DateTime now =
//        ///    DependencyResolver
//        ///      .Current
//        ///      .GetInstance<IEnvironmentService>().Now;
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <typeparam name="TService">The type of the service.</typeparam>
//        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
//        /// <returns>
//        /// An instance of the given Type.
//        /// </returns>
//        public TService GetInstance<TService>(bool raiseExceptionIfNotFound = true)
//        {
//            //Wraps the Microsoft ServiceLocator:
//            try
//            {
//                TService result = ServiceLocator.Current.GetInstance<TService>();

//                return result;
//            }
//#pragma warning disable 168
//            catch (Microsoft.Practices.ServiceLocation.ActivationException activationException)
//#pragma warning restore 168
//            {
//                string innerMessage = (activationException.InnerException ?? activationException).Message;
//                if (raiseExceptionIfNotFound)
//                {
//                    throw;
//                }

//                return default(TService);
//            }
//        }



//        /// <summary>
//        /// Gets an named instance of the given service Type.
//        /// <para>
//        /// Internally, this is just wrapping MSPP's 
//        /// ServiceLocator.Current.GetInstance() method.
//        /// </para>
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Usage Example:
//        /// <code>
//        /// <![CDATA[
//        /// DateTime now =
//        ///    DependencyResolver
//        ///      .Current
//        ///      .GetInstance<IEnvironmentService>("Secondary").Now;
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <typeparam name="TService">The type of the service.</typeparam>
//        /// <param name="key">The key.</param>
//        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
//        /// <returns>
//        /// An instance of the given Type.
//        /// </returns>
//        public TService GetInstance<TService>(string key, bool raiseExceptionIfNotFound = true)
//        {
//            try
//            {
//                //Wraps the Microsoft ServiceLocator:
//                TService result = ServiceLocator.Current.GetInstance<TService>(key);

//                return result;
//            }
//#pragma warning disable 168
//            catch (Microsoft.Practices.ServiceLocation.ActivationException activationException)
//#pragma warning restore 168
//            {
//                if (raiseExceptionIfNotFound)
//                {
//                    throw;
//                }
//                return default(TService);
//            }
//        }

//        /// <summary>
//        ///   Gets all instances of the given service Type currently registered in the container.
//        /// <para>
//        /// Internally, this is just wrapping MSPP's 
//        /// ServiceLocator.Current.GetAllInstances() method.
//        /// </para>
//        /// </summary>
//        /// <remarks>
//        /// <para>
//        /// Usage Example:
//        /// <code>
//        /// <![CDATA[
//        /// DateTime now =
//        ///    DependencyResolver
//        ///      .Current
//        ///      .GetAllInstances<IEnvironmentService>()[0].Now;
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </remarks>
//        /// <typeparam name = "TService">The type of the service.</typeparam>
//        /// <returns>A collection of the given Type.</returns>
//        public IEnumerable<TService> GetAllInstances<TService>()
//        {
//            //Wraps the Microsoft ServiceLocator:
//            return ServiceLocator.Current.GetAllInstances<TService>();
//        }


//        #region Implementation of IServiceProvider

//        /// <summary>
//        ///   Gets the Service object of the specified type.
//        /// </summary>
//        /// <param name = "serviceType">An object that specifies the type of service object to get.</param>
//        /// <returns>
//        ///   A service object of type <paramref name = "serviceType" />.-or- null if there is no service object of type <paramref name = "serviceType" />.
//        /// </returns>
//        /// <returns>An instance of the given Type.</returns>
//        public object GetService(Type serviceType)
//        {
//            object result = ServiceLocator.Current.GetService(serviceType);
//            return result;
//        }

//        #endregion


//        /// <summary>
//        /// Get the <see cref="IServiceLocatorService"/>
//        /// from within MSPP's ServiceLocator,
//        /// without throwing an error if it is not registered in MSPP.
//        /// </summary>
//        /// <returns></returns>
//        [DebuggerHidden]
//        [DebuggerStepThrough]
//        [DebuggerNonUserCode]
//        private static IServiceLocatorService GetServiceLocatorService()
//        {
//            try
//            {
//                return
//                    ServiceLocator.Current.GetInstance<IServiceLocatorService>();
//            }
//            catch
//            {
//                return null;
//            }
//        }



//    }
//}