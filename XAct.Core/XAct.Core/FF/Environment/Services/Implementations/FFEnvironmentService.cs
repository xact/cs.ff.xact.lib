﻿namespace XAct.Environment.Implementations
{
    using System.IO;
    using XAct.Environment;
    using XAct.Environment.Services.Implementations;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IEnvironmentService), BindingLifetimeType.Undefined, Priority.Normal /*OK: An Override of PCL*/)]
    public class FFEnvironmentService : PCLEnvironmentService, IFFEnvironmentService
    {

        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IEnvironmentServiceConfiguration IEnvironmentService.Configuration
        {
            get { return base.Configuration; }
        }

        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public new IFFEnvironmentServiceConfiguration Configuration
        {
            get { return base.Configuration as IFFEnvironmentServiceConfiguration; }
        }


        /// <summary>
        /// Is the environment service.
        /// </summary>
        /// <param name="environmentServiceConfiguration">The environment service configuration.</param>
        /// <param name="environmentManagementService">The environment management service.</param>
        public FFEnvironmentService(
            IFFEnvironmentServiceConfiguration environmentServiceConfiguration,
            IEnvironmentManagementService environmentManagementService )
            : base(environmentServiceConfiguration, environmentManagementService)
        {
            
        }
        /// <summary>
        /// Maps the VirtualPath ('~/SubDir/SomeFile.htm') to the Apps dir ('c:\somedir\SubDir\SomeFile.htm');
        /// </summary>
        /// <param name="virtualPath"></param>
        /// <returns></returns>
        /// <internal><para>8/2/2011: Sky</para></internal>
        public new string MapPath(string virtualPath)
        {
            //if (HttpContext == null)
            //{
                if (virtualPath.StartsWith("~"))
                {
                    virtualPath = virtualPath.Substring(1);
                }
                virtualPath = virtualPath.Replace('/', '\\');
                if (virtualPath.StartsWith("\\"))
                {
                    virtualPath = virtualPath.Substring(1);
                }
                if (Path.IsPathRooted(virtualPath))
                {
                    return virtualPath;
                }
                // ReSharper disable RedundantThisQualifier
                string result = Path.Combine(this.ApplicationBasePath, virtualPath);
                return result;
                // ReSharper restore RedundantThisQualifier

                //throw new NotImplementedException("Not yet sure how to do it for Non Http scenarios.");
            //}
            //throw new NotImplementedException("Not yet sure how I will do MapPath, using Reflection...but coming.");
        }


    }
}
