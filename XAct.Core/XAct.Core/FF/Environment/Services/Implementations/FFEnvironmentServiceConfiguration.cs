﻿namespace XAct.Environment.Implementations
{
    using XAct.Environment;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IFFEnvironmentServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.Low /*OK: An Override Priority*/)]
    [DefaultBindingImplementation(typeof(IPCLEnvironmentServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.Normal /*OK: An Override Priority*/)] //Beats .Low
    [DefaultBindingImplementation(typeof(IEnvironmentServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.Normal /*OK: An Override Priority*/)] //Beats .VeryLow
    public class FFEnvironmentServiceConfiguration : PCLEnvironmentServiceConfiguration, IFFEnvironmentServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FFEnvironmentServiceConfiguration"/> class.
        /// </summary>
        public FFEnvironmentServiceConfiguration()
            : base()
        {
            EnvironmentIdentifier = "/";
            DomainName = System.Environment.UserDomainName;
            MachineName = System.Environment.MachineName;
            UserInteractive = System.Environment.UserInteractive;


            //Will be foo/bar/bin/debug in NUnit and Console apps.
            //In web app, this will be foo/bar (excluding bin/debug)
            //This is AppDomain.CurrentDomain.BaseDirectory 
            AppDir = AppDomainExtensions.BaseDir;

            //Hence tack on:
            if (ApplicationEnvironment.HttpContext != null)
            {

                //AppDomain does not exist in PCL, but it would be:
                //AppDomain.CurrentDomain.SetupInformation.PrivateBinPath

                AppDir = AppDir + "\\bin";
            }

        }
    }
}