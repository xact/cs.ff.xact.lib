﻿//namespace XAct.Environment
//{
//    using System;

//    /// <summary>
//    /// 
//    /// </summary>
//    public static class AppEnvironment
//    {
//        private static string _appDir;

//        /// <summary>
//        ///   Gets the Application's base path,
//        ///   <para>
//        ///     Note: without a slash at the end.
//        ///   </para>
//        /// </summary>
//        /// <internal>
//        ///   Messy, I know, but I wish to keep it portable
//        ///   to CF (even if used only 1% of time), while
//        ///   not causing a CAS registry access requirement
//        ///   for Full Framework development conditions.
//        /// </internal>
//        /// <value>The app dir.</value>
//        public static string AppDir
//        {
//            get
//            {
//                //Assembly.GetExecutingAssembly().CodeBase;

//                if (string.IsNullOrEmpty(_appDir))
//                {
//#if ! ((CE) || (PocketPC) || (pocketPC) || (WindowsCE))
//                    //_appDir = Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory);
//                    _appDir = AppDomain.CurrentDomain.BaseDirectory;
//#else
//                            _appDir = EnvCF.AppDir;
//#endif
//                    //Note that the problem is a bit more tricky actually.
//                    //It's important to also figure out the root path even in the Designer, 
//                    //when HttpContext is not available.
//                    //Options are:
//                    //System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
//                    //* System.AppDomain.CurrentDomain.BaseDirectory =
//                    //= D:\WEBSITES\DEMOS.XACT-SOLUTIONS.COM\ 
//                    //* System.Web.HttpContext.Current.Request.PhysicalApplicationPath =
//                    //= D:\WEBSITES\DEMOS.XACT-SOLUTIONS.COM\ 
//                    //....Which deep down wraps around:
//                    //System.Web.HttpRuntime.AppDomainAppPath =
//                    //D:\WEBSITES\DEMOS.XACT-SOLUTIONS.COM\ 

//                    //Not Portable as it doesn't exist on CF:
//                    //System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase
//                    //Other options to know about:
//                    //string s1 = System.Reflection.Assembly.GetEntryAssembly().GetName().CodeBase;
//                    //file:///d:/USERS/../XAct.Study.ConfigHrefAssembly/bin/Debug/XAct.Study.ConfigHrefAssembly.EXE
//                    //string s2= Environment.CurrentDirectory;
//                    //d:\USERS\...\XAct.Study.ConfigHrefAssembly\bin\Debug
//                    //string s3 = System.AppDomain.CurrentDomain.BaseDirectory;
//                    //d:\USERS\..\XAct.Study.ConfigHrefAssembly\bin\Debug\

//                    //Noticed that in Console Apps, coming back with slash
//                    if (_appDir.EndsWith("\\"))
//                    {
//                        _appDir = _appDir.Substring(0, _appDir.Length - 1);
//                    }
//                }
//                return _appDir;
//            }
//        }
//    }
//}
