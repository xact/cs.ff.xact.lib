﻿namespace XAct.Environment.Implementations
{
    using System.Security.Principal;
    using System.Threading;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IPrincipalService), BindingLifetimeType.Undefined, Priority.Normal /*OK: An Override Priority*/)]
    public class FFPrincipalService : XActLibServiceBase, IFFPrincipalService
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FFPrincipalService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public FFPrincipalService(ITracingService tracingService, IEnvironmentService environmentService) : base(tracingService)
        {
            _environmentService = environmentService;
        }

        /// <summary>
        /// Sets the thread principal (and HttpContext User if within a web application).
        /// </summary>
        public IPrincipal Principal
        {
            get { return System.Threading.Thread.CurrentPrincipal; }
            set
            {
                IPrincipal principal = value;

                SetHttpUserPropertyIfAvailable(principal);

                //Set on thread *AS WELL*:
                Thread.CurrentPrincipal = principal;
            }
        }




        /// <summary>
        /// Gets the current identity identifier (the most unique identifier for a user).
        /// <para>
        /// For a windows identity, this will be the SID, others, the name -- but 
        /// note that a name is not guaranteed to be consistent.
        /// </para>
        /// </summary>
        /// <value>
        /// The current identity identifier.
        /// </value>
        public string CurrentIdentityIdentifier
        {
            get
            {
                //System.Security.Principal.
                WindowsIdentity wi =
                    this.Principal.Identity as WindowsIdentity;
                if ((wi != null) && (wi.User != null))
                {
                    return wi.User.AccountDomainSid.ToString();

                    //return wi.Owner.AccountDomainSid.ToString();
                }
                var result = this.Principal.Identity.Name;
                return result;

            }
        }










 
        private void SetHttpUserPropertyIfAvailable(IPrincipal principal)
        {
            object httpContext = this._environmentService.HttpContext;

            //But set on Context:
            if (httpContext != null)
            {
                httpContext.GetType().GetProperty("User").SetValue(httpContext, principal, null);
            }
        }
    }
}