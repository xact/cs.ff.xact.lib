﻿namespace XAct.Environment
{
    using XAct.Environment;

    /// <summary>
    /// 
    /// </summary>
    public interface IFFEnvironmentService : IEnvironmentService , IHasXActLibService
    {
        /// <summary>
        /// Gets the common singleton settings shared between 
        /// all instances of this service
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        new IFFEnvironmentServiceConfiguration Configuration { get; }
        
    }
}