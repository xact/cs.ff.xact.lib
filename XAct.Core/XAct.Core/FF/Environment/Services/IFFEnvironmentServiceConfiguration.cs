﻿namespace XAct.Environment
{
    /// <summary>
    /// 
    /// </summary>
    public interface IFFEnvironmentServiceConfiguration : IPCLEnvironmentServiceConfiguration, IHasXActLibServiceConfiguration
    {
        
    }
}