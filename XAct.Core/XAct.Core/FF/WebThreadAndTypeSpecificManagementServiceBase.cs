﻿namespace XAct
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics;
    using XAct.Extensions;
    using XAct.State;

    /// <summary>
    /// Base class for ManagementServices to return a current context per thread,
    /// that is cloned from a default base one. Useful for having base settings, 
    /// but allowing customization as needed for a request.
    /// </summary>
    /// <typeparam name="TContext">The type of the context.</typeparam>
    public abstract class WebThreadAndTypeSpecificManagementServiceBase<TContext> : IWebThreadAndTypeSpecificManagementServiceBase<TContext>, IHasXActLibService
        //NO: where TContext : ICloneable
    {
        /// <summary>
        /// Flag determining whether values should be cloned to a new object.
        /// </summary>
        protected  bool IsMutable = true;

        private readonly string _cacheKey;
        private readonly ITracingService _tracingService;
        private readonly IContextStateService _stateService;


        ///// <summary>
        ///// Gets or sets the default instance of the type.
        ///// </summary>
        ///// <value>The default.</value>
        //protected static TContext Default { get; set; }



        /// <summary>
        /// Gets the existing or newly Factoried Context object.
        /// </summary>
        /// <value>The current.</value>
        public TContext GetCurrent<TSpecializedContext>() where TSpecializedContext : TContext
        {
                //Get a web thread specific object:
                TContext context = EnsureThreadSpecificInstance<TSpecializedContext>();

                _tracingService.Trace(TraceLevel.Verbose, "Popping Thread specific Context: {0}", context);
            
            return context;
        }




        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="WebThreadSpecificStackableContextManagementServiceBase&lt;TContext&gt;"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="stateService">The state service.</param>
        protected WebThreadAndTypeSpecificManagementServiceBase(ITracingService tracingService, IContextStateService stateService)
        {
            //We want the cache key for the end class -- not this base, so
            //we set it in Constructor, not static constructor.
            _cacheKey = this.GetType().FullName;

            _tracingService = tracingService;
            _stateService = stateService;
        }


        /// <summary>
        /// Pushes a new context on to the Context stack, for <see cref="GetCurrent{TSpecializedContext}"/> to return.
        /// </summary>
        /// <param name="context">The context.</param>
        public void Push<TSpecializedContext>(TSpecializedContext context) where TSpecializedContext :TContext
        {
            _tracingService.Trace(TraceLevel.Verbose, "Pushing new Thread specific Context: {0}", context);

            //Get the stack that is specific to this thread, and push a new instance onto it.
            GetOrCreateThreadSpecificStack<TSpecializedContext>().Push(context);
        }


        /// <summary>
        /// Pops the specified context off the Context stack.
        /// </summary>
        public TContext Pop<TSpecializedContext>() where TSpecializedContext :TContext
        {

            //Get the stack that is specific to this thread, and pop off.
            TContext context = GetOrCreateThreadSpecificStack<TSpecializedContext>().Pop();

            _tracingService.Trace(TraceLevel.Verbose, "Popping Thread specific Context: {0}", context);

            return context;
        }

        /// <summary>
        /// Counts the number of items pushed onto the internal stack.
        /// </summary>
        /// <returns></returns>
        public int Count<TSpecializedContext>() where TSpecializedContext : TContext
        {
            var result = GetOrCreateThreadSpecificStack<TSpecializedContext>().Count;
            return result;
        }



        /// <summary>
        /// Returns existing or newly Factoried Context object.
        /// </summary>
        /// <returns></returns>
        private TContext EnsureThreadSpecificInstance<TSpecializedContext>() where TSpecializedContext : TContext
        {
            Stack<TContext> stack = GetOrCreateThreadSpecificStack<TSpecializedContext>();

            if (stack.Count == 0)
            {
                TContext result = Create<TSpecializedContext>();

                stack.Push(result);
            }
            return stack.Peek();
        }




        /// <summary>
        /// Gets the thread specific stack in which contexts are kept.
        /// </summary>
        /// <returns></returns>
        private Stack<TContext> GetOrCreateThreadSpecificStack<TContextSpecialization>() where TContextSpecialization : TContext
        {
            //Get or create Dictionary of Stacks:
            Dictionary<object, Stack<TContext>> dictionary =
                _stateService.Items[_cacheKey] as Dictionary<object, Stack<TContext>>;
            if (dictionary == null)
            {
                _stateService.Items[_cacheKey] = dictionary = new Dictionary<object, Stack<TContext>>();
            }

            //Within dictionary, try to find a stack by it's key, which is the Type of the Context we are looking for.
            Stack<TContext> stack;

            Type specializedContextType = typeof(TContextSpecialization);

            if (!dictionary.TryGetValue(specializedContextType, out stack))
            {
                stack = new Stack<TContext>();

                dictionary.Add(specializedContextType, stack);
            }

            return stack;
        }


        /// <summary>
        /// Creates a new  <typeparam name="TSpecializedContext"></typeparam>.
        /// <para>
        /// Invoked by <see cref="GetCurrent{TSpecializedContext}"/>
        /// to create a thread specific instance if one doesn't exist.
        /// </para>
        /// <para>
        /// If <see cref="IsMutable"/>, clones the object before returning it.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public TContext Create<TSpecializedContext>() where TSpecializedContext :TContext
        {

            //Invoke protected method to 
            //get hands on default singleton (using ServiceLocator).
            TContext srcContext = CreateNewSourceInstance<TSpecializedContext>();

            _tracingService.Trace(TraceLevel.Verbose, "Retrieved source Context: {0}", srcContext);


            if (!IsMutable)
            {
                return srcContext;
            }

            return CloneSourceInstanceValuesToNewThreadInstance(srcContext);
        }

        /// <summary>
        /// Invoked by <see cref="Create{TSpecializedContext}"/> to creates the source instance
        /// before <see cref="Create{TSpecializedContext}"/> calls <see cref="CloneSourceInstanceValuesToNewThreadInstance"/>
        /// if <see cref="IsMutable"/>=<c>true</c>.
        /// </summary>
        /// <internal>
        /// Implementors can use <see cref="CreateNewSourceInstanceHelper"/> to 
        /// use the ServiceLocator to create the instance.
        /// </internal>
        /// <returns></returns>
        protected abstract TContext CreateNewSourceInstance<TSpecializedContext>();

        /// <summary>
        /// Clones the source instance values to the new thread specific instance.
        /// </summary>
        /// <internal>
        /// <para>
        /// Implementators can use <see cref="CloneSourceInstanceValuesToNewThreadInstanceHelper"/> if they don't have a 
        /// more custom solution.
        /// </para>
        /// </internal>
        /// <param name="srcContext">The SRC context.</param>
        /// <returns></returns>
        protected abstract TContext CloneSourceInstanceValuesToNewThreadInstance(TContext srcContext);





        /// <summary>
        /// Creates the new source instance using ServiceLocator.
        /// <para>
        /// The default/recommended solution for <see cref="CreateNewSourceInstance{TSpecializedContext}"/>
        /// </para>
        /// </summary>
        /// <returns></returns>
        protected virtual TContext CreateNewSourceInstanceHelper()
        {

            return XAct.DependencyResolver.Current.GetInstance<TContext>();

            //if (Default == null)
            //{
            //    Default = XAct.DependencyResolver.Current.GetInstance<TContext>();
            //}
            //return Default;
        }



        /// <summary>
        /// Helper method to provide cloning if the srcContext is <see cref="ICloneable"/>.
        /// If not, uses the <c>ObjectReflectionExtensions.CloneTheUncloneable{T}</c> extension method.
        /// </summary>
        /// <param name="srcContext">The SRC context.</param>
        /// <returns></returns>
        protected virtual TContext CloneSourceInstanceValuesToNewThreadInstanceHelper(TContext srcContext)
        {
            //Clone to another copy to leave the original untouched:
            if (srcContext is ICloneable)
            {
                TContext result = (TContext)((ICloneable)srcContext).Clone();

                _tracingService.Trace(TraceLevel.Verbose, "Clonned Context to: {0}", result);
                return result;
            }
            return srcContext.CloneTheUncloneable();
        }



    }
}
