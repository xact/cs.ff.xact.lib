﻿// ReSharper disable CheckNamespace
namespace XAct.Security.Permissions
// ReSharper restore CheckNamespace
{
    using System.Security;
    using System.Security.Permissions;

    /// <summary>
    /// Extension methods to the <see cref="PermissionSet"/>
    /// object
    /// </summary>
    public static class PermissionSetExtensions
    {

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        /// An extension method to check whether the user
        /// has the required permission.
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// var requestedPermission = new FileIOPermission(FileIOPermissionAccess.Write,folder);
        /// 
        /// if (AppDomain.CurrentDomain.PermissionSet.IsPermitted(requestedPermission))
        /// {
        /// //go ahead...
        /// }else{
        ///   //...hum...
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="permissionSet"></param>
        /// <param name="requestedPermission"></param>
        public static bool IsPermitted(this PermissionSet permissionSet, IPermission requestedPermission)
        {
            //var permission = new FileIOPermission(FileIOPermissionAccess.Write, folder);

            PermissionSet requestedPermissionSet = new PermissionSet(PermissionState.None);
            requestedPermissionSet.AddPermission(requestedPermission);

            return requestedPermissionSet.IsSubsetOf(permissionSet);
        }

        /// <summary>
        /// <para>
        /// An XActLib Extension.
        /// </para>
        /// <para>
        /// Usage Example:
        /// <code>
        /// <![CDATA[
        /// var requestedPermission = new FileIOPermission(FileIOPermissionAccess.Write,folder);
        /// var requestedPermissionSet = new RequestedPermissionSet(PermissionState.None);
        /// requestedPermissionSet.AddPermission(requestedPermissionSet);
        /// 
        /// if (AppDomain.CurrentDomain.PermissionSet.IsPermitted(requestedPermissionSet))
        /// {
        /// //go ahead...
        /// }else{
        ///   //...hum...
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="permissionSet">The permission set.</param>
        /// <param name="requestedPermissionSet">The requested permission set.</param>
        /// <returns></returns>
        public static bool IsPermitted(this PermissionSet permissionSet, PermissionSet requestedPermissionSet)
        {
            return permissionSet.IsSubsetOf(permissionSet);
        }

        

    }
}
