﻿namespace XAct.IO
{
    /// <summary>
    /// A specialization of the <see cref="IIOService"/> Contract, to work with IsolatedStorage.
    /// <para>
    /// This is ultimately a safer ('greener') solution than using FSIOService.
    /// </para>
    /// </summary>
    public interface IIsolatedStorageIOService : IIOService, IHasXActLibService
    {
        //string GetPathToFileStore(IsolatedStorageFile isoStore);
    }
}