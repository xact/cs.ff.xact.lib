namespace XAct.IO.Implementations
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using XAct.Diagnostics;
    using XAct.Enums;
    using XAct.FF.IO.Entities;
    using XAct.Services;
    using XAct.Threading;

    /// <summary>
    /// A File based implementation of <see cref="FSIOService"/>
    /// <para>
    /// Note that usage of this implementation implies a much higher
    /// security risk (direct access to HD always does).
    /// </para>
    /// <para>
    /// Consider using an IsolatedStorage implementation of 
    /// <see cref="IIOService"/> (provided in XAct.IO.dll) instead.
    /// </para>
    /// </summary>
    /// <remarks>
    /// <para>
    /// 
    /// </para>
    /// </remarks>
    /// <internal><para>5/8/2011: Sky</para></internal>
    [DefaultBindingImplementation(typeof(IIOService), BindingLifetimeType.Undefined, Priority.Normal  /*OK: An Override Priority*/)]
// ReSharper disable InconsistentNaming
    public class FSIOService : XActLibServiceBase, IFSIOService
// ReSharper restore InconsistentNaming
    {

        /// <summary>
        /// Gets the name of the Directory Accessibility test file.
        /// </summary>
        /// <value>
        /// The name of the access test file.
        /// </value>
        public string DirectoryAccessTestFileName {
            get
            {
                return _accessTestFileName ;
            }
        }
        private const string _accessTestFileName = "_integrationTest.txt";


        private readonly IPathService _pathService;


        /// <summary>
        /// Initializes a new instance of the <see cref="FSIOService" /> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        /// <param name="pathService">The path service.</param>
        /// <internal>7/17/2011: Sky</internal>
        public FSIOService(ITracingService tracingService, IPathService pathService):base(tracingService)
        {
            _pathService = pathService;
        }


        /// <summary>
        /// Opens the specified resource with the given settings.
        /// </summary>
        /// <param name="fileFullName">The URI.</param>
        /// <param name="fileMode">The file mode.</param>
        /// <param name="fileAccess">The file access.</param>
        /// <param name="fileShare">The file share.</param>
        /// <returns></returns>
#pragma warning disable 1998
        public async Task<Stream> FileOpenAsync(string fileFullName, XAct.IO.FileMode fileMode,
#pragma warning restore 1998
                                                XAct.IO.FileAccess fileAccess,
                                                XAct.IO.FileShare fileShare)
        {
            XXX(ref fileFullName);

            return
                //await Task<Stream>.Run(
                //() => 
                new FileStream(fileFullName, fileMode.MapTo(), fileAccess.MapTo(), fileShare.MapTo());
            //     );
        }


        /// <summary>
        /// Opens the read.
        /// </summary>
        /// <param name="fileFullName">The path.</param>
        /// <returns></returns>
        /// <internal><para>5/8/2011: Sky</para></internal>
#pragma warning disable 1998
        public async Task<Stream> FileOpenReadAsync(string fileFullName)
#pragma warning restore 1998
        {
            _tracingService.Trace(TraceLevel.Verbose, "OpenRead('{0}')", fileFullName);

            XXX(ref fileFullName);

            return File.OpenRead(fileFullName);
            //return await Task<Stream>.Run(()=>File.OpenRead(fileFullName));
            //Underneath, it's doing:      return new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);

        }

        /// <summary>
        /// Opens the write.
        /// </summary>
        /// <param name="fileFullName">The path.</param>
        /// <param name="replaceAnyIfFound">if set to <c>true</c> replace any if file already exists.</param>
        /// <param name="append">if set to <c>true</c> opens the stream for appending.</param>
        /// <returns></returns>
        /// <internal>5/8/2011: Sky</internal>
#pragma warning disable 1998
        public async Task<Stream> FileOpenWriteAsync(string fileFullName, bool replaceAnyIfFound=true, bool append=false)
#pragma warning restore 1998
        {
            _tracingService.Trace(TraceLevel.Verbose, "OpenWrite('{0}')", fileFullName);

            XXX(ref fileFullName);
            
            //File.OpenWrite is equiv to
            //FileStream(OpenOrCreate, Write, share=None).

            if ((!replaceAnyIfFound)&&(FileExistsAsync(fileFullName).Result))
            {
// ReSharper disable LocalizableElement
                throw new ArgumentException("uri","File {0} already exists.".FormatStringExceptionCulture(fileFullName));
// ReSharper restore LocalizableElement
            }

            FileMode fileMode = (append) ? FileMode.OpenOrCreate : FileMode.Append;

            //return File.OpenWrite(path);
            return new FileStream(fileFullName, fileMode, FileAccess.Write, FileShare.None);
            //return await Task.Run(() => new FileStream(fileFullName, fileMode, FileAccess.Write, FileShare.None));
        }

        /// <summary>
        /// Opens an existing UTF-8 text for reading.
        /// </summary>
        /// <param name="fileFullName">The path.</param>
        /// <returns></returns>
        /// <internal><para>5/8/2011: Sky</para></internal>
        [Obsolete("Obfuscates what is really going on. Prefer instead using ioService.OpenRead(path).OpenStreamReader()")]
        public async Task<StreamReader> FileOpenTextAsync(string fileFullName)
        {
            _tracingService.Trace(TraceLevel.Verbose, "OpenText('{0}')", fileFullName);
            
            XXX(ref fileFullName);


            return await Task.Run<StreamReader>(()=> File.OpenText(fileFullName));
        }

        /// <summary>
        /// Replaces the specified source file name.
        /// </summary>
        /// <param name="sourceFileFullName">Name of the source file.</param>
        /// <param name="destinationFileFullName">Name of the destination file.</param>
        /// <param name="destinationBackupFileName">Name of the destination backup file.</param>
        /// <internal><para>5/8/2011: Sky</para></internal>
#pragma warning disable 1998
        public async Task FileReplaceAsync(string sourceFileFullName, string destinationFileFullName, string destinationBackupFileName)
#pragma warning restore 1998
        {
            _tracingService.Trace(TraceLevel.Verbose, "Replace('{0}','{1}','{2}')", sourceFileFullName, destinationFileFullName,
                                destinationBackupFileName);

            XXX(ref sourceFileFullName);
            XXX(ref destinationFileFullName);

            File.Replace(sourceFileFullName, destinationFileFullName, destinationBackupFileName);
            //await Task.Run(() => File.Replace(sourceFileFullName, destinationFileFullName, destinationBackupFileName));
        }


        /// <summary>
        /// Creates the specified path.
        /// </summary>
        /// <param name="sourceFileFullName">Full name of the source file.</param>
        /// <returns></returns>
        /// <internal>5/8/2011: Sky</internal>
        [Obsolete("Obfuscates what is really going on: prefer using ioService.OpenWrite(path,true,false)")]
#pragma warning disable 1998
        public async Task<Stream> FileCreateAsync(string sourceFileFullName)
#pragma warning restore 1998
        {
            _tracingService.Trace(TraceLevel.Verbose, "Create('{0}')", sourceFileFullName);

            XXX(ref sourceFileFullName);

            return File.Create(sourceFileFullName);
            //return await Task.Run<Stream>(() => File.Create(sourceFileFullName));
        }

        /// <summary>
        /// Deletes the specified path.
        /// </summary>
        /// <param name="fileFullName">The path.</param>
        /// <internal><para>5/8/2011: Sky</para></internal>
#pragma warning disable 1998
        public async Task FileDeleteAsync(string fileFullName  )
#pragma warning restore 1998
        {
            _tracingService.Trace(TraceLevel.Verbose, "Delete('{0}')", fileFullName);

            XXX(ref fileFullName);

            File.Delete(fileFullName);
            //await Task.Run(() => File.Delete(fileFullName));
        }

        /// <summary>
        /// Existses the specified path.
        /// </summary>
        /// <param name="fileFullName">The path.</param>
        /// <returns></returns>
        /// <internal><para>5/8/2011: Sky</para></internal>
#pragma warning disable 1998
        public async Task<bool> FileExistsAsync(string fileFullName)
#pragma warning restore 1998
        {
            _tracingService.Trace(TraceLevel.Verbose, "Exists('{0}')", fileFullName);

            XXX(ref fileFullName);

            return File.Exists(fileFullName);
            //return await Task.Run(() => File.Exists(fileFullName));
        }

        /// <summary>
        /// Moves the specified source file name.
        /// </summary>
        /// <param name="sourceFileName">Name of the source file.</param>
        /// <param name="destinationFileName">Name of the dest file.</param>
        /// <param name="overWriteAllowed">if set to <c>true</c> [over write allowed].</param>
        /// <internal>5/8/2011: Sky</internal>
        public async Task FileMoveAsync(string sourceFileName, string destinationFileName, bool overWriteAllowed = false)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Move('{0}','{1}')", sourceFileName, destinationFileName);

            XXX(ref sourceFileName);
            XXX(ref destinationFileName);

            if (await FileExistsAsync(destinationFileName))
            {
                if (!overWriteAllowed)
                {
                    throw new ArgumentNullException("destinationFileName",
                                                    "Target FileName ({0}) already exists.".FormatStringExceptionCulture
                                                        (destinationFileName));
                }
                await FileDeleteAsync(destinationFileName);
            }

            File.Move(sourceFileName, destinationFileName);
            //await Task.Run(() => File.Move(sourceFileName, destinationFileName));
        }


        /// <summary>
        /// Appends all text.
        /// </summary>
        /// <param name="fileFullName">The path.</param>
        /// <param name="contents">The contents.</param>
        [Obsolete("Obfuscates whats really going on. Prefer using ioService.OpenWrite(uri,true, true).OpenStreamWriter().Write(contents);")]
#pragma warning disable 1998
        public async Task FileAppendAllTextAsync(string fileFullName, string contents)
#pragma warning restore 1998
        {
            _tracingService.Trace(TraceLevel.Verbose, "AppendAllText('{0}','Length:{1}')", fileFullName, contents.LengthSafe());

            XXX(ref fileFullName);

            File.AppendAllText(fileFullName, contents);
            //await Task.Run(() => File.AppendAllText(fileFullName, contents));
        }

        /// <summary>
        /// Copies the specified source file name.
        /// </summary>
        /// <param name="sourceFileFullName">Name of the source file.</param>
        /// <param name="destinationFileFullName">Name of the destination file.</param>
        /// <param name="overWriteAllowed">if set to <c>true</c> [over write allowed].</param>
        public async Task FileCopyAsync(string sourceFileFullName, string destinationFileFullName, bool overWriteAllowed=false)
        {
            _tracingService.Trace(TraceLevel.Verbose, "Copy('{0}','{1}')", sourceFileFullName, destinationFileFullName);

            XXX(ref sourceFileFullName);
            XXX(ref destinationFileFullName);

            if ((!overWriteAllowed) && await FileExistsAsync(destinationFileFullName))
            {
                throw new ArgumentNullException("destinationFileFullName", "Target FileName ({0}) already exists.".FormatStringExceptionCulture(destinationFileFullName));
            }

            File.Copy(sourceFileFullName, destinationFileFullName);
            //await Task.Run(() => File.Copy(sourceFileFullName, destinationFileFullName));
        }


        /// <summary>
        /// A wrapping of Directory.GetFiles 
        /// <para>
        /// At present, don't think this deserves its own service. We'll see.
        /// </para>
        /// </summary>
        /// <param name="directoryFullName">Name of the directory.</param>
        /// <param name="searchPattern">The search pattern.</param>
        /// <param name="searchOption">The search option.</param>
        /// <returns></returns>
#pragma warning disable 1998
        public async Task<string[]> GetDirectoryFileNamesAsync(string directoryFullName, string searchPattern = null, HierarchicalOperationOption searchOption = HierarchicalOperationOption.TopOnly)
#pragma warning restore 1998
        {

            XXX(ref directoryFullName);

            if (string.IsNullOrEmpty(searchPattern))
            {
                searchPattern = "*";
            }

            return Directory.GetFiles(directoryFullName, searchPattern, searchOption.MapTo());
            //return await Task.Run(() => Directory.GetFiles(directoryFullName, searchPattern, searchOption.MapTo()));
        }

        /// <summary>
        /// Creates the directory.
        /// </summary>
        /// <param name="directoryFullName">The directory path.</param>
        /// <returns></returns>
#pragma warning disable 1998
        public async Task DirectoryCreateAsync(string directoryFullName)
#pragma warning restore 1998
        {
            XXX(ref directoryFullName);
            
            Directory.CreateDirectory(directoryFullName);
            //await Task.Run(() => Directory.CreateDirectory(directoryFullName));
        }

        /// <summary>
        /// Determines if the directory exists.
        /// </summary>
        /// <param name="directoryFullName">The directory path.</param>
        /// <param name="createIfMissing">if set to <c>true</c> [create if missing].</param>
        /// <returns></returns>
#pragma warning disable 1998
        public async Task<bool> DirectoryExistsAsync(string directoryFullName,bool createIfMissing=false)
#pragma warning restore 1998
        {
            XXX(ref directoryFullName);

            //return await 
            //Task.Run(() =>
                         //{
                             if (Directory.Exists(directoryFullName))
                             {
                                 return true;
                             }

                             if (!createIfMissing)
                             {
                                 return false;
                             }
                             DirectoryCreateAsync(directoryFullName).Wait();

                             return Directory.Exists(directoryFullName);
                         //});
        }

        /// <summary>
        /// Determines.
        /// </summary>
        /// <param name="directoryFullName">The directory path.</param>
#pragma warning disable 1998
        public async Task DirectoryDeleteAsync(string directoryFullName)
#pragma warning restore 1998
        {
            XXX(ref directoryFullName);

            Directory.Delete(directoryFullName);
            //await Task.Run(() => Directory.Delete(directoryFullName));
        }




        /// <summary>
        /// Directories the get date time asynchronous.
        /// </summary>
        /// <param name="directoryFullName">Full name of the directory.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
#pragma warning disable 1998
        public async Task<DateTime> DirectoryGetDateTimeAsync(string directoryFullName, AuditableEvent type)
#pragma warning restore 1998
        {

            XXX(ref directoryFullName);

            //return await Task.Run(() =>
            //{
                switch (type)
                {
                    case AuditableEvent.Created:
                        return  Directory.GetCreationTimeUtc(directoryFullName);
                    case AuditableEvent.Accessed:
                        return Directory.GetLastAccessTimeUtc(directoryFullName);
                    case AuditableEvent.Modified:
                        return Directory.GetLastWriteTimeUtc(directoryFullName);
                    default:
                        return TaskConstants<DateTime>.NotImplemented.Result;
                }
            //});
        }


        /// <summary>
        /// Sets the directory's auditing dates.
        /// </summary>
        /// <param name="directoryFullName">The directory information.</param>
        /// <param name="type">The type.</param>
        /// <param name="dateTimeUtc">The date time UTC.</param>
        /// <exception cref="System.NotSupportedException">Only Type=Created, Accessed, Modified are supported.</exception>
#pragma warning disable 1998
        public async Task DirectorySetDateTimeAsync(string directoryFullName, AuditableEvent type, DateTime dateTimeUtc)
#pragma warning restore 1998
        {

            XXX(ref directoryFullName);

            //await Task.Run(() =>
            //                   {
                                   switch (type)
                                   {
                                       case AuditableEvent.Created:
                                           Directory.SetCreationTimeUtc(directoryFullName, dateTimeUtc);
                                           break;
                                       case AuditableEvent.Accessed:
                                           Directory.SetLastAccessTimeUtc(directoryFullName, dateTimeUtc);
                                           break;
                                       case AuditableEvent.Modified:
                                           Directory.SetLastWriteTimeUtc(directoryFullName, dateTimeUtc);
                                           break;
                                       default:
                                           throw new NotSupportedException(
                                               "Only Type=Created, Accessed, Modified are supported.");
                                   }
                               //});

            
        }


        /// <summary>
        /// Gets the files's size.
        /// </summary>
        /// <param name="fileFullName">Full name of the directory.</param>
#pragma warning disable 1998
        public async Task<long> FileGetSizeAsync(string fileFullName)
#pragma warning restore 1998
        {
            //return await Task.Run(()=>
            //{
             FileInfo fileInfo = new FileInfo(fileFullName);
            return (fileInfo.Exists) ? fileInfo.Length : 0;
            //});
        }


        /// <summary>
        /// Gets the directory's auditing dates.
        /// </summary>
        /// <param name="directoryFullName">Full name of the file.</param>
        /// <param name="type">The type.</param>
        /// <exception cref="System.NotSupportedException">Only Type=Created, Accessed, Modified are supported.</exception>
#pragma warning disable 1998
        public async Task<DateTime> FileGetDateTimeAsync(string directoryFullName, AuditableEvent type)
#pragma warning restore 1998
        {
            XXX(ref directoryFullName);

            //return await Task.Run(() =>
            //{
                switch (type)
                {
                    case AuditableEvent.Created:
                        return File.GetCreationTimeUtc(directoryFullName);
                    case AuditableEvent.Accessed:
                        return File.GetLastAccessTimeUtc(directoryFullName);
                    case AuditableEvent.Modified:
                        return File.GetLastWriteTimeUtc(directoryFullName);
                    default:
                        return TaskConstants<DateTime>.NotImplemented.Result;
                }
            //});
        }





        /// <summary>
        /// Sets the directory's auditing dates.
        /// </summary>
        /// <param name="fileFullName">Full name of the file.</param>
        /// <param name="type">The type.</param>
        /// <param name="dateTimeUtc">The date time UTC.</param>
        /// <exception cref="System.NotSupportedException">Only Type=Created, Accessed, Modified are supported.</exception>
#pragma warning disable 1998
        public async Task FileSetDateTimeAsync(string fileFullName, AuditableEvent type, DateTime dateTimeUtc)
#pragma warning restore 1998
        {
            XXX(ref fileFullName);

            //await Task.Run(() =>
            //                   {
                                   switch (type)
                                   {
                                       case AuditableEvent.Created:
                                           File.SetCreationTimeUtc(fileFullName, dateTimeUtc);
                                           break;
                                       case AuditableEvent.Accessed:
                                           File.SetLastAccessTimeUtc(fileFullName, dateTimeUtc);
                                           break;
                                       case AuditableEvent.Modified:
                                           File.SetLastWriteTimeUtc(fileFullName, dateTimeUtc);
                                           break;
                                       default:
                                           break;
                                   }
                                   //return TaskConstants<DateTime>.NotImplemented.Result;
                               //});


        }


        /// <summary>
        /// Gets the file information.
        /// </summary>
        /// <param name="fileFullName">Name of the file.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
#pragma warning disable 1998
        public async Task<IFileInfo> GetFileInfoAsync(string fileFullName)
#pragma warning restore 1998
        {
            XXX(ref fileFullName);

            return new FileSystemFileInfo(this, _pathService, fileFullName);
            //return await Task.Run(() => new FileSystemFileInfo(this, _pathService, fileFullName));
        }


        /// <summary>
        /// Determines whether file is available (not locked or non-existent).
        /// </summary>
        /// <param name="fileFullName">Full name of the file.</param>
        /// <param name="desiredFileAccess">The desired file access.</param>
        /// <param name="desiredFileShare">The desired file share.</param>
        /// <returns>
        ///   <c>true</c> if [is file unavailable] [the specified file]; otherwise, <c>false</c>.
        /// </returns>
#pragma warning disable 1998
        public async Task<bool> FileCheckAccessibilityAsync(string fileFullName, XAct.IO.FileAccess desiredFileAccess = XAct.IO.FileAccess.Read, XAct.IO.FileShare desiredFileShare = XAct.IO.FileShare.Read)
#pragma warning restore 1998
        {
            XXX(ref fileFullName);

            Stream stream = null;

            //return await Task.Run(() =>
            //                          {


                                          try
                                          {
                                              stream =
                                                  this.FileOpenAsync(fileFullName, XAct.IO.FileMode.Open,
                                                                     desiredFileAccess, XAct.IO.FileShare.None).Result;
                                              return true;
                                          }
                                          catch (IOException)
                                          {
                                              //the file is unavailable because it is:
                                              //still being written to
                                              //or being processed by another thread
                                              //or does not exist (has already been processed)
                                          }
                                          finally
                                          {
                                              if (stream != null)
                                              {
                                                  stream.Close();
                                              }
                                          }

                                          //file is not locked
                                          return false;
                                      //});
        }


        /// <summary>
        /// Checks whether the directory is accessible.
        /// </summary>
        /// <param name="directoryFullName">Full name of the directory.</param>
        /// <param name="tryForceCreateDirectory">if set to <c>true</c> [try force create directory].</param>
#pragma warning disable 1998
        public async Task<DirectoryAccessibilityCheckResults> DirectoryCheckAccessibilityAsync(
#pragma warning restore 1998
            string directoryFullName, 
            bool tryForceCreateDirectory)
        {
            XXX(ref directoryFullName);

            //return await Task.Run(() =>
            //                   {
                                   DirectoryAccessibilityCheckResults results = new DirectoryAccessibilityCheckResults();

                                   results.Exists = true;
                                   results.CanRead = ResultStatus.Fail;
                                   results.CanWrite = ResultStatus.Fail;
                                   results.CanDelete = ResultStatus.Fail;
                                   results.Exception = null;

                                   //IMPORTANT: Yes...I did try a more formal approach:
                                   //MoreFormalWayThatDoesntWork();
                                   //So don't go retrying it...
                                   //Best description as to why it won't work:
                                   //http://bit.ly/qJcBds


                                   //----------------------------------
                                   results.Exists = DirectoryExistsAsync(directoryFullName, false).Result;

                                   if (!results.Exists)
                                   {
                                       if (!tryForceCreateDirectory)
                                       {
                                           return results;
                                       }
                                       try
                                       {
                                           results.Exists = this.DirectoryExistsAsync(directoryFullName, true).Result;
                                           if (!results.Exists)
                                           {
                                               return results;
                                           }
                                       }
                                       catch (Exception e)
                                       {
                                           results.Exception = e;
                                           //If it doesn't exist, and can't be created, 
                                           //not much point checking whether one has read/write access to it.
                                           results.Exists = false;
                                           return results;
                                       }
                                   }

                                   //----------------------------------

                                   FileInfo fileInfo = new FileInfo(Path.Combine(directoryFullName, _accessTestFileName));
                                   string fileContents =
                                       "Please Ignore: this is an automated deployment test, performed at {0}"
                                           .FormatStringInvariantCulture(
                                               DateTime.Now);

                                   //----------------------------------

                                   try
                                   {
                                       /*_ioService*/


// ReSharper disable RedundantArgumentDefaultValue
                                       using (Stream stream = this.FileOpenWriteAsync(fileInfo.FullName, true, false).Result)
// ReSharper restore RedundantArgumentDefaultValue
                                       {
                                           using (StreamWriter sw = new StreamWriter(stream))
                                           {
                                               sw.Write(fileContents);
                                           }
                                       }
                                       //As no exception, can conclude that...
                                       results.CanWrite = ResultStatus.Success;

                                       //But Don't return yet: havn't done read part
                                   }
// ReSharper disable EmptyGeneralCatchClause
                                   catch (Exception e)
// ReSharper restore EmptyGeneralCatchClause
                                   {
                                       results.Exception = e;
                                       //Can't write:
                                       //canWrite = ResultStatus.Fail;
                                       //Don't return yet: havn't done read part
                                   }

                                   //----------------------------------


                                   if (fileInfo.Exists)
                                   {
                                       try
                                       {
                                           if (this.FileOpenReadAsync(fileInfo.FullName).Result.Length > 0)
                                           {
                                               results.CanRead = ResultStatus.Success;
                                           }
                                       }
// ReSharper disable EmptyGeneralCatchClause
                                       catch
// ReSharper restore EmptyGeneralCatchClause
                                       {
                                           //Can't read.
                                           //Don't return yet: havn't tried to cleanup
                                       }

                                       //As it exists:
                                       //Try to cleanup:

                                       try
                                       {
                                           fileInfo.Delete();
                                           results.CanDelete = ResultStatus.Success;
                                       }
// ReSharper disable EmptyGeneralCatchClause
                                       catch (Exception e)
// ReSharper restore EmptyGeneralCatchClause
                                       {
                                           //Bummer
                                           results.Exception = e;

                                       }

                                       //Whereas we don't return early for Write,
                                       //We can return now for Read test:
                                       return results;
                                   }

                                   //----------------------------------

                                   //The Folder doesn't have Write Access,
                                   //so cannot read/write to ensure can read/write...
                                   //but maybe we can find a file in it that we can use for the test?

                                   string[] fileNames = this.GetDirectoryFileNamesAsync(directoryFullName).Result;
                                   if (fileNames.Length == 0)
                                   {
                                       //If one doesn't find an existing file, doesn't mean we 
                                       //don't have read access either, just means
                                       //we can't be 100% sure.
                                       results.CanRead = ResultStatus.Warn;
                                       return results;
                                   }


                                   //----------------------------------

                                   try
                                   {
#pragma warning disable 168
                                       using (Stream fileStream = FileOpenReadAsync(fileNames[0]).Result)
#pragma warning restore 168
                                       {
                                           //Worked!
                                           results.CanRead = ResultStatus.Success;
                                           //We have read, but not write, and we can't test for Delete
                                           //without causing Damage.... that's as good as we get right
                                           //now...
                                           return results;
                                       }
                                   }
// ReSharper disable EmptyGeneralCatchClause
                                   catch (Exception e)
// ReSharper restore EmptyGeneralCatchClause
                                   {
                                       results.Exception = e;
                                       return results;
                                   }





                                   //----------------------------------

                                   //void MoreFormalWayThatDoesntWork()
                                   //{
                                   //    DirectoryInfo directoryInfo = new DirectoryInfo(@"c:\Windows");
                                   //    FileIOPermission fileIOPermission =
                                   //        new FileIOPermission(FileIOPermissionAccess.Write, directoryInfo.FullName);

                                   //    if (SecurityManager.IsGranted(fileIOPermission))
                                   //    {
                                   //        "Granted?".Dump();

                                   //        try
                                   //        {
                                   //            AuthorizationRuleCollection collection =
                                   //              Directory
                                   //                  .GetAccessControl(directoryInfo.FullName)
                                   //                  .GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));

                                   //            foreach (FileSystemAccessRule rule in collection)
                                   //            {
                                   //                if (rule.AccessControlType == AccessControlType.Allow)
                                   //                {

                                   //                    hasWriteAccess = true.Dump();
                                   //                    break;
                                   //                }
                                   //            }
                                   //        }
                                   //        catch
                                   //        {
                                   //        }

                                   //    }

                                   //    if (hasWriteAccess)
                                   //    {
                                   //        IoService.WriteAllText(Path.Combine(directoryInfo.FullName, "test2.txt"), "Test");
                                   //    }

                                   //}
                               //});
        }


        private void XXX(ref string sourcePath)
        {
            if (!sourcePath.StartsWith("%"))
            {
                return;
            }
            sourcePath = Environment.ExpandEnvironmentVariables(sourcePath)
                              .Replace("%mydocuments%", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                                       StringComparison.OrdinalIgnoreCase);
        }

    }
}