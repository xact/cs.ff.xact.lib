﻿//namespace XAct.IO
//{
//    using System;
//    using System.IO.IsolatedStorage;
//    using XAct.Services;

//    /// <summary>
//    /// An implementation of the
//    /// <see cref="IIsolatedStorageUserFileSystemService"/>
//    /// contract
//    /// </summary>
//    [DefaultBindingImplementation(typeof(IIsolatedStorageUserFileSystemService))]
//    public class IsolatedStorageUserFileSystemService : XActLibServiceBase,  IIsolatedStorageUserFileSystemService
//    {
//        private readonly IIsolatedStorageIOService _isolatedStorageIOService;
//        private readonly IPathService _pathService;


//        /// <summary>
//        /// The current identity's local folder
//        /// associated to this application,
//        /// on this device only .
//        /// <para>
//        /// Contents are not synchronized with other devices.
//        /// </para>
//        /// <para>
//        /// Note that the application identity is the app user on mobile,
//        /// service account in a web application)
//        /// </para>
//        /// </summary>
//        public IDirectoryInfo UserLocal
//        {

//            get
//            {
//                IsolatedStorageFile Root = IsolatedStorageFile.GetUserStoreForApplication();
//                IsoStoreFolder(Root);
//            }
//        }

//        /// <summary>
//        /// The current identity's roaming folder,
//        /// which is synchronized across devices.
//        /// <para>
//        /// Note that the application identity is the app user on mobile,
//        /// service account in a web application)
//        /// </para>
//        /// </summary>
//        public IDirectoryInfo UserRoaming
//        {
//            get
//            {
//                return UserLocal;
//            }
//        }

//        /// <summary>
//        /// The current identity's temp folder
//        /// <para>
//        /// Potentially cleared
//        /// every time the application is run).
//        /// </para>
//        /// </summary>
//        /// <exception cref="System.NotImplementedException"></exception>
//        public IDirectoryInfo UserTemp
//        {
//            get
//            {
//                throw new NotImplementedException();
//            }
//        }

//        /// <summary>
//        /// The common (shared by all users of this application on this device) local folder,
//        /// associated to this application, on this device only.
//        /// <para>
//        /// Contents are not synchronized with other devices.
//        /// </para>
//        /// <para>
//        /// Note that the application identity is the app user on mobile,
//        /// service account in a web application)
//        /// </para>
//        /// <para>
//        /// Note that there is no concept of CommonRoaming.
//        /// </para>
//        /// </summary>
//        public IDirectoryInfo CommonLocal
//        {
//            get
//            {
//                IsolatedStorageFile Root = IsolatedStorageFile.GetMachineStoreForApplication();

//                return _isolatedStorageIOService.GetFileInfoAsync();
//            }
//        }

//        /// <summary>
//        /// The application's root folder.
//        /// <para>
//        /// Not available under all platforms (use
//        /// <see cref="UserRoaming" />
//        /// or
//        /// <see cref="UserLocal" />
//        /// for user files.
//        /// </para>
//        /// </summary>
//        /// <exception cref="System.NotImplementedException"></exception>
//        public IDirectoryInfo ApplicationRoot
//        {
//            get
//            {
                
//                throw new NotImplementedException();
//            }
//        }



//        /// <summary>
//        /// Initializes a new instance of the <see cref="IsolatedStorageUserFileSystemService" /> class.
//        /// </summary>
//        /// <param name="isolatedStorageIOService">The isolated storage io service.</param>
//        /// <param name="pathService">The path service.</param>
//        public IsolatedStorageUserFileSystemService(IIsolatedStorageIOService isolatedStorageIOService, IPathService pathService)
//        {
//            _isolatedStorageIOService = isolatedStorageIOService;
//            _pathService = pathService;
//        }
//    }
//}