﻿namespace XAct.IO
{
    using System.Runtime.InteropServices;

    /// <summary>
    /// An <see cref="IIsolatedStorageIOService"/>
    /// specific specialization of 
    /// <see cref="DirectoryInfoBase{TIsolatedStorageIOService,TIsolatedStorageDirectoryInfo}"/>
    /// </summary>
    [ComVisible(false)]
    public class IsolatedStorageDirectoryInfo :
        DirectoryInfoBase<IIsolatedStorageIOService,IsolatedStorageFileInfo>
    {
                /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStorageDirectoryInfo"/> class.
        /// </summary>
        /// <param name="fullName">The full name.</param>
        public IsolatedStorageDirectoryInfo(string fullName)
            : this(XAct.DependencyResolver.Current.GetInstance<IIsolatedStorageIOService>(), fullName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStorageDirectoryInfo"/> class.
        /// </summary>
        /// <param name="managingIOService">The managing io service.</param>
        /// <param name="fullName">The full name.</param>
        public IsolatedStorageDirectoryInfo(IIOService managingIOService, string fullName)
            : this(managingIOService, 
            XAct.DependencyResolver.Current.GetInstance<IPathService>(),
            fullName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStorageDirectoryInfo"/> class.
        /// </summary>
        /// <param name="managingIOService">The managing io service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fullName">The full name.</param>
        public IsolatedStorageDirectoryInfo(IIOService managingIOService, IPathService pathService, string fullName)
            : base(managingIOService, pathService, fullName)
        {
        }

    }
}