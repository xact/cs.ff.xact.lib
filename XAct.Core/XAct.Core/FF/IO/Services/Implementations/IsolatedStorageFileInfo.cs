﻿using System;

namespace XAct.IO
{
    using System.Runtime.InteropServices;
    using XAct.IO.Implementations;

    /// <summary>
    /// An <see cref="IIsolatedStorageIOService"/>
    /// specific specialization of 
    /// <see cref="FileInfoBase{TIsolatedStorageIOService,TIsolatedStorageDirectoryInfo}"/>
    /// </summary>
    [ComVisible(false)]
    public class IsolatedStorageFileInfo :
        FileInfoBase<IIsolatedStorageIOService,IsolatedStorageDirectoryInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStorageFileInfo"/> class.
        /// </summary>
        /// <param name="fullName">The full name.</param>
        public IsolatedStorageFileInfo(string fullName)
            : this(XAct.DependencyResolver.Current.GetInstance<IIsolatedStorageIOService>(), fullName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStorageFileInfo"/> class.
        /// </summary>
        /// <param name="managingIOService">The managing io service.</param>
        /// <param name="fullName">The full name.</param>
        public IsolatedStorageFileInfo(IIOService managingIOService, string fullName)
            : this(managingIOService, 
            XAct.DependencyResolver.Current.GetInstance<IPathService>(),
            fullName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStorageFileInfo"/> class.
        /// </summary>
        /// <param name="managingIOService">The managing io service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fullName">The full name.</param>
        public IsolatedStorageFileInfo(IIOService managingIOService, IPathService pathService, string fullName)
            : base(managingIOService, pathService, fullName)
        {
        }
    }
}
