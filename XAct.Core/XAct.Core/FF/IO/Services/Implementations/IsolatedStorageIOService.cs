﻿namespace XAct.IO.Implementations
{
    using System;
    using System.IO;
    using System.IO.IsolatedStorage;
    using System.Threading.Tasks;
    using XAct.Diagnostics;
    using XAct.Enums;
    using XAct.Services;

    /// <summary>
    /// A default implementation of <see cref="IIsolatedStorageIOService"/>
    /// providing a Service that reads and writes from IsolatedStorage.
    /// <para>
    /// As reading and writting from IsolatedStorage requires less
    /// rights, and is more portable (ie ClickOnce, etc.), it is always 
    /// preferable to investigate using this service, before using
    /// <c>FSIOService</c>
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IIOService), BindingLifetimeType.Undefined, Priority.Low /*OK: It's low, overridden by FS based one.*/)]
    public class IsolatedStorageIOService : XActLibServiceBase, IIsolatedStorageIOService
    {
        private readonly IPathService _pathService;


        /// <summary>
        /// Gets the name of the Directory Accessibility test file.
        /// </summary>
        /// <value>
        /// The name of the access test file.
        /// </value>
        public string DirectoryAccessTestFileName
        {
            get
            {
                return _accessTestFileName;
            }
        }
        private const string _accessTestFileName = "_integrationTest.txt";


        IsolatedStorageFile ApplicationIsolatedStorageFile
        {
            get
            {
                return IsolatedStorageExtensions.GetApplicationIsolatedStorageFile();
            }
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="IsolatedStorageIOService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="pathService">The path service.</param>
        public IsolatedStorageIOService(ITracingService tracingService, IPathService pathService):base(tracingService)
        {
            _pathService = pathService;
        }

        /// <summary>
        /// Opens the specified resource with the given settings.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="fileMode">The file mode.</param>
        /// <param name="fileAccess">The file access.</param>
        /// <param name="fileShare">The file share.</param>
        /// <returns></returns>
        public async Task<Stream> FileOpenAsync(string uri, XAct.IO.FileMode fileMode, XAct.IO.FileAccess fileAccess, XAct.IO.FileShare fileShare)
        {
            if (uri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("uri");
            }
            CheckPath(uri);



            Task<IsolatedStorageFileStream> task = new Task<IsolatedStorageFileStream>(
                () =>
                new IsolatedStorageFileStream(uri,
                                              fileMode.MapTo(),
                                              fileAccess.MapTo(),
                                              fileShare.MapTo(),
                                              this.ApplicationIsolatedStorageFile));

            return await task;
        }


        /// <summary>
        /// Opens the named resource and returns a readonly stream.
        /// <para>
        /// Usage:
        /// <code>
        /// <![CDATA[
        /// IIsolatedStorageIOService isolatedStorageIOService =
        ///   DependencyResolver.Current.GetInstance<IIsolatedStorageIOService>();
        /// 
        ///   using (Stream stream = isolatedStorageIOService.OpenRead("/UserSettings/UI.config")){
        ///     ...
        ///   }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="uri">The full uri to the resource.</param>
        /// <returns>An <see cref="IsolatedStorageFileStream"/></returns>
        public async Task<Stream> FileOpenReadAsync(string uri)
        {
            if (uri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("uri");
            }
            CheckPath(uri);

            return await new Task<Stream>(()=> this.ApplicationIsolatedStorageFile.OpenReadStream(uri));
            
        }

        /// <summary>
        /// Opens the named resource and returns a write only stream.
        /// <para>
        /// Usage:
        /// <code>
        /// 			<![CDATA[
        /// IIsolatedStorageIOService isolatedStorageIOService =
        /// DependencyResolver.Current.GetInstance<IIsolatedStorageIOService>();
        /// using (Stream stream = isolatedStorageIOService.OpenWrite("/UserSettings/UI.config")){
        /// ...
        /// }
        /// ]]>
        /// 		</code>
        /// 	</para>
        /// </summary>
        /// <param name="uri">The uri.</param>
        /// <param name="replaceAnyIfFound">if set to <c>true</c> replace any if file already exists.</param>
        /// <param name="append">if set to <c>true</c> opens the stream for appending.</param>
        /// <returns>
        /// An <see cref="IsolatedStorageFileStream"/>
        /// </returns>
        public async Task<Stream> FileOpenWriteAsync(string uri, bool replaceAnyIfFound = true, bool append = false)
        {
            if (uri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("uri");
            }
            CheckPath(uri);

            if ((!replaceAnyIfFound)&& await FileExistsAsync(uri))
            {
                throw new ArgumentException("uri", "File {0} already exists".FormatStringExceptionCulture(uri));
            }

            return await new Task<Stream>(()=> this.ApplicationIsolatedStorageFile.OpenWriteStream(uri, append));
        }

        /// <summary>
        /// Opens the named resource and returns a <see cref="StreamReader"/>.
        /// </summary>
        /// <param name="uri">The uri.</param>
        /// <returns>
        /// A <see cref="StreamReader"/>.
        /// </returns>
        [Obsolete("Obfuscates what is really going on. Prefer instead using ioService.OpenRead(path).OpenStreamReader()")]
        public async Task<StreamReader> FileOpenTextAsync(string uri)
        {
            return await new Task<StreamReader>(() => new StreamReader(FileOpenReadAsync(uri).Result));
        }


        /// <summary>
        /// Replaces the destination, with the source,
        /// after moving the destination to a Backup position.
        /// </summary>
        /// <param name="sourceUri">Name of the source file.</param>
        /// <param name="destinationUri">Name of the destination file.</param>
        /// <param name="destinationBackupUri">Name of the destination backup file.</param>
        public async Task FileReplaceAsync(string sourceUri, string destinationUri, string destinationBackupUri = null)
        {
            //Check vars:
            if (sourceUri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("sourceUri");
            }
            if (destinationUri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("destinationUri");
            }
            CheckPath(sourceUri);
            CheckPath(destinationUri);

            if (string.IsNullOrEmpty(destinationBackupUri))
            {
                //If no destinationBackup supplied, 
                //we can't move it to safety first.
                //But we can assume it is meant to not be there:
                if (FileExistsAsync(destinationUri).Result)
                {
                    await FileDeleteAsync(destinationUri);
                }
            }
            else
            {
                //The safety path exists:
                CheckPath(destinationUri);

                if (await FileExistsAsync(destinationUri))
                {
                    //Have to delete the destination backup before
                    //anything can be moved to it:
                    if (await FileExistsAsync(destinationBackupUri))
                    {
                        await FileDeleteAsync(destinationBackupUri);
                    }

                    //Moves destination out of the way:
                    await this.FileMoveAsync(destinationBackupUri, destinationBackupUri, true);
                }
            }

            //Moves Source to Target:
           await new Task(()=>  this.FileMoveAsync(destinationUri,sourceUri,true).Wait());
        }

        /// <summary>
        /// Copies the specified source resource to the target URI.
        /// </summary>
        /// <param name="sourceUri">The source URI.</param>
        /// <param name="destinationUri">The destination URI.</param>
        /// <param name="overwriteAllowed">if set to <c>true</c> [overwrite] any existing file.</param>
        /// <internal>5/8/2011: Sky</internal>
        public async Task FileCopyAsync(string sourceUri, string destinationUri, bool overwriteAllowed)
        {
            //Check vars:
            if (sourceUri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("sourceUri");
            }
            if (destinationUri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("destinationUri");
            }

            CheckPath(sourceUri);

            CheckPath(destinationUri);

            //Note that OpenWriteStream will happily go ahead and write over an existing file.
            //Hence:
            if ((!overwriteAllowed) && (await FileExistsAsync(destinationUri)))
            {
                throw new ArgumentException("destinationUri", "Target file ('{0}') already exists.");
            }


            await new Task(() =>
                               {
                                   using (
                                       Stream inputStream =
                                           this.ApplicationIsolatedStorageFile.OpenReadStream(sourceUri))
                                   {
                                       using (
                                           Stream outputStream =
                                               this.ApplicationIsolatedStorageFile.OpenWriteStream(destinationUri
                                                   /*, false*/))
                                       {
                                           outputStream.CopyStreamFrom(inputStream);
                                       }
                                   }
                               });
        }


        /// <summary>
        /// Creates a new resource at the specified location, returning a stream.
        /// </summary>
        /// <param name="uri">The uri.</param>
        /// <returns>An <see cref="IsolatedStorageFileStream"/></returns>
        [Obsolete("Obfuscates what is really going on: prefer using ioService.OpenWrite(path,false)")]
        public async Task<Stream> FileCreateAsync(string uri)
        {
            if (uri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("uri");
            }
            CheckPath(uri);

// ReSharper disable RedundantArgumentDefaultValue
            return await FileOpenWriteAsync(uri, true, false);
// ReSharper restore RedundantArgumentDefaultValue
        }

        /// <summary>
        /// Deletes the specified target file.
        /// <para>
        /// Example of usage would be
        /// <code>
        /// <![CDATA[
        /// IIsolatedStorageIOService isolatedStorageIOService =
        ///   DependencyResolver.Current.GetInstance<IIsolatedStorageIOService>();
        /// 
        /// isolatedStorageIOService.Delete("/UserSettings/UI.config");
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <internal>5/8/2011: Sky</internal>
        public async Task FileDeleteAsync(string uri)
        {
            if (uri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("uri");
            }
            CheckPath(uri);

            if (uri.Contains("*"))
            {
                throw new ArgumentException("uri cannot contain wild cards");
            }
            if (uri.Contains("?"))
            {
                throw new ArgumentException("uri cannot contain wild cards");
            }
            await new Task(()=> this.ApplicationIsolatedStorageFile.DeleteFile(uri));
        }

        /// <summary>
        /// Determines if the specified resource exists.
        /// <para>
        /// Usage:
        /// <code>
        /// <![CDATA[
        /// IIsolatedStorageIOService isolatedStorageIOService =
        ///   DependencyResolver.Current.GetInstance<IIsolatedStorageIOService>();
        /// 
        /// isolatedStorageIOService.Exists("/UserSettings/UI.config");
        /// ]]>
        /// </code>
        /// </para>
        /// <para>
        /// See: http://stackoverflow.com/a/5208545
        /// </para>
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns></returns>
        /// <internal>5/8/2011: Sky</internal>
        public async Task<bool> FileExistsAsync(string uri)
        {
            if (uri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("uri");
            }
            CheckPath(uri);

            string foundFileName;

            return await new Task<bool>(()=>this.ApplicationIsolatedStorageFile.FileExists(uri, out foundFileName));
        }


        /// <summary>
        /// Gets the files's size.
        /// </summary>
        /// <param name="fileFullName">Full name of the directory.</param>
        public async Task<long> FileGetSizeAsync(string fileFullName)
        {

            //http://stackoverflow.com/questions/3808259/calculate-the-size-of-a-file-in-isolated-storage


            return await Task.Run(() =>
            {
                throw new NotImplementedException();
#pragma warning disable 162
                return 0;
#pragma warning restore 162
                //return (this.ApplicationIsolatedStorageFile.FileExists(fileFullName) ? this.ApplicationIsolatedStorageFile..UsedSize : 0;
            });
        }


        /// <summary>
        /// Moves the specified resource.
        /// <para>
        /// Usage:
        /// <code>
        /// 			<![CDATA[
        /// IIsolatedStorageIOService isolatedStorageIOService =
        /// DependencyResolver.Current.GetInstance<IIsolatedStorageIOService>();
        /// isolatedStorageIOService.Move("/UserSettings/UI.config","/CommonSettings/UI.config");
        /// ]]>
        /// 		</code>
        /// 	</para>
        /// </summary>
        /// <param name="sourceUri">Name of the source file.</param>
        /// <param name="destinationUri">Name of the destination file.</param>
        /// <param name="overWriteAllowed">if set to <c>true</c> [over write allowed].</param>
        /// <internal>5/8/2011: Sky</internal>
        public async Task FileMoveAsync(string sourceUri, string destinationUri, bool overWriteAllowed)
        {
            //Check vars:
            if (sourceUri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("sourceUri");
            }
            if (destinationUri.IsNullOrEmpty())
            {
                throw new ArgumentNullException("destinationUri");
            }
            CheckPath(sourceUri);
            CheckPath(destinationUri);

            //reuse:
            await FileCopyAsync(sourceUri, destinationUri, overWriteAllowed);

            //No Exception raised. 

            //Once moved, get rid of source stream:
            await FileDeleteAsync(sourceUri);
        }

        /// <summary>
        /// Appends the given string to the specified resource.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <param name="contents">The contents.</param>
        /// <internal>5/8/2011: Sky</internal>
        [Obsolete("Obfuscates whats really going on. Prefer using ioService.OpenWrite(uri,true,true).OpenStreamWriter().Write(contents);")]
        public async Task FileAppendAllTextAsync(string uri, string contents)
        {
            await new Task(() => FileOpenWriteAsync(uri, true, true).Result.CreateStreamWriter().Write(contents));
        }

        /// <summary>
        /// Get's the filenames of the files in the directory.
        /// <para>
        /// At present, don't think this deserves its own service. We'll see.
        /// </para>
        /// </summary>
        /// <param name="directoryName">Name of the directory.</param>
        /// <param name="searchPattern">The search pattern.</param>
        /// <param name="searchOption">The search option.</param>
        /// <returns></returns>
        public async Task<string[]> GetDirectoryFileNamesAsync(string directoryName, string searchPattern = null,
                                              HierarchicalOperationOption searchOption = HierarchicalOperationOption.TopOnly)
        {
            return await XAct.Threading.TaskConstants<string[]>.NotImplemented;
        }


        /// <summary>
        /// A wrapping of Directory.GetFiles 
        /// <para>
        /// At present, don't think this deserves its own service. We'll see.
        /// </para>
        /// </summary>
        /// <param name="directoryName">Name of the directory.</param>
        /// <param name="searchPattern">The search pattern.</param>
        /// <param name="searchOption">The search option.</param>
        /// <returns></returns>
        public Task<string[]> GetDirectoryFiles(string directoryName,string searchPattern=null, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            throw new NotImplementedException("Not yet implemented for IsolatedStorageIOService");
        }

        /// <summary>
        /// Creates the directory.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <returns></returns>
        public Task DirectoryCreateAsync(string directoryPath)
        {
            throw new NotImplementedException("Not yet implemented for IsolatedStorageIOService");
        }

        /// <summary>
        /// Determines if the directory exists.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <param name="createIfMissing">if set to <c>true</c> [create if missing].</param>
        /// <returns></returns>
        public Task<bool> DirectoryExistsAsync(string directoryPath, bool createIfMissing=false)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// Directories the delete.
        /// </summary>
        /// <param name="directoryPath">The directory path.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        public Task DirectoryDeleteAsync(string directoryPath)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the files's dates.
        /// </summary>
        /// <param name="fileFullName">Full name of the directory.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Task<DateTime> FileGetDateTimeAsync(string fileFullName, AuditableEvent type)
        {
            throw new NotImplementedException();
        }

        private static void CheckPath(string relativeFileNamePath)
        {
            //NO:Always makes it relative to appPath: if (new FileInfo(relativeFileNamePath).IsPathAbsolute())
            if (IsPathAbsolute(relativeFileNamePath))
            {
                throw new ArgumentException("paths in IsolatedStorage cannot be Absolute ('{0}').".FormatStringExceptionCulture(relativeFileNamePath));
            }
        }

        //Not portable. SL4 doesn't allow private reflection.
        //public string GetPathToFileStore(IsolatedStorageFile isoStore)
        //{
        //    FieldInfo fieldInfo  = isoStore.GetType().GetField(
        //        "m_RootDir",
        //        (System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance));

        //        return fieldInfo.GetValue(isoStore).ToString();
        //}



        private static bool IsPathAbsolute(string path)
        {
            if (path == string.Empty)
            {
                return false;
            }

            //If not long enough to ever be Physical path, return false:
            if (path.Length < 3)
            {
                return false;
            }

            //if Path is either format "D:\" or "\\" or "d:/" or "//":
            if ((path[1] == ':') && IsDirectorySeparatorChar(path[2], true))
            {
                return true;
            }

            //If Path starts with '\\' it's a network absolute
            if (IsDirectorySeparatorChar(path[0], false) && IsDirectorySeparatorChar(path[1], false))
            {
                return true;
            }
            return false;
        }


        /// <summary>
        ///   Returns true if the given char is '\', and/or '/' character.
        /// </summary>
        /// <param name = "sepChar">The char to test.</param>
        /// <param name = "eitherDirection">Flag to test for forward slash as well.</param>
        /// <returns>True if char is '\\' and if eitherDirection flag is set, '/'.</returns>
        private static bool IsDirectorySeparatorChar(char sepChar, bool eitherDirection)
        {
            if (sepChar == '\\')
            {
                return true;
            }
            return (eitherDirection) && (sepChar == '/');
        }




        /// <summary>
        /// Gets the portable information about the file.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public async Task<IFileInfo> GetFileInfoAsync(string fileName)
        {
            //FileInfo fileInfo = new FileInfo(fileName);

            return await Task.Run(() =>
                                {
                                    IFileInfo result = new IsolatedStorageFileInfo(this, _pathService, fileName);

                                    return result;
                                });
        }


        /// <summary>
        /// Sets the directory's dates.
        /// </summary>
        /// <param name="fileFullName">Full name of the directory.</param>
        /// <param name="type">The type.</param>
        /// <param name="dateTimeUtc">The date time UTC.</param>
        public Task FileSetDateTimeAsync(string fileFullName, AuditableEvent type, DateTime dateTimeUtc)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Gets the directory's dates.
        /// </summary>
        /// <param name="directoryFullName">Full name of the directory.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public Task<DateTime> DirectoryGetDateTimeAsync(string directoryFullName, AuditableEvent type)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Sets the directory's dates.
        /// </summary>
        /// <param name="directoryFullName">Full name of the directory.</param>
        /// <param name="type">The type.</param>
        /// <param name="dateTimeUtc">The date time UTC.</param>
        public Task DirectorySetDateTimeAsync(string directoryFullName, AuditableEvent type, DateTime dateTimeUtc)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks the file accessibility.
        /// </summary>
        /// <param name="fileFullName">Full name of the file.</param>
        /// <param name="desiredFileAccess">The desired file access.</param>
        /// <param name="desiredFileShare">The desired file share.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
#pragma warning disable 1998
        public async Task<bool> FileCheckAccessibilityAsync(string fileFullName,
#pragma warning restore 1998
                                           XAct.IO.FileAccess desiredFileAccess = XAct.IO.FileAccess.Read,
                                           XAct.IO.FileShare desiredFileShare = XAct.IO.FileShare.Read)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Checks the directory's accessibility.
        /// </summary>
        /// <param name="directoryFileName">The directory information.</param>
        /// <param name="tryForceCreateDirectory">if set to <c>true</c> [try force create directory].</param>
        public Task<DirectoryAccessibilityCheckResults> DirectoryCheckAccessibilityAsync(string directoryFileName, bool tryForceCreateDirectory)
        {
            throw new NotImplementedException();
        }
    }
}
