﻿namespace XAct.IO
{
    using System.IO;
    using System.Security.Principal;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.FF.IO.Entities;
    using XAct.Services;


    /// <summary>
    /// An implementation of the 
    /// <see cref="IFileSystemUserFileSystemService"/>
    /// Contract for a service to return 
    /// entry points to the current 
    /// <see cref="IIdentity"/>'s WellKnown 
    /// file system locations (Local, Roaming, Temp).
    /// <para>
    /// Very rarely relevant to Server based development.
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IUserFileSystemService), BindingLifetimeType.Undefined,Priority.Low /*OK:Second Binding*/)]
    public class FileSystemUserFileSystemService : XActLibServiceBase, IFileSystemUserFileSystemService 
    {
        private readonly IPathService _pathService;
        private readonly IFSIOService _fsIOService;
        private readonly IProductInformationService _productInformationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemUserFileSystemService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fsIOService">The fs io service.</param>
        /// <param name="productInformationService">The product information service.</param>
        public FileSystemUserFileSystemService(
            ITracingService tracingService,
            IPathService pathService, 
            IFSIOService fsIOService,
            IProductInformationService productInformationService):base(tracingService)
        {
            _pathService = pathService;
            _fsIOService = fsIOService;
            _productInformationService = productInformationService;
        }

        /// <summary>
        /// The application identity's local folder
        /// associated to this application,
        /// on this device only .
        /// <para>
        /// Contents are not synchronized with other devices.
        /// </para>
        /// <para>
        /// Note that the application identity is the app user on mobile,
        /// service account in a web application)
        /// </para>
        /// </summary>
        public IDirectoryInfo UserLocal
        {
            get
            {
                string result =
                    System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);

                result =
                    _pathService.Combine(
                    result,
                    _productInformationService.ProductName(ProductInformationType.Default));

                //SpecialFolder.LocalApplicationData is not app-specific, so use the Windows Forms API to get the app data path
                //gives: C:\Users\SkyS\AppData\Local
                //var localAppData = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);

                //On Desktop app -- not server -- you could consider
                //but it relies on EntryAssembly. Which in web cases is IIS.exe
                //Type t = Type.GetType("System.Windows.Forms.Application, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
                //PropertyInfo propertyInfo = t.GetProperty("LocalUserAppDataPath", BindingFlags.Public | BindingFlags.Static);
                //propertyInfo.GetValue(null).Dump();

                return new FileSystemDirectoryInfo(result);
            }
        }

        /// <summary>
        /// The current identity's roaming folder,
        /// which is synchronized across devices.
        /// <para>
        /// Note that the application identity is the app user on mobile,
        /// service account in a web application)
        /// </para>
        /// </summary>
        public IDirectoryInfo UserRoaming
        {
            get
            {
                string result =
                    System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);

                AddCompanyAndProductNameToBasePath(ref result);

                //Don't use this, as it relies on WinForm specific (and ClickOnce) 
                //reference to EntryAssembly (which is not available on Servers, and in PCL)
                //var roamingAppData = System.Windows.Forms.Application.UserAppDataPath;

                return new FileSystemDirectoryInfo(result);
            }
        }

        private void AddCompanyAndProductNameToBasePath(ref string result)
        {
//Add CompanyName:
            _pathService.Combine(
                result,
                _productInformationService.CompanyName(ProductInformationType.Default));

            //Add ProductName:
            result =
                _pathService.Combine(
                    result,
                    _productInformationService.ProductName(ProductInformationType.Default));


        }

        /// <summary>
        /// The current identity's temp folder
        /// <para>
        /// Potentially cleared
        /// every time the application is run).
        /// </para>
        /// </summary>
        public IDirectoryInfo UserTemp
        {
            get
            {
                //Do not use: Path.GetTempPath() -- it forces unsafe use of win32 api.

                //See: http://stackoverflow.com/a/946017/1052767

                string result = System.Environment.GetEnvironmentVariable("TMP");
                if (result.IsNullOrEmpty())
                {
                    result = System.Environment.GetEnvironmentVariable("TEMP");
                }
                if (result.IsNullOrEmpty())
                {
                    string tmp = System.Environment.GetEnvironmentVariable("USERPROFILE");
                    if (tmp != null)
                    {
                        result = Path.Combine(tmp, "TEMP");
                    }
                }
                if (result.IsNullOrEmpty())
                {
                    string tmp = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Windows);
                    result = Path.Combine(tmp, "TEMP");
                }

                AddCompanyAndProductNameToBasePath(ref result);


                return new FileSystemDirectoryInfo(result);
            }
        }








        /// <summary>
        /// The common (shared by all users of this application on this device) local folder,
        /// associated to this application, on this device only.
        /// <para>
        /// Contents are not synchronized with other devices.
        /// </para>
        /// <para>
        /// Note that the application identity is the app user on mobile,
        /// service account in a web application)
        /// </para>
        /// <para>
        /// Note that there is no concept of CommonRoaming.
        /// </para>
        /// </summary>
        public IDirectoryInfo CommonLocal
        {
            get
            {
                string result =
                    System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData);

                result =
                    _pathService.Combine(
                    result,
                    _productInformationService.ProductName(ProductInformationType.Default));

                //SpecialFolder.LocalApplicationData is not app-specific, so use the Windows Forms API to get the app data path
                //gives: C:\Users\SkyS\AppData\Local
                //var localAppData = System.Environment.GetFolderPath(System.Environment.SpecialFolder.LocalApplicationData);

                //On Desktop app -- not server -- you could consider
                //but it relies on EntryAssembly. Which in web cases is IIS.exe
                //Type t = Type.GetType("System.Windows.Forms.Application, System.Windows.Forms, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
                //PropertyInfo propertyInfo = t.GetProperty("LocalUserAppDataPath", BindingFlags.Public | BindingFlags.Static);
                //propertyInfo.GetValue(null).Dump();

                return new FileSystemDirectoryInfo(result);
            }
        }








        /// <summary>
        /// The application's root folder.
        /// <para>
        /// Not available under all platforms (use
        /// <see cref="UserRoaming" />
        /// or
        /// <see cref="UserLocal" />
        /// for user files.
        /// </para>
        /// </summary>
        /// <exception cref="System.NotImplementedException"></exception>
        public IDirectoryInfo ApplicationRoot
        {
            get
            {
                throw new System.NotImplementedException();
            }
        }

        
    }
}