﻿
namespace XAct.IO
{
    /// <summary>
    /// A specialization of <see cref="IIOService"/>
    /// for access to files in the NT FileSystem
    /// (not IsolatedStorage).
    /// </summary>
    /// <remarks>
    /// Defined in <c>XAct.IO</c> (so that it is available
    /// to <see cref="IIOService"/> as needed), 
    /// but implemented in <c>XAct.IO.FS</c>.
    /// </remarks>
// ReSharper disable InconsistentNaming
    public interface IFSIOService : IIOService, IHasXActLibService
// ReSharper restore InconsistentNaming
    {
    }
}
