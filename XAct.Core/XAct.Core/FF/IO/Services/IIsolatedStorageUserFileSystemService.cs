﻿namespace XAct.IO
{
    /// <summary>
    /// An <see cref="IIsolatedStorageIOService"/>
    /// based specialization of the 
    /// <see cref="IUserFileSystemService"/>
    /// contract.
    /// </summary>
    public interface IIsolatedStorageUserFileSystemService : IUserFileSystemService, IHasXActLibService
    {
    }
}
