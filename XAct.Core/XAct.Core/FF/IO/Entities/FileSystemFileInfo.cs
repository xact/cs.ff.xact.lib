﻿namespace XAct.FF.IO.Entities
{
    using System.Runtime.InteropServices;
    using XAct.IO;

    /// <summary>
    /// 
    /// </summary>
    [ComVisible(false)]
    public class FileSystemFileInfo : FileInfoBase<IFSIOService, FileSystemDirectoryInfo>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemFileInfo"/> class.
        /// </summary>
        /// <param name="fullName">The full name.</param>
        public FileSystemFileInfo(string fullName) : 
            this(
            XAct.DependencyResolver.Current.GetInstance<IFSIOService>(),
            XAct.DependencyResolver.Current.GetInstance<IPathService>(), 
            fullName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemFileInfo"/> class.
        /// </summary>
        /// <param name="pathService">The path service.</param>
        /// <param name="fullName">The full name.</param>
        public FileSystemFileInfo(IPathService pathService, string fullName) :
            this(
            XAct.DependencyResolver.Current.GetInstance<IFSIOService>(), 
            pathService, fullName)
        {
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="FileSystemFileInfo"/> class.
        /// </summary>
        /// <param name="ifsIOService">The ifs io service.</param>
        /// <param name="pathService">The path service.</param>
        /// <param name="fullName">The full name.</param>
        public FileSystemFileInfo(IFSIOService ifsIOService, IPathService pathService, string fullName) :
            base(ifsIOService, pathService, fullName)
        {
        }
    }
}