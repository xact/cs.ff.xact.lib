﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
// ReSharper disable CheckNamespace
namespace XAct {
    using System;
    using System.IO;
    using System.Threading;
    using XAct.Diagnostics;
    using XAct.IO;

// ReSharper restore CheckNamespace
#endif

    /// <summary>
    /// Extensions to the FileInfo object
    /// </summary>
    public static class FileInfoExtensions2
    {

        static ITracingService TracingService
        {
            get { return XAct.DependencyResolver.Current.GetInstance<ITracingService>(); }
        }

        /// <summary>
        /// Tries to move a file while allowing for failure a couple of times.
        /// </summary>
        /// <param name="from">From.</param>
        /// <param name="toPath">To path.</param>
        /// <param name="attempts">The attempts.</param>
        /// <param name="retryMilliseconds">The retry milliseconds.</param>
        /// <param name="iftargetExistsRenameWithStamp">if set to <c>true</c> [iftarget exists rename with stamp].</param>
        /// <returns></returns>
        public static bool SafeMove(this FileInfo from, string toPath, int attempts = 3, int retryMilliseconds = 100,bool iftargetExistsRenameWithStamp=false)
        {
            from.ValidateIsNotDefault("from");
            from.FullName.ValidateIsNotNullOrEmpty("from");
            toPath.ValidateIsNotNullOrEmpty("destPath");

            //Have to convert from FileInfo, as after moving it, it will still 'exist'...just in new position.

            string fromPath = from.FullName;

            attempts = Math.Max(3, attempts);


            if ((File.Exists(toPath))&& (iftargetExistsRenameWithStamp))
            {

                toPath = new FileInfo(toPath).PrefixFileName("{0:yyyyMMddHHmm_ffff}__".FormatStringInvariantCulture(DateTime.UtcNow));
            }


            for (int i = 0; i < attempts; i++)
            {
                try
                {
                    IIOService ioService =
                        XAct.DependencyResolver.Current.GetInstance<IIOService>();


                    ioService.FileMoveAsync(fromPath, toPath);

                    if (ioService.FileExistsAsync(toPath).Result)
                    {
                        return true;
                    }
                    Thread.Sleep(retryMilliseconds);
                }
// ReSharper disable EmptyGeneralCatchClause
                catch(System.Exception e)
// ReSharper restore EmptyGeneralCatchClause
                {
                    TracingService.TraceException(TraceLevel.Verbose, e,"Could not move file ...trying again in a sec...");
                }
            }
            return false;
        }


        /// <summary>
        /// Rotates file names for rolling backup.
        /// </summary>
        /// <remarks>
        /// <para>
        /// Usage
        /// </para>
        /// </remarks>
        /// <param name="fileInfo">The file info.</param>
        /// <param name="ioService">The specific (Isolated or HD) service to use to Move/Delete files around.</param>
        /// <returns></returns>
        public static bool RotateToMakeSpace(this FileInfo fileInfo, IIOService ioService)
        {
            fileInfo.ValidateIsNotDefault("fileInfo");
            ioService.ValidateIsNotDefault("ioService");
            string fileName = fileInfo.FullName;

#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif
            long i;

            //	Path.GetDirectoryName(@"/ping/pong/file.ext").Dump(); => \ping\pong\
            //	Path.GetDirectoryName(@"\ping\pong\file.ext").Dump(); => \ping\pong\
            //Path.GetDirectoryName(@"ping\pong\file.ext").Dump(); => ping\pong\

            string tPath = Path.GetDirectoryName(fileName);
            string tBase = Path.GetFileNameWithoutExtension(fileName);
            string tStub = tPath + tBase;

            for (i = 5; i > 0; i--)
            {
                string tFrom;
                if (i > 1)
                {
                    tFrom = tStub + "." + (i - 1);
                }
                else
                {
                    tFrom = fileName;
                }

                string tTo = tStub + "." + i;
                if (ioService.FileExistsAsync(tFrom).Result)
                {
                    ioService.FileDeleteAsync(tTo);
                    ioService.FileMoveAsync(tFrom, tTo);
                }
            }
            return true;
        }






    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
