﻿namespace XAct.IO
{
    using System.Security.Principal;

    /// <summary>
    /// An specialization of the 
    /// <see cref="IUserFileSystemService"/>
    /// Contract for a service to return 
    /// entry points to the current 
    /// <see cref="IIdentity"/>'s WellKnown 
    /// file system locations (Local, Roaming, Temp).
    /// <para>
    /// Very rarely relevant to Server based development.
    /// </para>
    /// </summary>
    public interface IFileSystemUserFileSystemService : IUserFileSystemService, IHasXActLibService
    {
        
    }
}