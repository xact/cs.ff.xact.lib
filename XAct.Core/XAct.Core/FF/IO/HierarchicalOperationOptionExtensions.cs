﻿namespace XAct
{
    using System;

    /// <summary>
    /// Extensions to the <see cref="HierarchicalOperationOption"/>
    /// enum.
    /// </summary>
    public static class HierarchicalOperationOptionExtensions
    {
        /// <summary>
        /// Converts a <see cref="HierarchicalOperationOption"/>
        /// to a <see cref="System.IO.SearchOption"/>
        /// </summary>
        /// <param name="hierarchicalOperationOption">The hierarchical operation option.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public static System.IO.SearchOption MapTo(this HierarchicalOperationOption hierarchicalOperationOption)
        {
            switch (hierarchicalOperationOption)
            {
                case HierarchicalOperationOption.TopOnly:
                    return System.IO.SearchOption.TopDirectoryOnly;
                case HierarchicalOperationOption.Recursive:
                    return System.IO.SearchOption.AllDirectories;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
