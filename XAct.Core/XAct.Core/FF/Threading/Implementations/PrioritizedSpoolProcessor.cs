﻿// ReSharper disable CheckNamespace
namespace XAct.Threading
// ReSharper restore CheckNamespace
{
    using System;
    using System.Threading;
    using System.Timers;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of the <see cref="IPrioritizedSpoolProcessor"/>
    /// to process prioritized items using threads.
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class PrioritizedSpoolProcessor<TItem> : SpoolProcessor<TItem>
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="PrioritizedSpoolProcessor{TItem}" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="retrievePrioritizedItemsFunction">The method used to retrieve prioritized items.</param>
        /// <param name="itemProcessingFunction">The item processing function.</param>
        /// <param name="preTimerIntervalEvent">The pre timer interval event.</param>
        /// <param name="preTimerIntervalEventInterval">The pre timer interval event interval.</param>
        /// <param name="postTimerIntervalEvent">The _general cleanup function to do every timer event.</param>
        /// <param name="postTimerIntervalEventInterval">The pro timer interval event interval.</param>
        /// <param name="intervalInMilliseconds">The interval in milliseconds.</param>
        /// <param name="minThreads">The min threads.</param>
        /// <param name="maximumNumberOfThreads">The max threads.</param>
        public PrioritizedSpoolProcessor(
            ITracingService tracingService,
            Func<int, object, TItem[]> retrievePrioritizedItemsFunction,
            Func<TItem, bool> itemProcessingFunction,
            Action preTimerIntervalEvent = null,
            Action postTimerIntervalEvent = null,
            int preTimerIntervalEventInterval = 1,
            int postTimerIntervalEventInterval = 1,
            int intervalInMilliseconds = 1000,
            int minThreads = 5,
            int maximumNumberOfThreads = 50) :
            base
                (
                tracingService,
                retrievePrioritizedItemsFunction,
                itemProcessingFunction,
                preTimerIntervalEvent,
                postTimerIntervalEvent,
                preTimerIntervalEventInterval,
                postTimerIntervalEventInterval,
                intervalInMilliseconds,
                minThreads,
                maximumNumberOfThreads
                )
        {

        }


        /// <summary>
        /// Retrieves the items and allocate threads.
        /// </summary>
        /// <param name="totalNumberOfThreadsToAllocateThisTime">The total number of threads to allocate this time.</param>
        protected override void RetrieveItemsAndAllocateThreads(int totalNumberOfThreadsToAllocateThisTime)
        {
            //The number of slots that were not filled by items during last priority iteration.
            int leftOvers = 0;

            //Priority has the following 6 values (Critical[3],Urgent[2],High[1],Normal[0],Low[-1],VeryLow[-2]).
            //An easy distribution of the values is to spread them over (6x2=12) slots.

            //Priorities have to be Even number of Priorities.

            //So loop through the priorities, starting with low priority (3), and get just it's records.
            //then move on to next higher one.
            //The advantage of this is that the low's always get processed (but only with few threads)
            //If there were not the full amount of lows, the next higher priority can reuse the leftover count
            //to claim it for itself (eg: if we had 10 threads for low, and only 7 were used, 
            //Mediums can then use their normal 20 threads, plus the leftover 3, and so on...)
            //Notice that when we say Medium we really are saying "Get Mediums, and Lower"...
            //so they never go to waste.

            for (int i = 1; i <= 3; i++)
            {
                RetrieveItemsAndAllocateThreadPerPriority(i, totalNumberOfThreadsToAllocateThisTime, ref leftOvers);
            }
        }

        void RetrieveItemsAndAllocateThreadPerPriority(int priorityI, int totalNumberOfThreadsToAllocateThisTime, ref int leftOvers)
        {

            Priority priority;

            switch (priorityI)
            {
                case 1:
                    priority = Priority.Low;
                    break;
                case 2:
                    priority = Priority.Normal;
                    break;
                case 3:
                    priority = Priority.High;
                    break;
                default:
                    priority = Priority.Low;
                    break;
            }

            float percentageOfThreadsToUseDuringThisPriorityLoop = priorityI / (float)(1 + 2 + 3);

            //Get max number of items of "this priority or lower" to retrieve.
            // ReSharper disable SuggestUseVarKeywordEvident
            int maxNumberOfItemsToRetrieve =
                (int)
                Math.Round(percentageOfThreadsToUseDuringThisPriorityLoop *
                           totalNumberOfThreadsToAllocateThisTime);
            // ReSharper restore SuggestUseVarKeywordEvident


            //Get Items
            TItem[] items = _getItems(maxNumberOfItemsToRetrieve + leftOvers, priority);

            if ((items == null) || (items.Length == 0))
            {
                //If none came back we're done for this Timer.Event:
                Trace(TraceLevel.Verbose, "No items found: so we stop, until next timer interval...");

                return;
            }


            Trace(TraceLevel.Verbose,
                                  "Found {0} Messages (Priority<='{1}'). Beginning creating worker threads"
                                      .FormatStringInvariantCulture(items.Length, priorityI));

            //Foreach item found, we create a WorkerThread
            foreach (TItem item in items)
            {
                //Retrieves an available thread, and 
                this.AllocateAndIntializeNewWorkerThread(item, null);
            }


            //Leftovers?
            leftOvers = maxNumberOfItemsToRetrieve - items.Length;


            if (leftOvers > 0)
            {
                //This means there are no items of this value, or higher priority, left to process
                //at this moment. Let's get out, rather than moving on to next priority.
                Trace(TraceLevel.Verbose,
                                      "Although we had space for {0} items, we only found {1}: indicates no more to process at this time. Exiting till next Timer_Event.."
                                          .FormatStringInvariantCulture(maxNumberOfItemsToRetrieve, items.Length));

                return;
            }

            Trace(TraceLevel.Verbose,
                                  "Get next items (with higher priority than current '{0}')"
                                      .FormatStringInvariantCulture(priorityI));

        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TItem">The type of the item.</typeparam>
    public class SpoolProcessor<TItem> : ISpoolProcessor, IDisposable 
    {
        private int preIterationCounter = 0;
        private int postIterationCounter = 0;

        /// <summary>
        /// The optional tracing service.
        /// </summary>
        protected readonly ITracingService _tracingService;

        /// <summary>
        /// </summary>
        protected readonly Func<int, object, TItem[]> _getItems;


        /// <summary>
        /// The timer is a loop to periodically invoke a delegate to retrieve items to process. 
        /// </summary>
        protected readonly System.Timers.Timer _threadAllocationTimer;

        /// <summary>
        /// The function that the Thread will invoke when processing the Item.
        /// </summary>
        protected readonly Func<TItem, bool> _itemProcessingFunction;


        /// <summary>
        /// 
        /// </summary>
        protected readonly Action _preTimerIntervalEvent;
        private readonly int _preTimerIntervalEventInterval;

        /// <summary>
        /// 
        /// </summary>
        protected readonly Action _postTimerIntervalEvent;
        private readonly int _postTimerIntervalEventInterval;


        /// <summary>
        /// Gets the millisecond interval between checking whether 
        /// threads are available
        /// to issue.
        /// </summary>
        public long TimerInterval
        {
            get { return _timerInterval; }
        }
        private readonly long _timerInterval;

        /// <summary>
        /// Gets a value indicating whether to randomize the interval to the next Timer event,
        /// by a factor of <see cref="TimerInterval"/>, divided by <see cref="RandomizeTimerIntervalDivisor"/>
        /// </summary>
        /// <value>
        /// <c>true</c> if [randomize timer interval]; otherwise, <c>false</c>.
        /// </value>
        public bool RandomizeTimerInterval
        {
            get { return _randomizeTimerInterval; }
        }
        private bool _randomizeTimerInterval;

        /// <summary>
        /// If <see cref="RandomizeTimerInterval"/> is <c>True</c>,
        /// Gets the divisor to use when calculating the next interval.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [random timer divisor]; otherwise, <c>false</c>.
        /// </value>
        public int RandomizeTimerIntervalDivisor
        {
            get { return _randomizeTimerIntervalDivisor; }
        }
        private int _randomizeTimerIntervalDivisor = 6;

        /// <summary>
        /// The mimimum number of threads required to be available prior to 
        /// requesting new threads.
        /// <para>
        /// Explainatino:
        /// </para>
        ///   <para>
        /// Every Timer interval, the number of available threads is checked.
        ///   </para>
        ///   <para>
        /// If the number of threads returned/available for re-issuance is less than
        /// this minimum threshold, nothing is done until the next timer interval.
        ///   </para>
        /// <para>
        /// The setting can allow some release of pressure from a server that 
        /// is working extra hard.
        /// </para>
        /// </summary>
        public int MinimumNumberOfAvailableThreadsRequiredToRequestNewThreads
        {
            get { return _minimumNumberOfAvailableThreadsRequiredToRequestNewThreads; }
        }
        private readonly int _minimumNumberOfAvailableThreadsRequiredToRequestNewThreads;


        /// <summary>
        /// Gets the maximum number of new threads to issue at any one time.
        /// <para>
        /// If the number of available threads is larger than
        /// <see cref="MinimumNumberOfAvailableThreadsRequiredToRequestNewThreads"/>,
        /// threads will be allocated. But to ensure that threads are not all in one go 
        /// (spiking the cpu) no more than a certain number of threads are issued
        /// until the next Timer interval.
        /// </para>
        /// </summary>
        public int MaximumNumberOfNewThreadsToIssueAtAnyOneTime
        {
            get { return _maximumNumberOfNewThreadsToIssueAtAnyOneTime; }
        }
        private readonly int _maximumNumberOfNewThreadsToIssueAtAnyOneTime;



        /// <summary>
        /// Initializes a new instance of the <see cref="SpoolProcessor{TItem}" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="getItems">The get prioritized items.</param>
        /// <param name="itemProcessingFunction">The item processing function.</param>
        /// <param name="preTimerIntervalEvent">The pre timer interval event.</param>
        /// <param name="preTimerIntervalEventInterval">The pre timer interval event interval.</param>
        /// <param name="postTimerIntervalEvent">The general cleanup function to do every timer event.</param>
        /// <param name="postTimerIntervalEventInterval">The post timer interval event interval.</param>
        /// <param name="intervalInMilliseconds">The interval in milliseconds.</param>
        /// <param name="minimumNumberOfAvailableThreadsRequiredToRequestNewThreads">The minimum threads.</param>
        /// <param name="maximumNumberOfNewThreadsToIssueAtAnyOneTime">The maximum threads.</param>
        /// <param name="randomizeTimerInterval">if set to <c>true</c> [randomize timer interval].</param>
        /// <param name="randomizeFraction">The randomize fraction.</param>
        public SpoolProcessor(
            ITracingService tracingService,
            Func<int, object, TItem[]> getItems,
            Func<TItem, bool> itemProcessingFunction,
            Action preTimerIntervalEvent = null,
            Action postTimerIntervalEvent = null,
            int preTimerIntervalEventInterval = 1,
            int postTimerIntervalEventInterval = 1,
            int intervalInMilliseconds = 1000,
            int minimumNumberOfAvailableThreadsRequiredToRequestNewThreads = 5,
            int maximumNumberOfNewThreadsToIssueAtAnyOneTime = 50,
            bool randomizeTimerInterval = false,
            int randomizeFraction = 6)
        {
            _tracingService = tracingService;
            _getItems = getItems;
            _itemProcessingFunction = itemProcessingFunction;

            _preTimerIntervalEvent = preTimerIntervalEvent;
            _preTimerIntervalEventInterval = preTimerIntervalEventInterval;
            _postTimerIntervalEvent = postTimerIntervalEvent;
            _postTimerIntervalEventInterval = postTimerIntervalEventInterval;

            //Min 5, max 50:
            _minimumNumberOfAvailableThreadsRequiredToRequestNewThreads = minimumNumberOfAvailableThreadsRequiredToRequestNewThreads;
            _maximumNumberOfNewThreadsToIssueAtAnyOneTime = maximumNumberOfNewThreadsToIssueAtAnyOneTime;


            _randomizeTimerInterval = randomizeTimerInterval;
            _randomizeTimerIntervalDivisor = randomizeFraction;

            //Trigger used to trigger check:
            _threadAllocationTimer = new System.Timers.Timer();

            //Attach event handler
            this._threadAllocationTimer.Elapsed += Timer_Elapsed;

            //Make interval no less than 1sec:
            if (intervalInMilliseconds < 100)
            {
                intervalInMilliseconds = 100;
            }

            _timerInterval = intervalInMilliseconds;

            _threadAllocationTimer.Interval = _timerInterval;


        }



        /// <summary>
        /// Starts the specified randomize timer interval.
        /// </summary>
        public virtual void Start()
        {

            try
            {
                //Wire up interval only if not yet started:
                if (!_threadAllocationTimer.Enabled)
                {
                    this._threadAllocationTimer.Start();
                }
            }
            catch (Exception exception)
            {
                _tracingService.TraceException(TraceLevel.Error, exception, "An error occurred while trying to start the Queue polling timer.");
                throw;
            }

        }

        /// <summary>
        /// Stop Processing the Prioritized Items.
        /// </summary>
        public virtual void Stop()
        {
            try
            {
                this._threadAllocationTimer.Stop();
                Trace(TraceLevel.Info, "Queue polling timer successfully stopped.");
            }
            catch (Exception exception)
            {
                TraceException(TraceLevel.Error, exception, "An error occurred while trying to stop the Queue polling timer.");
                throw;
            }

        }

        //Event raised periodically once Start is invoked.
        //Notice that Event interval is randomized as needed to 
        //stagger processors a bit.
        //Event raised periodically once Start is invoked.
        //Notice that Event interval is randomized as needed to 
        //stagger processors a bit.
        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_randomizeTimerInterval)
            {
                //If we are to stagger the interval, let's update the timer now:
                this._threadAllocationTimer.Interval =
                    new TimeSpan(0, 0, 0, 0, (int)_threadAllocationTimer.Interval).AddRandonmess(_randomizeTimerIntervalDivisor).Milliseconds;
            }


            try
            {
                int availableWorkerThreads;
                int availableCompletionPortThreads;

                ThreadPool.GetAvailableThreads(out availableWorkerThreads, out availableCompletionPortThreads);

                if (availableWorkerThreads < _minimumNumberOfAvailableThreadsRequiredToRequestNewThreads)
                {
                    Trace(TraceLevel.Verbose, "Not enough threads at this moment. Waiting till next Timer.Event to check again.");

                    return;
                }

                //We want no more than 50 Threads processing anything at a time.
                int totalNumberOfThreadsToAllocateThisTime = Math.Min(_maximumNumberOfNewThreadsToIssueAtAnyOneTime, availableWorkerThreads);

                if (totalNumberOfThreadsToAllocateThisTime == 0)
                {
                    return;
                }


                RetrieveItemsAndAllocateThreads(totalNumberOfThreadsToAllocateThisTime);

            }
            catch (Exception exception)
            {
                TraceException(TraceLevel.Error, exception, "An unhandled exception was caught while trying to dispatch Items. IMPORTANT: Absorbing error.");
            }


            //At this point, probably none of the current batch has been processed
            //as they may not yet have started.

            //But maybe we have a general function to cleanup older records.
            PostTimerInterval();
        }

        /// <summary>
        /// </summary>
        protected void PreTimerInterval()
        {
            //Increment timer:
            preIterationCounter++;
            if (preIterationCounter != _preTimerIntervalEventInterval)
            {
                return;
            }


            if (_preTimerIntervalEvent != null)
            {
                _preTimerIntervalEvent.Invoke();
            }

            //Reset counter:
            preIterationCounter = 0;
        }



        /// <summary>
        /// </summary>
        protected virtual void RetrieveItemsAndAllocateThreads(int totalNumberOfThreadsToAllocateThisTime)
        {

            //Get Items
            TItem[] items = RetrieveItems(totalNumberOfThreadsToAllocateThisTime);

            if ((items == null) || (items.Length == 0))
            {
                //If none came back we're done for this Timer.Event:
                Trace(TraceLevel.Verbose, "No items found: so we stop, until next timer interval...");
                return;
            }

            AllocateAndIntializeNewWorkerThreads(items);
        }

        /// <summary>
        /// </summary>
        protected TItem[] RetrieveItems(int maxNumberOfItemsToRetrieve)
        {
            //Get Items
            TItem[] items = _getItems.Invoke(maxNumberOfItemsToRetrieve, null);
            return items;
        }
        /// <summary>
        /// Creates intialize new worker threads
        /// up to the number of available threads for this trigger event.
        /// </summary>
        protected void AllocateAndIntializeNewWorkerThreads(TItem[] items)
        {
            int i = 0;
            foreach (TItem item in items)
            {
                i++;
                AllocateAndIntializeNewWorkerThread(item, DateTime.UtcNow + ":" + i);
            }
        }


        /// <summary>
        /// Retrieves a thread from the available thread pool.
        /// Ie, if there are 30 threads left, takes x (depending on priority)
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="tag">The tag.</param>
        protected void AllocateAndIntializeNewWorkerThread(TItem item, string tag)
        {
            //Get A worker thread:
            // ReSharper disable SuggestUseVarKeywordEvident
            WorkerThread<TItem> thread = new WorkerThread<TItem>(_tracingService);
            // ReSharper restore SuggestUseVarKeywordEvident

            //Wire up eventhandlers:


            //Start it with its payload, so that when invoked, it goes off and does it's thing.
            thread.Start(new WorkerThreadConfiguration<TItem>(item, _itemProcessingFunction, tag));


        }


        /// <summary>
        /// A post trigger interval operation.
        /// <para>
        /// Useful for cleaning up -- eg, ensuring that records that were marked as started, 
        /// but that got hung, are released, so that they can be processed again.
        /// </para> 
        /// </summary>
        protected void PostTimerInterval()
        {
            postIterationCounter++;
            if (postIterationCounter != _postTimerIntervalEventInterval)
            {
                return;
            }

            if (_postTimerIntervalEvent != null)
            {
                _postTimerIntervalEvent.Invoke();
            }
            postIterationCounter = 0;
        }


        /// <summary>
        /// Traces the message -- if the tracing service is not null.
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The arguments.</param>
        protected void Trace(TraceLevel traceLevel, string message, params object[] args)
        {
            if (_tracingService == null)
            {
                return;
            }
            _tracingService.Trace(traceLevel, message, args);
        }
        /// <summary>
        /// Traces the exception.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <param name="traceLevel">The trace level.</param>
        /// <param name="message">The message.</param>
        /// <param name="args">The arguments.</param>
        protected void TraceException(TraceLevel traceLevel, Exception exception, string message, params object[] args)
        {
            if (_tracingService == null)
            {
                return;
            }
            _tracingService.TraceException(traceLevel, exception, message, args);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            _threadAllocationTimer.Dispose();
        }
    }
}