﻿//// Copyright (c) Microsoft Open Technologies, Inc. All rights reserved. See License.txt in the project root for license information.

//using System.Collections.Concurrent;
//using System.Collections.Generic;
//using System.Diagnostics.CodeAnalysis;
//using System.Globalization;
//using System.Linq;
//using System.Reflection;

//namespace XAct
//{





//    //READ:
//    //http://bradwilson.typepad.com/blog/2010/10/service-location-pt5-idependencyresolver.html
//    //and also notice code example in comments.





//    using System;

//    /// <summary>
//    /// 
//    /// </summary>
//    public class DependencyResolver
//    {
//        /// <summary>
//        /// Back <see cref="Current"/> 
//        /// property.
//        /// </summary>
//        private static DependencyResolver _instance = new DependencyResolver();

//        private IDependencyResolver _current;

//        /// <summary>
//        /// Cache should always be a new CacheDependencyResolver(_current).
//        /// </summary>
//        private CacheDependencyResolver _currentCache;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="DependencyResolver"/> class.
//        /// </summary>
//        public DependencyResolver()
//        {
//            //Use the default (anemic) DependencyResolver:
//            InnerSetResolver(new DefaultDependencyResolver());
//        }

//        /// <summary>
//        /// Gets the current 
//        /// <see cref="IDependencyResolver"/>.
//        /// </summary>
//        /// <value>
//        /// The current.
//        /// </value>
//        public static IDependencyResolver Current
//        {
//            get { return _instance.InnerCurrent; }
//        }

//        internal static IDependencyResolver CurrentCache
//        {
//            get { return _instance.InnerCurrentCache; }
//        }

//        /// <summary>
//        /// Gets the inner current.
//        /// </summary>
//        /// <value>
//        /// The inner current.
//        /// </value>
//        public IDependencyResolver InnerCurrent
//        {
//            get { return _current; }
//        }

//        /// <summary>
//        /// Provides caching over results returned by Current.
//        /// </summary>
//        internal IDependencyResolver InnerCurrentCache
//        {
//            get { return _currentCache; }
//        }

//        /// <summary>
//        /// </summary>
//        public static void SetResolver(IDependencyResolver resolver)
//        {
//            _instance.InnerSetResolver(resolver);
//        }

//        /// <summary>
//        /// </summary>
//        public static void SetResolver(object commonServiceLocator)
//        {
//            _instance.InnerSetResolver(commonServiceLocator);
//        }

//        /// <summary>
//        /// </summary>
//        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an appropriate nesting of generic types.")]
//        public static void SetResolver(Func<Type, object> getService, Func<Type, IEnumerable<object>> getServices)
//        {
//            _instance.InnerSetResolver(getService, getServices);
//        }

//        /// <summary>
//        /// </summary>
//        public void InnerSetResolver(IDependencyResolver resolver)
//        {
//            if (resolver == null)
//            {
//                throw new ArgumentNullException("resolver");
//            }

//            _current = resolver;
//            _currentCache = new CacheDependencyResolver(_current);
//        }

//        /// <summary>
//        /// </summary>
//        public void InnerSetResolver(object commonServiceLocator)
//        {
//            if (commonServiceLocator == null)
//            {
//                throw new ArgumentNullException("commonServiceLocator");
//            }

//            Type locatorType = commonServiceLocator.GetType();
//            MethodInfo getInstance = locatorType.GetMethod("GetInstance", new[] {typeof (Type)});
//            MethodInfo getInstances = locatorType.GetMethod("GetAllInstances", new[] {typeof (Type)});

//            if (getInstance == null ||
//                getInstance.ReturnType != typeof (object) ||
//                getInstances == null ||
//                getInstances.ReturnType != typeof (IEnumerable<object>))
//            {
//                throw new ArgumentException(
//                    String.Format(
//                        CultureInfo.CurrentCulture,
//                        "DependencyResolver does not implement ICommonServiceLocator",
//                        locatorType.FullName),
//                    "commonServiceLocator");
//            }

//            var getService =
//                (Func<Type, object>)
//                Delegate.CreateDelegate(typeof (Func<Type, object>), commonServiceLocator, getInstance);
//            var getServices =
//                (Func<Type, IEnumerable<object>>)
//                Delegate.CreateDelegate(typeof (Func<Type, IEnumerable<object>>), commonServiceLocator, getInstances);

//            InnerSetResolver(new DelegateBasedDependencyResolver(getService, getServices));
//        }

//        /// <summary>
//        /// </summary>
//        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an appropriate nesting of generic types.")]
//        public void InnerSetResolver(Func<Type, object> getService, Func<Type, IEnumerable<object>> getServices)
//        {
//            if (getService == null)
//            {
//                throw new ArgumentNullException("getService");
//            }
//            if (getServices == null)
//            {
//                throw new ArgumentNullException("getServices");
//            }

//            InnerSetResolver(new DelegateBasedDependencyResolver(getService, getServices));
//        }

//        /// <summary>
//        /// Wraps an IDependencyResolver and ensures single instance per-type.
//        /// </summary>
//        /// <remarks>
//        /// Note it's possible for multiple threads to race and call the _resolver service multiple times.
//        /// We'll pick one winner and ignore the others and still guarantee a unique instance.
//        /// </remarks>
//        private sealed class CacheDependencyResolver : IDependencyResolver
//        {
//            private readonly ConcurrentDictionary<Type, object> _cache = new ConcurrentDictionary<Type, object>();
//            private readonly ConcurrentDictionary<Type, IEnumerable<object>> _cacheMultiple = new ConcurrentDictionary<Type, IEnumerable<object>>();
//            private readonly Func<Type, object> _getServiceDelegate;
//            private readonly Func<Type, IEnumerable<object>> _getServicesDelegate;

//            private readonly IDependencyResolver _resolver;

//            public CacheDependencyResolver(IDependencyResolver resolver)
//            {
//                _resolver = resolver;
//                _getServiceDelegate = _resolver.GetService;
//                _getServicesDelegate = _resolver.GetServices;
//            }

//            public object GetService(Type serviceType)
//            {
//                // Use a saved delegate to prevent per-call delegate allocation
//                return _cache.GetOrAdd(serviceType, _getServiceDelegate);
//            }

//            public IEnumerable<object> GetServices(Type serviceType)
//            {
//                // Use a saved delegate to prevent per-call delegate allocation
//                return _cacheMultiple.GetOrAdd(serviceType, _getServicesDelegate);
//            }
//        }

//        public static class X
//        {
//            public static TValue GetOrAdd<TKey,TValue>(this Dictionary<TKey,TValue> dictionary, TKey key, TValue value)
//            {
//                TValue value2;
//             if (!dictionary.TryGetValue(key,out value2))
//             {
//                 dictionary.Add(key,value);
//                 value2 = value;
//             }
//                return value2;

//            }

//        }


//        private class DefaultDependencyResolver : IDependencyResolver
//        {
//            [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "This method might throw exceptions whose type we cannot strongly link against; namely, ActivationException from common service locator")]
//            public object GetService(Type serviceType)
//            {
//                // Since attempting to create an instance of an interface or an abstract type results in an exception, immediately return null
//                // to improve performance and the debugging experience with first-chance exceptions enabled.
//                if (serviceType.IsInterface || serviceType.IsAbstract)
//                {
//                    return null;
//                }

//                try
//                {
//                    return Activator.CreateInstance(serviceType);
//                }
//                catch
//                {
//                    return null;
//                }
//            }

//            public IEnumerable<object> GetServices(Type serviceType)
//            {
//                return Enumerable.Empty<object>();
//            }
//        }


//        private class DelegateBasedDependencyResolver : IDependencyResolver
//        {
//            private Func<Type, object> _getService;
//            private Func<Type, IEnumerable<object>> _getServices;

//            public DelegateBasedDependencyResolver(Func<Type, object> getService, Func<Type, IEnumerable<object>> getServices)
//            {
//                _getService = getService;
//                _getServices = getServices;
//            }

//            [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "This method might throw exceptions whose type we cannot strongly link against; namely, ActivationException from common service locator")]
//            public object GetService(Type type)
//            {
//                try
//                {
//                    return _getService.Invoke(type);
//                }
//                catch
//                {
//                    return null;
//                }
//            }

//            public IEnumerable<object> GetServices(Type type)
//            {
//                return _getServices(type);
//            }
//        }
//    }
//}
