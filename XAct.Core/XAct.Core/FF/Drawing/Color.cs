﻿namespace XAct.Drawing
{
    using System.Runtime.Serialization;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> Enum.
    /// </para>
    /// Enumeration of basic Colors (BW,GrayScale,Color)
    /// </summary>
    /// <remarks>
    /// <para>
    /// Used by <c>XAct.Imaging.Vision.VisionService</c>
    /// </para>
    /// </remarks>
    [DataContract(Namespace = "http://xact-solutions.com/contracts/v0.1/xact")]
    public enum ColorType
    {
        /// <summary>
        /// Black and White
        /// </summary>
        BW = 1,
        
        /// <summary>
        /// Grayscale Images
        /// </summary>
        Grayscale = 2,

        /// <summary>
        /// Color Images
        /// </summary>
        Color=3,
    }
}