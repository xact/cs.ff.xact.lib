﻿#if NET40
using System.ComponentModel.DataAnnotations;
#endif
#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

namespace XAct.Domain
{
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;

    /// <summary>
    /// <para>
    /// An <c>XActLib</c> contract.
    /// </para>
    /// Abstract base class for an Observable Model that has properties
    /// that when updated raise the PropertyChanged event.
    /// </summary>
    /// <internal><para>8/10/2011: Sky</para></internal>
    public abstract class ObservableModelBase : INotifyPropertyChanging, INotifyPropertyChanged, IEditableObject
    {
        #region Events Raised

        #region INotifyPropertyChanged Members

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        /// <internal><para>8/10/2011: Sky</para></internal>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region INotifyPropertyChanging Members

        /// <summary>
        /// Occurs when a property value is changing.
        /// </summary>
        /// <internal><para>8/13/2011: Sky</para></internal>
        public event PropertyChangingEventHandler PropertyChanging;

        #endregion

        #endregion

        /// <summary>
        /// The dictionary of saved values, in case <see cref="CancelEdit"/> is invoked.
        /// </summary>
        /// <internal>
        /// Note that the dictionary can't be any more type specific than 'object'.
        /// </internal>
        protected Dictionary<string, object> BackupValues =
            new Dictionary<string, object>();

        /// <summary>
        /// The dictionary of current values (accessed via <see cref="GetPropertyValue{T}"/>).
        /// </summary>
        /// <internal>
        /// Note that the dictionary can't be any more type specific than 'object'.
        /// </internal>
        protected Dictionary<string, object> CurrentValues =
            new Dictionary<string, object>();

        #region Raise Events

        /// <summary>
        /// Called when a observable property is changed.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// public string FirstName {
        ///  get {GetPropertyValue("FirstName");}
        ///  set {SetPropertyValue("FirstName", value);}
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }


        /// <summary>
        /// Raises the <see cref="PropertyChanged"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangedEventArgs"/> instance containing the event data.</param>
        /// <internal><para>8/10/2011: Sky</para></internal>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }


        /// <summary>
        /// Called when a observable property is changing.
        /// <para>
        /// <code>
        /// <![CDATA[
        /// public string FirstName {
        ///  get {GetPropertyValue("FirstName");}
        ///  set {SetPropertyValue("FirstName", value);}
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        protected virtual void OnPropertyChanging(string propertyName)
        {
            OnPropertyChanging(new PropertyChangingEventArgs(propertyName));
        }

        /// <summary>
        /// Raises the <see cref="PropertyChanging"/> event.
        /// </summary>
        /// <param name="e">The <see cref="System.ComponentModel.PropertyChangingEventArgs"/> instance containing the event data.</param>
        /// <internal><para>8/13/2011: Sky</para></internal>
        protected virtual void OnPropertyChanging(PropertyChangingEventArgs e)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, e);
            }
        }

        #endregion

        #region IEditableObject Members

        /// <summary>
        /// Begins an edit on an object.
        /// </summary>
        /// <internal><para>8/10/2011: Sky</para></internal>
        public virtual void BeginEdit()
        {
            //Save the values so that we can rollback:
            CloneDictionary(CurrentValues, BackupValues);
        }

        /// <summary>
        /// Discards changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> call.
        /// </summary>
        /// <internal><para>8/10/2011: Sky</para></internal>
        public virtual void CancelEdit()
        {
            //Roll back the values to how they were before we invoked BeginEdit
            CloneDictionary(BackupValues, CurrentValues);
        }

        /// <summary>
        /// Pushes changes since the last <see cref="M:System.ComponentModel.IEditableObject.BeginEdit"/> or 
        /// <see cref="M:System.ComponentModel.IBindingList.AddNew"/> call into the underlying object.
        /// </summary>
        /// <internal><para>8/10/2011: Sky</para></internal>
        public virtual void EndEdit()
        {
            CloneDictionary(CurrentValues, BackupValues);
        }

        #endregion

        #region Protected Helper Methods

        /// <summary>
        /// Gets the property value from the internal current value dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <returns></returns>
        /// <internal><para>8/10/2011: Sky</para></internal>
        protected virtual T GetPropertyValue<T>(string propertyName)
        {
            object result = ExtractPropertyFromDictionaryValue<object>(propertyName);

            return (
                       (result != null)
                       &&
                       (Comparer.Default.Compare((T) result, default(T)) != 0))
                       ? (T) result
                       : default(T);
        }


        /// <summary>
        /// Sets the property value into the internal current value dictionary.
        /// <para>
        /// Note that if the value matches what is there already, no
        /// change takes place, and <see cref="OnPropertyChanged(string)"/>
        /// is not invoked.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">The value.</param>
        /// <internal><para>8/10/2011: Sky</para></internal>
        protected virtual void SetPropertyValue<T>(string propertyName, object value)
        {
            if (Equals(value, GetPropertyValue<T>(propertyName)))
            {
                return;
            }

            ValidatePropertyValue(propertyName, value);

            //If validation did not raise exception, we are going to update value:
            OnPropertyChanging(propertyName);

            SetPropertyValueInDictionaryValue(propertyName, value);

            OnPropertyChanged(propertyName);
        }


        /// <summary>
        /// <para>
        /// This could get more complicated at a later date.
        /// </para>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        protected virtual object ExtractPropertyFromDictionaryValue<T>(string propertyName)
        {
            object dictionaryValue;

            CurrentValues.TryGetValue(propertyName, out dictionaryValue);

            return dictionaryValue;
        }

        /// <summary>
        /// Sets the property value in dictionary value.
        /// <para>
        /// This could get more complicated at a later date.
        /// </para>
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <internal><para>8/10/2011: Sky</para></internal>
        protected virtual void SetPropertyValueInDictionaryValue(string propertyName, object value)
        {
            CurrentValues[propertyName] = value;
        }


        /// Validates the value against any
        /// System.ComponentModel.DataAnnotations rules
        /// applied to the property.
        /// 
        /// Name of the property.
        /// The value.
        protected virtual void ValidatePropertyValue(string propertyName, object value)
        {
#if NET40 // Requires .Net 4, so hold for the moment 
            System.ComponentModel.DataAnnotations.ValidationContext validationContext =
              new System.ComponentModel.DataAnnotations.ValidationContext(this, null, null);
        
            //point back to Property, and therefore to its Attributes.
            validationContext.MemberName = propertyName;

            //Kick in validation
            System.ComponentModel.DataAnnotations.Validator.ValidateProperty(
              value, //The value to validate before setting field
              validationContext);
#endif
        }


        /// Clones the dictionary.
        /// 
        /// 
        /// Invoked by 
        /// ,
        /// , 
        /// and
        /// 
        /// 
        /// The dictionary whose values are to be copied from.
        /// The dictionary the values are to be copied to.
        protected virtual void CloneDictionary(IDictionary<string, object> from, IDictionary<string, object> to)
        {
            to.Clear();

            foreach (KeyValuePair<string, object> pair in from)
            {
                to[pair.Key] = pair.Value;
            }
        }

        #endregion
    }
}