﻿namespace XAct.Net
{
    using System;
    using System.Net;

    /// <summary>
    /// A WebClient that can be timed out.
    /// <para>
    /// Useful for diagnostics.
    /// </para>
    /// </summary>
    public class TimeoutWebClient : WebClient
    {
        /// <summary>
        /// Gets or sets the timeout (default 30 seconds).
        /// </summary>
        /// <value>
        /// The timeout.
        /// </value>
        public TimeSpan Timeout { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeoutWebClient"/> class.
        /// </summary>
        /// <param name="timeOut">The time out.</param>
        public TimeoutWebClient(TimeSpan timeOut = default(TimeSpan))
        {
            if (timeOut.TotalMilliseconds == 0)
            {
                timeOut = TimeSpan.FromMilliseconds(30*1000);
            }
            Timeout = timeOut;
        }

        /// <summary>
        /// Gets the web request.
        /// </summary>
        /// <param name="uri">The URI.</param>
        /// <returns></returns>
        protected override WebRequest GetWebRequest(Uri uri)
        {
            WebRequest webRequest = base.GetWebRequest(uri);
            webRequest.Timeout = (int) Timeout.TotalMilliseconds;
            return webRequest;
        }
    }
}