#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

namespace XAct.Xml
{
    using System;
    using System.Collections;
    using System.Diagnostics.CodeAnalysis;
    using System.Globalization;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Xml;

    /// <summary>
    ///   XmlNodeWriter builds a tree of XmlNodes based on 
    ///   the XmlWriter methods that are called.
    /// </summary>
    [ComVisible(false)]
    public class XmlNodeWriter : XmlWriter
    {
        private static readonly CultureInfo _invariantCulture =
            CultureInfo.CurrentCulture;

        private XmlAttribute _ca; // current attribute (if any)
        private XmlNode _current;
        private XmlNameTable _nameTable;
        private XmlDocument _owner;
        private XmlNode _root;

        private WriteState _state;
        private string _xmlns;
        private string _xmlnsURI;

        /// <summary>
        ///   Construct XmlNodeWriter for building the content of the given root XmlElement node.
        /// </summary>
        /// <param name = "root">If root contains any content it will be completely replaced by what the XmlNodeWriter produces.</param>
        /// <param name = "clearCurrentContents">Clear the current children of the given node</param>
        public XmlNodeWriter(XmlElement root, bool clearCurrentContents)
        {
            Init(root, clearCurrentContents);
        }

        /// <summary>
        ///   Construct XmlNodeWriter for building the content of 
        ///   the given root XmlDocument node.
        ///   The current children of the given document are cleared.
        /// </summary>
        /// <param name = "root">The root document.</param>
        public XmlNodeWriter(XmlDocument root) : this(root, true)
        {
        }

        /// <summary>
        ///   Construct XmlNodeWriter for building the content 
        ///   of the given root XmlDocument node.
        /// </summary>
        /// <param name = "root">If root contains any content it will be completely replaced by what the XmlNodeWriter produces.</param>
        /// <param name = "clearCurrentContents">Clear the current children of the given node</param>
        public XmlNodeWriter(XmlDocument root, bool clearCurrentContents)
        {
            Init(root, clearCurrentContents);
        }

        /// <summary>
        ///   Return the current state of the writer.
        /// </summary>
        public override WriteState WriteState
        {
            get
            {
                switch (_state)
                {
                    case WriteState.Start:
                        return WriteState.Start;
                    case WriteState.Prolog:
                        return WriteState.Prolog;
                    case WriteState.Element:
                        return WriteState.Element;
                    case WriteState.Attribute:
                        return WriteState.Attribute;
                    case WriteState.Content:
                        return WriteState.Content;
                    case WriteState.Closed:
                        return WriteState.Closed;
                }
                return WriteState.Closed;
            }
        }

        /// <summary>
        ///   Return the current XmlLang state.  This does not have an efficient implementation, so use at your own risk.
        /// </summary>
        public override string XmlLang
        {
            get
            {
                XmlNode p = _current;
                while (p != null)
                {
                    XmlElement e = p as XmlElement;
                    if (e != null)
                    {
                        string s = e.GetAttribute("lang", "http://www.w3.org/XML/1998/namespace");
                        if (!s.IsNullOrEmpty())
                        {
                            return s;
                        }
                    }
                    p = p.ParentNode;
                }
                return null;
            }
        }

        /// <summary>
        ///   Return the current XmlSpace state.  This does not have an efficient implementation, so use at your own risk.
        /// </summary>
        public override XmlSpace XmlSpace
        {
            get
            {
                XmlNode p = _current;
                while (p != null)
                {
                    XmlElement e = p as XmlElement;
                    if (e != null)
                    {
                        string s = e.GetAttribute("space", "http://www.w3.org/XML/1998/namespace");
                        if (s == "default")
                        {
                            return XmlSpace.Default;
                        }
                        else if (s == "preserve")
                        {
                            return XmlSpace.Preserve;
                        }
                    }
                    p = p.ParentNode;
                }
                return XmlSpace.None;
            }
        }

        private void Init(XmlNode rootNode, bool clearCurrentContents)
        {
            _root = rootNode;
            if (clearCurrentContents)
            {
                _root.RemoveAll();
            }
            if (rootNode is XmlDocument)
            {
                _owner = (XmlDocument) rootNode;
                _state = WriteState.Start;
            }
            else
            {
                _owner = rootNode.OwnerDocument;
                _state = WriteState.Content;
            }
            _current = rootNode;
            _nameTable = _owner.NameTable;
            _xmlnsURI = _nameTable.Add("http://www.w3.org/2000/xmlns/");
            _xmlns = _nameTable.Add("xmlns");
        }

        /// <summary>
        ///   This auto-closes any open elements and puts the writer in the WriteState.Closed state.
        /// </summary>
        public override void Close()
        {
            _current = _root;
            _state = WriteState.Closed;
        }

        /// <summary>
        ///   This is a noop.
        /// </summary>
        public override void Flush()
        {
        }

        /// <summary>
        ///   Returns the result of GetPrefixOfNamespace on the current node.
        /// </summary>
        /// <param name = "ns">The namespaceURI to lookup the associated prefix for.</param>
        /// <returns>The prefix or null if no matching namespaceURI is in scope.</returns>
        public override string LookupPrefix(string ns)
        {
            // We cannot use doc.GetPrefixOfNamespace because we need to return null if there
            // is no prefix in scope.

            ns = _nameTable.Add(ns);

            if (ns == _xmlnsURI)
            {
                return _xmlns;
            }

            if (_current != null)
            {
                XmlNode node = _current;
                while (node != null)
                {
                    if (node.NodeType == XmlNodeType.Element)
                    {
                        if (node.NamespaceURI == ns)
                        {
                            return node.Prefix;
                        }

                        // search for namespace decl in attributes
                        XmlElement e = (XmlElement) node;
                        if (e.HasAttributes)
                        {
                            int cAttr = e.Attributes.Count;
                            for (int iAttr = cAttr - 1; iAttr >= 0; iAttr--)
                            {
                                XmlNode attr = e.Attributes[iAttr];
                                if (attr.Prefix == _xmlns &&
                                    attr.Value == ns)
                                {
                                    return attr.LocalName;
                                }
                            }
                        }
                        node = node.ParentNode;
                    }
                    else if (node.NodeType == XmlNodeType.Attribute)
                    {
                        node = ((XmlAttribute) node).OwnerElement;
                    }
                    else
                    {
                        node = node.ParentNode;
                    }
                }
            }
            return null;
        }

        /// <summary>
        ///   This method is implemented using Convert.ToBase64String.
        /// </summary>
        public override void WriteBase64(byte[] buffer, int index, int count)
        {
            WriteString(Convert.ToBase64String(buffer, index, count));
        }

        /// <summary>
        ///   This is implementd using a temporary XmlTextWriter to turn the 
        ///   given binary blob into a string, then it calls WriteString with
        ///   the result.
        /// </summary>
        public override void WriteBinHex(byte[] buffer, int index, int count)
        {
            using (StringWriter stringWriter = new StringWriter(_invariantCulture))
            {
                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stringWriter))
                {
                    xmlTextWriter.WriteBinHex(buffer, index, count);
                    xmlTextWriter.Close();
                    WriteString(stringWriter.ToString());
                }
            }
        }

        /// <summary>
        ///   Creates a System.Xml.XmlCDataSection node.
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA2204", Justification = "Spelling ('CData') is ok.")]
        public override void WriteCData(string text)
        {
            if (_state == WriteState.Attribute || _state == WriteState.Element)
            {
                _state = WriteState.Content;
            }
            if (_state != WriteState.Content)
            {
                throw new InvalidOperationException("Writer is in the state '" + WriteState +
                                                    "' which is not valid for writing CData elements");
            }
            _current.AppendChild(_owner.CreateCDataSection(text));
        }

        /// <summary>
        ///   Writes the given char as a string.  The XmlDocument has no representation for 
        ///   character entities, so the fact that this was called will be lost.
        /// </summary>
        public override void WriteCharEntity(char ch)
        {
            WriteString(Convert.ToString(ch, _invariantCulture));
        }

        /// <summary>
        ///   Calls WriteString with new string(buffer, index, count).
        /// </summary>
        public override void WriteChars(char[] buffer, int index, int count)
        {
            WriteString(new string(buffer, index, count));
        }

        /// <summary>
        ///   Creates an System.Xml.XmlComment node.
        /// </summary>
        public override void WriteComment(string text)
        {
            if (_state == WriteState.Attribute || _state == WriteState.Element)
            {
                _state = WriteState.Content;
            }
            if (_state != WriteState.Content && _state != WriteState.Prolog && _state != WriteState.Start)
            {
                throw new InvalidOperationException("Writer is in the state '" + WriteState +
                                                    "' which is not valid for writing comments");
            }
            _current.AppendChild(_owner.CreateComment(text));
            if (_state == WriteState.Start)
            {
                _state = WriteState.Prolog;
            }
        }

        /// <summary>
        ///   Creates an System.Xml.XmlDocumentType node.
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA2204", Justification = "Spelling ('XmlDocument') is ok.")]
        public override void WriteDocType(string name, string pubid, string sysid, string subset)
        {
            if (_state != WriteState.Prolog && _state != WriteState.Start)
            {
                throw new InvalidOperationException(
                    "Writer is not in the Start or Prolog state, or root node is not an XmlDocument object");
            }
            if (_owner.DocumentType != null)
            {
                _owner.RemoveChild(_owner.DocumentType);
            }
            _owner.XmlResolver = null;
            _current.AppendChild(_owner.CreateDocumentType(name, pubid, sysid, subset));
            _state = WriteState.Prolog;
        }

        /// <summary>
        ///   Closes the previous WriteStartAttribute call.
        /// </summary>
        public override void WriteEndAttribute()
        {
            if (_state != WriteState.Attribute)
            {
                throw new InvalidOperationException("Writer is not in the Attribute state");
            }
            _state = WriteState.Element;
        }

        /// <summary>
        ///   Closes any open elements and puts the writer back in the Start state.
        /// </summary>
        public override void WriteEndDocument()
        {
            _current = _root;
            _state = WriteState.Start;
        }

        /// <summary>
        ///   Closes the previous WriteStartElement call.
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA2204", Justification = "Spelling (Method name) is ok.")]
        public override void WriteEndElement()
        {
            if (_current == _root)
            {
                throw new InvalidOperationException("Too many WriteEndElement calls have been made");
            }
            _current = _current.ParentNode;
            _state = WriteState.Content;
        }

        /// <summary>
        ///   Creates a System.Xml.XmlEntityReference node.
        /// </summary>
        /// <param name = "name">The name of the entity reference</param>
        public override void WriteEntityRef(string name)
        {
            if (_state == WriteState.Element)
            {
                _state = WriteState.Content;
            }
            XmlNode n = _current;
            if (_state == WriteState.Attribute)
            {
                n = _ca;
            }
            else if (_state != WriteState.Content)
            {
                throw new InvalidOperationException("Invalid state '" + WriteState + "' for entity reference");
            }
            n.AppendChild(_owner.CreateEntityReference(name));
        }

        /// <summary>
        ///   The DOM does not preserve this information, so this is equivalent to WriteEndElement.
        /// </summary>
        public override void WriteFullEndElement()
        {
            WriteEndElement();
        }

        /// <summary>
        ///   Calls WriteString if the name is a valid XML name.
        /// </summary>
        public override void WriteName(string name)
        {
            WriteString(XmlConvert.VerifyName(name));
        }

        /// <summary>
        ///   Calls WriteString if the name is a valid XML NMTOKEN.
        /// </summary>
        public override void WriteNmToken(string name)
        {
            // NmToken is the same as NcName, except it doesn't restrict first character.
            string temp = XmlConvert.VerifyName("a" + name);
            WriteString(temp.Substring(1));
        }

        /// <summary>
        ///   Creates a System.Xml.XmlProcessingInstruction node.
        /// </summary>
        public override void WriteProcessingInstruction(string name, string text)
        {
            if (_state == WriteState.Attribute || _state == WriteState.Element)
            {
                _state = WriteState.Content;
            }
            if (_state != WriteState.Content && _state != WriteState.Prolog && _state != WriteState.Start)
            {
                throw new InvalidOperationException("Writer is in the state '" + WriteState +
                                                    "' which is not valid for writing processing instructions");
            }
            if (name == "xml")
            {
                XmlDocument doc2 = new XmlDocument();
                doc2.InnerXml = "<?xml " + text + "?><root/>";
                _current.AppendChild(_owner.ImportNode(doc2.FirstChild, true));
            }
            else
            {
                _current.AppendChild(_owner.CreateProcessingInstruction(name, text));
            }
            if (_state == WriteState.Start)
            {
                _state = WriteState.Prolog;
            }
        }

        /// <summary>
        ///   Looks up the prefix in scope for the given namespace and calls WriteString
        ///   with the prefix+":"+localName (or just localName if the prefix is the empty string).
        /// </summary>
        [SuppressMessage("Microsoft.Performance", "CA1820", Justification = "I don't want string.IsNullOrEmpty")]
        public override void WriteQualifiedName(string localName, string ns)
        {
            string prefix = LookupPrefix(ns);
            if (prefix == null)
            {
                throw new InvalidOperationException("Namespace '" + ns + "' is not in scope");
            }
            if (prefix == string.Empty)
            {
                WriteString(localName);
            }
            else
            {
                WriteString(prefix + ":" + localName);
            }
        }

        /// <summary>
        ///   WriteRaw writes out the given string "unescaped", in other words it better be well formed XML markup.
        ///   So for the XmlNodeWriter we parse this string and build the resulting tree, so it maps to setting the
        ///   InnerXml property.
        /// </summary>
        /// <param name = "data"></param>
        public override void WriteRaw(string data)
        {
            data.ValidateIsNotDefault("data");

            if (data.IndexOf("<", StringComparison.OrdinalIgnoreCase) < 0)
            {
                WriteString(data);
                return;
            }

            switch (_state)
            {
                case WriteState.Start:
                    goto case WriteState.Content;
                case WriteState.Prolog:
                    goto case WriteState.Content;
                case WriteState.Element:
                    _state = WriteState.Content;
                    goto case WriteState.Content;
                case WriteState.Attribute:
                    {
                        ArrayList saved = new ArrayList();
                        if (_ca.HasChildNodes)
                        {
                            while (_ca.FirstChild != null)
                            {
                                saved.Add(_ca.FirstChild);
                                _ca.RemoveChild(_ca.FirstChild);
                            }
                        }
                        _ca.InnerXml = data;
                        for (int i = saved.Count - 1; i >= 0; i--)
                        {
                            _ca.PrependChild((XmlNode) saved[i]);
                        }
                    }
                    break;
                case WriteState.Content:
                    {
                        ArrayList saved = new ArrayList();
                        if (_current.HasChildNodes)
                        {
                            while (_current.FirstChild != null)
                            {
                                saved.Add(_current.FirstChild);
                                _current.RemoveChild(_current.FirstChild);
                            }
                        }
                        _current.InnerXml = data;
                        for (int i = saved.Count - 1; i >= 0; i--)
                        {
                            _current.PrependChild((XmlNode) saved[i]);
                        }
                        _state = WriteState.Content;
                    }
                    break;
                case WriteState.Closed:
                    throw new InvalidOperationException("Writer is closed");
            }
        }

        /// <summary>
        ///   Calls WriteRaw(string) with new string(buffer, index, count)
        /// </summary>
        public override void WriteRaw(char[] buffer, int index, int count)
        {
            WriteRaw(new string(buffer, index, count));
        }

        /// <summary>
        ///   Creates a System.Xml.XmlAttribute node.
        /// </summary>
        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            if ((prefix == _xmlns))
            {
                if (ns == null)
                {
                    ns = _xmlnsURI;
                }
            }
            else if ((string.IsNullOrEmpty(prefix) && (localName == _xmlns)))
            {
                if (ns == null)
                {
                    ns = _xmlnsURI;
                }
                prefix = "";
            }
            else if (prefix == null && ns != null && ns.Length > 0)
            {
                prefix = LookupPrefix(ns);
            }
            if (_state == WriteState.Attribute)
            {
                _state = WriteState.Element;
            }
            if (_state != WriteState.Element)
            {
                throw new InvalidOperationException("Writer is not in a start tag, so it cannot write attributes.");
            }

            _ca = _owner.CreateAttribute(prefix, localName, ns);
            _current.Attributes.Append(_ca);
            _state = WriteState.Attribute;
        }

        /// <summary>
        ///   Writes the XmlDeclaration node with a standalone attribute.  This is only allowed when the
        ///   writer is in the Start state, which only happens if the writer was constructed with an
        ///   XmlDocument object.
        /// </summary>
        [SuppressMessage("Microsoft.Naming", "CA2204", Justification = "Spelling.")]
        public override void WriteStartDocument()
        {
            if (_state != WriteState.Start)
            {
                throw new InvalidOperationException(
                    "Writer is not in the Start state or root node is not an XmlDocument object");
            }
            _current.AppendChild(_owner.CreateXmlDeclaration("1.0", null, null));
            _state = WriteState.Prolog;
        }

        /// <summary>
        ///   Writes the XmlDeclaration node with a standalone attribute.  This is only allowed when the
        ///   writer is in the Start state, which only happens if the writer was constructed with an
        ///   XmlDocument object.
        /// </summary>
        /// <param name = "standalone">If true, standalone attribute has value "yes" otherwise it has the value "no".</param>
        [SuppressMessage("Microsoft.Naming", "CA2204", Justification = "Spelling ('XmlDocument') is ok.")]
        public override void WriteStartDocument(bool standalone)
        {
            if (_state != WriteState.Start)
            {
                throw new InvalidOperationException(
                    "Writer is not in the Start state or root node is not an XmlDocument object");
            }
            _current.AppendChild(_owner.CreateXmlDeclaration("1.0", null, standalone ? "yes" : "no"));
            _state = WriteState.Prolog;
        }

        /// <summary>
        ///   Creates a System.Xml.XmlElement node.
        /// </summary>
        public override void WriteStartElement(string prefix, string localName, string ns)
        {
            if (_state == WriteState.Attribute || _state == WriteState.Element || _state == WriteState.Start ||
                _state == WriteState.Prolog)
            {
                _state = WriteState.Content;
            }
            if (_state != WriteState.Content)
            {
                throw new InvalidOperationException("Writer is in the wrong state for writing element content");
            }

            if (prefix == null && !string.IsNullOrEmpty(ns))
            {
                prefix = LookupPrefix(ns);
            }
            XmlElement e = _owner.CreateElement(prefix, localName, ns);
            _current.AppendChild(e);
            _current = e; // push this element on the stack so to speak.
            _state = WriteState.Element;
        }

        /// <summary>
        ///   Creates a System.Xml.XmlText node.  If the current node is already an XmlText
        ///   node it appends the text to that node.
        /// </summary>
        public override void WriteString(string text)
        {
            XmlNode parent = _current;
            if (_state == WriteState.Attribute)
            {
                parent = _ca;
            }
            else if (_state == WriteState.Element)
            {
                _state = WriteState.Content;
            }
            if (_state != WriteState.Attribute && _state != WriteState.Content)
            {
                throw new InvalidOperationException("Writer is in the wrong state to be writing text content");
            }

            XmlNode last = parent.LastChild;
            if (last == null || !(last is XmlText))
            {
                last = _owner.CreateTextNode(text);
                parent.AppendChild(last);
            }
            else
            {
                XmlText t = last as XmlText;
                t.AppendData(text);
            }
        }

        /// <summary>
        ///   Calls WriteString with the character data.
        /// </summary>
        public override void WriteSurrogateCharEntity(char lowChar, char highChar)
        {
            WriteString(new string(new[] {lowChar, highChar}));
        }

        /// <summary>
        ///   Create a System.Xml.XmlWhitespace node.
        /// </summary>
        public override void WriteWhitespace(string ws)
        {
            if (_state == WriteState.Attribute || _state == WriteState.Element)
            {
                _state = WriteState.Content;
            }
            if (_state != WriteState.Content && _state != WriteState.Prolog && _state != WriteState.Start)
            {
                throw new InvalidOperationException("Writer is not in the right state to be writing whitespace nodes");
            }
            _current.AppendChild(_owner.CreateWhitespace(ws));
            if (_state == WriteState.Start)
            {
                _state = WriteState.Prolog;
            }
        }
    }


}


