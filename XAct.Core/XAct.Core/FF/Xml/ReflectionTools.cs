//using System;
//using System.Collections.Specialized;
//using System.Collections.Generic;
//using System.Xml;
//using System.Reflection;
//using System.Configuration;

//using XAct.Core.Utils;

//namespace XAct.Data.Utils {
//  /// <summary>
//  /// 
//  /// </summary>
//  internal class ReflectionTools {

//    #region Constants
//    private const string C_ATTRIBUTES_NAME = "Attributes";
//    #endregion


//    #region Public Methods

//    /// <summary>
//    /// Sets the passed instance's public properties with 
//    /// the values of the passed xml node's attributes.
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// Note:<br/>
//    /// The matching of attribute to property name is case-insensitive.
//    /// </para>
//    /// <para>
//    /// <b>Attribute parsing:</b>
//    /// The attributes can be in the form of <c>fieldName="..."</c>, or even more 
//    /// complex, in the form of <c>db.FieldName="..."</c>.
//    /// <br/>
//    /// In the case of the later, it will scan the <c>objectInstance</c> for a sub object called <c>db</c>,
//    /// and set <c>db</c>'s <c>FieldName</c> property. In other words:
//    /// <br/>
//    /// <code>
//    /// <![CDATA[
//    /// <nodeToParse db.fieldName="...";
//    /// ]]>
//    /// </code>
//    /// will set:
//    /// <code>
//    /// o.Db.FieldName = "...";
//    /// </code>
//    /// </para>
//    /// <para>
//    /// <b>ChildNode parsing:</b><br/>
//    /// <code>
//    /// <![CDATA[
//    /// <nodeToParse>
//    ///   <db fieldName="..."/>
//    /// </nodeToParse>
//    /// ]]>
//    /// </code>
//    /// will do the equivalent of the following:
//    /// <code>
//    /// o.Db.FieldName = "...";
//    /// </code>
//    /// </para>
//    /// <para>
//    /// <b>The (Optional) Attributes bag:</b><br/>
//    /// In addition, if the object has a NameValueCollection property called 'Attributes' (C_ATTRIBUTES_NAME) it will
//    /// be filled as well. In other words:
//    /// <code>
//    /// <![CDATA[
//    /// <nodeToParse db.fieldName="...";
//    /// ]]>
//    /// </code>
//    /// will set:
//    /// <code>
//    /// o.Db.FieldName = "...";
//    /// </code>
//    /// and will do the following:
//    /// <code>
//    /// o.Db.Attributes['fieldName'] = "...";
//    /// </code>
//    /// but will not do this:
//    /// <code>
//    /// o.Attributes['db.fieldName'] = "...";
//    /// </code>
//    /// </para>
//    /// <para>
//    /// Note:<br/>
//    /// If <c>optionalAttributesTag</c> is not set, it will default to looking for a public
//    /// <c>NameValueCollection</c> typed property called 'Attributes'.
//    /// </para>
//    /// </remarks>
//    /// <param name="objectInstance">The object instance.</param>
//    /// <param name="node">The node to process.</param>
//    /// <param name="optionalAttributesTag">The name of a NameValueCollection property of the passed object in which to place string node attribute name/values before they are processed.</param>
//    /// <exception cref="System.Exception">An error is thrown if objectInstance is null.</exception>
//    /// <exception cref="System.Exception">An error is thrown if node is null.</exception>
//    public static void InitializeInstanceProperties(object objectInstance, XmlElement node, string optionalAttributesTag) {
//      InitializeInstanceProperties(objectInstance, node, optionalAttributesTag, null);
//    }

//    /// <summary>
//    /// Sets the passed instance's public properties with 
//    /// the values of the passed xml node's attributes.
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// Note:<br/>
//    /// The matching of attribute to property name is case-insensitive.
//    /// </para>
//    /// <para>
//    /// <b>Attribute parsing:</b>
//    /// The attributes can be in the form of <c>fieldName="..."</c>, or even more 
//    /// complex, in the form of <c>db.FieldName="..."</c>.
//    /// <br/>
//    /// In the case of the later, it will scan the <c>objectInstance</c> for a sub object called <c>db</c>,
//    /// and set <c>db</c>'s <c>FieldName</c> property. In other words:
//    /// <br/>
//    /// <code>
//    /// <![CDATA[
//    /// <nodeToParse db.fieldName="...";
//    /// ]]>
//    /// </code>
//    /// will set:
//    /// <code>
//    /// o.Db.FieldName = "...";
//    /// </code>
//    /// </para>
//    /// <para>
//    /// <b>ChildNode parsing:</b><br/>
//    /// <code>
//    /// <![CDATA[
//    /// <nodeToParse>
//    ///   <db fieldName="..."/>
//    /// </nodeToParse>
//    /// ]]>
//    /// </code>
//    /// will do the equivalent of the following:
//    /// <code>
//    /// o.Db.FieldName = "...";
//    /// </code>
//    /// </para>
//    /// <para>
//    /// <b>The (Optional) Attributes bag:</b><br/>
//    /// In addition, if the object has a NameValueCollection property called 'Attributes' (C_ATTRIBUTES_NAME) it will
//    /// be filled as well. In other words:
//    /// <code>
//    /// <![CDATA[
//    /// <nodeToParse db.fieldName="...";
//    /// ]]>
//    /// </code>
//    /// will set:
//    /// <code>
//    /// o.Db.FieldName = "...";
//    /// </code>
//    /// and will do the following:
//    /// <code>
//    /// o.Db.Attributes['fieldName'] = "...";
//    /// </code>
//    /// but will not do this:
//    /// <code>
//    /// o.Attributes['db.fieldName'] = "...";
//    /// </code>
//    /// </para>
//    /// <para>
//    /// If a property cannot be found, it will attempt to find a property called 'Attributes'
//    /// to set.
//    /// </para>
//    /// <para>
//    /// Note:<br/>
//    /// If <c>optionalAttributesTag</c> is not set, it will default to looking for a public
//    /// <c>NameValueCollection</c> typed property called 'Attributes'.
//    /// </para>
//    /// </remarks>
//    /// <param name="objectInstance">The object instance.</param>
//    /// <param name="node">The node to process.</param>
//    /// <param name="optionalAttributesTag">The name of a NameValueCollection property of the passed object in which to place string node attribute name/values before they are processed.</param>
//    /// <param name="propertiesSet">Result list of names of the properties that were set.</param>
//    /// <exception cref="System.Exception">An error is thrown if objectInstance is null.</exception>
//    /// <exception cref="System.Exception">An error is thrown if node is null.</exception>
//    public static void InitializeInstanceProperties(object objectInstance, XmlElement node, string optionalAttributesTag, List<string> propertiesSet) {
//      if (objectInstance == null) {
//        throw new System.ArgumentNullException("::objectInstance");
//      }
//      if (node == null) {
//        throw new System.ArgumentNullException("::node");
//      }

//      //Set Default Vars:
//      if (string.IsNullOrEmpty(optionalAttributesTag)) {
//        optionalAttributesTag = C_ATTRIBUTES_NAME;
//      }

//      if (propertiesSet == null) {
//        propertiesSet = new List<string>();
//      }

//      //Get the Type of the object whose properties
//      //we are trying to set (eg: T:DataObjectSchema)
//      System.Type objectType = objectInstance.GetType();

//      //The attributes based solution is called last, and therefore
//      //overrides any values set by the first technique:
//      InitializeInstancePropertiesFromChildNodes(objectInstance, node, optionalAttributesTag, propertiesSet);
//      InitializeInstancePropertiesFromAttributes(objectInstance, node, optionalAttributesTag, propertiesSet);

      
//    }


//    /// <summary>
//    /// Initializes an object's properties from the xmlElement's attribute values.
//    /// </summary>
//    /// <param name="objectInstance">The object instance.</param>
//    /// <param name="node">The xmlElement.</param>
//    /// <param name="optionalAttributesTag">The optional attributes tag.</param>
//    public static void InitializeInstancePropertiesFromAttributes(object objectInstance, XmlElement node, string optionalAttributesTag) {
//      InitializeInstancePropertiesFromAttributes(objectInstance, node, optionalAttributesTag, null);
//    }

//    /// <summary>
//    /// Sets the properties of the passed object instance from the values 
//    /// of the passed node's attributes.
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// If a property cannot be found, it will attempt to find a property called 'Attributes'
//    /// to set.
//    /// </para>
//    /// <para>
//    /// Note:<br/>
//    /// If <c>optionalAttributesTag</c> is not set, it will default to looking for a public
//    /// <c>NameValueCollection</c> typed property called 'Attributes'.
//    /// </para>
//    /// </remarks>
//    /// <param name="objectInstance">The object instance.</param>
//    /// <param name="node">The node to process.</param>
//    /// <param name="optionalAttributesTag">The name of a NameValueCollection property of the passed object in which to place string node attribute name/values before they are processed.</param>
//    /// <param name="propertiesSet">Result list of names of the properties that were set.</param>
//    /// <exception cref="System.Exception">An error is thrown if objectInstance is null.</exception>
//    /// <exception cref="System.Exception">An error is thrown if node is null.</exception>
//    public static void InitializeInstancePropertiesFromAttributes(object objectInstance, XmlElement node, string optionalAttributesTag, List<string> propertiesSet) {
//      if (objectInstance == null) {
//        throw new System.ArgumentNullException("objectInstance");
//      }
//      if (node == null) {
//        throw new System.ArgumentNullException("::node");
//      }

//      if (node == null) {
//        //Get out early:
//        return;
//      }

//      if (propertiesSet == null) {
//        propertiesSet = new List<string>();
//      }


//      //Set Default Vars:
//      if (string.IsNullOrEmpty(optionalAttributesTag)) {
//        optionalAttributesTag = C_ATTRIBUTES_NAME;
//      }

//      //Get the Type of the object whose properties
//      //we are trying to set (eg: T:DataObjectSchema)
//      System.Type objectType = objectInstance.GetType();

//      //Create a ref to the Attributes collection of this object.
//      //(It may or may not exist):
//      //Ie: we are looking for a property called 
//      //'DataObjectSchema.Attributes'
//      //Which should be a NameValueCollection of some kind.
//      PropertyInfo attributesPropertyInfo = objectType.GetProperty(optionalAttributesTag, BindingFlags.Instance | BindingFlags.Public | BindingFlags.IgnoreCase);
//      NameValueCollection attributeBag = (attributesPropertyInfo != null) ? (NameValueCollection)attributesPropertyInfo.GetValue(objectInstance, null) : null;

//      //Now that we have found/not found the attribute bag,
//      //Loop through all attributes of the node:
//      foreach (XmlAttribute attribute in node.Attributes) {
        
//        //Get the Property to set's Name and value:
//        //Note that propertyName may be of type
//        //'simple=...' or 'subObject.complexPropertyName=...'
//        string propertyName = attribute.Name;
//        string propertyValue = attribute.Value;

//        //If we found a storeall place to put this key/value,
//        //do it now, before processing:
//        if (attributeBag != null) {
//          attributeBag.Add(propertyName, propertyValue);
//        }

//        //Now set the property if you can:
//        //Note that SetProperty will recurse if it has to
//        //depending on the number of dots within propertyName:
//        bool wasSet = SetProperty(objectInstance, propertyName, propertyValue);

//        if (wasSet) {
//          //may have been property of objectInstance, 
//          //or a subObject of objectInstance:
//          propertiesSet.Add(propertyName);
//        }

//      }//Loop:End 

//      InitializeII(objectInstance, node, propertiesSet);


//    }


//    /// <summary>
//    /// Sets the object instance's properties from child nodes of the passed xml node.
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// Generally intended to be invoked by <see cref="M:InitializeInstancePropertiesFromAttributes"/>.
//    /// </para>
//    /// <para>
//    /// Note:<br/>
//    /// If <c>optionalAttributesTag</c> is not set, it will default to looking for a public
//    /// <c>NameValueCollection</c> typed property called 'Attributes'.
//    /// </para>
//    /// </remarks>
//    public static void InitializeInstancePropertiesFromChildNodes(object objectInstance, XmlElement node, string optionalAttributesTag) {
//      InitializeInstancePropertiesFromChildNodes(objectInstance, node, optionalAttributesTag, null);
//    }

//    /// <summary>
//    /// Sets the element properties from child nodes.
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// Generally intended to be invoked by <see cref="M:InitializeInstancePropertiesFromAttributes"/>.
//    /// </para>
//    /// <para>
//    /// Note:<br/>
//    /// If <c>optionalAttributesTag</c> is not set, it will default to looking for a public
//    /// <c>NameValueCollection</c> typed property called 'Attributes'.
//    /// </para>
//    /// </remarks>
//    /// <param name="objectInstance">The object instance.</param>
//    /// <param name="node">The node to process.</param>
//    /// <param name="optionalAttributesTag">The name of a NameValueCollection property of the passed object in which to place string node attribute name/values before they are processed.</param>
//    /// <param name="propertiesSet">Result list of names of the properties that were set.</param>
//    public static void InitializeInstancePropertiesFromChildNodes(object objectInstance, XmlElement node, string optionalAttributesTag, List<string> propertiesSet) {
//      if (objectInstance == null) {
//        throw new System.ArgumentNullException("objectInstance");
//      }

//      if (node == null) {
//        //Get out early:
//        return;
//      }

//      if (propertiesSet == null) {
//        propertiesSet = new List<string>();
//      }

//      //Get the Type of the object whose properties
//      //we are trying to set (eg: T:DataObjectSchema)
//      System.Type objectType = objectInstance.GetType();

//      //Loop through child Nodes:
//      foreach (XmlNode childNode in node.ChildNodes) {
//        XmlElement child = childNode as XmlElement;
//        if (child == null) {
//          continue;
//        }

//        //Get the name of the property from the Node's tag:
//        string propertyName = child.Name;

//        //Is there a subObject by that name:
//        object subObject = GetPropertyValue(objectInstance, propertyName, false);

//        if (subObject == null) {
//          //No...loop to next childNode:
//          continue;
//        }

//        //If one was found then
//        //we have to recurse deeper,
//        //with the child node we found.

//        //What we do is actually use the subObject, and childNode
//        //as the basis for a fresh call to the top method (InitializeInstanceProperties),
//        //which restarts the whole process again...
//        InitializeInstanceProperties(subObject, child, optionalAttributesTag, propertiesSet);

//      }//Loop:End
//      //Done.
//    }


//    /// <summary>
//    /// Instantiate an object's properties 
//    /// from an XmlElement's attributes.
//    /// </summary>
//    /// <param name="objectInstance">object to hydrate.</param>
//    /// <param name="node">XmlElement to scan for values.</param>
//    /// <param name="propertiesSet">(Optional) list of properties already been set.</param>
//    public static void InitializeII(object objectInstance, XmlElement node, List<string> propertiesSet) {
//      //Check Args:
//      if (objectInstance == null) {
//        throw new System.ArgumentNullException("objectInstance");
//      }
//      //Initialize:
//      if (propertiesSet == null) {
//        propertiesSet = new List<string>();
//      }
//      //Get type of instance that is being hydrated:
//      Type objectType = objectInstance.GetType();

//      bool wasSet = false;

//      //Loop through each property, looking
//      //for properties that are marked with ConfigurationPropertyAttribute.
//      //Check for required fields:
//      foreach (PropertyInfo propertyInfo in objectType.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)) {
//        if (!propertyInfo.CanWrite) {
//          continue;
//        }
//        object[] propertyAttributes = propertyInfo.GetCustomAttributes(typeof(ConfigurationPropertyAttribute), false);
//        if ((propertyAttributes == null) || (propertyAttributes.Length == 0)) {
//          //No attirbutes, so ignore:
//          continue;
//        }
//        //Have one, convert to typed version for easier usage:
//        ConfigurationPropertyAttribute configAttribute =
//          propertyAttributes[0] as ConfigurationPropertyAttribute;


//        if (propertiesSet.Contains(propertyInfo.Name)) {
//          //Already been set.
//        }

//        //Was never set:
//        //This means that there was no Property that had a name
//        //equal to the attribute name (not the configAttribute).
//        //BUT... ConfigurationPropertyAttribute decorated properties
//        //can be associated an attribute with a different name...
//        //So we have to look for it that way:
//        XmlAttribute a;
//        string propertyName;
//        string propertyValue;


//        propertyName = configAttribute.Name;

//        //Value comes from Attributes (not child elements):
//        a = node.Attributes[propertyName];

//        if (a != null) {
//          //Found an attribute by the ConfigurationPropertyAttribute's Name
//          //Get its string value, and set the same way we did above:
//          propertyValue = a.Value;
//          if (propertyInfo.Name == "ParentSchemaName")
//          {
//              System.Diagnostics.Debug.WriteLine("...");
//          }
//          wasSet = SetProperty(objectInstance, propertyInfo.Name, propertyValue);
//          if (wasSet) {
//            propertiesSet.Add(propertyName); //using ConfigurationPropertyAttribute's Name
//            //Since set, IsRequired/DefaultValue checks no longer apply:
//            continue;
//          }
//        }


//        if (configAttribute.IsRequired) {
//          //but required, so raise flag.
//          throw new System.ArgumentException(
//            string.Format(
//              "A required property ('{0}') of ('{1}') was not set. Use the attribute '{2}' in the schema.xml file.(Location:{3})",
//              propertyInfo.Name,
//              objectType.Name,
//              configAttribute.Name,
//              node.ParentNode.InnerXml));
//        }
//        if ((configAttribute.DefaultValue != null) && (configAttribute.DefaultValue.GetType() != typeof(object))) {
//          object o = configAttribute.DefaultValue;

//          System.Type t = o.GetType();
//          //not required, but has default value,
//          //which we can use to set the property.
//          //Obviously, if not of right type, will throw error:
//          propertyInfo.SetValue(objectInstance, configAttribute.DefaultValue, null);
//        }
//      }//End:Loop

//    }

//    #endregion


//    #region Private Methods


    
    


//    /// <summary>
//    /// <para>
//    /// Sets the passed object's property/field with the given name, by the given value. 
//    /// </para>
//    /// See Remarks.
//    /// </summary>
//    /// <remarks>
//    /// <para>
//    /// If the name has a '.' in it, recurses deeper into the object to find it.
//    /// Therefore the following attribute is valid:
//    /// <code>
//    /// <![CDATA[
//    /// <Note form.backColor="Black".../>
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// </remarks>
//    /// <param name="objectInstance">The objectInstance.</param>
//    /// <param name="propertyName">Name of the property.</param>
//    /// <param name="propertyValue">The property value.</param>
//    /// <returns>Returns true if value set.</returns>
//    private static bool SetProperty(object objectInstance, string propertyName, string propertyValue) {
//      if (objectInstance == null) {
//        return false;
//      }
//      System.Type objectType = objectInstance.GetType();

//      if (propertyName.IndexOf('.') > -1) {
//        //this is a two part name (something like 'form.subme.IsVisible="..."':
//        string keyLeft = propertyName.Substring(0, propertyName.IndexOf('.')); //eg: "form"
//        string keyRight = propertyName.Substring(keyLeft.Length + 1); //eg: "subme.IsVisible"
//        object subProperty = GetPropertyValue(objectInstance, keyLeft,false);
//        if (subProperty != null) {
//          //eg: if have "form", recurse deeper with 
//          //form object, and "subme.IsVisible", and same value,
//          //and see if it recurse down deeper till it finally gets to
//          //the 'subme' instance, etc...
//          return SetProperty(subProperty, keyRight, propertyValue);
//        }
//      }

//      //We have recursed down till we finally 
//      //have a propertyName that doesn't have a '.' in it:
//      MemberInfo memberInfo;
        

//      if (!FindPropertyInfo(objectType, propertyName, out memberInfo,false)) {
//        //But it doesn't exist as a property unfortunately:
//        //Get out:
//        return false;
//      }

//      //It exists...can we set it?
//      if (memberInfo is PropertyInfo) {
//        if (((PropertyInfo)memberInfo).CanWrite) {
//          object propVal = ConvertFromString(propertyValue, ((PropertyInfo)memberInfo).PropertyType);
//          try {
//            ((PropertyInfo)memberInfo).SetValue(objectInstance, propVal, null);
//          }
//          catch (System.Exception E) {
//            System.Diagnostics.Trace.Assert(false, E.Message);
//          }
//        }
//      }
//      else {
//        object propVal = ConvertFromString(propertyValue, ((FieldInfo)memberInfo).FieldType);
//        ((FieldInfo)memberInfo).SetValue(objectInstance, propertyValue);
//      }
//      return true;
//    }


//         /// <summary>
//         /// Finds the PropertyInfo or FieldInfo that matches the name given.
//         /// </summary>
//         /// <remarks>
//         /// <para>
//         /// The search is case-insensitive.
//         /// </para>
//         /// <para>
//         /// Note tha if a PropertyInfo and a FieldInfo both have the same name, 
//         /// the PropertyInfo will be returned.
//         /// </para>
//         /// WARNING: Requires more rights.
//         /// </remarks>
//         /// <param name="type">The type.</param>
//         /// <param name="propertyName">Name of the property.</param>
//         /// <param name="resultInfo">The result info.</param>
//         /// <param name="includePrivateFields">Include search within private fields.WARNING: Requires more rights.</param>
//         /// <returns></returns>
//         private static bool FindPropertyInfo(System.Type type, string propertyName, out MemberInfo resultInfo, bool includePrivateFields) {
//             if (type == null) { throw new System.ArgumentNullException("::type"); }
//             if (string.IsNullOrEmpty(propertyName)) { throw new System.ArgumentNullException("::propertyName"); }
 
//             resultInfo = null;
 
//             PropertyInfo propertyInfo;
//             FieldInfo fieldInfo;
             
//             BindingFlags scope = 
//               (
//                BindingFlags.Instance | 
//                              BindingFlags.Public |
//                              BindingFlags.NonPublic |  //Added this for iSight so that it could set Xact.Data.Schemas.RelationshipSchema.ParentSchemaName -- but pretty sure this will cause me to require higher CAS rights....Check later.
//                              BindingFlags.IgnoreCase
//                              );
 
 
//             //Can we find a public property of this object that has the same name?
//             propertyInfo = type.GetProperty(propertyName, scope);
 
//             if (propertyInfo != null) {
//                 resultInfo = propertyInfo;
//                 return true;
//             }

//                        //Look for a field of same name:
//                        fieldInfo = type.GetField(propertyName, scope);
//                        if (fieldInfo != null) {
//                                resultInfo = fieldInfo;
//                                return true;
//                        }
             
//                        //Look for a field of same name (don't forget prefix '_'):
//                        fieldInfo = type.GetField("_" + propertyName, scope);
//                        if (fieldInfo != null) {
//                                resultInfo = fieldInfo;
//                                return true;
//                        }
             
             
//             if (!includePrivateFields){
//              return false;
//             }
             
//                        scope = scope | BindingFlags.NonPublic;
 
 
//             //Can we find a public property of this object that has the same name?
//             propertyInfo = type.GetProperty(propertyName, scope);
//             if (propertyInfo != null) {
//                 resultInfo = propertyInfo;
//                 return true;
//             }

//                        //None found -- maybe it is read only?
//                        fieldInfo = type.GetField(propertyName, scope);
//                        if (fieldInfo != null) {
//                                resultInfo = fieldInfo;
//                                return true;
//                        }
 
//                        //Look for a field of same name (don't forget prefix '_'):
//                        fieldInfo = type.GetField("_" + propertyName, scope);
//                        if (fieldInfo != null) {
//                                resultInfo = fieldInfo;
//                                return true;
//                        }
 
//            return false;
//         }
         


//    /// <summary>
//    /// Gets the object's value, whether it is a field, or property.
//    /// </summary>
//    /// <param name="instantiatedObject">The instantiated object</param>
//    /// <param name="propertyName">Name of the property.</param>
//    /// <param name="throwErrorIfNotFound"></param>
//    /// <returns></returns>
//    private static object GetPropertyValue(object instantiatedObject, string propertyName, bool throwErrorIfNotFound) {
////Check Args:
//      if (instantiatedObject == null) {
//        throw new System.ArgumentNullException("instantiatedObject");
//      }
//      if (string.IsNullOrEmpty(propertyName)) {
//        throw new System.ArgumentNullException("propertyName");
//      }
//      //Get any member of the object by this name 
//      //whether field, property, or method:
//      MemberInfo memberInfo;

//      if (!FindPropertyInfo(instantiatedObject.GetType(), propertyName, out memberInfo, false))
//      {
//          //No member by this name.
//        if (throwErrorIfNotFound) {
//          throw new System.ArgumentException(
//            string.Format(
//            "{0} does not have a member called {1}.",instantiatedObject.GetType().Name,propertyName));
//        }
//        //Or don't throw error, just return null:
//        return null;
//      }
//      //Yes:
//      if (memberInfo is PropertyInfo) {
//        //Extract the property:
//        return ((PropertyInfo)memberInfo).GetValue(instantiatedObject, null);
//      }
//      else if (memberInfo is FieldInfo) {
//        //Extract the Field:
//        return ((FieldInfo)memberInfo).GetValue(instantiatedObject);
//      }
//      else {
//        throw new System.Exception(
//          string.Format(
//          "{0} has a Member called {1}, but it is not a Field or Property.", 
//          instantiatedObject.GetType().Name,
//          propertyName));
//      }
//    }


//    private static object ConvertFromString(string val, System.Type destinationType) {
//      if (destinationType == typeof(Type)) { //special case where the destination type is System.Type
//        //Common short hand:
        
//        if (val.IndexOf(".")==-1) {
//          if (val.ToLower() == "bool") {
//            val = "Boolean";
//          }
//          if (val.ToLower() == "int") {
//            val = "Int32";
//          }
//          if (val.ToLower() == "float") {
//            val = "Single";
//          }
//          val = "System." + val;
//        }
//        try {
//          return Type.GetType(val, true, true);
//        }
//        catch (System.Exception E) {
//          throw E;
//        }
//      }

//      return Types.ConvertTo(val,destinationType);
//    }
//    #endregion


//    /*
//    /// <summary>
//    /// Returns the <see cref="Type"/> from the given Type Name.
//    /// </summary>
//    /// <param name="dataObjectClassTypeName"></param>
//    /// <returns></returns>
//    protected static Type GetTypeFromName(string dataObjectClassTypeName) {
//      //At present we have the ClassName and the dataStoreName, let's move on and 
//      //see if we can actually get the computer to recognize a name of the System.Type
//      //we gave it:
//      Type dataObjectType = Assembly.GetExecutingAssembly().GetType(dataObjectClassTypeName, false, true);
//      //Were we lucky first time?
//      //Or is it in a sub assembly?
//      if (dataObjectType == null) {
//        //Hum. Get all assemblies and go through them asking each one:
//        Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
//        foreach (Assembly assembly in assemblies) {
//          dataObjectType = assembly.GetType(dataObjectClassTypeName);
//          //Found?
//          if (dataObjectType != null) {
//            break;
//          }
//        }
//      }
//      return dataObjectType;
//    }
//    */

//    private MemberInfo GetMemberInfo(object o, string memberName)
//    {
//        Type t = o.GetType();
//        MemberInfo[] members = t.GetMember(memberName);
//        return (members.Length > 0) ? members[0] : null;
//    }


//  }//Class:End
//}//Namespace:End
