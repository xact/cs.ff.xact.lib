#if CONTRACTS_FULL || NET40 || NET45 // Requires .Net 4, so hold for the moment 

#endif

namespace XAct.Xml
{
    using System;
    using System.Xml;
    using System.Xml.Serialization;

    /// <summary>
    ///   Class to help serialize and deserialize objects  
    ///   to Xml format.
    /// </summary>
    public class XmlNodeSerializer
    {
        private readonly XmlSerializer _serializer;

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the <see cref = "T:XAct.Xml.XmlNodeSerializer" /> class.
        /// </summary>
        /// <param name = "destinationType">Type of the destination.</param>
        public XmlNodeSerializer(Type destinationType)
        {
            _serializer = new XmlSerializer(destinationType);
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Serializes the specified obj, returning it 
        ///   as an XmlNode within a new XmlDocument 
        ///   (it can then be cloned to a destination document).
        /// </summary>
        /// <param name = "value">The obj.</param>
        /// <returns>An XmlNode</returns>
        public XmlNode Serialize(object value)
        {
            XmlDocument doc = new XmlDocument();

            using (XmlNodeWriter writer = new XmlNodeWriter(doc))
            {
                _serializer.Serialize(writer, value);
                return doc.DocumentElement;
            }
        }

        /// <summary>
        ///   Deserialize the specified node into a typed object.
        /// </summary>
        /// <param name = "node">The XmlNode to deserialize.</param>
        /// <returns></returns>
        public object Deserialize(XmlNode node)
        {
            using (XmlNodeReader reader = new XmlNodeReader(node))
            {
                return _serializer.Deserialize(reader);
            }
        }

        #endregion
    }


}


