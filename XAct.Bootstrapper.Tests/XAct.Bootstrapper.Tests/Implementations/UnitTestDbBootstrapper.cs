//namespace XAct.Bootstrapper.Tests
//{
//    using System;
//    using System.Data.Entity;
//    using XAct.Diagnostics;
//    using XAct.Services;

//    /// <summary>
//    /// An implementation of the <see cref="IUnitTestDbBootstrapper"/>
//    /// to ensure the creation of a database as required for the resources.
//    /// </summary>
//    public class UnitTestHostSettingsDbBootstrapper : IUnitTestDbBootstrapper
//    {
//        private readonly ITracingService _tracingService;
//        private readonly string _typeName;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="UnitTestHostSettingsDbBootstrapper"/> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        public UnitTestHostSettingsDbBootstrapper(ITracingService tracingService)
//        {
//            _typeName = this.GetType().Name;
//            _tracingService = tracingService;
//        }

//        public void Initialize<TEntity>()
//            where TEntity : class,new()
//        {
//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Begin...", _typeName, "Initialize");

//            //FIX: Initializer has to be same type of DbContext as DbContext we want (using a generic DbContext,
//            //or any DbContext that is not exact same match as DbContext used for first call to Db, will cause
//            //the Seed method to never be called:
//            //Does not work: Database.SetInitializer(_unitTestDatabaseInitializer);

//            UnitTestDropCreateDatabaseAlwaysInitializer<UnitTestDbContext> unitTestDatabaseInitializer =
//                new UnitTestDropCreateDatabaseAlwaysInitializer<UnitTestDbContext>(XAct.DependencyResolver.Current.GetInstance<ITracingService>());

//            Database.SetInitializer<UnitTestDbContext>(unitTestDatabaseInitializer);


//            UnitTestDbContext dbContext = new UnitTestDbContext();


//            DbSet<TEntity> Cases = dbContext.Set<TEntity>();

//            try
//            {
//                Cases.Load();
//            }
//            catch (System.Exception e)
//            {

//                _tracingService.TraceException(TraceLevel.Error, e, "Bootstrap failed initial contact with database.");
//            }

//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Complete.", _typeName, "Initialize");

//        }

//        public void Initialize()
//        {
//            throw new NotImplementedException();
//        }
//    }


//}