﻿using System.Data.Entity.ModelConfiguration;

namespace XAct.Bootstrapper.Tests
{
    using System;
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Initialization;

    /// <summary>
    /// 
    /// </summary>
    public class UnitTestDbContext : DbContext
    {
        protected readonly string _typeName;
        protected readonly ITracingService _tracingService;

        public UnitTestDbContext()
            : base()
        {
            if (System.Configuration.ConfigurationManager.AppSettings[XAct.Library.Constants.AppSettingKeys.EnableDebugger].ToBool())
            {
                if (!System.Diagnostics.Debugger.IsAttached)
                {
                    System.Diagnostics.Debugger.Launch();
                }
            }
            _typeName = this.GetType().Name;


        }

        public UnitTestDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }


        /// <summary>
        /// This method is called when the model for a derived context has been initialized, but
        /// before the model has been locked down and used to initialize the context.  The default
        /// implementation of this method does nothing, but it can be overridden in a derived class
        /// such that the model can be further configured before it is locked down.
        /// </summary>
        /// <param name="modelBuilder">The builder that defines the model for the context being created.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            
            modelBuilder.HasDefaultSchema(XAct.Library.Settings.Db.DefaultXActLibSchema);

            //HACK
            //We search through AppDomain, as UnitTestDbContext is in Test assembly...not the source (
            //which would be the equiv of Infrastructure assembly in an app...
            //this.AutoBuildModels<IDbModelBuilder>(modelBuilder, true, searchDomain: true);

            this.AutoBuildModels(modelBuilder, true, searchDomain: true);

            base.OnModelCreating(modelBuilder);
        }
    }

}
