//namespace XAct.Bootstrapper.Tests
//{
//    using System;
//    using System.Data.Entity;
//    using XAct.Data.EF.CodeFirst;
//    using XAct.Data.EF.CodeFirst.Seeders.Implementations;
//    using XAct.Diagnostics;


//    /// <summary>
//    /// 
//    /// </summary>
//    /// <internal>
//    /// Important: 
//    /// Contrary to most other classes in the library,
//    /// UnitTestDatabaseInitializer is *not* backed by an interface 
//    /// because it must be instantiated using the Type of the DbContext.
//    /// 
//    /// Initializer has to be same type of DbContext as DbContext we want (using a generic DbContext,
//    /// or any DbContext that is not exact same match as DbContext used for first call to Db, will cause
//    /// the Seed method to never be called:
//    /// </internal>
//    /// <typeparam name="TDbContext"></typeparam>
//    public class UnitTestDropCreateDatabaseAlwaysInitializer<TDbContext> : DropCreateDatabaseAlways<TDbContext> //DropCreateDatabaseAlways | DropCreateDatabaseIfModelChanges<TDbContext>
//        where TDbContext : DbContext
//    {
//        private readonly string _typeName;
//        private readonly ITracingService _tracingService;

//        /// <summary>
//        /// Initializes a new instance of the <see cref="UnitTestDropCreateDatabaseAlwaysInitializer{TDbContext}"/> class.
//        /// </summary>
//        /// <param name="tracingService">The tracing service.</param>
//        public UnitTestDropCreateDatabaseAlwaysInitializer(ITracingService tracingService)
//        {
//            _typeName = this.GetType().Name;
//            //Repository settings were used for when not being built using CodeFirst.
//            //Keeping it just in case
//            _tracingService = tracingService;
//        }

//        protected override void Seed(TDbContext dbContext)
//        {

//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Begin...", _typeName,"Seed");

//            //Reminder:
//            //Core DbContext is not the entry point.
//            //Entry is via M01DbContext in this assembly.
//            //Which sets this DatabaseInitializer.
//            //No other DatabaseInitializer should be being called (as they are not the entry point).
//            //Which uses Reflection to find all the IDbContextSeeders (Core First, then Modules.


//            //Between each call to this method
//            //we clear the cache:
//            DbContextSeederContext.ClearCache();


//            //This Db Initializer in turn looks around (via reflection)
//            //for all instances decorated with the IDbContextSeeder interface
//            //in order for them to have a chance to seed the database as required.


//            Type[] seederTypes =
//    AppDomain.CurrentDomain.GetAssemblies()
//               //Get all instantiable -- classes -- implementing the IDbContextSeeder contract:
//             .GetTypesImplementingType<IDbContextSeeder>(true);
//            foreach (var seederType in seederTypes)
//            {
//                var seeder = seederType.ActivateEx<IDbContextSeeder>();

//                seeder.Seed(dbContext);
//            }

//            //The downside of this approach is that it won't find everything. Can't because not every test is referencing
//            //every other test, and therefore they don't all end up in the bin of the current test.

//            base.Seed(dbContext);

//            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}::Complete.", _typeName, "Seed");

//        }


//    }
//}