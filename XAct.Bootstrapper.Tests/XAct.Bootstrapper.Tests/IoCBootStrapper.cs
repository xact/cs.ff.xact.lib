﻿namespace XAct.Bootstrapper.Tests
{
    using XAct.Diagnostics;
    using XAct.Services;
    using XAct.Services.IoC;

    public class IoCBootStrapper
    {
        private static IoCBootStrapper Instance { get { return _instance; }
            set
            {
                _instance = value;

                if (value == null)
                {
                    UnityBootstrapper.Reset();
                }
            }
        }
        private static IoCBootStrapper _instance;

        public static void Initialize(bool forceReinitialization, params IBindingDescriptorBase[] servicesToRegister)
        {
            if (forceReinitialization)
            {
                Instance = null;
            }

            if (Instance == null)
            {
                Instance = new IoCBootStrapper();

                Instance.InitializeInstance(servicesToRegister);

                var report = XAct.DependencyResolver.BindingResults.CreateBindingReport();
            }
        }



        protected void InitializeInstance(params IBindingDescriptorBase[] servicesToRegister)
        {
            UnityBootstrapper.Initialize(
                null,//IoC
                true,
                null,//Precall
                (bindingResult) =>
                    {
                        if (bindingResult.Skipped)
                        {
                            XAct.Shortcuts.Trace(TraceLevel.Verbose, "Binding: {0}".FormatStringInvariantCulture(bindingResult.BindingDescriptor));
                            XAct.Shortcuts.Trace(TraceLevel.Verbose, "Skipped: {0}".FormatStringInvariantCulture(bindingResult.Skipped));
                        }
                        else
                        {
                            if (bindingResult.Exception == null)
                            {
                                //XAct.Shortcuts.Trace(TraceLevel.Verbose, "Binding: {0}".FormatStringInvariantCulture(bindingResult.BindingDescriptor));
                                //XAct.Shortcuts.Trace(TraceLevel.Verbose, "Successfully registered.");
                            }
                            else
                            {
                                XAct.Shortcuts.Trace(TraceLevel.Error,  "Binding: {0}".FormatStringInvariantCulture(bindingResult.BindingDescriptor));
                                XAct.Shortcuts.Trace(TraceLevel.Error, "Exception: {0}".FormatStringInvariantCulture(bindingResult.Exception));
                            }

                        }
                    },
                servicesToRegister);
        }



    }
}
