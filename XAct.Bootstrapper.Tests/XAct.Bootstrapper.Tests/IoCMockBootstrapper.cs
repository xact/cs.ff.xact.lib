﻿namespace XAct.Bootstrapper.Tests
{
    using Microsoft.Practices.Unity;
    using XAct.Services;

    public class IoCMockBootstrapper
    {

        public static IUnityContainer Initialize(IBindingDescriptorBase[] serviceBindingInformation = null)
        {
            IUnityContainer unityContainer = new UnityContainer();// new UnityAutoMoq.UnityAutoMoqContainer();

            //Pass it to the overload that will load it with any :
            XAct.Services.IoC.UnityBootstrapper.Initialize(unityContainer,true, null, null, serviceBindingInformation);

            return unityContainer;
        }

    }
}


