﻿using System;

namespace XAct.Tests
{
    using NUnit.Framework;
    using XAct.Bootstrapper.Tests;

    /// <summary>
    /// Specialized case of <see cref="InitializeDbIoCContextAttribute"/>
    /// used by UnitTests.
    /// <para>
    /// Attribute that can be aplied to UnitTests to indicate
    /// which tests require a a reset of the test database.
    /// </para>
    /// <para>
    /// Has no functionality in itself.
    /// </para>
    /// <para>
    /// But does have a static method that is simply there
    /// for convenient:
    /// </para>
    /// <para>
    /// Usage is generally something akin to:
    /// <code>
    /// <![CDATA[
    ///    [TestFixture]
    ///    public class SomeServiceUnitTests
    ///    {
    ///        [TestFixtureSetUp]
    ///        public void TestFixtureSetUp()
    ///        {
    ///            //run once before any tests in this testfixture have been run...
    ///            InitializeUnitTestDbIoCContextAttribute.BeforeTest();
    ///        }
    ///        [TestSetUp]public void TestFixtureSetUp(){}
    ///        [TearDown]public void MyTestTearDown() {GC.Collect();}
    ///        [TestFixtureTearDown] public void TestFixtureTearDown() {}
    ///    }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public class InitializeUnitTestDbIoCContextAttribute :XAct.Tests.InitializeDbIoCContextAttribute
    {
        const string C_CONNSTR_NAME = "UnitTestDbContext";
        const string C_CONNSTR_NAME_CE = "UnitTestDbContext_CE";

        /// <summary>
        /// Initializes a new instance of the <see cref="InitializeUnitTestDbIoCContextAttribute"/> class.
        /// </summary>
        /// <param name="allowSqlCompactEdition">if set to <c>true</c> [allow SQL compact edition].</param>
        /// <param name="customSeederTypesToRegister">The custom seeder types to register.</param>
        /// <param name="entityTypesToSeed">The entity types to seed.</param>
        /// <param name="sqlServerConnectionStringName">Name of the SQL server connection string.</param>
        /// <param name="sqlServerCEConnectionStringName">Name of the SQL server ce connection string.</param>
        public InitializeUnitTestDbIoCContextAttribute(
            bool allowSqlCompactEdition = true, 
            Type[] customSeederTypesToRegister = null, 
            Type[] entityTypesToSeed = null,
            string sqlServerConnectionStringName = C_CONNSTR_NAME,
            string sqlServerCEConnectionStringName = C_CONNSTR_NAME_CE) :
            base(
                typeof(UnitTestDbContext), 
                allowSqlCompactEdition, 
                customSeederTypesToRegister, 
                entityTypesToSeed,
                sqlServerConnectionStringName,
                sqlServerCEConnectionStringName)

        {
        }

        /// <summary>
        /// Befores the test.
        /// </summary>
        /// <param name="sqlServerConnectionStringName">Name of the SQL server connection string.</param>
        /// <param name="sqlServerCeConnectionStringName">Name of the SQL server ce connection string.</param>
        /// <param name="allowSqlCompactEdition">if set to <c>true</c> [allow SQL compact edition].</param>
        /// <param name="entityTypesToSeed">The entity types to seed.</param>
        /// <param name="seederTypesToRegister">The seeder types to register.</param>
        /// <param name="testDetails">The test details.</param>
        public static void BeforeTest(
            string sqlServerConnectionStringName = C_CONNSTR_NAME,
            string sqlServerCeConnectionStringName = C_CONNSTR_NAME_CE,
            bool allowSqlCompactEdition = true,
            Type[] entityTypesToSeed = null,
            Type[] seederTypesToRegister = null,
            TestDetails testDetails = null
            )
        {

            //Invoke base static method
            //passing the UnitTest specific DbContext:
            BeforeTest(
                typeof (UnitTestDbContext),
                sqlServerConnectionStringName,
                sqlServerCeConnectionStringName,
                allowSqlCompactEdition,
                entityTypesToSeed, 
                seederTypesToRegister, 
                testDetails);
        }

    }
}
