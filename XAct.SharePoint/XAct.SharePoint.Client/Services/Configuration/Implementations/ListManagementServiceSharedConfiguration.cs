namespace XAct.SharePoint.Implementations
{

    /// <summary>
    /// Configuration Package for initializing 
    /// <see cref="IListManagementService"/>
    /// </summary>
    public class ListManagementServiceConfiguration : IListManagementServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the Sharepoint site URL.
        /// <para>
        /// Example: http://MyServer/sites/MySiteCollection
        /// </para>
        /// </summary>
        /// <value>
        /// The site URL.
        /// </value>
        public string SiteUrl { get; set; }  //http://MyServer/sites/MySiteCollection

    }
}