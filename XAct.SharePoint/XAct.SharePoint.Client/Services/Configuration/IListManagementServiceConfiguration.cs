namespace XAct.SharePoint
{
    /// <summary>
    /// Contract for the configuration required by 
    /// <see cref="IListManagementService"/>
    /// </summary>
    public interface IListManagementServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets the Sharepoint site URL.
        /// <para>
        /// Example: http://MyServer/sites/MySiteCollection
        /// </para>
        /// </summary>
        /// <value>
        /// The site URL.
        /// </value>
        string SiteUrl { get; set; }
    }
}