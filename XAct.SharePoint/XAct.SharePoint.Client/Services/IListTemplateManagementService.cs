﻿namespace XAct.SharePoint
{
    using Microsoft.SharePoint.Client;

    /// <summary>
    /// Contract for a service to manage List Templates.
    /// </summary>
    public interface IListTemplateManagementService : IHasXActLibService
    {
        /// <summary>
        /// Gets the specified List Template.
        /// </summary>
        /// <param name="templateName">Name of the template.</param>
        /// <returns></returns>
        ListTemplate Get(string templateName);
    }
}
