namespace XAct.SharePoint
{
    using System.Collections.Generic;
    using Microsoft.SharePoint.Client;
    using XAct.SharePoint;

    /// <summary>
    /// Contract for a Service to manage Sharepoint Lists.
    /// </summary>
    public interface IListManagementService : IHasXActLibService
    {

        /// <summary>
        /// Gets or sets the configuration used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        IListManagementServiceConfiguration Configuration { get; }


        /// <summary>
        /// Gets all lists.
        /// </summary>
        /// <returns></returns>
        IEnumerable<List> GetLists();

        /// <summary>
        /// Gets the lists of the specified type.
        /// </summary>
        /// <param name="listTemplateType">Type of the list template.</param>
        /// <returns></returns>
        IEnumerable<List> GetLists(ListTemplateType listTemplateType);

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="listTitle">The list title.</param>
        /// <returns></returns>
        List GetList(string listTitle);


        /// <summary>
        /// Creates a new list according to the provided definition.
        /// </summary>
        /// <param name="listDefinition">The list definition.</param>
        List CreateList(ListCreationInformation listDefinition);

        /// <summary>
        /// Updates the specified list definition.
        /// </summary>
        /// <param name="listDefinition">The list definition.</param>
        void UpdateList(ListCreationInformation listDefinition);


        /// <summary>
        /// Deletes the list.
        /// </summary>
        /// <param name="listDefinition">The list definition.</param>
        void DeleteList(ListCreationInformation listDefinition);
    }
}