//Microsoft.Office.Server.dll
//Microsoft.SharePoint.dll

namespace XAct.SharePoint.Implementations
{
    using System;
    using System.Collections.Generic;
    using Microsoft.SharePoint.Client;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IListManagementService"/>
    /// to manage SharePoint Lists, using the Client API.
    /// </summary>
    public class ClientListManagementService : IListManagementService
    {
        private readonly ITracingService _tracingService;
        /// <summary>
        /// Gets or sets the configuration used by this service.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IListManagementServiceConfiguration Configuration { get { return _listManagementServiceConfiguration; } }
        private /*readonly*/ IListManagementServiceConfiguration _listManagementServiceConfiguration;
        private bool _configured;


        /// <summary>
        /// Initializes a new instance of the 
        /// <see cref="ClientListManagementService"/> class.
        /// </summary>
        public ClientListManagementService(ITracingService tracingService, IListManagementServiceConfiguration listManagementServiceConfiguration)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            listManagementServiceConfiguration.ValidateIsNotDefault("_listManagementServiceConfiguration");

            _tracingService = tracingService;
            _listManagementServiceConfiguration = listManagementServiceConfiguration;
        }


        /// <summary>
        /// Configures the specified list management service configuration.
        /// </summary>
        /// <param name="listManagementServiceConfiguration">The list management service configuration.</param>
        public void Configure(IListManagementServiceConfiguration listManagementServiceConfiguration)
        {
            if (_configured)
            {
                throw new ArgumentException("Already Configured Once. Use another Service instance.");
            }
            lock (this)
            {
                _listManagementServiceConfiguration = listManagementServiceConfiguration;
                _configured = true;
            }
        }


        /// <summary>
        /// Gets all lists.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<List> GetLists()
        {
            using (ClientContext clientContext = new ClientContext(this._listManagementServiceConfiguration.SiteUrl))
            {
                return clientContext.GetAllLists();
            }
        }

        /// <summary>
        /// Gets the lists of the specified type.
        /// </summary>
        /// <param name="listTemplateType">Type of the list template.</param>
        /// <returns></returns>
        public IEnumerable<List> GetLists(SharePoint.ListTemplateType listTemplateType)
        {
            using (ClientContext clientContext = new ClientContext(this._listManagementServiceConfiguration.SiteUrl))
            {
                return clientContext.GetLists(listTemplateType);
            }
        }

        /// <summary>
        /// Gets the list.
        /// </summary>
        /// <param name="listTitle">The list title.</param>
        /// <returns></returns>
        public List GetList(string listTitle)
        {
            using (ClientContext clientContext = new ClientContext(this._listManagementServiceConfiguration.SiteUrl))
            {
                return clientContext.GetList(listTitle);
            }
        }

        /// <summary>
        /// Creates a new list according to the provided definition.
        /// </summary>
        /// <param name="listDefinition">The list definition.</param>
        public List CreateList(SharePoint.ListCreationInformation listDefinition)
       {
           listDefinition.ValidateIsNotDefault("listDefinition");
           if (listDefinition.ListTemplateType != SharePoint.ListTemplateType.InvalidType)
           {
               throw new ArgumentException("listDefinition.ListTemplateType");
           }

           listDefinition.Name.ValidateIsNotNullOrEmpty("listTitle");

           using (ClientContext clientContext = new ClientContext(this._listManagementServiceConfiguration.SiteUrl))
           {
               return clientContext.CreateList(listDefinition);
           }

       }


        /// <summary>
        /// Updates the specified list definition.
        /// </summary>
        /// <param name="listDefinition">The list definition.</param>
        public void UpdateList(SharePoint.ListCreationInformation listDefinition)
        {

            throw new NotImplementedException();
        }

        /// <summary>
        /// Deletes the list.
        /// </summary>
        /// <param name="listDefinition">The list definition.</param>
        public void DeleteList(SharePoint.ListCreationInformation listDefinition)
        {
            using (ClientContext clientContext = new ClientContext(this._listManagementServiceConfiguration.SiteUrl))
            {
                clientContext.DeleteList(listDefinition.Name);
            }

        }
      
    }
}