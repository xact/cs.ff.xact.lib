﻿namespace XAct.Services.Comm.ServiceModel
{
    using System.ServiceModel;
    using System.Text;

    /// <summary>
    /// 
    /// </summary>
    public class BindingFactory
    {
        /// <summary>
        /// Creates the ws HTTP binding.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        public static WSHttpBinding CreateWsHttpBinding(BindingTimeOutConfiguration configuration)
        {
            // new WSHttpBinding { Security = { Mode = SecurityMode.Message, Transport = { ClientCredentialType = HttpClientCredentialType.Windows } } }
            // todo what should the binding values be here?
            WSHttpBinding binding = new WSHttpBinding{
                                            Security =
                                                {
                                                    Mode = SecurityMode.Message,
                                                    Transport = {ClientCredentialType = HttpClientCredentialType.Windows}
                                                }
                                        };
            

            binding.OpenTimeout = configuration.OpenTimeout;
            binding.SendTimeout = configuration.SendTimeout;
            binding.ReceiveTimeout = configuration.ReceiveTimeout;
            binding.CloseTimeout = configuration.CloseTimeout;

            // The following are default values for a Binding             
            binding.AllowCookies = false;
            binding.BypassProxyOnLocal = false;
            binding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            //binding.MaxBufferSize = 65536;
            binding.MaxBufferPoolSize = 524288;
            binding.MaxReceivedMessageSize = 65536;
            binding.MessageEncoding = WSMessageEncoding.Text;
            binding.TextEncoding = Encoding.UTF8;
            //binding.TransferMode = TransferMode.Buffered;
            binding.UseDefaultWebProxy = true;

            System.Xml.XmlDictionaryReaderQuotas readerQuotas = new System.Xml.XmlDictionaryReaderQuotas();
            readerQuotas.MaxStringContentLength = 8192;
            readerQuotas.MaxArrayLength = 16384;
            readerQuotas.MaxBytesPerRead = 4096;
            readerQuotas.MaxDepth = 32;
            readerQuotas.MaxNameTableCharCount = 16384;

            binding.ReaderQuotas = readerQuotas;
            return binding;
        }

        /// <summary>
        /// Creates the basic HTTP binding.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        public static BasicHttpBinding CreateBasicHttpBinding(BindingTimeOutConfiguration configuration)
        {
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.TransportCredentialOnly);

            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
            
            binding.OpenTimeout = configuration.OpenTimeout;
            binding.SendTimeout = configuration.SendTimeout;
            binding.ReceiveTimeout = configuration.ReceiveTimeout;
            binding.CloseTimeout = configuration.CloseTimeout;

            //// The following are default values for a Binding             
            //binding.AllowCookies = false;
            //binding.BypassProxyOnLocal = false;
            //binding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            //binding.MaxBufferSize = 65536;
            //binding.MaxBufferPoolSize = 524288;
            //binding.MaxReceivedMessageSize = 65536;
            //binding.MessageEncoding = WSMessageEncoding.Text;
            //binding.TextEncoding = Encoding.UTF8;
            //binding.TransferMode = TransferMode.Buffered;
            //binding.UseDefaultWebProxy = true;

            //System.Xml.XmlDictionaryReaderQuotas readerQuotas = new System.Xml.XmlDictionaryReaderQuotas();
            //readerQuotas.MaxStringContentLength = 8192;
            //readerQuotas.MaxArrayLength = 16384;
            //readerQuotas.MaxBytesPerRead = 4096;
            //readerQuotas.MaxDepth = 32;
            //readerQuotas.MaxNameTableCharCount = 16384;

            //binding.ReaderQuotas = readerQuotas;

            return binding;
        }
    }
}
