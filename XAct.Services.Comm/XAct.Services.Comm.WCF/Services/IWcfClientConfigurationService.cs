﻿namespace XAct.Services.Comm.ServiceModel.Services
{
    /// <summary>
    /// Contract for a service
    /// to manage the configuration settings
    /// required to create WCF Session Agents.
    /// <para>
    /// Used by the <see cref="IWcfServiceAgentService"/>.
    /// </para>
    /// </summary>
    public interface IWcfClientConfigurationService : IHasXActLibService
    {
        /// <summary>
        /// Gets the configuration name for client for a particular service contract type from the web.config system.serviceModel configuration section.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <returns></returns>
        string GetConfigNameForClient<TService>();
    }
}
