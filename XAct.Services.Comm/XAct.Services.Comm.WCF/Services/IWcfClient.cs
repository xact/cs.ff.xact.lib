﻿namespace XAct.Services.Comm.ServiceModel.Services.Agents
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TServiceType">The type of the service type.</typeparam>
    public interface IWcfClient<out TServiceType> : IDisposable
    {
        /// <summary>
        /// Gets the proxy object for the service.
        /// Each call is passed through the socket (precious resource). Avoid foreach loops etc.
        /// </summary>
        /// <value>
        /// The proxy.
        /// </value>
        TServiceType Proxy { get; }
    }
}