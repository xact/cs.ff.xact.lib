﻿namespace XAct.Services.Comm.ServiceModel.Services
{
    using System;

    /// <summary>
    /// Contract for a service
    /// to return results cached results of 
    /// previous WCF requests. 
    /// </summary>
    public interface ICachedWcfServiceAgentService : IHasXActLibService
    {

        /// <summary>
        /// Checks for a cached value, and if not found/stale
        /// Connects to the specified service, executes the remote Operation,
        /// caching the result before returning it.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="func">The function to be executed on the service</param>
        /// <param name="cacheKey">The cache key.</param>
        /// <param name="cacheTimeSpan">The cache time span.</param>
        /// <param name="bypassCaching">if set to <c>true</c> [bypass caching].</param>
        /// <param name="forceUpdateOfCache">if set to <c>true</c> [force update of cache].</param>
        /// <returns></returns>
        TResult Query<TService, TResult>(Func<TService, TResult> func, string cacheKey, TimeSpan? cacheTimeSpan = null, bool bypassCaching = false, bool forceUpdateOfCache = false);

    }
}