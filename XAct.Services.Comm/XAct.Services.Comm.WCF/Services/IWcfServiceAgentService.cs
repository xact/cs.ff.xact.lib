﻿namespace XAct.Services.Comm.ServiceModel.Services
{
    using System;

    /// <summary>
    /// Contract for a service to create
    /// Service Agents for remote
    /// WCF Services.
    /// </summary>
    public interface IWcfServiceAgentService : IHasXActLibService
    {
        /// <summary>
        /// Connects to the service of the specified type, executes the function, returns the service call result
        /// and closes the connection.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="func">The function to be executed on the service</param>
        /// <returns></returns>
        TResult Query<TService, TResult>(Func<TService, TResult> func);

        /// <summary>
        /// Connects to the service of the specified type, executes the action and closes the connection.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <param name="action">The action.</param>
        void Query<TService>(Action<TService> action);

    }
}