﻿namespace XAct.Services.Comm.ServiceModel.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.ServiceModel.Configuration;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="IWcfClientConfigurationService"/>
    /// to manage the configuration settings
    /// required to create WCF Session Agents.
    /// <para>
    /// Used by the <see cref="IWcfServiceAgentService"/>.
    /// </para>
    /// </summary>
    public class WcfClientConfigurationService : XActLibServiceBase, IWcfClientConfigurationService
    {
        private IList<ChannelEndpointElement> _endPointConfigs;

        /// <summary>
        /// Initializes a new instance of the <see cref="WcfClientConfigurationService"/> class.
        /// </summary>
        public WcfClientConfigurationService(ITracingService tracingService):base(tracingService)
        {
            LoadEndpointsFromConfig();
        }

        /// <summary>
        /// Loads the endpoints from the web.config.
        /// </summary>
        /// <exception cref="System.Exception">Client secion missing in the configuration file.</exception>
        private void LoadEndpointsFromConfig()
        {
            ClientSection clientSection = ConfigurationManager.GetSection("system.serviceModel/client") as ClientSection;
            if (clientSection == null) throw new Exception("Client secion missing in the configuration file.");

            ChannelEndpointElement[] tmpArray = new ChannelEndpointElement[clientSection.Endpoints.Count];
            if (tmpArray.Length > 0)
            {
                //don't do anything as
                clientSection.Endpoints.CopyTo(tmpArray, 0);
                //has to copied from the configs custom collection so that it can be converted to list and linq operations used
            }
            _endPointConfigs = tmpArray.ToList();

            //here we can put additional configuration validations - are all service configured? do they contain all required config attributes etc.

            //IDEA : I think we should inject the configuration to this service and not 'let' it read from the app config file.
            //the services and their behaviours are tightly coupled with this configuration and don't make sense without it.            
        }

        /// <summary>
        /// Gets the configuration name for a client for a particular service contract type from the web.config system.serviceModel configuration section.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// </exception>
        public string GetConfigNameForClient<TService>()
        {
            string typeFullName = typeof(TService).FullName;
            ChannelEndpointElement clientConfig = _endPointConfigs.FirstOrDefault(config => config.Contract == typeFullName);
            if (clientConfig == null)
                throw new Exception(string.Format("Unable to find client configuration for the requested service type {0} or " +
                                                  "contract attribute is missing.", typeFullName));
            if (string.IsNullOrEmpty(clientConfig.Name))
                throw new Exception(string.Format("Client configuration for the service {0} is missing the name attribute", typeFullName));

            return clientConfig.Name;
        }
    }
}