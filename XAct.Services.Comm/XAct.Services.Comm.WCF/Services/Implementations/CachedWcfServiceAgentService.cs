﻿namespace XAct.Services.Comm.ServiceModel.Services.Implementations
{
    using System;
    using XAct.Caching;
    using XAct.Diagnostics;
    using XAct.Services;

    /// <summary>
    /// Implementation of the 
    /// <see cref="ICachedWcfServiceAgentService"/>
    /// to return results cached results of 
    /// previous WCF requests. 
    /// </summary>
    public class CachedWcfServiceAgentService : XActLibServiceBase, ICachedWcfServiceAgentService
    {
        private readonly ICachingService _hostBasedCachingService;
        private readonly IWcfServiceAgentService _wcfServiceAgentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CachedWcfServiceAgentService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="hostBasedCachingService">The caching service.</param>
        /// <param name="wcfServiceAgentService">The WCF service agent service.</param>
        public CachedWcfServiceAgentService(
            ITracingService tracingService,
            ICachingService hostBasedCachingService,
            IWcfServiceAgentService wcfServiceAgentService):base(tracingService)
        {
            _hostBasedCachingService = hostBasedCachingService;
            _wcfServiceAgentService = wcfServiceAgentService;
        }

        /// <summary>
        /// Checks for a cached value, and if not found/stale
        /// Connects to the specified service, executes the remote Operation,
        /// caching the result before returning it.
        /// </summary>
        /// <typeparam name="TService">The type of the service.</typeparam>
        /// <typeparam name="TResult">The type of the result.</typeparam>
        /// <param name="func">The function to be executed on the service</param>
        /// <param name="cacheKey">The cache key.</param>
        /// <param name="cacheTimeSpan">The cache time span.</param>
        /// <param name="bypassCaching">if set to <c>true</c> [bypass caching].</param>
        /// <param name="forceUpdateOfCache">if set to <c>true</c> [force update of cache].</param>
        /// <returns></returns>
        public TResult Query<TService, TResult>(Func<TService, TResult> func, string cacheKey, TimeSpan? cacheTimeSpan = null, bool bypassCaching = false, bool forceUpdateOfCache = false)
        {
            if (!cacheTimeSpan.HasValue)
            {
                cacheTimeSpan = XAct.Library.Settings.Caching.DefaultReferenceCachingTimeSpan;
            }

            TResult results;

            //Try getting it out of the cache, and if not possible,
            //define how to get the results from across the wire:
            if (bypassCaching)
            {
                return _wcfServiceAgentService.Query(func);
            }

            if (forceUpdateOfCache)
            {
                _hostBasedCachingService.Remove(cacheKey);
            }

            _hostBasedCachingService.TryGet(
                cacheKey,
                out results,
                () => _wcfServiceAgentService.Query(func),
                cacheTimeSpan.Value,
                true);

            return results;

        }

    }
}