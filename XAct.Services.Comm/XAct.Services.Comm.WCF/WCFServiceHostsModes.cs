﻿namespace XAct.Services.Comm.ServiceModel
{
    using System;

    /// <summary>
    ///   Enumeration of the types of WCF Services to host.
    /// </summary>
    [Flags]
    public enum WCFServiceHostsModes
    {
        /// <summary>
        ///   Can't think of a good reason to host an offline service...
        /// </summary>
        Offline = 0,

        /// <summary>
        ///   For hosting Admin WCF Services
        /// </summary>
        Admin = 1,

        /// <summary>
        ///   For hosting PowerUser Admin WCF Services
        /// </summary>
        PowerUser = 2,

        /// <summary>
        ///   For hosting authenticated Read/Write access.
        ///   (eg: for submitting a new package for delivery)
        /// </summary>
        PublicSet = 4,

        /// <summary>
        ///   For hosting un authenticated Public Readonly Services.
        ///   (eg: finding out how a package is doing on its transit to destination).
        /// </summary>
        PublicGet = 8,
    }
}