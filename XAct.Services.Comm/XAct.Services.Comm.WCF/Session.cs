﻿
namespace XAct.Services.Comm.ServiceModel.Constants
{
    /// <summary>
    /// 
    /// </summary>
    public static class Session
    {
        //public const string SessionTokenCookieName = "SessionToken";

        /// <summary>
        /// The session token header namespace
        /// </summary>
        public const string SessionTokenHeaderNamespace = "SessionTokenNamespace";
        /// <summary>
        /// The session token header name
        /// </summary>
        public const string SessionTokenHeaderName = "SessionTokenHeader";

        /// <summary>
        /// The session token response header namespace
        /// </summary>
        public const string SessionTokenResponseHeaderNamespace = "SessionTokenResponseNamespace";
        /// <summary>
        /// The session token response header name
        /// </summary>
        public const string SessionTokenResponseHeaderName = "SessionTokenResponseHeader";


    }
}
