﻿
namespace XAct.Services.Comm.ServiceModel.Services.Implementations
{
    using System;
    using System.Linq;
    using System.Security.Principal;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Dispatcher;
    using XAct;
    using XAct.Diagnostics;
    using XAct.State;

    /// <summary>
    /// 
    /// </summary>
    public class SecurityClientMessageInspector : IClientMessageInspector
    {
        ITracingService TracingService
        {
            get
            {
                return XAct.DependencyResolver.Current.GetInstance<ITracingService>();
            }
        }
        IContextStateService ContextStateService
        {
            get
            {
                return XAct.DependencyResolver.Current.GetInstance<IContextStateService>();
            }
        }

        /// <summary>
        /// Enables inspection or modification of a message before a request message is sent to a service.
        /// </summary>
        /// <param name="request">The message to be sent to the service.</param>
        /// <param name="channel">The WCF client object channel.</param>
        /// <returns>
        /// The object that is returned as the <c>correlationState</c> argument of the
        /// <see cref="M:System.ServiceModel.Dispatcher.IClientMessageInspector.AfterReceiveReply(System.ServiceModel.Channels.Message@,System.Object)" /> method. This is null if no correlation state is used.The best practice is to make this a <see cref="T:System.Guid" /> to ensure that no two <c>correlationState</c> objects are the same.
        /// </returns>
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            //Do *NOT* get SessionId from Token. 
            //Avoid causing the whole app to have a dependency on System.Web and a current HttpContext
            //Guid sessionToken = HttpContext.Current.Request.GetSessionToken(true); //get session token from cookies of current request


            //Get the SessionToken:
            //You can either embed it in this Tier's Identity (we can do that when we retrieve the Identity using the Cookie, higher up)
            //or we could also have passed it around, separately, using XActLib's ContextState:
            //AppIdentity appIdentity = System.Threading.Thread.CurrentPrincipal.Identity as AppIdentity;
            Guid? sessionToken;

            sessionToken = ExtractSessionToken();


            //Embed the Guid as a WCF Header:
            TracingService.Trace(TraceLevel.Verbose, "SecurityClientMessageInspector.BeforeSendRequest: attaching header.");
            SessionTokenEnvelope sessionTokenEnvelope = new SessionTokenEnvelope(sessionToken); //add session token to the headers of wcf message

            //For every time the front invokes the back, we increment a counter.
            //The higher the number, the less chunky the design...worth reviewing for performance reasons.
            int counter = (int)(ContextStateService.Items["CrossTierRequestCounter"] ?? 0);
            sessionTokenEnvelope.RequestCounter = counter;
            counter++;
            ContextStateService.Items["CrossTierRequestCounter"] = counter;


            //Attach the header:
            request.Headers.Add(sessionTokenEnvelope.ToMessageHeader());

            //Done:
            TracingService.Trace(TraceLevel.Verbose, "SecurityClientMessageInspector.BeforeSendRequest: done.");
            return null; //sessionTokenEnvelope
        }

        private Guid? ExtractSessionToken()
        {
            Guid? sessionToken;
            GenericIdentity identity = System.Threading.Thread.CurrentPrincipal.Identity as GenericIdentity;
            if (identity != null)
            {
                TracingService.Trace(TraceLevel.Verbose,
                                     "SecurityClientMessageInspector.BeforeSendRequest: have appIdentity: " +
                                     identity.Name.SafeString(3));

                var claim = identity.Claims.FirstOrDefault(x => x.Type == "SessionToken");

                var value = (claim != null) ? claim.Value : null;


                if (claim == null)
                {
                    throw new Exception("SecuritClientMessageInspector.BeforeSendRequest: missing SessionToken Claim.");
                }
                if (value == null)
                {
                    throw new Exception("SecuritClientMessageInspector.BeforeSendRequest: missing SessionToken Value.");
                }
                sessionToken = Guid.Parse(value);
            }
            else
            {
                TracingService.Trace(TraceLevel.Verbose,
                                     "SecurityClientMessageInspector.BeforeSendRequest: have no appIdentity.");
                sessionToken = null;
            }
            return sessionToken;
        }

        /// <summary>
        /// Enables inspection or modification of a message after a reply message is received but prior to passing it back to the client application.
        /// </summary>
        /// <param name="reply">The message to be transformed into types and handed back to the client application.</param>
        /// <param name="correlationState">Correlation state data.</param>
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            TracingService.Trace(TraceLevel.Verbose, "SecurityClientMessageInspector.AfterReceiveReply: Done.");
        }
    }
}
