﻿namespace XAct.Services.Comm.ServiceModel.Inspectors
{
    using System;
    using System.ServiceModel.Dispatcher;
    using System.ServiceModel.Channels;
    using System.Threading;
    using System.Globalization;
    using XAct.Services.Comm.ServiceModel.Behaviors;

    /// <summary>
    /// A Message Inspector (that implements both <see cref="IClientMessageInspector"/>
    /// and <see cref="IDispatchMessageInspector"/>) that is applied to both Front and Back Tier
    /// WCF Service endpoints, using <see cref="CultureEndpointBehavior"/>
    /// </summary>
    public class CultureMessageInspector : IClientMessageInspector, IDispatchMessageInspector
    {

        /// <summary>
        /// Gets the xml namespace used to disambiguate <see cref="CurrentCultureHeaderElementName"/>
        /// </summary>-
        /// <value>
        /// The namespace.
        /// </value>
        public static string HeaderElementNamespace
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// The well known name of a message Header element name used for transmitting the culture
        /// used for numbers, dates, currencies, etc.
        /// </summary>
        public static string CurrentCultureHeaderElementName
        {
            get { return _currentCultureHeaderElementName; }
            set { _currentCultureHeaderElementName = value; }
        }

        private static string _currentCultureHeaderElementName = "currentCulture";


        /// <summary>
        /// The well known name of a message Header element name used for transmitting the culture
        /// used for string resource rendering.
        /// </summary>
        public static string CurrentUICultureHeaderElementName
        {
            get { return _currentUICultureHeaderElementName; }
            set { _currentUICultureHeaderElementName = value; }
        }

        private static string _currentUICultureHeaderElementName = "currentUICulture";



        /// <summary>
        /// The well known name of a message Header element name used for transmitting a custom culture
        /// (eg Currency).
        /// </summary>
        public static string CurrentCustomCultureHeaderElementName
        {
            get { return _currentCustomCultureHeaderElementName; }
            set { _currentCustomCultureHeaderElementName = value; }
        }

        private static string _currentCustomCultureHeaderElementName = "currentCustomCulture";


        /// <summary>
        /// A delegate that can be used to set the custom Culture name.
        /// <para>
        /// eg:
        /// <code>
        /// <![CDATA[
        /// //Allows setting currency to USD/SF/etc. no matter what CurrentCulture is set to:
        /// CultureMessageInspector.GetCustomCultureName = ()=>EnvironmentService.CurrentCurrencyCulture;
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        public static Func<string> GetCustomCultureName { get; set; }

        /// <summary>
        /// A delegate that can be used to set the custom Culture when received:
        /// <para>
        /// eg:
        /// <code>
        /// <![CDATA[
        /// //Allows setting currency to USD/SF/etc. no matter what CurrentCulture is set to:
        /// CultureMessageInspector.SetCustomCultureName = (x)=>EnvironmentService.CurrentCurrencyCulture=x;
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        public static Action<CultureInfo> SetCustomCultureName { get; set; }

        #region IClientMessageInspector Members
        // Method implementations to inspect messages prior to being sent to the remote service.



        /// <summary>
        /// Enables inspection or modification of a message before a request message is sent to a service.
        /// </summary>
        /// <param name="request">The message to be sent to the service.</param>
        /// <param name="channel">The WCF client object channel.</param>
        /// <returns>
        /// The object that is returned as the <c>correlationState</c> argument of the <see cref="M:System.ServiceModel.Dispatcher.IClientMessageInspector.AfterReceiveReply(System.ServiceModel.Channels.Message@,System.Object)" /> method. This is null if no correlation state is used.The best practice is to make this a <see cref="T:System.Guid" /> to ensure that no two <c>correlationState</c> objects are the same.
        /// </returns>
        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request,
                                        System.ServiceModel.IClientChannel channel)
        {
            //Attach a simple Key/Value header:
            request.Headers.Add(
                MessageHeader.CreateHeader(
                    CurrentUICultureHeaderElementName,
                    HeaderElementNamespace, //namespace
                    Thread.CurrentThread.CurrentUICulture.Name //The value -- in this case 'en-EN', etc.
                    )
                );

            //Attach a simple Key/Value header:
            request.Headers.Add(
                MessageHeader.CreateHeader(
                    CurrentCultureHeaderElementName,
                    HeaderElementNamespace, //namespace
                    Thread.CurrentThread.CurrentCulture.Name //The value -- in this case 'en-EN', etc.
                    )
                );

            var func = GetCustomCultureName;
            if (func != null)
            {
                var customCultureName = func.Invoke();
                if (!string.IsNullOrEmpty(customCultureName))
                {
                    request.Headers.Add(
                        MessageHeader.CreateHeader(
                            CurrentCustomCultureHeaderElementName,
                            HeaderElementNamespace, //namespace
                            customCultureName //The value -- in this case 'en-EN', etc.
                            )
                        );
                }
            }

            // No correlation info required.
            return null;
        }


        /// <summary>
        /// Enables inspection or modification of a message after a reply message is received but prior to passing it back to the client application.
        /// </summary>
        /// <param name="reply">The message to be transformed into types and handed back to the client application.</param>
        /// <param name="correlationState">Correlation state data.</param>
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            // This inspector is only concerned with attaching a header to newly created messages
            // intended for a remote service -- hence nothing to do here.
        }
        #endregion



        // This is a very simple Inspector that is is *both* a client inspector (client side) *and* a dispatch inspector (server side)
        // making it easier to attach using a behaviour.


        #region IDispatchMessageInspector Members

        /// <summary>
        /// Called after an inbound message has been received but before the message is dispatched to the intended operation.
        /// </summary>
        /// <param name="request">The request message.</param>
        /// <param name="channel">The incoming channel.</param>
        /// <param name="instanceContext">The current service instance.</param>
        /// <returns>
        /// The object used to correlate state. This object is passed back in the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.BeforeSendReply(System.ServiceModel.Channels.Message@,System.Object)" /> method.
        /// </returns>
        public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            //Now do the UI culture (strings...)
            int headerIndex = request.Headers.FindHeader(CurrentUICultureHeaderElementName, HeaderElementNamespace);
            if (headerIndex != -1)
            {

                // Build a new cultureInfo
                CultureInfo cultureUIInfo = new CultureInfo(request.Headers.GetHeader<String>(headerIndex));


                // And set it.
                Thread.CurrentThread.CurrentUICulture = cultureUIInfo;

                // Done!
            }



            //Now do the culture, which handles numbers, dates:
            headerIndex = request.Headers.FindHeader(CurrentCultureHeaderElementName, HeaderElementNamespace);
            if (headerIndex != -1)
            {
                // Found

                // Build a new cultureInfo
                CultureInfo cultureInfo = new CultureInfo(request.Headers.GetHeader<String>(headerIndex));

                // And set it.
                Thread.CurrentThread.CurrentCulture = cultureInfo;

                // Done!
            }

            //Now do the culture, which handles numbers, dates:
            headerIndex = request.Headers.FindHeader(CurrentCustomCultureHeaderElementName, HeaderElementNamespace);
            if (headerIndex != -1)
            {
                // Found

                // Build a new cultureInfo
                CultureInfo cultureInfo = new CultureInfo(request.Headers.GetHeader<String>(headerIndex));

                // And set it.
                if (SetCustomCultureName != null)
                {
                    SetCustomCultureName.Invoke(cultureInfo);
                }

                // Done!
            }



            // No correlation info required.
            return null;
        }

        /// <summary>
        /// Called after the operation has returned but before the reply message is sent.
        /// </summary>
        /// <param name="reply">The reply message. This value is null if the operation is one way.</param>
        /// <param name="correlationState">The correlation object returned from the <see cref="M:System.ServiceModel.Dispatcher.IDispatchMessageInspector.AfterReceiveRequest(System.ServiceModel.Channels.Message@,System.ServiceModel.IClientChannel,System.ServiceModel.InstanceContext)" /> method.</param>
        public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
            //This server-side inspector is not concerned with setting a culture in the server side.
        }

        #endregion

    }

}
