﻿namespace XAct.Services.Comm.ServiceModel.Services.Implementations
{
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;

    /// <summary>
    /// 
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <system.serviceModel>
    ///   <services>
    ///     <service>
    ///       <endpoint contract="contract" binding="basicHttpBinding" behaviourConfiguration="behavior1"/>
    ///     </service>
    ///   </services>
    ///   <behaviors>
    ///     <endpointBehaviors>
    ///       <behavior name="behavior1" >
    ///         <inspector1 .../>
    ///         <inspector2 ... />
    ///       </behavior>
    ///     </endpointBehaviors>
    ///   </behaviors>
    /// </system.serviceModel>
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    public class SecurityClientEndpointBehavior : IEndpointBehavior
    {
        /// <summary>
        /// Implement to pass data at runtime to bindings to support custom behavior.
        /// </summary>
        /// <param name="endpoint">The endpoint to modify.</param>
        /// <param name="bindingParameters">The objects that binding elements require to support the behavior.</param>
        public void AddBindingParameters(
            ServiceEndpoint endpoint, BindingParameterCollection bindingParameters
            ) { }

        /// <summary>
        /// Implements a modification or extension of the client across an endpoint.
        /// </summary>
        /// <param name="endpoint">The endpoint that is to be customized.</param>
        /// <param name="clientRuntime">The client runtime to be customized.</param>
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            clientRuntime.MessageInspectors.Add(new SecurityClientMessageInspector());
            //foreach (ClientOperation op in clientRuntime.Operations) op.ParameterInspectors.Add(new Inspector());
        }

        /// <summary>
        /// Implements a modification or extension of the service across an endpoint.
        /// </summary>
        /// <param name="endpoint">The endpoint that exposes the contract.</param>
        /// <param name="endpointDispatcher">The endpoint dispatcher to be modified or extended.</param>
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            //endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new Inspector());
            //foreach (DispatchOperation op in endpointDispatcher.DispatchRuntime.Operations)
            //    op.ParameterInspectors.Add(new Inspector());
        }

        /// <summary>
        /// Implement to confirm that the endpoint meets some intended criteria.
        /// </summary>
        /// <param name="endpoint">The endpoint to validate.</param>
        public void Validate(ServiceEndpoint endpoint) {  }
    }
}