﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Services.Comm.ServiceModel.Behaviors.Configuration
{
    using System.Configuration;
    using System.ServiceModel.Configuration;


    /// <summary>
    /// A Configuration element used to register an element
    /// in a config file:
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <system.serviceModel>
    ///   <extensions>
    ///     <behaviorExtensions>
    ///         <!-- refer to the ExtensionElement that implements BehaviorExtensionElement -->
    ///      <add name="MyCultureCulture"
    ///            type="XAct.Services.Comm.ServiceModel.Behaviors.Configuration.CultureEndpointBehaviorExtensionElement, Namespace, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" />
    ///     </behaviorExtensions>
    ///   </extensions>    
    ///   <behaviors>
    ///        <!-- there are many behaviours, here we're attaching an endpoint behavior... -->
    ///     <endpointBehaviors>
    ///       <!-- the endpointBehaviors/behavior tag is a placeholder/wrapper of a child BehaviorExtensionElement mentioned above -->
    ///       <behavior name="DefaultEndpointBehavior">
    ///         <MyCultureeCulture />
    ///       </behavior>
    ///     </endpointBehaviors>
    ///     <serviceBehaviors>
    ///               ...
    ///     </serviceBehaviors>
    ///   </behaviors>
    ///   <services>
    ///     <service name="Namespace.Service" >
    ///       <endpoint address="" binding="wsHttpBinding" contract="Namespace.IService"  behaviorConfiguration="DefaultEndpointBehavior" />
    ///       <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange"  />
    ///     </service>
    ///   </services>
    /// </system.serviceModel>
    /// 
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    /// <configuration>
    /// 
    ///   <appSettings>
    ///     <add key="aspnet:UseTaskFriendlySynchronizationContext" value="true" />
    ///   </appSettings>
    ///   <system.web>
    ///     <compilation debug="true" targetFramework="4.5" />
    ///     <httpRuntime targetFramework="4.5"/>
    ///   </system.web>
    ///   <system.serviceModel>
    ///     <extensions>
    ///       <behaviorExtensions>
    ///         <!-- refer to the ExtensionElement that implements BehaviorExtensionElement -->
    ///         <add name="MyCultureBehaviorExtensionElement"     
    ///              type="XAct.Services.Comm.ServiceModel.Behaviors.Configuration.CultureEndpointBehaviorExtensionElement, XAct.Spikes.WCFBehavior, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    ///       </behaviorExtensions>
    ///     </extensions>
    /// 
    ///      <behaviors>
    ///        <!-- there are many behaviours, here we're attaching an endpoint behavior... -->
    ///       <endpointBehaviors>
    ///         <!-- the endpointBehaviors/behavior tag is a placeholder/wrapper of a 
    ///              child BehaviorExtensionElement mentioned above -->
    ///         <behavior name="MyCultureBehaviorExtensionElement" >
    ///           <MyCultureBehaviorExtensionElement />
    ///         </behavior>
    ///       </endpointBehaviors>
    ///       
    ///       <serviceBehaviors>
    ///         <behavior>
    ///           <serviceMetadata httpGetEnabled="true" httpsGetEnabled="true"/>
    ///           <serviceDebug includeExceptionDetailInFaults="true"/>
    ///         </behavior>
    ///       </serviceBehaviors>
    ///     </behaviors>
    /// 
    ///     <bindings>
    ///       <basicHttpBinding>
    ///         <binding name="mySecureBasicHttpBinding">
    ///           <security mode="TransportWithMessageCredential">
    ///             <transport clientCredentialType="None"  />
    ///             <message clientCredentialType="UserName" />
    ///       
    ///           </security>
    ///         </binding>
    ///       </basicHttpBinding>
    ///     
    ///       <customBinding>
    ///         <binding name="Soap11Addr10">
    ///           <textMessageEncoding messageVersion="Soap11WSAddressing10" />
    ///           <httpTransport/>
    ///         </binding>
    ///       </customBinding>    
    ///     </bindings>
    ///       
    ///         
    ///     <services>
    ///       <service name="XAct.Spikes.WCFBehavior.Service1"  behaviorConfiguration="" >
    ///         <!-- notice how the EndpointBehavior is applied to the Endpoint, not the Service -->
    ///        <endpoint binding="customBinding" bindingConfiguration="Soap11Addr10"
    ///                  contract="XAct.Spikes.WCFBehavior.IService1" 
    ///                  behaviorConfiguration="MyCultureBehaviorExtensionElement"/>
    ///       </service>
    ///     </services>
    ///     
    ///   <protocolMapping>
    ///         <add binding="basicHttpsBinding" scheme="https" />
    ///     </protocolMapping>    
    ///     <serviceHostingEnvironment aspNetCompatibilityEnabled="true" multipleSiteBindingsEnabled="true" />
    ///   </system.serviceModel>
    /// 
    ///   <system.webServer>
    ///     <modules runAllManagedModulesForAllRequests="true"/>
    ///     <!--
    ///         To browse web app root directory during debugging, set the value below to true.
    ///         Set to false before deployment to avoid disclosing web app folder information.
    ///       -->
    ///     <directoryBrowse enabled="true"/>
    ///   </system.webServer>
    /// 
    /// </configuration>
    /// ]]></code>
    /// </para>
    /// </summary>
    public class CultureEndpointBehaviorExtensionElement : BehaviorExtensionElement 
    {

        /*
        [ConfigurationProperty("example", IsRequired = false)]
        public string Example
        {
            get { return this["example"] as string; }
            set { this["example"] = value; }
        }
        */


        /// <summary>
        /// Creates a behavior extension based on the current configuration settings.
        /// </summary>
        /// <returns>
        /// The behavior extension.
        /// </returns>
        protected override object CreateBehavior()
        {
            //Factory up the Behavior:
            //return new CultureEndpointBehavior(Example);
            return new CultureEndpointBehavior();
        }

        /// <summary>
        /// Gets the type of the Behavior that this element describes the configuration of.
        /// </summary>
        public override Type BehaviorType
        {
            get
            {
                return typeof(CultureEndpointBehavior);
            }
        }
    }
}
