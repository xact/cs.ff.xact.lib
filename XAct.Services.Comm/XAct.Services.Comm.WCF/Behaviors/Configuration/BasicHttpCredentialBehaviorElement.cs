﻿namespace XAct.Services.Comm.ServiceModel.Behaviors.Configuration
{
    using System;
    using System.Configuration;
    using System.ServiceModel.Configuration;

    /// <summary>
        /// A web config configuration element, in order to provide 
        /// the ability to define the basic credentials used to communicate
        /// to the investor service.
        /// </summary>
    /// <internal>
    /// /// http://blog.shutupandcode.net/?p=22
    /// </internal>
    public class BasicHttpCredentialBehaviorElement : BehaviorExtensionElement
        {
            /// <summary>
            /// Gets or sets the name of the user.
            /// </summary>
            /// <value>The name of the user.</value>
            [ConfigurationProperty("userName", IsRequired = true)]
            public string UserName
            {
                get { return this["userName"] as string; }
                set { this["userName"] = value; }
            }

            /// <summary>
            /// Gets or sets the password.
            /// </summary>
            /// <value>The password.</value>
            [ConfigurationProperty("password", IsRequired = true)]
            public string Password
            {
                get { return this["password"] as string; }
                set { this["password"] = value; }
            }

            /// <summary>
            /// Creates a behavior extension based on the current configuration settings.
            /// </summary>
            /// <returns>The behavior extension.</returns>
            protected override object CreateBehavior()
            {
                return new BasicHttpCredentialEndpointBehavior(UserName, Password);
            }

            /// <summary>
            /// Gets the type of behavior.
            /// </summary>
            /// <value></value>
            /// <returns>A <see cref="T:System.Type"/>.</returns>
            public override Type BehaviorType
            {

                get
                {
                    return typeof(BasicHttpCredentialEndpointBehavior);
                }
            }
        }
}
