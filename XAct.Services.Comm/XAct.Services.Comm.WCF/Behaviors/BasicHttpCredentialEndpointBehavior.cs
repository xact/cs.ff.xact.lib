﻿namespace XAct.Services.Comm.ServiceModel.Behaviors
{
    using System.ServiceModel.Description;

    /// <summary>
        /// Provides the custom behavior on an endpoint to all a client to talk
        /// to a service using basic authentication.
        /// </summary>
        /// <internal>
        /// /// http://blog.shutupandcode.net/?p=22
        /// </internal>
        public class BasicHttpCredentialEndpointBehavior : IEndpointBehavior
        {
            static object _lockObject = new object();
            string username;
            string password;

            /// <summary>
            /// Initializes a new instance of the <see cref="BasicHttpCredentialEndpointBehavior"/> class.
            /// </summary>
            public BasicHttpCredentialEndpointBehavior()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="BasicHttpCredentialEndpointBehavior"/> class.
            /// </summary>
            /// <param name="username">The username.</param>
            /// <param name="password">The password.</param>
            public BasicHttpCredentialEndpointBehavior(string username, string password)
            {
                this.username = username;
                this.password = password;
            }

            #region IEndpointBehavior Members

            /// <summary>
            /// Implement to pass data at runtime to bindings to support custom behavior.
            /// </summary>
            /// <param name="endpoint">The endpoint to modify.</param>
            /// <param name="bindingParameters">The objects that binding elements require to support the behavior.</param>
            public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
            {

            }

            /// <summary>
            /// Implements a modification or extension of the client across an endpoint.
            /// </summary>
            /// <param name="endpoint">The endpoint that is to be customized.</param>
            /// <param name="clientRuntime">The client runtime to be customized.</param>
            public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
            {
                //No
                //lock (_lockObject)
                //{
                //    var cc = endpoint.Behaviors.Find<clientCredentials>();

                //    cc.UserName.UserName = this.username;
                //    cc.UserName.Password = this.password;
                //}
            }

            /// <summary>
            /// Implements a modification or extension of the service across an endpoint.
            /// </summary>
            /// <param name="endpoint">The endpoint that exposes the contract.</param>
            /// <param name="endpointDispatcher">The endpoint dispatcher to be modified or extended.</param>
            public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher) { }

            /// <summary>
            /// Implement to confirm that the endpoint meets some intended criteria.
            /// </summary>
            /// <param name="endpoint">The endpoint to validate.</param>
            public void Validate(ServiceEndpoint endpoint) { }

            #endregion

        }
    }

