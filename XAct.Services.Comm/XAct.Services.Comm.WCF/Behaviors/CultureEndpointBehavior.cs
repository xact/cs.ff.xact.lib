﻿namespace XAct.Services.Comm.ServiceModel.Behaviors
{
    using System.ServiceModel.Description;
    using XAct.Services.Comm.ServiceModel.Inspectors;


    /// <summary>
    /// An <see cref="IEndpointBehavior"/> that can be applied to both
    /// front and back tier service endpoints in an N-Tier application
    /// to flow the client's culture to the back tier.
    /// <para>
    /// TIP: Ensure it is the first behavior processed, so that any messages
    /// raised by subsequent Message Inspectors translate the messages
    /// to a culture appropriate to the client/presentation tier.
    /// </para>
    /// </summary>
    public class CultureEndpointBehavior : IEndpointBehavior
        {
            #region IEndpointBehavior Members

            /// <summary>
            /// Implement to pass data at runtime to bindings to support custom behavior.
            /// </summary>
            /// <param name="endpoint">The endpoint to modify.</param>
            /// <param name="bindingParameters">The objects that binding elements require to support the behavior.</param>
            public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
            {
            }

            /// <summary>
            /// Implements a modification or extension of the client across an endpoint.
            /// </summary>
            /// <param name="endpoint">The endpoint that is to be customized.</param>
            /// <param name="clientRuntime">The client runtime to be customized.</param>
            public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
            {
                CultureMessageInspector inspector = new CultureMessageInspector();
                clientRuntime.MessageInspectors.Add(inspector);
            }

            /// <summary>
            /// Implements a modification or extension of the service across an endpoint.
            /// </summary>
            /// <param name="endpoint">The endpoint that exposes the contract.</param>
            /// <param name="endpointDispatcher">The endpoint dispatcher to be modified or extended.</param>
            public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
            {
                CultureMessageInspector inspector = new CultureMessageInspector();
                endpointDispatcher.DispatchRuntime.MessageInspectors.Add(inspector);
            }

            /// <summary>
            /// Implement to confirm that the endpoint meets some intended criteria.
            /// </summary>
            /// <param name="endpoint">The endpoint to validate.</param>
            public void Validate(ServiceEndpoint endpoint)
            {
            }

            #endregion
        }
}
