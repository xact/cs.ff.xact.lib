﻿using System;

namespace XAct.ServiceModel
{
    public class WcfInfo
    {
        public readonly Type ServiceContractType;
        public readonly Type ServiceType;
        public WCFServiceHostsModes Mode;
        public object Service;
        public IWCFServiceHost ServiceHost;

        public WcfInfo(WCFServiceHostsModes wcfServiceHostsMode, Type serviceContractType, Type serviceType)
        {
            Mode = wcfServiceHostsMode;
            ServiceContractType = serviceContractType;
            ServiceType = serviceType;
            Service = null;
            ServiceHost = null;
        }
    }
}