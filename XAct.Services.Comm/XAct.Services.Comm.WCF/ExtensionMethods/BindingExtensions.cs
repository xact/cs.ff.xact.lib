﻿namespace XAct
{
    using System.ServiceModel.Channels;
    using XAct.Services.Comm.ServiceModel;

    /// <summary>
    /// Extensions to the <see cref="Binding"/>
    /// objects.
    /// </summary>
    public static class BindingExtensions
    {
        /// <summary>
        /// Configures the Binding's time out values.
        /// </summary>
        /// <param name="binding">The binding.</param>
        /// <param name="bindingConfiguraion">The binding configuraion.</param>
        public static void ConfigureTimeOuts(this Binding binding, BindingTimeOutConfiguration bindingConfiguraion)
        {
            bindingConfiguraion.ValidateIsNotDefault("bindingConfiguraion");

            binding.OpenTimeout = bindingConfiguraion.OpenTimeout;
            binding.SendTimeout = bindingConfiguraion.SendTimeout;
            binding.ReceiveTimeout = bindingConfiguraion.ReceiveTimeout;
            binding.CloseTimeout = bindingConfiguraion.CloseTimeout;

        }
    }
}
