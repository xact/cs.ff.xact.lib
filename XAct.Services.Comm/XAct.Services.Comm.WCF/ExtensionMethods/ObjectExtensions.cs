﻿
//#if (!REMOVENS4EXTENSIONS)
////See: http://bit.ly/snE0xY
//namespace XAct {
//    using System.IO;
//    using System.Runtime.Serialization.Json;
//    using System.Text;

//#endif

//    /// <summary>
//    /// Extensions to the Object
//    /// </summary>
//    /// <internal><para>7/2/2011: Sky</para></internal>
//    public static class ObjectExtensions
//    {
//        /// <summary>
//        /// Serializes an object to a string, in the JSON format,
//        /// using UTF-8 encoding.
//        /// <para>
//        /// Reminder: for an object to be serializable, it must
//        /// be decorated with DataContract/DataMember attributes.
//        /// </para>
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <param name="objToSerialize">The obj to serialize.</param>
//        /// <returns></returns>
//        /// <internal><para>7/2/2011: Sky</para></internal>
//        public static string ToJSON<T>(this T objToSerialize) where T : class
//        {
//            return objToSerialize.ToJSON(Encoding.UTF8);
//        }

//        /// <summary>
//        /// Serializes an object to a string, in the JSON format,
//        /// using the specified encoding.
//        /// <para>
//        /// Reminder: for an object to be serializable, it must
//        /// be decorated with DataContract/DataMember attributes.
//        /// </para>
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <param name="objToSerialize">The obj to serialize.</param>
//        /// <param name="encoding">The encoding.</param>
//        /// <returns></returns>
//        /// <internal><para>7/16/2011: Sky</para></internal>
//        public static string ToJSON<T>(this T objToSerialize, Encoding encoding) where T : class
//        {
//            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof (T));

//            using (MemoryStream stream = new MemoryStream())
//            {
//                serializer.WriteObject(stream, objToSerialize);

//                return encoding.GetString(stream.ToArray());
//            }
//        }
//    }



//#if (!REMOVENS4EXTENSIONS)
//    //See: http://bit.ly/snE0xY
//}
//#endif
