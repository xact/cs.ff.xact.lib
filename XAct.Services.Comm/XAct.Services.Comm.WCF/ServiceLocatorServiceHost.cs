﻿namespace XAct.Services.Comm.ServiceModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.ServiceModel;
    using System.ServiceModel.Channels;
    using System.ServiceModel.Description;
    using System.ServiceModel.Dispatcher;

    /// <summary>
    /// A custom instance provider that uses the ServiceLocator from MS Pattern and Practices to resolve service types.
    /// </summary>
    /// <internal>
    /// <para>
    /// http://www.i-avington.com/Posts/Post/usng-unity-with-a-wcf-service
    /// </para>
    /// <para>
    /// Src: http://msdn.microsoft.com/en-us/library/vstudio/hh323725(v=vs.100).aspx
    /// </para>
    /// </internal>
    public class ServiceLocatorInstanceProvider : IInstanceProvider
    {
        /// <summary>
        /// Gets or sets the type of the service.
        /// </summary>
        /// <value>
        /// The type of the service.
        /// </value>
        public Type ServiceType { set; get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceLocatorInstanceProvider"/> class.
        /// </summary>
        public ServiceLocatorInstanceProvider()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceLocatorInstanceProvider"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        public ServiceLocatorInstanceProvider(Type type)
        {
            ServiceType = type;
        }


        /// <summary>
        /// Returns a service object given the specified <see cref="T:System.ServiceModel.InstanceContext" /> object.
        /// </summary>
        /// <param name="instanceContext">The current <see cref="T:System.ServiceModel.InstanceContext" /> object.</param>
        /// <returns>
        /// A user-defined service object.
        /// </returns>
        public object GetInstance(InstanceContext instanceContext)
        {
            var result = this.GetInstance(instanceContext, null);
            return result;
        }

        /// <summary>
        /// Returns a service object given the specified <see cref="T:System.ServiceModel.InstanceContext" /> object.
        /// </summary>
        /// <param name="instanceContext">The current <see cref="T:System.ServiceModel.InstanceContext" /> object.</param>
        /// <param name="message">The message that triggered the creation of a service object.</param>
        /// <returns>
        /// The service object.
        /// </returns>
        public object GetInstance(InstanceContext instanceContext, Message message)
        {
            object result = XAct.DependencyResolver.Current.GetInstance(ServiceType);
            return result;
        }


        /// <summary>
        /// Called when an <see cref="T:System.ServiceModel.InstanceContext" /> object recycles a service object.
        /// </summary>
        /// <param name="instanceContext">The service's instance context.</param>
        /// <param name="instance">The service object to be recycled.</param>
        public void ReleaseInstance(InstanceContext instanceContext, object instance)
        {

        }


    }



    /// <summary>
    /// 
    /// </summary>s
    /// <internal>
    /// <para>
    /// http://www.i-avington.com/Posts/Post/usng-unity-with-a-wcf-service
    /// </para>
    /// <para>
    /// Src: http://msdn.microsoft.com/en-us/library/vstudio/hh323725(v=vs.100).aspx
    /// </para>
    /// </internal>
    public class ServiceLocatorServiceHostFactory : System.ServiceModel.Activation.ServiceHostFactory, IDisposable
    {
        private ServiceLocatorServiceHost _serviceHost;

        /// <summary>
        /// Creates a <see cref="T:System.ServiceModel.ServiceHost" /> for a specified type of service with a specific base address.
        /// </summary>
        /// <param name="serviceType">Specifies the type of service to host.</param>
        /// <param name="baseAddresses">The <see cref="T:System.Array" /> of type <see cref="T:System.Uri" /> that contains the base addresses for the service hosted.</param>
        /// <returns>
        /// A <see cref="T:System.ServiceModel.ServiceHost" /> for the type of service specified with a specific base address.
        /// </returns>
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            _serviceHost = new ServiceLocatorServiceHost(serviceType, baseAddresses);


            return _serviceHost;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// <para>
        /// The bulk of the clean-up code is implemented in Dispose(bool)
        /// </para>
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                ((IDisposable)_serviceHost).Dispose();
            }
        }
    }



    /// <summary>
    /// This service host is used to set up the service behavior 
    /// that replaces the instance provider to use dependency injection.
    /// </summary>
    /// <internal>
    /// <para>
    /// http://www.i-avington.com/Posts/Post/usng-unity-with-a-wcf-service
    /// </para>
    /// <para>
    /// Src: http://msdn.microsoft.com/en-us/library/vstudio/hh323725(v=vs.100).aspx
    /// </para>
    /// <para>
    /// Usage:
    /// <code>
    /// <![CDATA[
    /// <%@ ServiceHost Language="C#" Service="App.Core.Application.AppSpecific.Services.Facades.Implementations.SomeServiceFacade, App.Core.Back.Application" Debug="true"  Factory="XAct.Services.Comm.ServiceModel.ServiceLocatorServiceHostFactory" %>
    /// ]]>
    /// </code>
    /// backed by:
    /// <code>
    /// <![CDATA[
    /// [DefaultBindingImplementation(typeof(IBatchServiceContract))]
    /// [ServiceErrorBehaviour(typeof (HttpErrorHandler))]
    /// public class BatchServiceFacade : WcfSessionAuthenticatedServiceFacadeBase,   IBatchServiceContract
    /// {
    ///    public SomeServiceFacade(IBatchService resourceService, IUnitOfWorkService unitOfWorkService) : base(unitOfWorkService) {...}
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </internal>
    public class ServiceLocatorServiceHost : ServiceHost
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceLocatorServiceHost"/> class.
        /// </summary>
        public ServiceLocatorServiceHost()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceLocatorServiceHost"/> class.
        /// </summary>
        /// <param name="serviceSingletonInstance">The service singleton instance.</param>
        /// <param name="baseAddresses">The base addresses.</param>
        public ServiceLocatorServiceHost(object serviceSingletonInstance, params Uri[] baseAddresses)
            : base(serviceSingletonInstance, baseAddresses)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceLocatorServiceHost"/> class.
        /// </summary>
        /// <param name="serviceType">The type of hosted service.</param>
        /// <param name="baseAddresses">
        /// An array of type <see cref="T:System.Uri" /> 
        /// that contains the base addresses for the hosted service.
        /// </param>
        public ServiceLocatorServiceHost(Type serviceType, params Uri[] baseAddresses)
            //WARNING: base throws an exception if the Type is an Interface, and not a class.
            : base(serviceType, baseAddresses)
        {
        }

        /// <summary>
        /// Invoked during the transition of a 
        /// communication object into the opening state.
        /// </summary>
        protected override void OnOpening()
        {
            if (this.Description.Behaviors.Find<ServiceLocatorServiceBehavior>() == null)
            {
                this.Description.Behaviors.Add(new ServiceLocatorServiceBehavior());
            }
            base.OnOpening();
        }
    }





    /// <summary>
    /// A custom <see cref="IServiceBehavior"/> which will 
    /// register the <see cref="ServiceLocatorInstanceProvider"/> 
    /// </summary>
    /// <internal>
    /// <para>
    /// http://www.i-avington.com/Posts/Post/usng-unity-with-a-wcf-service
    /// </para>
    /// <para>
    /// Src: http://msdn.microsoft.com/en-us/library/vstudio/hh323725(v=vs.100).aspx
    /// </para>
    /// </internal>
    public class ServiceLocatorServiceBehavior : IServiceBehavior
    {
        /// <summary>
        /// Gets or sets the instance provider.
        /// </summary>
        /// <value>
        /// The instance provider.
        /// </value>
        public ServiceLocatorInstanceProvider InstanceProvider
        {
            get { return _instanceProvider ?? (_instanceProvider = new ServiceLocatorInstanceProvider()); }
            set { _instanceProvider = value; }
        }

        private ServiceLocatorInstanceProvider _instanceProvider;

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceLocatorServiceBehavior"/> class.
        /// </summary>
        public ServiceLocatorServiceBehavior()
        {
            InstanceProvider = new ServiceLocatorInstanceProvider();
        }

        /// <summary>
        /// Provides the ability to change run-time property values or insert 
        /// custom extension objects such as error handlers, message or parameter 
        /// interceptors, security extensions, and other custom extension objects.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The host that is currently being built.</param>
        public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            foreach (ChannelDispatcherBase cdb in serviceHostBase.ChannelDispatchers)
            {
                ChannelDispatcher cd = cdb as ChannelDispatcher;

                if (cd == null) continue;

                foreach (EndpointDispatcher ed in cd.Endpoints)
                {
                    InstanceProvider.ServiceType = serviceDescription.ServiceType;

                    ed.DispatchRuntime.InstanceProvider = this.InstanceProvider;
                }
            }
        }

        /// <summary>
        /// Validates the specified service description.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The service host base.</param>
        public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
        {
            //Do nothing.
        }

        /// <summary>
        /// Provides the ability to pass custom data to binding elements to support the contract implementation.
        /// </summary>
        /// <param name="serviceDescription">The service description.</param>
        /// <param name="serviceHostBase">The service host base.</param>
        /// <param name="endpoints">The endpoints.</param>
        /// <param name="bindingParameters">The binding parameters.</param>
        public void AddBindingParameters(
            ServiceDescription serviceDescription,
            ServiceHostBase serviceHostBase,
            Collection<ServiceEndpoint> endpoints,
            BindingParameterCollection bindingParameters)
        {
            //Do nothing.
        }
    }
}


