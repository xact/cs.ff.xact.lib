namespace XAct.Services.Comm.ServiceModel
{
    /// <summary>
    ///   Interface/contract used to expose
    ///   functionality of any WS hosted WCF Service -- 
    ///   namely that any WCF Service can be Started, or Stopped.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     Interface implemented by <see cref = "WCFServiceHost" />
    ///   </para>
    /// </remarks>
    public interface IWCFServiceHost
    {
        /// <summary>
        ///   Causes a communication object to transition 
        ///   from the created state into the opened state.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     For exceptions that could be raised during this operation,
        ///     see,
        ///     <see cref = "System.ServiceModel.Channels.CommunicationObject.Open()" />
        ///   </para>
        /// </remarks>
        void Open();

        /// <summary>
        ///   Causes a communication object to transition 
        ///   from its current state into the closed state.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     For exceptions that could be raised during this operation,
        ///     see,
        ///     <see cref = "System.ServiceModel.Channels.CommunicationObject.Close()" />
        ///   </para>
        /// </remarks>
        void Close();
    }
}