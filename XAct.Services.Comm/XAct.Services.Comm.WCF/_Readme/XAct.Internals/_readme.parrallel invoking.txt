﻿First part:
 
Guid[] correlationIdsToSend = tmpCorrelationIdsToSend.ToArray();
 
 
            //IEnumerable<IGrouping<int, Guid>> chunkedCorrelationIds =
            //    from index in Enumerable.Range(0, correlationIdsToSend.Length)
            //    group correlationIdsToSend[index] by index / maxCorrelationIdsPerRequest;
 
            IEnumerable<IGrouping<int, Guid>> chunkedCorrelationIds =
                Enumerable.Range(0, correlationIdsToSend.Length).GroupBy(index => index/maxCorrelationIdsPerRequest,
                                                                         index => correlationIdsToSend[index]);
 
int numberOfGroups = chunkedCorrelationIds.Count();
 
 
            IList<Guid> tmpCheck = new List<Guid>();
 
            string msg = "Requesting Audit for {0} items, chunked into {1} groups of {2}".FormatString(
                                       correlationIdsToSend, numberOfGroups, maxCorrelationIdsPerRequest);
            LoggerUtil.LogDebug(msg);
 
//Notice this first…which builds an array of async calls:
            IDictionary<IAsyncResult, AsyncMethodCaller> asyncResults =
                IntializeAndInvokeRequests(chunkedCorrelationIds, tmpCheck);
 
//And this waits till they come back in.
            int allResultsFound = GetAllRequestResults(outputList, asyncResults);
 
 
 
 
 
Right so the key part being InitializeAndInvokeRequests:
 
 
 
        private static IDictionary<IAsyncResult, AsyncMethodCaller> IntializeAndInvokeRequests(IEnumerable<IGrouping<int, Guid>> chunkedCorrelationIds, IList<Guid> tmpCheck)
        {
            IDictionary<IAsyncResult, AsyncMethodCaller> asyncResults =
                new Dictionary<IAsyncResult, AsyncMethodCaller>();
            foreach (IGrouping<int, Guid> correlationIdChunk in chunkedCorrelationIds)
            {
                //Build a Request Message (request package)
                IPdvsAuditHistoryRequest pdvsAuditHistoryRequest
                    = new PdvsAuditHistoryRequest();
 
                foreach (System.Guid correlationId in correlationIdChunk)
                {
                    tmpCheck.Add(correlationId);
                    pdvsAuditHistoryRequest.CorrelationIds.Add(correlationId);
                }
 
                //At this point, the argument package of guids to send of to the service is ready.
                //So send it off:
 
                // Create the delegate.
                AsyncMethodCaller func = new AsyncMethodCaller(QuickWrap);
 
                //Begin async invoke
                IAsyncResult asyncResult = func.BeginInvoke(pdvsAuditHistoryRequest, null, null);
 
                //Add to dictionary:
                asyncResults[asyncResult] = func;
 
            }
            return asyncResults;
        }
 
 
 
Second method puts a wait on each one…when they get past that point, one can invoke the EndInvoke method to pick up answers…
 
        private static int GetAllRequestResults(Enquiry_Search_NZPassportOutputList outputList, IDictionary<IAsyncResult, AsyncMethodCaller> asyncResults)
        {
            int allResultsFound = 0;
 
 
            foreach (KeyValuePair<IAsyncResult, AsyncMethodCaller> keypair in asyncResults)
            {
 
                IAsyncResult asyncResult = keypair.Key;
                AsyncMethodCaller asyncMethodCaller = keypair.Value;
 
                //...do anything here...?
                asyncResult.AsyncWaitHandle.WaitOne(25*1000);
                //...do anything here...?
 
                IPdvsAuditHistoryResponse pdvsResponse = asyncMethodCaller.EndInvoke(asyncResult);
 
                //Take returned results, and put them in the output:
                int foundInResult = FillAuditOutputList(pdvsResponse, outputList);
 
                allResultsFound += foundInResult;
            }
 
            return allResultsFound;
        }
 