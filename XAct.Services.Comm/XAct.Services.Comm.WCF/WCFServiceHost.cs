﻿//using System.ServiceProcess;

namespace XAct.Services.Comm.ServiceModel
{
    using System;
    using System.Configuration;
    using System.ServiceModel;
    using System.ServiceModel.Configuration;
    using XAct.Diagnostics;

    /// <summary>
    ///   A WCF ServiceHost for hosting a WCF Service.
    /// </summary>
    /// <remarks>
    ///   <para>
    ///     The important part of the class is that 
    ///     a) it exposes a well-known Start/Stop pattern,
    ///     as well as tries to start itself using various techniques.
    ///   </para>
    /// </remarks>
    public class WCFServiceHost : IWCFServiceHost
        /*: IDisposable */
    {
        #region Services

        private readonly ITracingService _tracingService;

        #endregion

        #region Fields

        //Save types for logging purposes:
        private readonly Type _ServiceContractType;
        private readonly string _ServiceContractTypeName;
        private readonly Type _ServiceImplementationType;
        private readonly string _ServiceImplementationTypeName;
        private readonly string _TraceThisTypeName;

        //The WCF Service host, that hosts our service:
        private ServiceHost _ServiceHost;

        #endregion

        #region Constructors

        /// <summary>
        ///   Initializes a new instance of the 
        ///   <see cref = "WCFServiceHost" /> class.
        /// </summary>
        public WCFServiceHost(ITracingService tracingService, Type serviceContractType, Type serviceImplementationType)
        {
            _tracingService = tracingService;

            //Get these names out of the way up front:
            //we'll use them later for logging:
            _TraceThisTypeName = this.GetType().FullName;
            _ServiceContractType = serviceContractType;
            _ServiceContractTypeName = _ServiceContractType.FullName;
            _ServiceImplementationType = serviceImplementationType;
            _ServiceImplementationTypeName = _ServiceImplementationType.FullName;


            //Instantiate the WCF Service, and the ServiceHost to host it:
            InstantiateWCFServiceHost();
        }

        #endregion

        #region Implementation of IWCFServiceHost

        /// <summary>
        ///   Opens the WCF Service to client
        ///   connections.
        /// </summary>
        public void Open()
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.Open() (ServiceContract:'{1}')", _TraceThisTypeName,
                                _ServiceContractType);

            if (_ServiceHost != null)
            {
                _ServiceHost.Open();
            }
        }

        /// <summary>
        ///   Closes the WCF Service to client
        ///   connections.
        /// </summary>
        public void Close()
        {
            _tracingService.Trace(TraceLevel.Verbose, "{0}.Close() (ServiceContract:'{1}')", _TraceThisTypeName,
                                _ServiceContractType);

            if (_ServiceHost != null)
            {
                if (_ServiceHost.State != CommunicationState.Closed)
                {
                    _ServiceHost.Close();
                }
            }
        }

        #endregion

        /// <summary>
        ///   Instantiate the ServiceHost that will be hosting
        ///   the defined WCF Service:
        /// </summary>
        private void InstantiateWCFServiceHost()
        {
            _tracingService.Trace(TraceLevel.Verbose,
                                "{0}.InstantiateWCFServiceHost() [Begin...] (ServiceContract:'{1}')",
                                _TraceThisTypeName,
                                _ServiceContractType);


            //The goal here is to first check 
            //in the Windows Service's Config file
            //has the definition of the WCF Service.
            //If it does, use it.

            //If none defined, fall back to a hard coded
            //solution, using default port and bindings
            //as defined somewhere in settings...

            //Look in config file, to see if we can see 
            //a definition for this service:

            bool definedInAppConfig =
                ServerServiceDefined(_ServiceImplementationTypeName);

            if (definedInAppConfig)
            {
                _tracingService.Trace(TraceLevel.Verbose,
                                    "Valid configuration for '{0}' WCF Service found in Windows Service Config File [Begin...].",
                                    _ServiceImplementationTypeName);

                InstantiateWCFServiceHost_UsingConfigFile();

                _tracingService.Trace(TraceLevel.Verbose,
                                    "Valid configuration for '{0}' WCF Service found in Windows Service Config File [Complete].",
                                    _ServiceImplementationTypeName);
            }

            else
            {
                _tracingService.Trace(TraceLevel.Error,
                                    "Endpoint for '{0}' is not defined in config file. Aborting installation.",
                                    _ServiceImplementationTypeName);

                throw new ConfigurationErrorsException(
                    "[@] Endpoint for '{0}' is not defined in config file. Aborting installation.".
                        FormatStringExceptionCulture(
                            _ServiceImplementationTypeName
                        ));


                /*
                Debugging.WriteLine (
                              "Building Endpoint inline......Begin...",
                              _ServiceImplementationTypeName);

                InstantiateWCFServiceHost_UsingDefaultSettings();
                LogMessage(TraceLevel.Verbose,
                              "Building Endpoint inline......Complete",
                              _ServiceImplementationTypeName);
                */
            }

            _tracingService.Trace(TraceLevel.Verbose,
                                "{0}.InstantiateWCFServiceHost() [Complete] (ServiceContract.Name:'{1}')",
                                _TraceThisTypeName,
                                _ServiceContractTypeName);
        }


        private void InstantiateWCFServiceHost_UsingConfigFile()
        {
            _tracingService.Trace(TraceLevel.Verbose,
                                "{0}.InstantiateWCFServiceHost_UsingConfigFile(): Creating New ServiceHost  [Begin...]",
                                _TraceThisTypeName);


            //Method #3: Using a config file's settings
            _ServiceHost = new ServiceHost(_ServiceImplementationType); //Type of WCF Service Implementation

            _tracingService.Trace(TraceLevel.Verbose,
                                "{0}.InstantiateWCFServiceHost_UsingConfigFile(): Creating New ServiceHost [Complete]",
                                _TraceThisTypeName);


            //But this relies on the app.config file containing the
            //following equivalent to the above alternate examples:
            /*
  <system.serviceModel>
    <services>
      <service name="XAct.Messaging.Services.MessageDeliveryStatusReportingService"
               behaviorConfiguration="ServiceBehavior">
        <endpoint address="MessageDeliveryStatusReportingService.svc"
                  binding="netTcpBinding"
                  contract="XAct.Messaging.Services.IMessageDeliveryStatusReportingService"/>
        <endpoint address="mex"
                  contract="IMetadataExchange"
                  binding="mexTcpBinding" />
        <host>
          <baseAddresses>
            <add baseAddress="net.tcp://+:55003/"/>
          </baseAddresses>
        </host>

      </service>
    </services>
    <behaviors>
      <serviceBehaviors>
        <behavior name="ServiceBehavior">
          <serviceMetadata httpGetEnabled="false"/>
        </behavior>
      </serviceBehaviors>
    </behaviors>

  </system.serviceModel>
             */
        }


        /*
        private void InstantiateWCFServiceHost_UsingDefaultSettings()
        {

            //Method #1: Using a base address:

            string serviceName =
                _ServiceInstanceType.Name +
                ".svc";


            Tracing.WriteLineIf(
                        "Creating ServiceHost for {0} with base address {1}...",
                        serviceName,
                        _ServiceBaseAddress
                    );
            

            
            _ServiceHost =
                new ServiceHost(
                    _ServiceInstanceType, //Type of WCF Service
                    _ServiceBaseAddress  //Base address
                    );



            //Add Bindings
            NetTcpBinding tcpBinding =
                new NetTcpBinding(SecurityMode.Transport);
            // tcpBinding.PortSharingEnabled = true;
            //tcpBinding.PortSharingEnabled = true;


            Tracing.WriteLineIf(
                        "Adding Service EndPoint for {0} (Type:'{1}', Binding.Name:'{2}', ServiceName:'{3}')...",
                        serviceName,
                        _ServiceContractType,
                        tcpBinding.Name,
                        serviceName);




            //Servicename should look like (given 
            //XAct.Messaging.Services.DeliveryStatusReporting 
            //service contract implementation):
            //"MessageDeliveryStatusReportingService.svc"

            _ServiceHost.AddServiceEndpoint(
                _ServiceContractType,   //Contract
                tcpBinding,                 //Binding
                serviceName                //Address
                );


            //TIP: 
            //you can use either 
            //"MessageDeliveryStatusReportingService" or
            //"MessageDeliveryStatusReportingService.svc" 
            //but if you plan to switch back and forth between
            //this example (hardcoded), and 
            //Example 3, relying on web.config
            //without having to rebind("add service reference..."
            //the client each time, make sure that the 
            //endpoint's address is consistently the same
            //each time (ie, decide if are sticking *.svc on or 
            //not, and stick with it...
            //(since IIS's convention is to have *.svc stuck on the
            //end, I'm going to stick with that approach, to unify
            //things a bit...

            ServiceMetadataBehavior metadataBehavior =
                new ServiceMetadataBehavior();


            Tracing.WriteLineIf(
                    "Adding Mex Endpoint for discoverability...");

   

            //Add a MEX endpoint so that remote clients
            //can query and build services against it:
            _ServiceHost.AddServiceEndpoint(
                typeof(IMetadataExchange),      //Contract
                MetadataExchangeBindings.CreateMexTcpBinding(),//Binding
                "mex"                           //Address
                );

            try
            {
                //Add metaData behavior before adding metadata Endpoint that 
                //will refer to it:
                _ServiceHost.Description.Behaviors.Add(metadataBehavior);
            }
            catch (System.Exception E)
            {
                Debugging.WriteLine(TraceLevel.Error, "Could not associate a MEX to this endpoint '{0}'.", _ServiceContractTypeName);
                Debugging.WriteLine(TraceLevel.Error, E.Message);
            }

        }
        */


        /*
        #region IDisposable Members


        /// <summary>
        /// Performs application-defined tasks associated with freeing, 
        /// releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /*
        // The bulk of the clean-up code is implemented in Dispose(bool)
        protected virtual void Dispose(bool disposing)
        {
        if (disposing)
        {
            // free managed resources
            if (_ServiceHost != null)
            {
                _ServiceHost.Dispose();
                _ServiceHost = null;
            }
        }
        // free native resources if there are any.
        if (nativeResource != IntPtr.Zero)
        {
            Marshal.FreeHGlobal(nativeResource);
            nativeResource = IntPtr.Zero;
        }
        }
        #endregion
         */

        #region Helper Methods

        private static bool ServerServiceDefined(string serviceName)
        {
            ServiceElement resultServiceElement;
            return ServerServiceDefined(serviceName, out resultServiceElement);
        }

        private static bool ServerServiceDefined(string serviceName,
                                                 out ServiceElement resultServiceElement)
        {
            if (serviceName.IsNullOrEmpty())
            {
                throw new ArgumentNullException("serviceName");
            }
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            resultServiceElement = null;

            System.Configuration.Configuration configuration =
                ConfigurationManager.OpenExeConfiguration(
                    ConfigurationUserLevel.None);
            ConfigurationSectionGroup configurationSectionGroup =
                configuration.GetSectionGroup("system.serviceModel");

            if (configurationSectionGroup == null)
            {
                return false;
            }

            ServicesSection servicesSection =
                configurationSectionGroup.Sections["services"]
                as ServicesSection;

            if (servicesSection == null)
            {
                return false;
            }
            //clientSection.Endpoints[serviceName] does not work because            
            //key is not what you would expect...not name, address, but            
            //something like:            
            //"contractType:ServiceReference1.Contracts.IMessageClientService;            
            //  name:WSHttpBinding_IClientService"            
            foreach (ServiceElement serviceElement in servicesSection.Services)
            {
                if (serviceElement.Name == serviceName)
                {
                    resultServiceElement = serviceElement;
                    break;
                }
            }
            if (resultServiceElement == null)
            {
                return false;
            }
            return true;
        }

        private static bool DoesEndpointHaveBehavior(
            ChannelEndpointElement endpointConfiguration)
        {
            EndpointBehaviorElement foundBehaviorElement;
            return DoesEndpointHaveBehavior(
                endpointConfiguration,
                out foundBehaviorElement);
        }

        private static bool DoesEndpointHaveBehavior(
            ChannelEndpointElement endpointConfiguration,
            out EndpointBehaviorElement foundBehaviorElement)
        {
            string behaviourConfigurationName =
                endpointConfiguration.BehaviorConfiguration;
            foundBehaviorElement = null;

            if (string.IsNullOrEmpty(behaviourConfigurationName))
            {
                return true;
            }

            System.Configuration.Configuration configuration =
                ConfigurationManager.OpenExeConfiguration(
                    ConfigurationUserLevel.None
                    );
            ConfigurationSectionGroup configurationSectionGroup =
                configuration.GetSectionGroup("system.serviceModel");
            if (configurationSectionGroup == null)
            {
                return false;
            }
            BehaviorsSection behaviorSection =
                configurationSectionGroup.Sections["behaviors"]
                as BehaviorsSection;
            foreach (EndpointBehaviorElement behaviorElement in
                behaviorSection.EndpointBehaviors)
            {
                if (behaviorElement.Name == behaviourConfigurationName)
                {
                    foundBehaviorElement = behaviorElement;
                    break;
                }
            }
            if (foundBehaviorElement == null)
            {
                return false;
            }
            return true;
        }

        #endregion
    }


}