﻿namespace XAct.Services.Comm.ServiceModel
{
    using System;
    using System.Globalization;
    using System.ServiceModel;

    /// <summary>
    /// 
    /// </summary>
#pragma warning disable 1591
    public static class SoapExceptionFactory
    {
        public static /*FaultException<AppSoapException>*/ Exception CreateFaultException(bool isPrivateInterTierApi,
                                                                                          Exception exception = null,
                                                                                          bool includeStack = true)
        {
            return CreateFaultException(isPrivateInterTierApi, null, exception, includeStack);
        }




        public static  /*FaultException<AppSoapException>*/ Exception CreateFaultException(bool isPrivateApi,
                                                                                            FaultReason faultReason,
                                                                                            Exception exception = null,
                                                                                            bool includeStack = true
            )
        {

            //If we are not running in a WCF Context, we are in same Process, 
            //so return the exception:
            if (OperationContext.Current == null)
            {
                //Note that 'throw e;' (versus just 'throw;') will break stack trace, 
                //but that's the best this can do (it would be better if invoker
                //were to check context first and 'throw;', but that would pollute
                //everything with a mention of soap...meh.
                return exception;
            }

            //Create a unique Ticket Id:
            Guid uniqueTicketId = Guid.NewGuid();
            //Log the Exception against the Guid

            //Createa a new SOAP Exception,
            //that will or will not contain the serialized Exception:
            AppSoapException faultDataContractPayload =
                new AppSoapException(
                    isPrivateApi,
                    exception,
                    true);


            faultReason = CreateFaultReason(exception, faultReason, isPrivateApi, uniqueTicketId);


            Exception result;

            if (exception != null)
            {
                result = new FaultException<AppSoapException>(
                    faultDataContractPayload,
                    faultReason, //new FaultReason(typeof(TException).Name),
                    new FaultCode(exception.GetType().Name)
                    );
            }
            else
            {
                result = new FaultException<AppSoapException>(
                    faultDataContractPayload,
                    faultReason //new FaultReason(typeof(TException).Name),
                    );
            }

            return result;
        }


        private static FaultReason CreateFaultReason(Exception exception, FaultReason faultReason, bool isPrivateInterTierApi,
                                                     Guid uniqueTicketId)
        {
            if (faultReason != null && !isPrivateInterTierApi)
            {
                //If there is one, use it, as long as not private
                //(if private, we will want to log it, hide everything,
                //and give out a rather lame message):
                return faultReason;
            }


            //If private API (between Tiers, etc.), it's safe, to 
            //expose Exception info if any. 
            //Otherwise, shield it with an inocuous message:
            string message;
            if (!isPrivateInterTierApi)
            {

                message = string.Format(CultureInfo.InvariantCulture,
                                        "An unhandled error occurred on the server. {0}Contact us with the following Ref:{0}Ticket #: {1}",
                                        System.Environment.NewLine, uniqueTicketId);
            }
            else
            {

                if (exception == null)
                {
                    message = string.Format(CultureInfo.InvariantCulture,
                                            "Exception: " + "N/A" + System.Environment.NewLine +
                                            "Message: " + "N/A" + System.Environment.NewLine +
                                            "Source: " + "N/A" + System.Environment.NewLine +
                                            "Ticket: " + uniqueTicketId);
                }
                else
                {
                    message = string.Format(CultureInfo.InvariantCulture,
                                            "Exception:" + exception.GetType().Name + System.Environment.NewLine +
                                            "Message:" + CreateNestedExceptionMessage(exception) + System.Environment.NewLine +
                                            "Source: " + exception.Source + System.Environment.NewLine +
                                            "Ticket: " + uniqueTicketId);
                }

            }


            faultReason = new FaultReason(
                new FaultReasonText(
                    message,
                    CultureInfo.InvariantCulture));


            return faultReason;
        }

        /// <summary>
        /// Creates a formatted, single line string of the exception ([Type] Message).
        /// Calls recursively through the InnerException, putting each on a tabbed new line.
        /// </summary>
        /// <param name="exception"></param>
        /// <param name="exceptionLevel"></param>
        /// <returns></returns>
        private static string CreateNestedExceptionMessage(Exception exception, int exceptionLevel = 1)
        {
            // For nested exceptions, start with indented line + nested level number
            var preamble = exceptionLevel == 1 ? string.Empty : string.Format("\n\t(Level {0})\t", exceptionLevel);

            var message = string.Format("{3} [{0} - {1}] {2}", exception.GetType().Name, exception.Source, exception.Message, preamble);

            // Stop at leaf nodes (no more nested InnerException)
            if (exception.InnerException == null)
            {
                return message;
            }

            // Recurse, getting the nested exceptions
            var nestedMessage = CreateNestedExceptionMessage(exception.InnerException, exceptionLevel + 1);

            return message + nestedMessage;
        }
    }
}
#pragma warning restore 1591

