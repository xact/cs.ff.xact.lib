﻿namespace XAct.Services.Comm.ServiceModel
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using XAct.Diagnostics;

    /// <summary>
    /// </summary>
    public interface IWCFServiceHosts
    {
        /// <summary>
        ///   Registers the WCF Services that will neeed hosting.
        /// </summary>
        /// <param name = "wcfServiceMode"></param>
        /// <param name = "wcfServiceContractType"></param>
        /// <param name = "wcfServiceInstanceType"></param>
        void RegisterWCFServiceHost(WCFServiceHostsModes wcfServiceMode, Type wcfServiceContractType,
                                    Type wcfServiceInstanceType);


        /// <summary>
        ///   Instantiates the WCF ServiceHosts needed to host the WCF Services.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Note that you must invoke <see cref = "RegisterWCFServiceHost" />
        ///     at least once before hand, or this method won't have anything to instantiate.
        ///   </para>
        /// </remarks>
        void InstantiateWCFServiceHosts();

        /// <summary>
        ///   Starts the WCF Services hosted by the one or more instantiated WCFServiceHost.
        /// </summary>
        void Start();

        /// <summary>
        ///   Stops the WCF Services hosted by the one or more instantiated WCFServiceHost.
        /// </summary>
        void Stop();
    }

    /// <summary>
    ///   TODO
    /// </summary>
    public class WCFServiceHosts : IWCFServiceHosts
    {
        private readonly Dictionary<Type, WcfInfo> _WCFServiceDefinitions = new Dictionary<Type, WcfInfo>();
        private readonly List<WcfInfo> _WCFServiceHosts = new List<WcfInfo>();
        private bool _Initialized;
        private WCFServiceHostsModes _WCFServiceHostsMode;

        #region Constructors

        /// <summary>
        ///   TODO
        /// </summary>
        /// <param name = "tracingService"></param>
        public WCFServiceHosts(ITracingService tracingService)
        {
            _tracingService = tracingService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        ///   Registers the WCF Services that will neeed hosting.
        /// </summary>
        /// <param name = "wcfServiceMode"></param>
        /// <param name = "wcfServiceContractType"></param>
        /// <param name = "wcfServiceInstanceType"></param>
        public void RegisterWCFServiceHost(WCFServiceHostsModes wcfServiceMode, Type wcfServiceContractType,
                                           Type wcfServiceInstanceType)
        {
            _WCFServiceDefinitions[wcfServiceContractType] =
                new WcfInfo(wcfServiceMode, wcfServiceContractType, wcfServiceInstanceType);
        }


        /// <summary>
        ///   Instantiates the WCF ServiceHosts needed to host the WCF Services.
        /// </summary>
        /// <remarks>
        ///   Note that you must invoke <see cref = "RegisterWCFServiceHost" />
        ///   at least once before hand, or this method won't have anything to instantiate.
        /// </remarks>
        public void InstantiateWCFServiceHosts()
        {
            foreach (WcfInfo wcfInfo in _WCFServiceDefinitions.Values)
            {
                LogMessage(TraceLevel.Verbose,
                           "Creating WCFServiceHost to host '{0}' ServiceContract, implemented as '{1}' [Begin...]",
                           wcfInfo.ServiceContractType.Name,
                           wcfInfo.ServiceType.FullName);

                //if (CheckManagementMode(_WCFServiceHostsMode, WCFServiceHostsModes.Client))
                //{
                wcfInfo.ServiceHost =
                    new WCFServiceHost(_tracingService, wcfInfo.ServiceContractType, wcfInfo.ServiceType);
                //}
                _WCFServiceHosts.Add(wcfInfo);
            }
            //Add to collection used to later turn on/off all running services:

            _Initialized = true;
        }


        /// <summary>
        ///   Starts the service hosts.
        /// </summary>
        public void Start()
        {
            if (!_Initialized)
            {
                InstantiateWCFServiceHosts();
            }
            LogMessage(TraceLevel.Verbose, "WindowsService.OnStart() [Begin...]");


            foreach (WcfInfo wcfInfo in _WCFServiceHosts)
            {
                try
                {
                    wcfInfo.ServiceHost.Open();
                }
                catch (Exception e)
                {
                    //Guess nothing is perfect...
                    LogException(e, "An exception occurred while processing WCFServiceHost.OnStart()");

                    //Pass it up:
                    //throw;
                }
            }

            LogMessage(TraceLevel.Verbose, "WindowsService.OnStart() [Complete]");
        }


        /// <summary>
        ///   Stops the service hosts.
        /// </summary>
        public void Stop()
        {
            LogMessage(TraceLevel.Verbose, "WindowsService.OnStop() [Begin...]");

            foreach (IWCFServiceHost wcfServiceHost in _WCFServiceHosts)
            {
                try
                {
                    wcfServiceHost.Close();
                }
                catch (Exception e)
                {
                    //Guess nothing is perfect...
                    LogException(e, "An exception occurred while processing WCFServiceHost.OnStop()");

                    //Pass it up:
                    //throw;
                }
            }

            LogMessage(TraceLevel.Verbose, "WindowsService.OnStop() [Complete]");
        }

        /// <summary>
        ///   TODO
        /// </summary>
        public void BuildServiceHosts(WCFServiceHostsModes wcfServiceHostsMode)
        {
            try
            {
                _WCFServiceHostsMode = wcfServiceHostsMode;

#if DEBUG
                //                Debugger.Launch();
#endif


                LogMessage(TraceLevel.Verbose, "Building ServiceHosts [Begin...]");
                LogMessage(TraceLevel.Verbose, "Config File Path:'{0}'",
                           AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

                try
                {
                    System.Configuration.Configuration configFile = ConfigurationManager.OpenExeConfiguration(
                        AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

                    LogMessage(TraceLevel.Verbose, "Parsing of Config File was successful.");
                }
                catch (Exception exception)
                {
                    LogException(exception, "Parsing of Config File caused an exception.");

                    //Pass it up:
                    throw;
                }


                LogMessage(TraceLevel.Verbose, "Mode='{0}'", _WCFServiceHostsMode);
                LogMessage(TraceLevel.Verbose, "*) OfflineMode: {0}",
                           CheckManagementMode(_WCFServiceHostsMode, WCFServiceHostsModes.Offline));
                LogMessage(TraceLevel.Verbose, "*) AdminMode: {0}",
                           CheckManagementMode(_WCFServiceHostsMode, WCFServiceHostsModes.Admin));
                LogMessage(TraceLevel.Verbose, "*) PowerUserMode: {0}",
                           CheckManagementMode(_WCFServiceHostsMode, WCFServiceHostsModes.PowerUser));
                LogMessage(TraceLevel.Verbose, "*) PublicSetMode: {0}",
                           CheckManagementMode(_WCFServiceHostsMode, WCFServiceHostsModes.PublicSet));
                LogMessage(TraceLevel.Verbose, "*) PublicGetMode: {0}",
                           CheckManagementMode(_WCFServiceHostsMode, WCFServiceHostsModes.PublicGet));

                InstantiateWCFServiceHosts();

                LogMessage(TraceLevel.Verbose, "Adding WCFServiceHosts to host various ServiceContracts [Complete].");
                LogMessage(TraceLevel.Verbose, "BuildServiceHosts [Complete].");
                LogMessage(TraceLevel.Verbose, "========================================");
            }
            catch (Exception e)
            {
                LogException(e, "An exception has occurred while processing WCFServiceHost.Constructor()");
                //Pass it up:
                throw;
            }

            LogMessage(TraceLevel.Verbose, "WCFServiceHosts Initialized.");
        }

        #endregion

        #region Private methods

        /// <summary>
        ///   Helper method to check boolean Flags
        /// </summary>
        /// <param name = "wcfServiceHostsMode"></param>
        /// <param name = "flag"></param>
        /// <returns>true if the specified flag is set.</returns>
        private bool CheckManagementMode(WCFServiceHostsModes wcfServiceHostsMode, WCFServiceHostsModes flag)
        {
            return (wcfServiceHostsMode & flag) == flag;
        }

        private void LogMessage(TraceLevel level, string message, params object[] args)
        {
            _tracingService.Trace(level, message, args);
        }

        private void LogException(Exception e, string message, params object[] args)
        {
            _tracingService.TraceException(TraceLevel.Error, e, message, args);
        }

        #endregion

        #region Nested type: WcfInfo

        private class WcfInfo
        {
            public readonly Type ServiceContractType;
            public readonly Type ServiceType;
            public WCFServiceHostsModes Mode;
            public object Service;
            public IWCFServiceHost ServiceHost;

            public WcfInfo(WCFServiceHostsModes wcfServiceHostsMode, Type serviceContractType, Type serviceType)
            {
                Mode = wcfServiceHostsMode;
                ServiceContractType = serviceContractType;
                ServiceType = serviceType;
                Service = null;
                ServiceHost = null;
            }
        }

        #endregion

        #region Services

        private readonly ITracingService _tracingService;

        #endregion
    }
}