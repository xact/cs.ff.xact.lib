namespace XAct.Services.Comm.ServiceModel
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class BindingTimeOutConfiguration
    {
        /// <summary>
        /// Gets or sets the open timeout seconds.
        /// <para>
        /// Default Value is 30 seconds.
        /// </para>
        /// </summary>
        /// <value>
        /// The open timeout seconds.
        /// </value>
        public TimeSpan OpenTimeout { get; set; }

        /// <summary>
        /// Gets or sets the send timeout.
        /// <para>
        /// Default Value is 30 seconds.
        /// </para>
        /// </summary>
        /// <value>
        /// The send timeout.
        /// </value>
        public TimeSpan SendTimeout{ get; set; }

        /// <summary>
        /// Gets or sets the receive timeout.
        /// <para>
        /// Default Value is 300 seconds (5 minutes).
        /// </para>
        /// </summary>
        /// <value>
        /// The receive timeout.
        /// </value>
        public TimeSpan ReceiveTimeout { get; set; }

        /// <summary>
        /// Gets or sets the close timeout.
        /// <para>
        /// Default Value is 30 seconds.
        /// </para>
        /// </summary>
        /// <value>
        /// The close timeout.
        /// </value>
        public TimeSpan CloseTimeout { get; set; }



        /// <summary>
        /// Initializes a new instance of the <see cref="BindingTimeOutConfiguration"/> class.
        /// </summary>
        public BindingTimeOutConfiguration()
        {
            // Set Defaults
            this.OpenTimeout = TimeSpan.FromSeconds(30);
            this.SendTimeout= TimeSpan.FromSeconds(30);
            this.ReceiveTimeout= TimeSpan.FromSeconds(600);
            this.CloseTimeout= TimeSpan.FromSeconds(30);
        }
    }
}