﻿namespace XAct.Services.IoC
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net.Http.Formatting;
    using System.Web.Http.Controllers;
    using System.Web.Http.Dependencies;
    using System.Web.Http.Dispatcher;
    using System.Web.Http.Hosting;
    using System.Web.Http.Metadata;
    using System.Web.Http.Tracing;
    using System.Web.Http.Validation;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class WebAPIServiceLocatorDependencyScope : IDependencyScope
    {
        private readonly ITracingService _tracingService;




        /// <summary>
        /// Initializes a new instance of the <see cref="WebAPIServiceLocatorDependencyScope"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public WebAPIServiceLocatorDependencyScope(ITracingService tracingService)
        {
            _tracingService = tracingService;

        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Retrieves a service from the scope.
        /// </summary>
        /// <param name="serviceType">The service to be retrieved.</param>
        /// <returns>
        /// The retrieved service.
        /// </returns>
        [DebuggerHidden]
        [DebuggerStepThrough]
        [DebuggerNonUserCode]
        public object GetService(Type serviceType)
        {
            if (IgnoredWebApiInterfaces.Contains(serviceType))
            {
                return null;
            }
            if (!_internalTypeAdded)
            {
                if (serviceType.FullName.Equals("System.Web.Http.Validation.IModelValidatorCache"))
                {

                    IgnoredWebApiInterfaces.Add(serviceType);
                    _internalTypeAdded = true;
                    return null;
                }
            }


            // Do normal service location:

            object result;
            try
            {

                result = DependencyResolver.Current.GetInstance(serviceType, false);
            }
            catch (Exception ex)
            {
                _tracingService.TraceException(
                    Diagnostics.TraceLevel.Warning,
                    ex,
                    "{0} - Unable to resolve type for '{1}'. (Check InnerException for nested failures)",
                    this.GetType().AssemblyQualifiedName,
                    serviceType.AssemblyQualifiedName
                    );

                throw;
            }

            return result;
        }

        // TODO: Is there a better way to have the dependency resolve deal with these weird default/bogus interfaces?
        private static bool _internalTypeAdded = false;
        private static List<Type> IgnoredWebApiInterfaces
        {
            get
            {

                // MVC/ASP asks for these interfaces - but they seem to have no implementations, or be needed.
                List<Type> webApiInterfacesWeIgnore = new List<Type>()
				    {
					    typeof (IHostBufferPolicySelector),
					    typeof (ModelMetadataProvider),
					    typeof (ITraceManager),
					    typeof (IHttpControllerSelector),
					    typeof (IAssembliesResolver),
					    typeof (IHttpControllerTypeResolver),
					    typeof (IHttpActionSelector),
					    typeof (IActionValueBinder),
					    typeof (IContentNegotiator),
					    typeof (IHttpActionInvoker),
					    typeof (ITraceWriter),
					    typeof (IHttpControllerActivator),
						typeof (IBodyModelValidator), 
					    //, typeof (System.Web.Http.Validation.IModelValidatorCache) // Can't ignore this one - internal interface. pointing to internal System.Web.Http.Validation.ModelValidatorCache
				    };
                return webApiInterfacesWeIgnore;
            }
        }

        /// <summary>
        /// Retrieves a collection of services from the scope.
        /// </summary>
        /// <param name="serviceType">The collection of services to be retrieved.</param>
        /// <returns>
        /// The retrieved collection of services.
        /// </returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            //using yield, so try/catch pointless.
            IEnumerable<object> result = XAct.DependencyResolver.Current.GetInstances(serviceType);
            return result;
        }
    }



    /// <summary>
    /// 
    /// </summary>
    public class WebAPIServiceLocatorDependencyResolver : WebAPIServiceLocatorDependencyScope, System.Web.Http.Dependencies.IDependencyResolver
    {
        private readonly ITracingService _tracingService;
        //System.Web.Http.Dependencies.IDependencyResolver _previousDependencyResolver
        //    = System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebAPIServiceLocatorDependencyResolver"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public WebAPIServiceLocatorDependencyResolver(ITracingService tracingService)
            : base(tracingService)
        {
            _tracingService = tracingService;
        }

        /// <summary>
        /// Starts a resolution scope.
        /// </summary>
        /// <returns>
        /// The dependency scope.
        /// </returns>
        public IDependencyScope BeginScope()
        {
            return new WebAPIServiceLocatorDependencyScope(_tracingService);
        }

    }

}