﻿namespace XAct.Http.Filters
{
    namespace WebAPI
    {
        using System.Net;
        using System.Net.Http;
        using System.Web.Http.Controllers;
        using System.Web.Http.Filters;

        /// <summary>
        /// An <see cref="ActionFilterAttribute"/> that ensures
        /// the method is invoked with a request that has the specified header
        /// <para>
        /// <code>
        /// <![CDATA[
        /// [HeaderRequired("SomeHeader")]
        /// void SomeMethod(){...}
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        public class HeaderRequiredAttribute : ActionFilterAttribute
        {
            private readonly string _tokenName;

            /// <summary>
            /// Initializes a new instance of the <see cref="HeaderRequiredAttribute"/> class.
            /// </summary>
            /// <param name="tokenName">Name of the token.</param>
            public HeaderRequiredAttribute(string tokenName)
            {
                _tokenName = tokenName;
            }

            /// <summary>
            /// Called when [action executing].
            /// </summary>
            /// <param name="actionContext">The action context.</param>
            public override void OnActionExecuting(HttpActionContext actionContext)
            {
                if (actionContext.Request.Headers.Contains(_tokenName))
                {
                    base.OnActionExecuting(actionContext);
                    return;
                }

                actionContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                             {
                                                 Content = new StringContent("Missing '{0}' Header".FormatStringCurrentUICulture(_tokenName))
                                             };
                return;
            }
        }
    }
}
