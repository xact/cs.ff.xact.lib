namespace XAct.Web.Http.Filters
{
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    /// <summary>
    /// An AuthorizationFilter
    /// <para>
    /// Reminder: <see cref="AuthorizationFilterAttribute"/>
    /// are Filters that run before other filters.
    /// </para>
    /// </summary>
    public abstract class AuthorizationTokenRequiredAttributeBase : AuthorizationFilterAttribute
    {
        private string _tokenName;

        /// <summary>
        /// Called to authorize the request before the Action is invoked.
        /// </summary>
        /// <param name="actionContext">The action context.</param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext != null)
            {
                if (!AuthorizeRequest(actionContext.ControllerContext.Request))
                {
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized) { RequestMessage = actionContext.ControllerContext.Request };
                }
                return;
            }
        }

        private bool AuthorizeRequest(HttpRequestMessage request)
        {
            IAuthorizationTokenRequiredAttributeConfiguration configuration =
                DependencyResolver.Current.GetInstance<IAuthorizationTokenRequiredAttributeConfiguration>();

            _tokenName = _tokenName ?? configuration.HeaderName;


            if (!request.Headers.Contains(_tokenName))
            {
                return false;
            }

            //IEnumerable<string> tokenValue = request.Headers.GetValues(_tokenName);

            if (!request.Headers.Contains(_tokenName))
            {
                return false;
            }
            //Do custom logic here:
            return CustomAuthorizationLogic(request);
        }

        /// <summary>
        /// The custom authorization logic.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        protected abstract bool CustomAuthorizationLogic(HttpRequestMessage request);
    }
}