﻿namespace XAct.Http.Filters
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    /// <summary>
    /// An ASP WebAPI <see cref="ActionFilterAttribute"/>
    /// to ensure the method is invoked only over an SSL line.
    /// <para>
    /// <example>
    /// <code>
    /// ...
    /// </code>
    /// </example>
    /// </para>
    /// </summary>
 public class HttpsRequiredAttribute : ActionFilterAttribute
 {
     /// <summary>
     /// Called just before the decorated ASP WebAPI method is invoked.
     /// </summary>
     /// <param name="actionContext">The action context.</param>
  public override void OnActionExecuting(HttpActionContext actionContext)
  {
      if (String.Equals(actionContext.Request.RequestUri.Scheme, "https", StringComparison.OrdinalIgnoreCase))
      {
          base.OnActionExecuting(actionContext);

          return;
      }

      actionContext.Response = new HttpResponseMessage(HttpStatusCode.BadRequest)
                                   {
                                       Content = new StringContent("HTTPS Required")
                                   };

      return;
  }
 }
}

