﻿namespace XAct.Web.Http.Filters
{
    using XAct.Services;

    /// <summary>
    /// Class of settings used to initialize
    ///      <see cref="AuthorizationTokenRequiredAttributeBase"/>
    /// <para>
    /// Default value is 'AuthenticationToken'
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IAuthorizationTokenRequiredAttributeConfiguration), BindingLifetimeType.Undefined, Priority.Low)]
    public class AuthorizationTokenRequiredAttributeConfiguration : IAuthorizationTokenRequiredAttributeConfiguration
    {
        /// <summary>
        /// Gets the name of the HTTP request header.
        /// <para>
        /// Default is 'AuthenticationToken':
        /// </para>
        /// </summary>
        /// <value>
        /// The name of the header.
        /// </value>
        public string HeaderName { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthorizationTokenRequiredAttributeConfiguration"/> class.
        /// </summary>
        public AuthorizationTokenRequiredAttributeConfiguration()
        {
            HeaderName = "AuthenticationToken";
        }
    }
}
