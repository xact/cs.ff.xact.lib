﻿namespace XAct.Web.Http.Filters
{
    /// <summary>
    /// Contract of settings used to initialize
    ///      <see cref="AuthorizationTokenRequiredAttributeBase"/>
    /// </summary>
    public interface IAuthorizationTokenRequiredAttributeConfiguration
    {
        /// <summary>
        /// Gets the name of the HTTP request header.
        /// <para>
        /// Default is 'AuthenticationToken':
        /// </para>
        /// </summary>
        /// <value>
        /// The name of the header.
        /// </value>
        string HeaderName { get; }
    }
}