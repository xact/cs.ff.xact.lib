using NUnit.Framework;
using XAct.Deployment.WiX;

namespace XAct.Deployment.WiX.Tests.UnitTests
{
    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class UnitTests1
    {
        #region Setup/Teardown

        /// <summary>
        ///   Sets up to do before each and every 
        ///   test within this test fixture is run.
        /// </summary>
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        ///   Tear down after each and every test.
        /// </summary>
        [TearDown]
        public void TearDown()
        {
        }

        #endregion

        #region Fixture Setup/Teardown
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
        }
        #endregion

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest01()
        {

            KW_PRIMARY ctrl = new KW_PRIMARY();
            Assert.IsTrue(true);
        }
    }

//Class:End
}

//Namespace:End