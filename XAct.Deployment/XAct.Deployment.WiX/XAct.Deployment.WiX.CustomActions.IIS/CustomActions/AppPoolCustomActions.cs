﻿//using System;
//using System.DirectoryServices;
//using Microsoft.Deployment.WindowsInstaller;

//namespace XAct.Deployment.WiX.CustomActions
//{


//    class AppPoolCustomActions
//    {

//        #region Public Static CustomActions
//        [CustomAction("BuildListOfIISAppPools")]
//        public static ActionResult BuildListOfIISAppPools(Session session)
//        {
//            session.Log("XAct.Deployment.WiX.CustomActions.BuildListOfWebSites(session) [START]");

//            try
//            {
//                //Call private static method:
//                BuildViews(session);
//            }
//            catch (Exception ex)
//            {
//                session.Log(string.Format("CustomActionException: {0}", ex));

//                session.Log("XAct.Deployment.WiX.CustomActions.BuildListOfWebSites(session) [FINISH] (Result={0})", ActionResult.Failure);

//                return ActionResult.Failure;
//            }

//            session.Log("XAct.Deployment.WiX.CustomActions.BuildListOfWebSites(session) [FINISH] (Result={0})", ActionResult.Success);
            
//            return ActionResult.Success;
//        }
//        #endregion

//        #region Private Methods


//        private static void BuildViews(Session session)
//        {
//            //Open two views to the msi database. 
//            //One for the prexisting table called 'ComboBox' (see: http://bit.ly/edQj9T)
//            //whose Schema is: 
//            //[Column Type Key Nullable] 
//            //Property Identifier  Y N 
//            //Order Integer  Y N 
//            //Value Formatted  N N 
//            //Text Text  N Y 


//            //and one for a CustomTable called _customTableAvailableWebSites
//            //that was created elsewhere, with the following CustomTable/EnsureTable elements:
//            //<CustomTable Id="_customTableAvailableAppPools" >
//            //  <Column Id="WebSiteNo" Category="Identifier" PrimaryKey="yes" Type="int" Width="4" />
//            //  <Column Id="WebSiteDescription" Category="Text" Type="string" PrimaryKey="no"/>
//            //  <Column Id="WebSitePort" Category="Text" Type="string" PrimaryKey="no"/>
//            //  <Column Id="WebSiteIP" Category="Text" Type="string" PrimaryKey="no" Nullable="yes"/>
//            //  <Column Id="WebSiteHeader" Category="Text" Type="string" PrimaryKey="no" Nullable="yes"/>
//            //</CustomTable>
//            //<EnsureTable Id="_customTableAvailableWebSites"/>

//            //A Quick note about the ComboBox table:
//            //A very weird/interesting thing about the WiX UI elements is 
//            //that it doesn't use a distinct list for each ComboBox displayed.
//            //All of ListItems - from *all* the distinct combos - are stored in the same table.
//            //The way that a combo pulls out the items for it is to look for all items
//            //with the same Property name.

//            session.Log("Querying comboBoxView...");
//            using (View comboBoxView = session.Database.OpenView("select * from ComboBox"))
//            {

//                session.Log("Querying availableWebSitesView...");

//                string query = "select * from _customTableAvailableAppPools";

//                using (View availableWebSitesView = session.Database.OpenView(
//                    query))
//                {

//                    //Get the root entry in IIS by using an LDAP query.
//                    session.Log("Querying Directory for IIS://localhost/W3SVC ...");

//                    string machineName = "localhost";
                    
//                    string directoryEntryPath = string.Format("IIS://{0}/W3SVC/AppPools",machineName);






//                    using (DirectoryEntry iisRoot = new DirectoryEntry(directoryEntryPath))
//                    {
//                        AppPoolSchema appPoolSchema = new AppPoolSchema();
                        


//                                            WL("=====");
//                    WL("{0}={1}","Name",childEntry.Name);
//                    WL("{0}={1}","PAth",childEntry.Path);
//                    WL("{0}={1}","Properties.Count",childEntry.Properties.Count);


//                    foreach(string propertyName in childEntry.Properties.PropertyNames) {
						
//                        PropertyValueCollection propertyValueCollection = childEntry.Properties[propertyName];
						
//                        foreach(object val  in propertyValueCollection){
//                          WL("{0}={1}", propertyName, val);
//                        }
//                    }
//                    //Not that it doesn't crash if missing Property:
//                    WL("{0}={1}","*MISSING", childEntry.Properties["MISSING"].Value);
//                    WL("{0}={1}","*AppPoolIdentityType", childEntry.Properties["AppPoolIdentityType"].Value);
//                    WL("{0}={1}","*WAMUserPassword", childEntry.Properties["WAMUserPass"].Value);
	



//                        int order = 1;

//                        session.Log(
//                            "Looping through {0} DirectoryEntries",
//                            iisRoot.Children);

//                        foreach (DirectoryEntry webSite in iisRoot.Children)
//                        {
//                            session.Log(
//                                "...WebSite: (SchemaClassName={0}, Name={1}",
//                                webSite.SchemaClassName, webSite.Name);

//                            string webSiteName = webSite.Name;

//                            if (webSite.SchemaClassName.ToLower() == "iiswebserver" &&
//                                webSiteName.ToLower() != "administration web site")
//                            {
//                                StoreWebSiteDataInComboBoxTable(
//                                    webSite,
//                                    order,
//                                    comboBoxView);

//                                StoreWebSiteDataInAvailableWebSitesTable(
//                                    webSite,
//                                    availableWebSitesView);

//                                order++;
//                            }
//                        }
//                    }
//                }
//            }
//        }

//        private void ListAppDomains()
//        {

//            string machine = "localhost";

//            using (DirectoryEntry appPoolsEntry = new DirectoryEntry(string.Format("IIS://{0}/W3SVC/AppPools", machine)))
//            {
//                foreach(DirectoryEntry childEntry in appPoolsEntry.Children)
//                {
//                    WL("=====");
//                    WL("{0}={1}", "Name", childEntry.Name);
//                    WL("{0}={1}", "PAth", childEntry.Path);
//                    WL("{0}={1}", "Properties.Count", childEntry.Properties.Count);


//                    foreach (string propertyName in childEntry.Properties.PropertyNames)
//                    {

//                        PropertyValueCollection propertyValueCollection = childEntry.Properties[propertyName];

//                        foreach (object val  in propertyValueCollection)
//                        {
//                            WL("{0}={1}", propertyName, val);
//                        }
//                    }
//                    //Not that it doesn't crash if missing Property:
//                    WL("{0}={1}", "*MISSING", childEntry.Properties["MISSING"].Value);
//                    WL("{0}={1}", "*AppPoolIdentityType", childEntry.Properties["AppPoolIdentityType"].Value);
//                    WL("{0}={1}", "*WAMUserPassword", childEntry.Properties["WAMUserPass"].Value);


////Name=DefaultAppPool
////PAth=IIS://localhost/W3SVC/AppPools/DefaultAppPool
////Properties.Count=6
////AppPoolIdentityType=4
////AppPoolState=2
////Win32Error=0
////AppPoolCommand=1
////KeyType=IIsApplicationPool
////ManagedRuntimeVersion=v4.0
////*MISSING=
////=====
////ManagedPipelineMode=1 <----- (not always)
////AppPoolAutoStart=True
////WAMUserName=EESAPPUSER
////WAMUserPass=M0catad1
////=====


//                    #endregion

//                }
//            }
