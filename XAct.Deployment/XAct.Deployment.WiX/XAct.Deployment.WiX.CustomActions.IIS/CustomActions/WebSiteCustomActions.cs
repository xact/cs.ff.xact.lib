﻿
using System;
using System.DirectoryServices;
using Microsoft.Deployment.WindowsInstaller;


namespace XAct.Deployment.WiX.CustomActions
{
    /// <summary>
    /// </summary>
    /// <remarks>
    /// This CustomAction is triggered by a 
    /// 
    /// in order to fill a CustomTable ('_customTableAvailableWebSites') that was created beforehand.
    /// </remarks>
    /// <internal>
    /// See: http://bit.ly/hdYmi9
    /// </internal>
    public class WebSiteCustomActions
    {

        #region Constants
        public enum ComboTableColumns
        {
            Key = 1,
            Order = 2,
            Value =3,
            Text = 4
            
        }

        public enum WebSiteTableColumns 
        {
            WebSiteNumber = 1,
            ServerComment =2,
            WebSitePort = 3,
            WebSiteIp = 4,
            WebSiteHeader = 5

        }            
        #endregion

        #region Public Static CustomActions
        [CustomAction("BuildListOfIISWebSiteNames")]
        public static ActionResult BuildListOfWebSites(Session session)
        {
            session.Log("XAct.Deployment.WiX.CustomActions.BuildListOfWebSites(session) [START]");

            try
            {
                //Call private static method:
                BuildViews(session);
            }
            catch (Exception ex)
            {
                session.Log(string.Format("CustomActionException: {0}", ex));

                session.Log("XAct.Deployment.WiX.CustomActions.BuildListOfWebSites(session) [FINISH] (Result={0})", ActionResult.Failure);

                return ActionResult.Failure;
            }

            session.Log("XAct.Deployment.WiX.CustomActions.BuildListOfWebSites(session) [FINISH] (Result={0})", ActionResult.Success);
            
            return ActionResult.Success;
        }


        [CustomAction("UpdatePropsWithSelectedWebSite")]
        public static ActionResult UpdatePropsWithSelectedWebSite(Session session)
        {
            session.Log("XAct.Deployment.WiX.CustomActions.UpdatePropsWithSelectedWebSite(session) [BEGIN]");

            try
            {
                //Get the value that was set by the ComboBox:
                //<UI>
                //  <Dialog Id="SelectWebSiteDlg" Width="370" Height="270" Title="Select WebSite">
                //   <Control Id="SelectWebSiteCombo" Type="ComboBox" Sorted="yes"
                //            X="20" Y="75" Width="200" Height="24" 
                //            Property="WEBSITE" />
                //   ...
                //  </Dialog>
                //</UI>
  
                string selectedWebSiteId = session["WEBSITE"];

                session.Log(string.Format("CA: Found web site id: {0}",selectedWebSiteId));

                string query = string.Format(
                    "Select * from _customTableAvailableWebSites where WebSiteNo={0}", 
                    selectedWebSiteId);

                View availableWebSitesView = session.Database.OpenView(query);
                
                availableWebSitesView.Execute();
                
                Record record = availableWebSitesView.Fetch();
                
                if ((record[1].ToString()) == selectedWebSiteId)
                {
                    //Save to Properties that we have defined elsewhere as follows:
                    //<Property Id="WEBSITE_DESCRIPTION">
                    //  <RegistrySearch Id="WebSiteDescription" Name="WebSiteDescription" Type="raw" 
                    //                  Root="HKLM" 
                    //                  Key="Software\$(var.PRODUCTMANUFACTURER)\$(var.PRODUCTTARGETNAME)\$(var.PRODUCTVERSION_X)\Install"/>
                    //</Property>
                    //<Property Id="WEBSITE_PORT">
                    //  <RegistrySearch Id="WebSitePort" Type="raw" Name="WebSitePort" 
                    //                  Root="HKLM" 
                    //                  Key="Software\$(var.PRODUCTMANUFACTURER)\$(var.PRODUCTTARGETNAME)\$(var.PRODUCTVERSION_X)\Install"/>
                    //</Property>
                    //<Property Id="WEBSITE_IP">
                    //  <RegistrySearch Id="WebSiteIP" Name="WebSiteIP" Type="raw" 
                    //                  Root="HKLM" 
                    //                  Key="Software\$(var.PRODUCTMANUFACTURER)\$(var.PRODUCTTARGETNAME)\$(var.PRODUCTVERSION_X)\Install"/>
                    //</Property>
                    //<Property Id="WEBSITE_HEADER">
                    //  <RegistrySearch Id="WebSiteHeader" Name="WebSiteHeader" Type="raw" 
                    //                  Root="HKLM" 
                    //                  Key="Software\$(var.PRODUCTMANUFACTURER)\$(var.PRODUCTTARGETNAME)\$(var.PRODUCTVERSION_X)\Install"/>
                    //</Property>
                    //
                    //<Component Id="_componentPersistWebSiteValues" Guid="*">
                    //  <RegistryKey Root="HKLM" 
                    //      Key="Software\$(var.PRODUCTMANUFACTURER)\$(var.PRODUCTTARGETNAME)\$(var.PRODUCTVERSION_X)\Install">
                    //    <RegistryValue Name="WebSiteDescription" Type="string" Value="[WEBSITE_DESCRIPTION]"/>
                    //    <RegistryValue Name="WebSitePort" Type="string" Value="[WEBSITE_PORT]"/>
                    //    <RegistryValue Name="WebSiteIP" Type="string" Value="[WEBSITE_IP]"/>
                    //    <RegistryValue Name="WebSiteHeader" Type="string" Value="[WEBSITE_HEADER]"/>
                    //  </RegistryKey>
                    //</Component>
                    //
                    //<ComponentRef Id="_componentPersistWebSiteValues" />
                    
                    session["WEBSITE_DESCRIPTION"] = (string)record[2];
                    session["WEBSITE_PORT"] = (string)record[3];
                    session["WEBSITE_IP"] = (string)record[4];
                    session["WEBSITE_HEADER"] = (string)record[5];
                }
            }
            catch (Exception ex)
            {
                session.Log("XAct.Deployment.WiX.CustomActions.UpdatePropsWithSelectedWebSite(session) [FINISH] (Result={0})", ActionResult.Failure);
                return ActionResult.Failure;
            }
            session.Log("XAct.Deployment.WiX.CustomActions.UpdatePropsWithSelectedWebSite(session) [FINISH] (Result={0})", ActionResult.Success);
            return ActionResult.Success;
        }

#endregion


        #region Private Methods
        private static void BuildViews(Session session)
        {
            //Open two views to the msi database. 
            //One for the prexisting table called 'ComboBox' (see: http://bit.ly/edQj9T)
            //whose Schema is: 
            //[Column Type Key Nullable] 
            //Property Identifier  Y N 
            //Order Integer  Y N 
            //Value Formatted  N N 
            //Text Text  N Y 


            //and one for a CustomTable called _customTableAvailableWebSites
            //that was created elsewhere, with the following CustomTable/EnsureTable elements:
            //<CustomTable Id="_customTableAvailableWebSites" >
            //  <Column Id="WebSiteNo" Category="Identifier" PrimaryKey="yes" Type="int" Width="4" />
            //  <Column Id="WebSiteDescription" Category="Text" Type="string" PrimaryKey="no"/>
            //  <Column Id="WebSitePort" Category="Text" Type="string" PrimaryKey="no"/>
            //  <Column Id="WebSiteIP" Category="Text" Type="string" PrimaryKey="no" Nullable="yes"/>
            //  <Column Id="WebSiteHeader" Category="Text" Type="string" PrimaryKey="no" Nullable="yes"/>
            //</CustomTable>
            //<EnsureTable Id="_customTableAvailableWebSites"/>

            //A Quick note about the ComboBox table:
            //A very weird/interesting thing about the WiX UI elements is 
            //that it doesn't use a distinct list for each ComboBox displayed.
            //All of ListItems - from *all* the distinct combos - are stored in the same table.
            //The way that a combo pulls out the items for it is to look for all items
            //with the same Property name.

            session.Log("Querying comboBoxView...");
            using (View comboBoxView = session.Database.OpenView("select * from ComboBox"))
            {

                session.Log("Querying availableWebSitesView...");

                string query = "select * from _customTableAvailableWebSites";
                using (View availableWebSitesView = session.Database.OpenView(
                    query))
                {

                    const string path = "IIS://localhost/W3SVC";

                    //Get the root entry in IIS by using an LDAP query.
                    session.Log("Querying Directory for '{0}' ...",path);

                    using (DirectoryEntry iisRoot = new DirectoryEntry(path))
                    {

                        //Combo Order is 1 based, not zero based.
                        int order = 1;

                        session.Log(
                            "Looping through {0} DirectoryEntries",
                            iisRoot.Children);

                        foreach (DirectoryEntry webSite in iisRoot.Children)
                        {
                            session.Log(
                                "...WebSite: (SchemaClassName={0}, Name={1}",
                                webSite.SchemaClassName, webSite.Name);

                            string webSiteName = webSite.Name;

                            if (webSite.SchemaClassName.ToLower() == "iiswebserver" &&
                                webSiteName.ToLower() != "administration web site")
                            {
                                StoreWebSiteDataInComboBoxTable(
                                    session,
                                    webSite,
                                    order,
                                    comboBoxView);

                                StoreWebSiteDataInAvailableWebSitesTable(
                                    session,
                                    webSite,
                                    availableWebSitesView);

                                //Increment index used to place order of 
                                //elements within combo
                                order++;
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="webSite"></param>
        /// <param name="order"></param>
        /// <param name="comboBoxView"></param>
        /// <internal>
        /// </internal>
        private static void StoreWebSiteDataInComboBoxTable(Session session, DirectoryEntry webSite, int order, View comboBoxView)
        {
            //Create a new Record with the request number of Fields for the 
            //ComboBox table: http://bit.ly/edQj9T
            Record newComboBoxRecord = new Record(4);

            //Using schema of ComboBox table, which is:
            //  [Column Type Key Nullable] 
            //  Property Identifier  Y N 
            //  Order Integer  Y N 
            //  Value Formatted  N N 
            //  Text Text  N Y 
            //fill in fields (notice Index starts at 1):
            newComboBoxRecord[(int)ComboTableColumns.Key] = "WEBSITE"; //Property must be same for every Element in same Listbox.
            newComboBoxRecord[(int)ComboTableColumns.Order] = order; //Order in which to display name
            newComboBoxRecord[(int)ComboTableColumns.Value] = webSite.Name; //returned Listitem Value
            newComboBoxRecord[(int)ComboTableColumns.Text] = webSite.Properties["ServerComment"].Value; //Visible Listitem Text

            //Use the new ListItem record to update the View:
            //Note: 
            // use InsertTemporary as not allowed to write permanent data 
            //to the MSI database during installation.
            comboBoxView.Modify(ViewModifyMode.InsertTemporary, newComboBoxRecord);
        }

        private static void StoreWebSiteDataInAvailableWebSitesTable(Session session, DirectoryEntry webSite, View availableWebSitesView)
        {
            //Parse the Get Ip, Port and Header from server bindings (format is 'ip:port:header')
            string serverBindingsString = webSite.Properties["ServerBindings"].Value as string;
            
            if (serverBindingsString == null)
            {
                session.Log(
                    "Error; No 'ServerBindings' found for {0}",
                    webSite.Name);

                throw new ArgumentException("website");
            }
            string[] serverBindings = serverBindingsString.Split(':');

            string ip = serverBindings[0];
            string port = serverBindings[1];
            string header = serverBindings[2];

            //Create a new Record with the request number of Fields:
            Record newFoundWebSiteRecord = new Record(5);

            //Fill in fields (notice Index starts at 1):

            newFoundWebSiteRecord[(int)WebSiteTableColumns.WebSiteNumber] = webSite.Name; //WebSiteNo
            newFoundWebSiteRecord[(int)WebSiteTableColumns.ServerComment] = webSite.Properties["ServerComment"].Value; //WebSiteDescription
            newFoundWebSiteRecord[(int)WebSiteTableColumns.WebSitePort] = port; //WebSitePort
            newFoundWebSiteRecord[(int)WebSiteTableColumns.WebSiteIp] = ip; //WebSiteIP
            newFoundWebSiteRecord[(int)WebSiteTableColumns.WebSiteHeader] = header; //WebSiteHeader

            //Update the given View:
            //Note: 
            // use InsertTemporary as not allowed to write permanent data 
            //to the MSI database during installation.
            availableWebSitesView.Modify(ViewModifyMode.InsertTemporary, newFoundWebSiteRecord);
        }
        #endregion
    }
}
