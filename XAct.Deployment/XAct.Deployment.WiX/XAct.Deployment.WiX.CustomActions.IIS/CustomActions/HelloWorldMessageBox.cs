﻿using System;
using System.Windows.Forms;
using Microsoft.Deployment.WindowsInstaller;

namespace XAct.Deployment.WiX.CustomActions
{


    public class ExampleCustomActions
    {

        #region Public Static CustomActions
        [CustomAction("SayHi")]
        public static ActionResult SayHello(Session session)
        {
            MessageBox.Show("Just a test...Hi!");

            return ActionResult.Success;
        }
        #endregion
    }
}
