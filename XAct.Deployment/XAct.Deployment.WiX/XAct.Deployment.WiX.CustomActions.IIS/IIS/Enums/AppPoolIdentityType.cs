﻿
namespace XAct.Deployment.WiX.CustomActions
{
    /// <summary>
    /// The Type of App Pool Identity.
    /// </summary>
    public enum AppPoolIdentityType
    {
        /// <summary>
        /// 
        /// </summary>
        LocalSystem = 0,
        /// <summary>
        /// 
        /// </summary>
        LocalService = 1,
        /// <summary>
        /// 
        /// </summary>
        NetworkService = 2,
        /// <summary>
        /// <para>
        /// If set, a User Name/Password will also be set.
        /// </para>
        /// </summary>
        SpecifiedUserAccount = 3,
        /// <summary>
        /// Virtual AppPoolIdentity.
        /// <para>
        /// See: <see cref="http://bit.ly/fvplea"/>
        /// </para>
        /// </summary>
        AppPoolIdentity=4,
    }
}
