
namespace XAct.Deployment.WiX.CustomActions
{
    /// <summary>
    /// State of an AppPool 
    /// (Stopped, Starting, Started, etc.)
    /// <para>
    /// Used by <see cref="IAppPool.State"/>
    /// </para>
    /// </summary>
    public enum AppPoolState
    {
        /// <summary>
        /// Error condition.
        /// <para>Value:0</para>
        /// </summary>
        Unknown = 0,

        /// <summary>
        /// The AppPool is starting.
        /// <para>Value:1</para>
        /// </summary>
        Starting = 1,

        /// <summary>
        /// The AppPool has started.
        /// <para>Value:2</para>
        /// </summary>
        Started = 2,

        /// <summary>
        /// The AppPool is stopping.
        /// <para>Value:3</para>
        /// </summary>
        Stopping = 3,

        /// <summary>
        /// The AppPool has stopped.
        /// <para>Value:4</para>
        /// </summary>
        Stopped = 4,
    }
}
