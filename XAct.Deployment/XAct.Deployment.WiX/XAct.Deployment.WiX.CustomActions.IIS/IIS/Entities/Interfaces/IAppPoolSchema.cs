﻿
namespace XAct.Deployment.WiX.CustomActions
{
    public interface IAppPoolSchema
    {
        /// <summary>
        /// Name of Application Pool
        /// eg: DefaultAppPool
        /// </summary>
                string Name { get; }


                /// <summary>
                /// DirectoryServices Path to Application Pool
                /// <para>
                /// eg: IIS://localhost/W3SVC/AppPools/DefaultAppPool
                /// </para>
                /// </summary>
                /// <value>The path.</value>
                string DirectoryServicesPath { get; }


        AppPoolIdentityType IdentityType { get; }


        AppPoolState State { get; }

        string Command { get; }

        string KeyType { get; }

        /// <summary>
        /// Gets the managed runtime version.
        /// <para>
        /// eg: 'v4.0'
        /// </para>
        /// </summary>
        /// <value>The managed runtime version.</value>
        string ManagedRuntimeVersion { get; }

        /// <summary>
        /// Gets a value indicating whether the AppPool is set to AutoStart.
        /// </summary>
        /// <value><c>true</c> if [app pool auto start]; otherwise, <c>false</c>.</value>
        bool AppPoolAutoStart { get; }
        
        /// <summary>
        /// Gets the name of the App Pool Identity
        /// if <see cref="IdentityType"/> = <see cref="AppPoolIdentityType.SpecifiedUserAccount"/>.
        /// </summary>
        /// <value>The name of the WAM user.</value>
        string WAMUserName { get; }

        /// <summary>
        /// Gets the password of the App Pool Identity
        /// if <see cref="IdentityType"/> = <see cref="AppPoolIdentityType.SpecifiedUserAccount"/>.
        /// </summary>
        /// <value>The WAM user password.</value>
        string WAMUserPassword { get; }


    }
}
