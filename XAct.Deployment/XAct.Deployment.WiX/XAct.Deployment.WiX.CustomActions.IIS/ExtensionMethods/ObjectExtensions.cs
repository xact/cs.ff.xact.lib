﻿
namespace XAct
{
    internal static class ObjectExtensions
    {
        /// <summary>
        ///   Throws an exception if the argument is <c>null</c>.
        /// </summary>
        /// <param name = "argument">The argument to check.</param>
        /// <remarks>
        ///   <para>
        ///     Used to validate method arguments.
        ///   </para>
        /// </remarks>
        public static bool IsNotNull(this object argument)
        {
            return (argument != null);
        }

    }
}
