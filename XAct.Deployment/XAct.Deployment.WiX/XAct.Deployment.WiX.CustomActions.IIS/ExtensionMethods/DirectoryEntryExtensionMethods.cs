﻿using System;
using System.DirectoryServices;
using XAct;

namespace XAct.Deployment.WiX
{
    public static class DirectoryEntryExtensionMethods
    {
        public static TValue GetTypedValue<TValue>(this DirectoryEntry directoryEntry, string propertyName)
        {
            Type t = typeof (TValue);
            object o = directoryEntry.Properties[propertyName].Value;
            return (TValue)XAct.TypeExtensions.ConvertTo(o,t);
        }
    }
}
