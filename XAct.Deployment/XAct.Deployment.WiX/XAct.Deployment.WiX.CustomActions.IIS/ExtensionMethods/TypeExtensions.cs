﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace XAct
{
    internal static class TypeExtensions
    {
        /// <summary>
        ///   Helper method to convert a type to another type.
        ///   Note how this is slightly better than the tradional
        ///   'ConvertTo' method, which returns all as an object that 
        ///   then needs to be boxed.
        /// </summary>
        /// <param name = "value">The value.</param>
        /// <returns></returns>
        public static TOut ConvertTo<TIn, TOut>(this TIn value)
        {
            return (TOut)value.ConvertTo(typeof(TOut));
        }

        /// <summary>
        ///   Connverts a value from given type to the destination type.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Can accept <c>null</c> and <c>DbNull.Value</c> (returns <c>null</c>).
        ///   </para>
        ///   <para>
        ///     Converts Guid using ToString("D") format, 
        ///     and can recreate Guid from string
        ///     in all Formats.
        ///   </para>
        /// </remarks>
        /// <param name = "value">The value to convert.</param>
        /// <returns>The value converted to the destination type.</returns>
        public static TDestination ConvertTo<TDestination>(this object value)
        {
            return (TDestination)value.ConvertTo(typeof(TDestination));
        }


        /// <summary>
        ///   Connverts a value from given type to the destination type.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Can accept <c>null</c> and <c>DbNull.Value</c> (returns <c>null</c>).
        ///   </para>
        /// </remarks>
        /// <param name = "value">The value to convert.</param>
        /// <param name = "destinationType">The destination Type wanted.</param>
        /// <returns>The value converted to the destination type.</returns>
        public static object ConvertTo(this object value, Type destinationType)
        {
            if (!destinationType.IsNotNull()) { throw new ArgumentNullException("destinationType"); }

            if ((value == null) || (value == DBNull.Value))
            {
                //The pre-generics equivalent of default(T):
                //Remark: strings are not ValueType, so are returned as null.
                //Remark: guid are ValueType, so are returned as Guid.Empty.
                //Remark: int are ValueType, so are returned as Int32.Empty.
                return (destinationType.IsValueType)
                           ? Activator.CreateInstance(destinationType)
                           : null;
            }

            //Value is not null, so safe to get type:
            Type srcType = value.GetType();

            /*
 
              if ((srcType == typeof(string)) && 
               (string.IsNullOrEmpty((string)value))) {
                  //Empty string... Hum...
                  //But being converted to a string, or something else?
                  if (destinationType == typeof(string)) {
                      //Don't change the normal behavior then...
                      //AND 
                      //Might as well get out early...
                      return value;








                  }
                  else {
                      //But if going to be converted........
                      //Much better results to let it decide from a null, 
                      //rather than choke on an empty string:
                      value = null;
                  }
              }
      */
            //Version 2 to add trim():

            if (srcType == typeof(string))
            {
                if (destinationType == typeof(string))
                {
                    //Don't change the normal behavior then...
                    //AND 
                    //Might as well get out early...
                    return value;
                }
                // ReSharper disable ConvertIfStatementToConditionalTernaryExpression
                if (string.IsNullOrEmpty((string)value))
                // ReSharper restore ConvertIfStatementToConditionalTernaryExpression
                {
                    //Empty string... Hum...
                    //But being converted to a string, or something else?
                    //But if going to be converted........
                    //Much better results to let it decide from a null, 
                    //rather than choke on an empty string:
                    value = null;
                }
                else
                {
                    //we know its not going to be a string
                    //so trim it:
                    value = ((string)value).Trim();
                }
            }


#if (!CE) && (!PocketPC) && (!pocketPC) && (!WindowsCE)
            //PC:
            //Unfortunately TypeConverter is not on CF...

            TypeConverter typeConverter =
                TypeDescriptor.GetConverter(destinationType);


            if ((typeConverter != null) && (typeConverter.CanConvertFrom(srcType)))
            {
                try
                {
                    // ReSharper disable AssignNullToNotNullAttribute
                    return typeConverter.ConvertFrom(value);
                    // ReSharper restore AssignNullToNotNullAttribute
                }
                catch
                {
                    return (destinationType.IsValueType)
                               ? Activator.CreateInstance(destinationType)
                               : null;
                }
            }

            typeConverter = TypeDescriptor.GetConverter(srcType);

            if ((typeConverter != null) && (typeConverter.CanConvertTo(destinationType)))
            {
                // ReSharper disable AssignNullToNotNullAttribute
                return typeConverter.ConvertTo(value, destinationType);
                // ReSharper restore AssignNullToNotNullAttribute
            }
#endif

            try
            {
                return Convert.ChangeType(value, destinationType, null);
            }
            catch
            {
            }

            //Ok  so we failed doing it the simplest way...
            //But is there a constructor that we can use?
            //For example, Version (which is a class) 
            //can take a string arg:
            ConstructorInfo constructorInfo =
                destinationType.GetConstructor(new[] { srcType });
            try
            {
                return constructorInfo.Invoke(new[] { value });
            }
            catch
            {
            }

            return (destinationType.IsValueType)
                       ? Activator.CreateInstance(destinationType)
                       : null;

            //return System.Convert.ChangeType(value, destinationType, null);
        }
    }
}
