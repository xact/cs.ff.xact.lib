﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Deployment.Application;
//using System.Linq;
//using System.Text;
//using XAct.Commands;

//namespace XAct.Deployment
//{
//    public interface ICheckCommandMessage : ICommandMessage
//    {
//        Exception Exception { get; set; }

//        void ad_CheckForUpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e);
//        void ad_CheckForUpdateCompleted(object sender, CheckForUpdateCompletedEventArgs e);
//        Action Callback { get; set; }
//    }
//    public class CheckCommandMessage : ICheckCommandMessage
//    {
//        public Exception Exception { get; set; }
//        public long sizeOfUpdate { get; set; }
//        private Action _callback;

//        public CheckCommandMessage(Action callback)
//        {
//            _callback = callback;
//        }

//        public virtual void ad_CheckForUpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
//        {
//            string text = String.Format("Downloading: {0}. {1:D}K of {2:D}K downloaded.", GetProgressString(e.State), e.BytesCompleted / 1024, e.BytesTotal / 1024);
//        }

//        public virtual void ad_CheckForUpdateCompleted(object sender, CheckForUpdateCompletedEventArgs e)
//        {
//            if (e.Error != null)
//            {
//                Exception = new Exception("ERROR: Could not retrieve new version of the application. Reason: \n" + e.Error.Message + "\nPlease report this error to the system administrator.");
//                return;
//            }
//            if (e.Cancelled == true)
//            {
//                Exception = new Exception("The update was cancelled.");
//                return;
//            }

//            // Ask the user if they would like to update the application now.
//            if (e.UpdateAvailable)
//            {
//                sizeOfUpdate = e.UpdateSizeBytes;

//                if (!e.IsUpdateRequired)
//                {
//                    DialogResult dr = MessageBox.Show("An update is available. Would you like to update the application now?\n\nEstimated Download Time: ", "Update Available", MessageBoxButtons.OKCancel);

//                    if (DialogResult.OK == dr)
//                    {
//                        _callback.Invoke();
//                    }
//                }
//                else
//                {
//                    MessageBox.Show("A mandatory update is available for your application. We will install the update now, after which we will save all of your in-progress data and restart your application.");
//                    _callback.Invoke();
//                }
//            }
//        }

//        private string GetProgressString(DeploymentProgressState state)
//        {
//            if (state == DeploymentProgressState.DownloadingApplicationFiles)
//            {
//                return "application files";
//            }
//            else if (state == DeploymentProgressState.DownloadingApplicationInformation)
//            {
//                return "application manifest";
//            }
//            else
//            {
//                return "deployment manifest";
//            }
//        }


//    }

//    public class UpdatePackage
//    {
//        public virtual void ad_UpdateProgressChanged(object sender, DeploymentProgressChangedEventArgs e)
//        {
//            String progressText = String.Format("{0:D}K out of {1:D}K downloaded - {2:D}% complete", e.BytesCompleted / 1024, e.BytesTotal / 1024, e.ProgressPercentage);
//        }

//        public virtual void ad_UpdateCompleted(object sender, AsyncCompletedEventArgs e){}

//    }







//    public class CheckCommandMessageHandler : CommandMessageHandlerBase<ICheckCommandMessage, ICommandResponse>
//    {

//        public override ICommandResponse ValidateForExecution(ICheckCommandMessage commandMessage)
//        {
//            CommandResponse commandResponse = new CommandResponse();
//            if (!ApplicationDeployment.IsNetworkDeployed)
//            {
//                commandResponse.AddException(new Exception("Application is not Network Deployed. Cannot Check for Updates."));
//                commandResponse.Message = "Application is not Network Deployed. Cannot Check for Updates.";
//                return commandResponse;
//            }
//            commandResponse.Success = true;

//            return commandResponse;
//        }

//        public override ICommandResponse ValidateForUnexecution(ICheckCommandMessage commandMessage)
//        {
//            throw new NotImplementedException();
//        }

//        public override ICommandResponse Execute(ICheckCommandMessage commandMessage)
//        {

//            ICommandResponse  commandResponse  = new CommandResponse();
//            if (!ValidateForExecution(commandMessage).Success)
//            {
                
//            }

//            if (!ApplicationDeployment.IsNetworkDeployed)
//            {
//                commandMessage.ErrorMessage = "Application is not Network Deployed. Cannot Check for Updates.";
//                return;
//            }

//            ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;

//            ad.CheckForUpdateCompleted +=
//                new CheckForUpdateCompletedEventHandler(commandMessage.ad_CheckForUpdateCompleted);
//            ad.CheckForUpdateProgressChanged +=
//                new DeploymentProgressChangedEventHandler(commandMessage.ad_CheckForUpdateProgressChanged);

//            ad.CheckForUpdateAsync();
//        }

//        public override ICommandResponse Unexecute(ICheckCommandMessage commandMessage)
//        {
//            throw new NotImplementedException();
//        }
//    }
//    class Class1
//    {



//        public void UpdateApplication(CheckCommandMessage checkCommandMessage)
//        {

//        }



//        private void BeginUpdate(UpdatePackage updatePackage)
//        {
//            ApplicationDeployment ad = ApplicationDeployment.CurrentDeployment;
//                        ad.UpdateCompleted += new AsyncCompletedEventHandler(updatePackage.ad_UpdateCompleted);


//            // Indicate progress in the application's status bar.
//            ad.UpdateProgressChanged += new DeploymentProgressChangedEventHandler(ad_UpdateProgressChanged);
//            ad.UpdateAsync();
//        }



//        void ad_UpdateCompleted(object sender, AsyncCompletedEventArgs e)
//        {
//            if (e.Cancelled)
//            {
//                MessageBox.Show("The update of the application's latest version was cancelled.");
//                return;
//            }
//            else if (e.Error != null)
//            {
//                MessageBox.Show("ERROR: Could not install the latest version of the application. Reason: \n" + e.Error.Message + "\nPlease report this error to the system administrator.");
//                return;
//            }

//            DialogResult dr = MessageBox.Show("The application has been updated. Restart? (If you do not restart now, the new version will not take effect until after you quit and launch the application again.)", "Restart Application", MessageBoxButtons.OKCancel);

//            if (DialogResult.OK == dr)
//            {
//                Application.Restart();
//            }
//        }
//    }
//}
