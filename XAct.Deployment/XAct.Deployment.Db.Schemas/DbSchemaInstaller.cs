namespace XAct.Deployment.Db
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Reflection;
    using System.Text.RegularExpressions;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.IO;

    /// <summary>
    ///   Class to create and keep providers that use Db tables
    ///   for backend storage, up to date.
    /// </summary>
    public class DbSchemaInstaller
    {

        /// <summary>
        ///   Name of Db Table used to check schema versions:
        /// </summary>
// ReSharper disable InconsistentNaming
        private const string C_DBSCHEMATABLENAME = "DbSchemaVersion";
// ReSharper restore InconsistentNaming

        #region Fields

        private Assembly _assembly;

        /// <summary>
        ///   Gets the unique name/alias for this provider's versioning info.
        /// </summary>
        private string _name;

        /// <summary>
        ///   Gets the connection settings for the local DB where graph structure is stored.
        /// </summary>
        /// <value>The connection settings.</value>
        private ConnectionStringSettings _connectionSettings;

        /// <summary>
        ///   The prefix used to mark parameters (For SqlServer its '@', etc.)
        /// </summary>
        private string _paramPrefix;



        #endregion


        #region Services

        private readonly ITracingService _tracingService;
        private readonly IEnvironmentService _environmentService;
        private readonly IIOService _ioService; //OK (as not Service)
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DbSchemaInstaller"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="ioService">The specific io service to use.</param>
        /// <remarks>
        /// At present do not try to convert this to a service -- as the IOService is not ready to be parsed automatically.
        /// </remarks>
        public DbSchemaInstaller(ITracingService tracingService, IEnvironmentService environmentService, IIOService ioService)
        {
            _tracingService = tracingService;
            _environmentService = environmentService;
            _ioService = ioService;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <param name="assemblyToScanForResourceFiles">The assembly.</param>
        /// <param name="resourceNamePrefix">The name.</param>
        /// <param name="optionalDirPathToScanForResourceFiles">The optional dir path.</param>
        /// <param name="nameOfConfigConnectionSetting">The unique name of the connection settings in the config file.</param>
        /// <param name="paramPrefix">The param prefix.</param>
        /// <remarks>
        /// Finds, or creates, a table called <c>DbSchemaVersionInfo</c>
        /// then processes any script files that are of a later
        /// version than the recorded version.
        /// </remarks>
        public void Initialize(Assembly assemblyToScanForResourceFiles, 
                                 string resourceNamePrefix, 
                                 string nameOfConfigConnectionSetting,
                                 string optionalDirPathToScanForResourceFiles = null,
                                 string paramPrefix = "@")
        {
            //Check Args:
            assemblyToScanForResourceFiles.ValidateIsNotDefault("assemblyToScanForResourceFiles");
            resourceNamePrefix.ValidateIsNotNullOrEmpty("resourceNamePrefix");
            nameOfConfigConnectionSetting.ValidateIsNotNullOrEmpty("nameOfConfigConnectionSetting");

#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            if (paramPrefix == null)
            {
                paramPrefix = "@";
            }
            ConnectionStringSettings connectionSettings =
                ConfigurationManager.ConnectionStrings[nameOfConfigConnectionSetting]
                    .ConfigureConnectionSettings();

            if (connectionSettings == null)
            {
                throw new ArgumentException("::Could not find ConnectionSetting called '{0}'.".FormatStringCurrentCulture(nameOfConfigConnectionSetting));
            }

            _assembly = assemblyToScanForResourceFiles;
            _name = resourceNamePrefix;
            _connectionSettings = connectionSettings;


            _paramPrefix = paramPrefix;

        
            EnsureDbTrackingTableExists();
            
            Version version = GetVersion();
            
            SortedList<string, string> scripts = GetScripts(_name, optionalDirPathToScanForResourceFiles);

            if (version == null)
            {
                version = new Version(0, 0, 0, 0);
            }

            ProccessScripts(version, scripts);
        }

        #endregion

        /// <summary>
        ///   Ensure that the Schema tracking db table exists.
        /// </summary>
        /// <internal>
        ///   This method's SQL is really generic stuff...
        ///   just about any dbms could handle it...
        ///   but it will throw an error if the table already exists,
        ///   hence the DebuggerHidden attribute to not drive you nuts
        ///   in debug mode.
        /// </internal>
        [DebuggerHidden]
        private void EnsureDbTrackingTableExists()
        {
            string sql = 
                "CREATE TABLE {0}" +
                "(" +
                " Name NVARCHAR(256) UNIQUE NOT NULL," +
                " NameLowered NVARCHAR(256) UNIQUE NOT NULL," +
                " Version NVARCHAR(20)," +
                " Note NVARCHAR(1024)," +
                " PRIMARY KEY (NameLowered)" +
                ")".FormatStringInvariantCulture(C_DBSCHEMATABLENAME);//"DbSchemaVersionInfo"


            try
            {
                using (IDbConnection connection = CreateConnection())
                {
                    using (IDbCommand command = connection.CreateCommand())
                    {
                        command.CommandText = sql;
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                _tracingService.TraceException(0,Diagnostics.TraceLevel.Info, e, "Most often not an error, but logging anyway.");
            }
        }

        /// <summary>
        ///   Gets the latest known version of this assembly from the 
        ///   DbSchemaVersion table.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     Note that it will return <c>null</c> 
        ///     and not an empty <c>Version</c>
        ///     if no entry exists in the Db table (ie, the first time).
        ///   </para>
        /// </remarks>
        /// <returns>A <see cref = "System.Version" /> or null.</returns>
        private Version GetVersion()
        {
            //Check Args:
            string sql =
                "SELECT Version FROM {0} WHERE NameLowered = @NameLowered".FormatStringInvariantCulture(
                    C_DBSCHEMATABLENAME);

            using (IDbConnection connection = CreateConnection())
            {
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.SetCommandTextWithoutCA2010Warning(sql.FormatStringInvariantCulture());
                    command.AttachParam("NameLowered", _name.ToLower(CultureInfo.InvariantCulture));
                    object result = command.ExecuteScalar();
                    if (result != null)
                    {
                        return new Version((string) result);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <internal>
        ///   Notice how it collects together Resources, 
        ///   and then goes for files.
        ///   This means that if any scripts are provided
        ///   in file format, with the same name as an embedded Resource, 
        ///   they are deemed to supercede the Resource.
        /// </internal>
        /// <param name = "name"></param>
        /// <param name = "pathName"></param>
        /// <returns></returns>
        private SortedList<string, string> GetScripts(
            string name, 
            string pathName = null)
        {
            //Check Args:
            name.ValidateIsNotNullOrEmpty("name");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            SortedList<string, string> result =
                new SortedList<string, string>(
                    StringComparer.OrdinalIgnoreCase);

            // get a reference to the current assembly
            //Assembly assembly = Assembly.GetExecutingAssembly();

            // get a list of resource names from the manifest
            GetScriptsFromAssembly(name, ref result);

            if (pathName != null)
            {
                GetScriptsFromDirectory(name, ref result, pathName);
            }

            return result;
        }

        private void GetScriptsFromAssembly(string name, ref SortedList<string, string> result)
        {
            string[] tmp = _assembly.GetManifestResourceNames();


            List<string> resNames = new List<string>(tmp);
            //At this point we should only have sql files, 
            //and in ascending order
            //(ie first one is oldest script):

            //Look for a Resource who's name starts with the name,
            //and ends with the version number.
            string pattern =
                "(" + name + ").*" +
                "([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)(.*)(\\.sql$)";

            foreach (string resName in resNames)
            {
                if (!Regex.Match(
                    resName,
                    pattern,
                    RegexOptions.IgnoreCase).Success)
                {
                    continue;
                }


                Stream resourceStream = _assembly.GetManifestResourceStream(resName);

                if (resourceStream == null)
                {
                    continue;
                }
                TextReader r = new StreamReader(resourceStream);
                string contents = r.ReadToEnd();
                result[resName.ToLower()] = contents;
            }
        }

        private void GetScriptsFromDirectory(string name, ref SortedList<string, string> result, string pathName)
        {
            if (string.IsNullOrEmpty(pathName))
            {
                return;
            }
            if (!Directory.Exists(pathName))
            {
                return;
            }

            string[] tmp = _ioService.GetDirectoryFileNamesAsync( //Still safe to refer to IOService as this is not a Service class.
                pathName,
                name,
                HierarchicalOperationOption.Recursive)
                .WaitAndGetResult();

            foreach (string fullPath in tmp)
            {
                string fileName = Path.GetFileName(fullPath);

                if (fileName == null)
                {
                    continue;
                }

                string pattern = "(" + name + ").*" +
                                 "([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)" +
                                 "(.*)(\\.sql$)";

                if (!Regex.Match(
                    fileName,
                    pattern,
                    RegexOptions.IgnoreCase).
                         Success)
                {
                    continue;
                }

                using (Stream fileStream = _ioService.FileOpenReadAsync(fullPath).WaitAndGetResult())
                {
                    TextReader r = new StreamReader(fileStream);
                    string contents = r.ReadToEnd();

                    result[fileName.ToLower()] = contents;
                }
            }

        }

        private void ProccessScripts(
            Version version,
            SortedList<string, string> scripts)
        {
            //Check Args:
            scripts.ValidateIsNotDefault("scripts");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            //bool aScriptProcessed = false;
            //We know list is in order from oldest to newest:
            foreach (KeyValuePair<string, string> entry in scripts)
            {
                //string name = entry.Key;

                //We know it will match:
                string capture =
                    Regex.Match(
                        entry.Key,
                        "[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+").Value;

                Version scriptVersion = new Version(capture);

                if (scriptVersion.CompareTo(version) <= 0)
                {
                    continue;
                }
                //The script is later than current version...run it:

                int statementIndex;
                if (ProcessScriptFile(entry.Key, entry.Value, out statementIndex))
                {
                    UpdateVersionNoteInDb(
                        scriptVersion,
                        "Updated on " + DateTime.Now);

                    //aScriptProcessed = true;
                }
            }
            //if (aScriptProcessed){
            //}
        }

        //[System.Diagnostics.DebuggerHidden()]
        private bool ProcessScriptFile(string name, string scriptText, out int statementIndex)
        {
            string sql = string.Empty;


            statementIndex = 0;

            try
            {
                string fileSql = scriptText;

                //string pattern = ";(?:[\n\r]|$)";
                const string pattern = @";\s*$";

                string[] sqlStatements =
                    Regex.Split(
                        fileSql,
                        pattern,
                        RegexOptions.Multiline);

                //FIX:CA1804:statementMax = sqlStatements.Length;
                statementIndex = 0;

                using (IDbConnection connection = CreateConnection())
                {
                    using (IDbTransaction transaction =
                        connection.BeginTransaction())
                    {
                        try
                        {
                            foreach (string sqlStatement in sqlStatements)
                            {
                                sql = sqlStatement.Trim();
                                //Fix: get rid of CA1820
                                if (sql.IsNullOrEmpty())
                                {
                                    continue;
                                }

                                using (IDbCommand command =
                                    connection.CreateCommand())
                                {
                                    command.Transaction = transaction;
                                    command.SetCommandTextWithoutCA2010Warning(sqlStatement.Trim());
                                    command.ExecuteNonQuery();
                                }
                            }
                            transaction.Commit();
                            statementIndex++;
                        }
                        catch
                        {
                            //First, write the critical/error error:
                            //if (XSourceSwitch.ShouldTrace(
                            //    TraceEventType.Error))
                            //{
                            //    Trace.WriteLine(E.Message);
                            //}
                            //Secondly, write the verbose 
                            //explanation of the error:
                            //if (XSourceSwitch.ShouldTrace(
                            //    TraceEventType.Verbose))
                            //{
                            //    Trace.WriteLine(E.StackTrace);
                            //}

                            transaction.Rollback();
                            throw;
                        }
                    }
                }
                return true;
            }
            catch (Exception exception)
            {
                _tracingService.TraceException(0, Diagnostics.TraceLevel.Warning, exception,
                                                   "DbSchemaInstaller SQL Statement Failed:" +
                                                   "{0}['{1}']{0}REASON:{2}{0}{3}{0}SQL:{0}{4}{0}"
                                                   .FormatStringInvariantCulture(
                                                   _environmentService.NewLine,
                                                   name,
                                                   exception.Message,
                                                   (exception.InnerException != null) ? exception.InnerException.Message : "---",
                                                   sql));

                return false;
            }
        }


        private void UpdateVersionNoteInDb(
            Version versionInfo, string note)
        {
            //Check Args:
            versionInfo.ValidateIsNotDefault("versionInfo");
#if CONTRACTS_FULL // Requires .Net 4, so hold for the moment
            Contract.EndContractBlock();
#endif

            //Prepare vars:
            if (note == null)
            {
                note = string.Empty;
            }
            string insertSql =
                "INSERT INTO {1} (Name, NameLowered, Version, Note)" +
                " VALUES({0}Name, {0}NameLowered, {0}Version, {0}Note)".FormatStringInvariantCulture(_paramPrefix,C_DBSCHEMATABLENAME);
            string updateSql =
                "UPDATE {1}" +
                " SET Version={0}Version," +
                " Note={0}Note" +
                " WHERE" +
                " NameLowered={0}NameLowered".FormatStringInvariantCulture(
                    _paramPrefix,
                    C_DBSCHEMATABLENAME);


            using (IDbConnection connection = CreateConnection())
            {
                //Ask for version for this name:
                //If it comes back as null, means line doesn't exist.
                //Note that the value can never 
                //be null due to Arg check above...
                Version version = GetVersion();

                string versionStr = versionInfo.ToString(4);
                using (IDbCommand command = connection.CreateCommand())
                {
                    if (version == null)
                    {
                        //INSERT:
                        command.SetCommandTextWithoutCA2010Warning(insertSql.FormatStringInvariantCulture());
                        //Attach params in order (for ODBC compliance):
                        command.AttachParam("Name", _name);
                        command.AttachParam("NameLowered", _name.ToLower());
                        command.AttachParam("Version", versionStr);
                        command.AttachParam("Note", note);
                    }
                    else
                    {
                        //UPDATE:
                        command.SetCommandTextWithoutCA2010Warning(updateSql.FormatStringInvariantCulture());
                        //Attach params in order (for ODBC compliance):
                        command.AttachParam("Version", versionStr);
                        command.AttachParam("Note", note);
                        command.AttachParam("NameLowered", _name.ToLower());
                    }
                    command.ExecuteNonQuery();
                }
            }
        }


        /// <summary>
        ///   Creates a connection to the database specified by the ConnectionSettings.
        /// </summary>
        /// <returns></returns>
        private IDbConnection CreateConnection()
        {
            return _connectionSettings.CreateConnection();
        }



    }


}


