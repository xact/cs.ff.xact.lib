namespace Tests.Tests.Services
{
    using XAct.Commands;

    public class BarCommandMessageHandler : CommandMessageHandlerBase<BarCommandMesssage>
    {
        public BarCommandMessageHandler()
            : base(x => x.Result = x.Bar + "R")
        {
        }
    }
}