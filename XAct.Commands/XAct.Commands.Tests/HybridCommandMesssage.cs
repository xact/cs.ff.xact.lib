namespace Tests.Tests.Services
{
    using System.Runtime.Serialization;
    using XAct.Commands;

    [DataContract]
    public class HybridCommandMesssage : CommandMessageHandlerBase<HybridCommandMesssage>, ICommandMessage, ICommandMessageResult<string>
    {
        public string ResourceFilter { get; set; }
        public string Title { get; set; }
        public string UndoTitle { get; set; }
        public HybridCommandMesssage() : base(x=>x.Result  = x.Foo + "R")
        {
            Title = "Do " + this.GetType().Name;
            UndoTitle = "Undo " + this.GetType().Name;
        }

        [DataMember]
        public string Foo { get; set; }
        [DataMember]
        public string Result { get; set; }


    }
}