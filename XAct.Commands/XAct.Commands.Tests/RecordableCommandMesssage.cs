namespace Tests.Tests.Services
{
    using System.Runtime.Serialization;
    using XAct;
    using XAct.Commands;

    [DataContract]
    public class RecordableCommandMesssage : CommandMessageHandlerBase<RecordableCommandMesssage>,
                                             ICommandMessageRecordable,
                                             ICommandMessage, ICommandMessageResult<string>, XAct.IHasResourceFilter
    {
        private string _title;
        private string _undoTitle;
        public string ResourceFilter
        {
            get { return null; }
            set {  }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string UndoTitle
        {
            get { return _undoTitle; }
            set { _undoTitle = value; }
        }

        public RecordableCommandMesssage()
            : base(x => x.Result = x.Foo + "R")
        {
            Title = "Do " + this.GetType().Name;
            UndoTitle = "Undo " + this.GetType().Name;
        }

        [DataMember]
        public string Foo { get; set; }
        
        [DataMember]
        public string Result { get; set; }


    }
}