namespace Tests.Tests.Services
{
    using System;
    using XAct.Commands;
    using XAct.Messages;

    public class CalcCommandMesssage : CommandMessageBase, ICommandMessage
    {
        public char Operator { get; set; }
        public decimal Operand { get; set; }
        public CalcCommandMesssage(){}
        public CalcCommandMesssage(char op, decimal operand)
        {
            Operator = op;
            Operand = operand;
        }
    }
}