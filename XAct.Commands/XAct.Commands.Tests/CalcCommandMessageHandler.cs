namespace Tests.Tests.Services
{
    using XAct.Commands;

    public class CalcCommandMessageHandler : CommandMessageHandlerBase<CalcCommandMesssage>
    {
        public CalcCommandMessageHandler() : base(x => { })
        {
        }

        public override void Execute(CalcCommandMesssage messsage)
        {
            var calc = XAct.DependencyResolver.Current.GetInstance<ICalculator>();

            var sum = calc.Sum;

            switch (messsage.Operator)
            {
                case '.':
                    sum = 0;
                    break;
                case '+':
                    sum += messsage.Operand;
                    break;
                case '-':
                    sum -= messsage.Operand;
                    break;
                case '*':
                    sum = sum * messsage.Operand;
                    break;
                case '/':
                    sum = sum / messsage.Operand;
                    break;

            }
            calc.Sum = sum;
        }

        public override void Unexecute(CalcCommandMesssage messsage)
        {
            var calc = XAct.DependencyResolver.Current.GetInstance<ICalculator>();

            var sum = calc.Sum;

            switch (messsage.Operator)
            {
                case '+':
                    sum -= messsage.Operand;
                    break;
                case '-':
                    sum += messsage.Operand;
                    break;
                case '*':
                    sum = sum / messsage.Operand;
                    break;
                case '/':
                    sum = sum * messsage.Operand;
                    break;

            }
            calc.Sum = sum;
        }

    }
}