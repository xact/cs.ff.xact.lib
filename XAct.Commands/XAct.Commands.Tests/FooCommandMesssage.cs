namespace Tests.Tests.Services
{
    using System;
    using XAct.Commands;
    using XAct.Messages;

    public class FooCommandMesssage : CommandMessageBase, ICommandMessage, ICommandMessageResult<string>
    {
        public string Foo { get; set; }
        public string Result { get; set; }
    }
}