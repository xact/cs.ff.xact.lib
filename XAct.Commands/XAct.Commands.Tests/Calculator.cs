namespace Tests.Tests.Services
{
    using XAct;

    public interface ICalculator : IHasSingletonBindingScope
    {
        decimal Sum { get; set; }
    } 
    public class Calculator : ICalculator
    {
        public Calculator()
        {
#pragma warning disable 168
#pragma warning disable 219
            bool b=true;
            b = false;
#pragma warning restore 219
#pragma warning restore 168
        }
        public decimal Sum { get; set; }
    }
}