using System;

namespace Tests.Tests.Services
{
    using System.Text;
    using Microsoft.CSharp.RuntimeBinder;
    using NUnit.Framework;
    using XAct;
    using XAct.Commands;
    using XAct.Commands.Services.Implementations;
    using XAct.Domain.Repositories;
    using XAct.Tests;


    [TestFixture]
    public class ICommandServiceTests
    {


        [SetUp]
        public void MyTestSetup()
        {
            //Run before every test:
            //Singleton<IocContext>.Instance.ResetIoC();
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            //DependencyResolver.BindingResults.DumpToFile();
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void Can_Get_ICommandService()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ICommandService>();
            //ASSERT:
            Assert.IsNotNull(service);
        }


        [Test]
        public void Can_Get_ICommandService_Of_Expected_Type()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<ICommandService>();
            //ASSERT:
            Assert.AreEqual(typeof(CommandService), service.GetType());
        }

        [Test]
        public void Can_Serialize_Command()
        {
            //ARRANGE
            var commandFoo = new FooCommandMesssage { Foo = "foo" };
            //ACT
            string json = commandFoo.ToJSON(Encoding.UTF8);
            //ASSERT:
            Assert.IsNotNullOrEmpty(json);
        }
        [Test]
        public void Can_DeSerialize_Command()
        {
            //ARRANGE
            var commandFoo = new FooCommandMesssage { Foo = "foo" };
            //ACT
            string json = commandFoo.ToJSON(Encoding.UTF8);
            var result = json.DeserialiseFromJSON<FooCommandMesssage>(Encoding.UTF8);
            //ASSERT:
            Assert.IsNotNull(result);
        }

        [Test]
        public void Obtain_CommandHandler_From_ServiceLocator()
        {
            //ARRANGE
            //ACT
            var handler = DependencyResolver.Current.GetInstance<ICommandMessageHandler<FooCommandMesssage>>();
            //ASSERT:
            Assert.IsNotNull(handler);
        }

        [Test]
        public void Obtain_CommandHandler_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new FooCommandMesssage { Foo = "foo" };
            //ACT
            var handler = ((CommandService)service).GetHandler(commandFoo);
            //ASSERT:
            Assert.IsNotNull(handler);
        }

        public void Obtain_HybridCommandHandler_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new HybridCommandMesssage { Foo = "foo" };
            //ACT
            var handler = ((CommandService)service).GetHandler(commandFoo);
            //ASSERT:
            Assert.IsNotNull(handler);
        }

        [Test]
        public void Execute_Handler_Obtained_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new FooCommandMesssage { Foo = "foo" };
            //ACT
            var handler = ((CommandService)service).GetHandler(commandFoo);
            handler.Execute(commandFoo);
            //ASSERT:
            Assert.IsNotNull(handler);
            Assert.AreEqual("foo" + "R",commandFoo.Result);
        }



        [Test]
        public void Execute_HybridCommandHandler_Obtained_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new HybridCommandMesssage { Foo = "foo" };
            //ACT
            var handler = ((CommandService)service).GetHandler(commandFoo);
            handler.Execute(commandFoo);
            //ASSERT:
            Assert.IsNotNull(handler);
            Assert.AreEqual("foo" + "R", commandFoo.Result);
        }

        [Test]
        public void Execute_Umtyped_CommandHandler_Obtained_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new FooCommandMesssage { Foo = "foo" };
            //ACT
            //Untyped, just a handler:
            object handler = ((CommandService)service).GetHandler(commandFoo);
            Assert.IsNotNull(handler,"Handler should not be null");
            Console.WriteLine(handler.ToString());

            ICommandMessageHandler<FooCommandMesssage> result = (ICommandMessageHandler<FooCommandMesssage>) handler;
            Assert.IsNotNull(result);
            Console.WriteLine(result.ToString());
            
            result.Execute(commandFoo);
            //ASSERT:
            Assert.IsNotNull(result);

            Assert.AreEqual("foo" + "R", commandFoo.Result);
        }


        [Test]
        //No idea why it's not smart enough to do it this way.
        [ExpectedException(typeof(RuntimeBinderException))]
        public void Execute_Using_Dynamic_Fails()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new FooCommandMesssage { Foo = "foo" };
            ICommandMessage commandMessage = (ICommandMessage) commandFoo;
            var commandMessageType = commandMessage.GetType();
            Console.WriteLine(commandMessageType.Name);
            var commandMessageHandlerType = typeof(ICommandMessageHandler<>).MakeGenericType(commandMessageType);
            Console.WriteLine(commandMessageHandlerType.Name);

            dynamic result = (object) XAct.DependencyResolver.Current.GetInstance(commandMessageHandlerType);
            Console.WriteLine(result.GetType().Name);
            var commandFoo2 = commandMessage.ConvertTo(commandMessage.GetType());
            Console.WriteLine(commandMessage.GetType().Name);
            Console.WriteLine(commandFoo2.GetType().Name);
            try
            {
                result.Execute(commandFoo2);
            }
            catch (System.Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            Assert.AreEqual("foo" + "R", commandFoo.Result);
        }

        [Test]
        //No idea why it's not smart enough to do it this way.
        public void Execute_Using_Reflection_Works()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new FooCommandMesssage { Foo = "foo" };
            ICommandMessage commandMessage = (ICommandMessage)commandFoo;
            var commandMessageType = commandMessage.GetType();
            Console.WriteLine(commandMessageType.Name);
            var commandMessageHandlerType = typeof(ICommandMessageHandler<>).MakeGenericType(commandMessageType);
            Console.WriteLine(commandMessageHandlerType.Name);

            object result = XAct.DependencyResolver.Current.GetInstance(commandMessageHandlerType);
// ReSharper disable CoVariantArrayConversion
            result.InvokeMethod("Execute", new[] {commandMessage});
// ReSharper restore CoVariantArrayConversion
            
            Assert.AreEqual("foo" + "R", commandFoo.Result);
        }



        [Test]
        public void Can_Execute_FooCommand_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new FooCommandMesssage { Foo = "Foo" };
            //ACT
            service.Execute(true, commandFoo);
            //ASSERT:
            Assert.AreEqual("FooR", commandFoo.Result);
        }

        [Test]
        public void Can_Execute_BarCommand_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new BarCommandMesssage { Bar = "Bar" };
            //ACT
            service.Execute(true, commandFoo);
            //ASSERT:
            Assert.AreEqual("BarR", commandFoo.Result);
        }

        [Test]
        public void Can_Execute_HybridCommand_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new HybridCommandMesssage { Foo = "Foo" };
            //ACT
            service.Execute(true, commandFoo);
            //ASSERT:
            Assert.AreEqual("FooR", commandFoo.Result);
        }


        [Test]
        public void Can_Execute_RecordableCommand_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var command = new RecordableCommandMesssage { Foo = "Foo" };
            //ACT
            service.Execute(true, command);
            //ASSERT:
            Assert.AreEqual("FooR", command.Result);

            XAct.DependencyResolver.Current.GetInstance<IRepositoryService>().GetContext().Commit();
        }


        [Test]
        public void Can_Execute_MultipleCommand_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();
            
            var commandFoo = new FooCommandMesssage { Foo = "Foo" };
            var commandBar = new BarCommandMesssage { Bar = "Bar" };
            var commandHybrid = new HybridCommandMesssage { Foo = "Foo" };
            //ACT
            service.Execute(true, commandFoo,commandBar,commandHybrid);
            //ASSERT:
            Assert.AreEqual("FooR", commandFoo.Result);
        }

        [Test]
        public void Can_Execute_MultipleCommand_Delayed_Using_Service()
        {
            //ARRANGE
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            ((CommandService)service).ClearHistory();

            var commandFoo = new FooCommandMesssage { Foo = "Foo" };
            var commandBar = new BarCommandMesssage { Bar = "Bar" };
            var commandHybrid = new HybridCommandMesssage { Foo = "Foo" };
            //ACT
            service.Execute(false, commandFoo, commandBar, commandHybrid);
            //ASSERT:
            Assert.AreNotEqual("FooR", commandFoo.Result);
            Assert.AreNotEqual("BarR", commandBar.Result);
            Assert.AreNotEqual("FooR", commandHybrid.Result);

            var commandFoo2 = new FooCommandMesssage { Foo = "Foo" };
            service.Execute(true, commandFoo2);
            Assert.AreEqual("FooR", commandFoo.Result);
            Assert.AreEqual("BarR", commandBar.Result);
            Assert.AreEqual("FooR", commandHybrid.Result);
            Assert.AreEqual("FooR", commandFoo2.Result);

        }


        [Test]
        public void Can_Execute_CalcCommand_Using_Service()
        {
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            new CalcCommandMesssage('.', 0).Execute();
            ((CommandService)service).ClearHistory();

            var calc = DependencyResolver.Current.GetInstance<ICalculator>();
            var original = calc.Sum;

            CalcCommandMesssage command = new CalcCommandMesssage();

            command.Operand = 22;
            command.Operator = '+';

            //Use extension that invokes service:
            command.Execute();

            Assert.IsNotNull(calc);
            Assert.AreEqual(original+22,calc.Sum);
        }

        [Test]
        public void Can_Execute_CalcCommands_Using_Service()
        {
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            new CalcCommandMesssage('.', 0).Execute();
            ((CommandService)service).ClearHistory();

            var calc = DependencyResolver.Current.GetInstance<ICalculator>();

            var original = calc.Sum;
            //Use extension that invokes service:
            new CalcCommandMesssage('+', 22).Execute();
            new CalcCommandMesssage('/', 2).Execute();

            Assert.AreEqual((original + 22m)/2m, calc.Sum);
        }

        [Test]
        public void Can_Execute_And_Unexecute_CalcCommands_Using_Service()
        {
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            new CalcCommandMesssage('.', 0).Execute();
            ((CommandService)service).ClearHistory();

            var calc = DependencyResolver.Current.GetInstance<ICalculator>();

            var original = calc.Sum;
            //Use extension that invokes service:
            
            for (int i = 0; i < 5; i++)
            {

                new CalcCommandMesssage('+', 20).Execute();
                new CalcCommandMesssage('+', 22).Execute();
                new CalcCommandMesssage('/', 2).Execute();

                Assert.AreEqual((original + 42m)/2m, calc.Sum, "#1 checked");


                service.Undo(1);

                Assert.AreEqual((original + 42m), calc.Sum, "#2 checked");

                service.Undo(1);

                Assert.AreEqual((original + 20m), calc.Sum, "#2 checked");

                service.Undo(1);

                Assert.AreEqual((original + 0m), calc.Sum, "#2 checked");

                for (int j = 0; j < 5; j++)
                {
                    service.Undo(1);
                }
                Assert.AreEqual((original + 0m), calc.Sum, "#2 checked");
            }
        }


        [Test]
        public void Can_Execute_And_Undo_And_Redo_CalcCommands_Using_Service()
        {
            var service = DependencyResolver.Current.GetInstance<XAct.Commands.ICommandService>();
            new CalcCommandMesssage('.', 0).Execute();
            ((CommandService)service).ClearHistory();

            Assert.AreEqual(0, service.HistoryIndex, "Start...");
            Assert.AreEqual(0, service.HistoryLength,"Start...");

            var calc = DependencyResolver.Current.GetInstance<ICalculator>();

            var original = calc.Sum;
            //Use extension that invokes service:
            
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Iteration:" + i);
            
                new CalcCommandMesssage('+', 20).Execute();
                Assert.AreEqual(1, service.HistoryIndex, "Index:1");
                Assert.AreEqual(1, service.HistoryLength, "Length:1");

                new CalcCommandMesssage('+', 22).Execute();
                Assert.AreEqual(2, service.HistoryIndex, "Index:2");
                Assert.AreEqual(2, service.HistoryLength, "Length:2");
                new CalcCommandMesssage('/', 2).Execute();
                Assert.AreEqual(3, service.HistoryIndex, "Index:3");
                Assert.AreEqual(3, service.HistoryLength, "Length:3");

                Assert.AreEqual(3, service.HistoryIndex, "A1...");
                Assert.AreEqual(3, service.HistoryLength, "A2...");

                Assert.AreEqual(21m, calc.Sum, "#1 checked");

                for (int j = 0; j < 3; j++)
                {
                    service.Undo(1);
                }
                Assert.AreEqual((original + 0m), calc.Sum, "#2.1 checked");

                Assert.AreEqual(0, service.HistoryIndex, "A3...");
                Assert.AreEqual(3, service.HistoryLength, "A4...");

                service.Redo(1);
                Assert.AreEqual((original + 20m), calc.Sum, "#2.2 checked");

                service.Redo(1);
                Assert.AreEqual((original + 42m), calc.Sum, "#2.3 checked");
                
                service.Redo(1);
                Assert.AreEqual((original + 21m), calc.Sum, "#2.4 checked");

                service.Redo(3);
                Assert.AreEqual((original + 21m), calc.Sum, "#2.5 checked");

                service.Undo(10);
                Assert.AreEqual((original + 0m), calc.Sum, "#B3 checked");

                service.Redo(10);
                Assert.AreEqual((original + 21m), calc.Sum, "#B4 checked");
                
                service.Undo(3);
                Assert.AreEqual((original + 0m), calc.Sum, "#B5 checked");
            }

            Assert.AreEqual(0, service.HistoryIndex);
            Assert.AreEqual(3, service.HistoryLength);

        }

    }
}
