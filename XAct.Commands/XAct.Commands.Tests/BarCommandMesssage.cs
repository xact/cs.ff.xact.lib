namespace Tests.Tests.Services
{
    using System.Runtime.Serialization;
    using XAct.Commands;

    public class BarCommandMesssage : CommandMessageBase, ICommandMessage, ICommandMessageResult<string>
    {
        public string Bar { get; set; }
        public string Result { get; set; }
    }
}