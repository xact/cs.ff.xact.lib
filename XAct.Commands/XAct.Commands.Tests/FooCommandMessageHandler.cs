namespace Tests.Tests.Services
{
    using XAct;
    using XAct.Commands;

    public class FooCommandMessageHandler : CommandMessageHandlerBase<FooCommandMesssage>
    {
        public FooCommandMessageHandler()
            : base((x) => x.Result = x.Foo + "R")
        {
        }

        //public override void Execute(FooCommandMesssage fooCommandMesssage)
        //{
        //    fooCommandMesssage.Result = fooCommandMesssage.Foo + "R";
        //} 
    }
}