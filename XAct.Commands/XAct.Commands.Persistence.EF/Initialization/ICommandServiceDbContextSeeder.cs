namespace KW_NAMESPACENAME.Services.Initialization
{
    using XAct.Commands.Models;
    using XAct.Data.EF.CodeFirst;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{CommandRecord}"/>
    /// to seed the Tip tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface ICommandServiceDbContextSeeder : IHasXActLibDbContextSeeder<CommandRecord>
    {

    }

}

