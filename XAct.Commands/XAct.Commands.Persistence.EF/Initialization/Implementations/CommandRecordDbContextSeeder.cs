﻿namespace KW_NAMESPACENAME.Services.Initialization.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using KW_NAMESPACENAME;
    using XAct.Commands.Models;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct;
    using XAct.Services;

#pragma warning disable 1591
    public class CommandRecordDbContextSeeder : XActLibDbContextSeederBase<CommandRecord>, ICommandServiceDbContextSeeder
#pragma warning restore 1591
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CommandRecordDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public CommandRecordDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        
        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }
    }
}

