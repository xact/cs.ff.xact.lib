﻿namespace KW_NAMESPACENAME.Services.Initialization.Maps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct;
    using XAct.Commands.Models;
 
    public class CommandRecordModelPersistenceMap : EntityTypeConfiguration<CommandRecord>,
                                                        ICommandRecordModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public CommandRecordModelPersistenceMap()
        {

            this.ToXActLibTable("Command");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Property(x => x.ApplicationTennantId)
                .DefineRequiredApplicationTennantId(colOrder++);

            this.Property(x => x.UserIdentifier)
                .DefineRequired64CharUserIdentifier(colOrder++);

            this.Property(x => x.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(x => x.SerializedValueType)
                .DefineRequired1024CharSerializationValueType(colOrder++);

            this.Property(x => x.SerializationMethod)
                .DefineRequiredSerializationMethod(colOrder++);

            this.Property(x => x.SerializedValue)
                .DefineOptional4000CharSerializationValue(colOrder++);

            this.Property(x => x.Tag)
                .DefineOptional256CharTag(colOrder++)
                ;

            this.Property(x => x.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++)
                ;

        }
    }
}
