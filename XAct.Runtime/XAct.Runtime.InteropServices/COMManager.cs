namespace XAct.Runtime.InteropServices
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>
    /// Class to manage ActiveX objects.
    /// </summary>
    internal static class COMManager
    {
        /// <summary>
        ///   Gets or Creates a new COM / ActiveX object.
        /// </summary>
        /// <param name = "qInstanceName"></param>
        /// <param name = "qCreateNew"></param>
        /// <param name = "IsNewInstance">Returns true if it is a new instance -- so that a function can close the COM after usage, or leave it alone, depending on how it was opened...</param>
        /// <returns></returns>
        public static object GetObject(string qInstanceName, bool qCreateNew, ref bool IsNewInstance)
        {
            object tResult = null;
            try
            {
                tResult = Marshal.GetActiveObject(qInstanceName);
                return tResult;
            }
                // ReSharper disable EmptyGeneralCatchClause
            catch
                // ReSharper restore EmptyGeneralCatchClause
            {
            }

            if (qCreateNew)
            {
                try
                {
                    /*
                 * Other Options:
                 * Type t = Type.GetTypeFromCLSID(new Guid("96749377-3391-11D2-9EE3-00C04F797396"));
                 * Type t = Type.GetType("SpeechLib.SpVoiceClass, SpeechLib, Version=5.0.0.0, Culture=neutral, PublicKeyToken=null");
                 */

                    tResult = Activator.CreateInstance(Type.GetTypeFromProgID(qInstanceName));
                    IsNewInstance = true;
                }
                    // ReSharper disable EmptyGeneralCatchClause
                catch
                    // ReSharper restore EmptyGeneralCatchClause
                {
                }
            }
            return tResult;
        }

        /// <summary>
        ///   Release a COM object.
        /// </summary>
        /// <param name = "o"></param>
        public static int NAR(object o)
        {
            try
            {
                if (!o.GetType().IsArray)
                {
                    return Marshal.ReleaseComObject(o);
                }
                else
                {
                    foreach (object oSub in (Array) o)
                    {
                        NAR(oSub);
                    }
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
            finally
            {
                o = null;
            }
        }

        /// <summary>
        ///   Release COM object application wide.
        /// </summary>
        /// <param name = "comObject"></param>
        public static void Kill(object comObject)
        {
            try
            {
                if (!comObject.GetType().IsArray)
                {
                    while (true)
                    {
                        int tCount = Marshal.ReleaseComObject(comObject);
                        if (tCount < 1)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    foreach (object oSub in (Array) comObject)
                    {
                        Kill(oSub);
                    }
                }
            }
                // ReSharper disable EmptyGeneralCatchClause
            catch
                // ReSharper restore EmptyGeneralCatchClause
            {
            }
            finally
            {
                //comObject = null;
            }
        }

        /*
    http://www.devx.com/vb2themax/Tip/18726
    ' this code assumes that you have used this Imports statement
    ' Imports Microsoft.Win32

    ' Print ProgID, CLSID, and path of all the COM components
    ' installed on this computer.
    Sub DisplayCOMComponents()
        ' Open the HKEY_CLASSES_ROOT\CLSID key
        Dim regClsid As RegistryKey = Registry.ClassesRoot.OpenSubKey("CLSID")

        ' Iterate over all the subkeys.
        Dim clsid As String
        For Each clsid In regClsid.GetSubKeyNames
            ' Open the subkey.
            Dim regClsidKey As RegistryKey = regClsid.OpenSubKey(clsid)

            ' Get the ProgID. (This is the default value for this key.)
            Dim ProgID As String = CStr(regClsidKey.GetValue(""))
            ' Get the InProcServer32 key, which holds the DLL path.
            Dim regPath As RegistryKey = regClsidKey.OpenSubKey("InprocServer32")
            If regPath Is Nothing Then
                ' If not found, it isn't an in-process DLL server, 
                ' let's see if it's an out-of-process EXE server.
                regPath = regClsidKey.OpenSubKey("LocalServer32")
            End If
            If Not (regPath Is Nothing) Then
                ' If either key has been found, retrieve its default value.
                Dim filePath As String = CStr(regPath.GetValue(""))
                ' Display all the relevant info gathered so far.
                Trace.WriteLine(ProgId & " " & clsid & " -> " & filePath)
                ' Always close registry keys.
                regPath.Close()
            End If
            ' Always close registry keys.
            regClsidKey.Close()
        Next
    End Sub
            */


        /*

            IsComDLL - Check whether a DLL is an COM self-registering server 

		
            <System.Runtime.InteropServices.DllImport("kernel32")> Shared Function _
        LoadLibrary(ByVal path As String) As Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32")> Shared Function _
        GetProcAddress(ByVal hModule As Integer, ByVal procName As String) As _
        Integer
    End Function

    <System.Runtime.InteropServices.DllImport("kernel32")> Shared Sub FreeLibrary _
        (ByVal hModule As Integer)
    End Sub

    ' Check whether a DLL is an COM self-registering server 
    Function IsComDLL(ByVal filename As String) As Boolean
        Dim hModule As Long
        Dim procAddress As Long

        ' attempt to load the DLL
        hModule = LoadLibrary(filename)

        If hModule <> 0 Then
            ' the DLL has been loaded
            ' get the address of the DllRegisterServer function
            procAddress = GetProcAddress(hModule, "DllRegisterServer")
            ' if non-zero then the function is there
            IsComDLL = (procAddress <> 0)
            ' free-memory
            FreeLibrary(hModule)
        End If
    End Function
            */


        /*
            /// <summary>
            /// Instantiates a Class no matter what assembly it comes from...
            /// </summary>
            /// <param name="tClassName"></param>
            /// <returns></returns>
            public static object IntantiateClass(string tClassName)
            {
                object tClassInstance=null;
                object[] tArgs = new object[1];
                tArgs[0] = this;
                string tAssemblyName="";

                System.Reflection.Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

                try 
                {
                    System.Type tClassType = System.Type.GetType(tClassName);
                    tClassInstance = Activator.CreateInstance(tClassType,tArgs);
                }
                catch (System.Exception E)
                {
                    XError.Trace("Note:" + E.Message);
                }
                if (tClassInstance == null)
                {
                    foreach (System.Reflection.Assembly a in assemblies) 
                    {
                        try 
                        {

                            tAssemblyName = a.FullName;
                            tClassInstance = a.CreateInstance(tClassName,true,System.Reflection.BindingFlags.CreateInstance ,null,tArgs,null,null);
                            XError.Trace("Assembly:" + tAssemblyName);
                        }
                        catch {}
                        if (tClassInstance != null){break;}
                    }
                }
                if (tClassInstance == null)
                {
                    throw (new System.Exception("Could not create Module ["+tClassName +"]!"));
                }
                else
                {
                    return tClassInstance;
                }
            }
            */
    }
}