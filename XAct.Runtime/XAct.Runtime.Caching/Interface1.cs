﻿using System;

namespace XAct.Runtime.Caching
{
    /// <summary>
    /// Interface defines a cache independent API, that hides whether the cache is implemented
    /// by web or windows functionality
    /// </summary>
    //[System.Runtime.InteropServices.ComVisible(false)] // COM doesn't support overloading
    public interface ICache
    {
        /// <summary>
        /// Add an item to the cache using the default caching settings
        /// </summary>
        /// <param name="key">key for the item to add to the cache</param>
        /// <param name="val">value of the item to add to the cache</param>
        void Add(string key, object val);

        /// <summary>
        /// Add an item to the cache for later retrieval, which will expire at an absolute time
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <param name="absoluteExpiration"></param>
        void Add(string key, object val, DateTime absoluteExpiration);

        /// <summary>
        /// Add an item to the cache for later retrieval, which will expire once net reference in the specified slidingExpiration time period
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <param name="slidingExpiration"></param>
        void Add(string key, object val, TimeSpan slidingExpiration);

        /// <summary>
        /// Add an item to the cache for later retrieval, with both absolute and sliding (time since last reference) expiry
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        /// <param name="absoluteExpiration"></param>
        /// <param name="slidingExpiration"></param>
        void Add(string key, object val, DateTime absoluteExpiration, TimeSpan slidingExpiration);

        /// <summary>
        /// Retrieve an item from the cache, this item may be null if the item is not in the cache
        /// </summary>
        /// <param name="key">key of the item to look for</param>
        /// <returns>cached item</returns>
        object Get(string key);

        /// <summary>
        /// Remove the item with this key from the cache
        /// </summary>
        /// <param name="key">key that the item is stored under.</param>
        void Remove(string key);
    }

}
