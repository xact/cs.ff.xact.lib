using System;
using System.Web;
using System.Web.Caching;

namespace XAct.Runtime.Caching
{

    /// <summary>
    /// Interface has to be more complex to match up with 
    /// .NET 4.0's new abstracted solution.
    /// </summary>
        internal class WebCache : ICache
        {

            private Cache Cache
            {
                get { 
                    return HttpContext.Current.Cache;
                }
            }


            #region ICache Members

            public void Add(string key, object val)
            {
                Cache.Insert(key, val);
            }

            public void Add(string key, object val, DateTime absoluteExpiration)
            {
                Cache.Insert(key, val, null, absoluteExpiration, Cache.NoSlidingExpiration);
            }

            public void Add(string key, object val, TimeSpan slidingExpiration)
            {
                Cache.Insert(key, val, null, Cache.NoAbsoluteExpiration, slidingExpiration);
            }

            public void Add(string key, object val, DateTime absoluteExpiration, TimeSpan slidingExpiration)
            {
                Cache.Insert(key, val, null, absoluteExpiration, slidingExpiration);
            }

            public object Get(string key)
            {
                object ret = Cache.Get(key);

                return ret;
            }

            public void Remove(string key)
            {
                Cache.Remove(key);
            }

            #endregion
        }

}
