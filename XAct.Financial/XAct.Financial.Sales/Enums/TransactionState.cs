﻿namespace XAct.Financial.Sales.Entities.Enums
{
    /// <summary>
    /// An enumeration of possible Transaction states.
    /// </summary>
    public enum TransactionState
    {
        /// <summary>
        /// The State is not stated.
        /// </summary>
        Undefined=0,
        /// <summary>
        /// The Transcation is being processed
        /// </summary>
        Processing=1,
        /// <summary>
        /// The transaction is declined
        /// </summary>
        Declined=2,
        /// <summary>
        /// The transaction is approved
        /// </summary>
        Approved=3
    }
}