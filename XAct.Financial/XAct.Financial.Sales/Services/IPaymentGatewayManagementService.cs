﻿namespace XAct.Financial.Sales
{
    /// <summary>
    /// Contract for a service to manage Sales with Payment Gateway.
    /// </summary>
    public interface IPaymentGatewayManagementService : IHasXActLibService
    {

    }
}