﻿namespace XAct.Financial.Sales
{
    using System;
    using XAct.Financial.Sales.Entities;

    /// <summary>
    /// Contract for a service to process purchases 
    /// made via a Payment Gateway.
    /// </summary>
    public interface IPaymentGatewayService : IHasXActLibService
    {
        /// <summary>
        /// Creates the URI.
        /// </summary>
        /// <returns></returns>
        Uri CreateUri(TransactionRequest transactionRequest);


        /// <summary>
        /// Processes the callback from the Payment Gateway.
        /// </summary>
        /// <returns></returns>
        TransactionSummary ProcessPurchaseRequest();
    }
}