﻿namespace XAct.Financial.Sales.Implementations
{
    using System;
    using System.Collections.Specialized;
    using System.Globalization;
    using XAct.Financial.Sales.Entities;

    /// <summary>
    /// An implementation of the <see cref="IPaymentGatewayService"/>
    /// using the PayMate service provider.
    /// </summary>
    public class PayMatePaymentGatewayService : IPaymentGatewayService
    {
        //http://www.paymate.com/cms/index.php/resources/integrating-paymate/paymate-api


        /// <summary>
        /// Initializes a new instance of the <see cref="PayMatePaymentGatewayService"/> class.
        /// </summary>
        /// <param name="purchaseServiceAccount">The purchase service account.</param>
        public PayMatePaymentGatewayService(PaymentGatewayAccountIdentity purchaseServiceAccount)
        {

        }

        /// <summary>
        /// Creates the URI.
        /// </summary>
        /// <returns></returns>
        public Uri CreateUri(TransactionRequest transactionRequest)
        {
            throw new NotImplementedException();

            //UriBuilder uriBuilder = new UriBuilder();
            //uriBuilder.Host = "https://www.paymate.com/PayMate/ExpressPayment"
            //  ? mid = demonstration & amt = 100.00 & currency = USD


            //            amt	The payment amount
            //amt_editable (1)	Indicate if payment amount is editable or not
            //currency (2)	The currency code of the payment amount.
            //ref	A seller reference number for a transaction
            //pmt_sender_email	The buyer's email address
            //pmt_contact_firstname	The buyer's first name
            //pmt_contact_surname	The buyer's surname
            //pmt_contact_phone (3)	The buyer's phone number
            //pmt_country	The buyer's country.
            //regindi_state	The buyer's state.
            //regindi_address1	The first address line of the buyer's address
            //regindi_address2	The second address line of the buyer's address
            //regindi_sub	The suburb of the buyer's address
            //regindi_pcode	The postcode of the buyer's address
            //return (4)	A fully-qualified (eg: http://www.yoursite.com/paymentreceived.asp) URL to which Paymate will redirect the buyer. This redirect will occur when they click the continue button on the receipt page of the Express Payment wizard.
            //popup (5)	Indicate if your website has opened Paymate Express in a new window. Must be either true or false, default is true. If this value is true, the Paymate Express window will close before displaying the 'return' URL in the browser window which opened Paymate Express. If this value is false, the 'return' URL will be displayed in the same window as Paymate Express.
        }



        /// <summary>
        /// Processes the callback from the Payment Gateway.
        /// </summary>
        /// <returns></returns>
        public TransactionSummary ProcessPurchaseRequest()
        {
            NameValueCollection arguments = System.Web.HttpContext.Current.Request.QueryString;

            TransactionSummary transactionSummary = new TransactionSummary();

            transactionSummary.Amount = decimal.Parse(arguments["paymentAmount"]);
            transactionSummary.BuyerEmail = arguments["buyerEmail"];

            transactionSummary.CurrencyFK = (Entities.Enums.Currency) Enum.Parse(typeof(Entities.Enums.Currency), arguments["currency"]);
            transactionSummary.PaymentGatewayTransactionId = arguments["transactionID"];

            //This will contain the FK used to get back to the TransactionRequest
            transactionSummary.SellerTransactionId = arguments["ref"];

            transactionSummary.TransactionRequestFK = Int32.Parse(arguments["ref"]);
            
            DateTime transactionDateTime; 
            if (DateTime.TryParseExact(
                arguments["paymentDate"],
                new string[]{"dd-MMM-yyyy"},
                CultureInfo.InvariantCulture,
                DateTimeStyles.None, out transactionDateTime))
            {
                transactionSummary.TransactionDateTimeUTC = transactionDateTime.ToUniversalTime();
            }
            //transactionSummary.BillingAddress = ... 

            switch (arguments["responseCode"])
            {
                case "PP":
                    transactionSummary.StateFK = Entities.Enums.TransactionState.Processing;
                    break;
                case "PA":
                    transactionSummary.StateFK = Entities.Enums.TransactionState.Approved;
                    break;
                case "PD":
                    transactionSummary.StateFK = Entities.Enums.TransactionState.Declined;
                    break;
            }



            //            transactionID	The unique, 11 digit, Paymate identifier associated with each payment processed
            //responseCode	An identifier of the state of each transaction - see Response Codes below
            //paymentAmount	The payment amount authorised by the buyer
            //currency	The currency code of the payment ('AUD', 'USD','EUR','GBP','NZD')
            //paymentDate	The payment date. Format: dd-MMM-yyyy (eg: 01-JAN-2005)
            //ref	The seller reference number either provided to Paymate from a seller's website, or entered during the payment process.
            //buyerEmail	The email address of the buyer
            //billingAddress1	The first street address line of the billing address
            //billingAddress2	The second street address line of the billing address
            //billingCity	The city of the billing address
            //billingState	The state of the billing address
            //billingCountry	The country of the billing address
            //billingPostcode	The postcode of the billing address
            //billingFirstName	The buyer's first name
            //billingSurname	The buyer's surname

            return transactionSummary;
        }


    }
}
