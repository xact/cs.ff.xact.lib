﻿namespace XAct.Financial.Sales.Entities
{
    using System;
    using XAct.Financial.Sales.Entities.Enums;
    using XAct.Identity;
    using XAct.Identity.Entities.Location.Postal;

    /// <summary>
    /// The summary of a transaction, as reported by a Payment Gateway
    /// </summary>
    public class TransactionSummary
    {
        /// <summary>
        /// Gets or sets the UTC transaction date time.
        /// </summary>
        /// <value>
        /// The transaction date time.
        /// </value>
        public virtual DateTime TransactionDateTimeUTC { get; set; }

        /// <summary>
        /// The unique Payment Gateway's transactionId.
        /// </summary>
        /// <internal>
        /// For example, paymate's is an 11 character unique code.
        /// </internal>
        public virtual string PaymentGatewayTransactionId { get; set; }

        /// <summary>
        /// Gets or sets the seller transaction identifier
        /// allowing the vendor to correlate this TransactionSummary
        /// with the placed order.
        /// <para>
        /// This value will have been entered on the website,
        /// roundtripped to the Payment Gateway and back.
        /// </para>
        /// </summary>
        /// <value>
        /// The seller transaction identifier.
        /// </value>
        public virtual string SellerTransactionId { get; set; }


        /// <summary>
        /// Gets or sets the FK of the <see cref="TransactionRequest"/> associated to this response.
        /// </summary>
        public virtual int? TransactionRequestFK { get; set; }
        /// <summary>
        /// Gets or sets the transaction request associated to this response.
        /// </summary>
        public virtual TransactionRequest TransactionRequest { get; set; }

        /// <summary>
        /// Gets or sets the amount of the transaction.
        /// </summary>
        /// <value>
        /// The amount.
        /// </value>
        public virtual decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the FK to the CurrencyRecord
        /// of the currency in which the purchase was made.
        /// </summary>
        /// <value>
        /// The currency record foreign key.
        /// </value>
        public virtual Currency CurrencyFK { get; set; }

        /// <summary>
        /// Gets or sets the buyer's email.
        /// as entered at the Payment Gateway's 
        /// purchase form.
        /// </summary>
        /// <value>
        /// The buyer's email.
        /// </value>
        public virtual string BuyerEmail { get; set; }

        /// <summary>
        /// Gets or sets the FK of the <see cref="TransactionState"/>.
        /// of this <see cref="TransactionSummary"/>
        /// </summary>
        public virtual Entities.Enums.TransactionState StateFK { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="TransactionState"/>.
        /// of this <see cref="TransactionSummary"/>
        /// </summary>
        public virtual TransactionState State { get; set; }

        /// <summary>
        /// Gets or sets the FK of the billing address.
        /// </summary>
        public virtual int? BillingAddressFK { get; set; }

        /// <summary>
        /// Gets or sets the billing address.
        /// </summary>
        public virtual PostalAddress BillingAddress { get; set; }

        /// <summary>
        /// Gets or sets the FK of the buyer's Billing Address.
        /// </summary>
        public virtual int? ShippingAddressFK { get; set; }

        /// <summary>
        /// Gets or sets the buyer's Billing Address.
        /// </summary>
        public virtual PostalAddress ShippingAddress { get; set; }
    }
}