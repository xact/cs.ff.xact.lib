﻿

namespace XAct.Financial.Sales.Entities
{
    using System.Runtime.Serialization;
    using XAct.Messages;

    /// <summary>
    /// A record of the possible TransactionStates of a <see cref="TransactionSummary"/>
    /// </summary>
    [DataContract]
    public class TransactionState : ReferenceDataBase<Enums.TransactionState>
    {
    }
}
