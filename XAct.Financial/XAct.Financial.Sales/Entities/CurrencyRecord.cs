﻿namespace XAct.Financial.Sales.Entities
{
    using System.Runtime.Serialization;
    using XAct.Financial.Sales.Entities.Enums;
    using XAct.Messages;

    /// <summary>
    /// The currency of the transaction.
    /// </summary>
    [DataContract]
    public class CurrencyRecord : ReferenceDataBase<Currency>
    {
    }
}