﻿

namespace XAct.Financial.Sales.Entities
{
    /// <summary>
    /// Information required to identity the 
    /// Vendor to the Payment Payment.
    /// </summary>
    public class PaymentGatewayAccountIdentity
    {
        /// <summary>
        /// The vendor'saccount  username.
        /// </summary>
        public virtual string Name { get; set; }


        /// <summary>
        /// Gets or sets the vendor's account password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public virtual string Password { get; set; }
    }
}
