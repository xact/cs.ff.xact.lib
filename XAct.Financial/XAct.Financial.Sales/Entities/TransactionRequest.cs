namespace XAct.Financial.Sales.Entities
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Identity;
    using XAct.Identity.Entities.Location.Postal;

    /// <summary>
    /// 
    /// </summary>
    public class TransactionRequest : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp
    {
        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public virtual Guid Id { get; set; }



                /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db -- 
        /// so it's usable to determine whether to generate the 
        /// Guid <c>Id</c>.
        ///  </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        ///// <summary>
        ///// Gets or sets the fk of the <see cref="Entity"/> associated to this Purchase request.
        ///// </summary>
        //public virtual int? EntityFK { get; set; }

        ///// <summary>
        ///// Gets or sets the the <see cref="Entity"/> associated to this Purchase request.
        ///// </summary>
        //public virtual Entity Entity { get; set; }

        /// <summary>
        /// Gets or sets the FK of the billing address associated to this <see cref="TransactionRequest"/>.
        /// </summary>
        public virtual int BillingAddressFK { get; set; }

        /// <summary>
        /// Gets or sets the Billing address associated to this <see cref="TransactionRequest"/>.
        /// </summary>
        public virtual PostalAddress BillingAddress { get; set; }

        /// <summary>
        /// Gets or sets the FK of the Shipping address associated to this <see cref="TransactionRequest"/>.
        /// </summary>
        public virtual int ShippingAddressFK { get; set; }

        /// <summary>
        /// Gets or sets the Shipping address associated to this <see cref="TransactionRequest"/>.
        /// </summary>
        public virtual PostalAddress ShippingAddress { get; set; }
    }
}