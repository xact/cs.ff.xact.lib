namespace XAct.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Assistance;
    using XAct.IO;
    using XAct.Tests;


    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class HelpServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            XAct.Library.Settings.Bindings.ScanByInterfaces = true;
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void ServiceIsAvailable()
        {
            XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();


            Assert.IsTrue(helpService != null);
        }



        //[Test]
        //public void HelpServiceUsingResourceserviceToGetAnEntryString()
        //{
        //    //IHostSettingsService 

        //    XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();

        //    HelpEntrySummary result = helpService.GetById(1);

        //    Assert.IsFalse(result.IsNull());

        //}


        [Test]
        public void HelpServiceUsingResourceserviceToGetAnEntryResourceStringByKey()
        {
            //IHostSettingsService 

            XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();

            HelpEntrySummary result = helpService.GetByKey("SomeHelpA");

            Assert.IsFalse(result.IsNull());

        }


        [Test]
        public void HelpServiceUsingResourceserviceToGetAnEntryResourceStringByKey2()
        {
            //IHostSettingsService 

            XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();

            HelpEntrySummary result = helpService.GetByKey("SomeHelpB");

            Assert.IsFalse(result.IsNull());

        }



        [Test]
        public void HelpServiceUsingResourceserviceToGetAnEntryResourceStringByKeyThatHasChildren()
        {
            //IHostSettingsService 

            XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();

            HelpEntrySummary result = helpService.GetByKey("SomeHelpB",true);

            
            Assert.IsFalse(result.Children.Count < 2);

        }


        [Test]
        public void HelpServiceUsingResourceserviceToGetAnEntryResourceStringByKey3AsHtml()
        {
            //IHostSettingsService 

            XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();



            XAct.DependencyResolver.Current.GetInstance<IHelpServiceConfiguration>().TransformationFormat = Format.Html;

            //XAct.DependencyResolver.Current.GetInstance<XAct.Caching.ICultureInfoBasedCachingService>().Clear(null);

            //Get it after we have reset the transformation:
                        HelpEntrySummary result = helpService.GetByKey("SomeHelpB", true,null);


            //Clear again for next test:
                        XAct.DependencyResolver.Current.GetInstance<XAct.Caching.ICultureInfoBasedCachingService>().Clear(null);

            //Should have a result that is not wrapped in <p>...</p>
            Assert.IsTrue(result.Value.IndexOf("<p>",StringComparison.InvariantCultureIgnoreCase)==-1);

        }

        [Test]
        public void HelpServiceUsingResourceserviceToGetAnEntryResourceStringThatDoesNotExist()
        {
            //IHostSettingsService 

            XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();

            HelpEntrySummary result = helpService.GetByKey("SomeHelpNothing");


            Assert.IsNull(result);

        }



        [Test]
        public void GetHelpEntryByKey_A()
        {
            //IHostSettingsService 

            XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();

            HelpEntrySummary result = helpService.GetByKey("SomeHelpB", true);


            Assert.IsNotEmpty(result.Value);

        }

        [Test]
        public void Get_HelpEntry_By_Key_Where_Record_Has_Correct_Casing_Of_Resource_Entry()
        {
            //IHostSettingsService 

            XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();

            HelpEntrySummary result = helpService.GetByKey("WelcomeView", true);


            Assert.IsNotEmpty(result.Value);

        }

        

        [Test]
        public void Get_HelpEntry_By_Key_Where_Record_Has_Incorrect_Casing_Of_Resource_Entry()
        {
            //IHostSettingsService 

            XAct.Assistance.IHelpService helpService = XAct.DependencyResolver.Current.GetInstance<XAct.Assistance.IHelpService>();

            HelpEntrySummary result = helpService.GetByKey("HomeView", true);

            //This works as we are using the pcl version
            //of resources. If it were using the default/old one
            //it would need to be cased correctly.
            Assert.IsNotNullOrEmpty(result.Value);

        }

    }


}


