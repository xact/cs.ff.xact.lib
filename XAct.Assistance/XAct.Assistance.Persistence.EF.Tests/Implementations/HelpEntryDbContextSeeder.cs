﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using XAct.Assistance;
    using XAct;
    using XAct.Assistance.Initialization;
    using XAct.Assistance.Initialization.DbContextSeeders;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.Resources;
    using XAct.Services;


    /// <summary>
    /// A default implementation of the <see cref="IHelpEntryDbContextSeeder"/> contract
    /// to seed the Help tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class HelpEntryDbContextSeeder : XActLibDbContextSeederBase<HelpEntry>, IHelpEntryDbContextSeeder,
                                            IHasMediumBindingPriority
    {
        private readonly IHelpEntryCategoryDbContextSeeder _helpEntryCategoryDbContextSeeder;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="helpEntryResourceDbContextSeeder">The help entry resource database context seeder.</param>
        /// <param name="helpEntryCategoryDbContextSeeder">The help entry category database context seeder.</param>
        /// <param name="helpEntryTagDbContextSeeder">The help entry tag database context seeder.</param>
        public HelpEntryDbContextSeeder(ITracingService tracingService,
                                        IHelpEntryResourceDbContextSeeder helpEntryResourceDbContextSeeder,
                                        IHelpEntryCategoryDbContextSeeder helpEntryCategoryDbContextSeeder,
                                        IHelpEntryTagDbContextSeeder helpEntryTagDbContextSeeder
            )
            : base(
                tracingService,
                helpEntryResourceDbContextSeeder,
                helpEntryCategoryDbContextSeeder,
                helpEntryTagDbContextSeeder
                )
        {
            _helpEntryCategoryDbContextSeeder = helpEntryCategoryDbContextSeeder;
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        /// <param name="dbContext">The context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x => x.Key);
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities()
        {

            this.InternalEntities = new List<HelpEntry>();

            Guid parentFK;

            Guid category1Guid = _helpEntryCategoryDbContextSeeder.Entities.First(x => x.Name == "Core").Id;


            this.InternalEntities.Add(new HelpEntry
                {
                    Id = Guid.NewGuid(),
                    Enabled = true,
                    Key = "SomeHelpA",
                    ResourceFilter=string.Empty,
                    Text = "LoginForm.UserName.Label",
                    CategoryFK = category1Guid,
                    Description = "..."
                });


            this.InternalEntities.Add(new HelpEntry
                {
                    Id = parentFK = Guid.NewGuid(),
                    Enabled = true,
                    Key = "SomeHelpB",
                    ResourceFilter = string.Empty,
                    Text = "LoginForm",
                    CategoryFK = category1Guid,
                    Description = "..."
                });


            this.InternalEntities.Add(new HelpEntry
                {
                    Id = Guid.NewGuid(),
                    Enabled = true,
                    Key = "SomeHelpB.1",
                    ResourceFilter = string.Empty,
                    Text = "UserName",
                    CategoryFK = category1Guid,
                    ParentFK = parentFK, //Pointing back to parent that will be "SomeHelpB"
                    Description = "..."
                });

            this.InternalEntities.Add(new HelpEntry
                {
                    Id = Guid.NewGuid(),
                    Enabled = true,
                    Key = "SomeHelpB.2",
                    ResourceFilter = string.Empty,
                    Text = "Password",
                    CategoryFK = category1Guid,
                    ParentFK = parentFK, //Pointing back to parent that will be "SomeHelpB"
                    Description = "..."
                });


            this.InternalEntities.Add(new HelpEntry
                {
                    Enabled = true,
                    Key = "WelcomeView",
                    ResourceFilter = "SomeCategory",
                    Text = "WelcomeView",
                    CategoryFK = category1Guid,
                    ParentFK = parentFK, //Pointing back to parent that will be "SomeHelpB"
                    Description = "..."
                });


            this.InternalEntities.Add(new HelpEntry
                {
                    Enabled = true,
                    Key = "HomeView",
                    ResourceFilter = "SomeCategorY",
                    Text = "HomevieW",
                    CategoryFK = category1Guid,
                    ParentFK = parentFK, //Pointing back to parent that will be "SomeHelpB"
                    Description = "..."
                });




        }





    }
}
