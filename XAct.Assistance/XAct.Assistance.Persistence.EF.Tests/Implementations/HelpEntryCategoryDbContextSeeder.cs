﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Assistance;
    using XAct.Assistance.Initialization;
    using XAct.Assistance.Initialization.DbContextSeeders;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.Services;


    public class HelpEntryCategoryDbContextSeeder : XActLibDbContextSeederBase<HelpEntryCategory>, IHelpEntryCategoryDbContextSeeder, IHasMediumBindingPriority
    {
        public HelpEntryCategoryDbContextSeeder(ITracingService tracingService) : base(tracingService)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x => x.Name);
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<HelpEntryCategory>();


            this.InternalEntities.Add(new HelpEntryCategory { Id = Guid.NewGuid(), Enabled = true, Name = "Core" });
            this.InternalEntities.Add(new HelpEntryCategory { Id = Guid.NewGuid(), Enabled = true, Name = "Module1" });

        }
    }
}