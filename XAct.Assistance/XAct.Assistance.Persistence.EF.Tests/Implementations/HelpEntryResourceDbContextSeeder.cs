﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Assistance;
    using XAct.Assistance.Initialization;
    using XAct.Assistance.Initialization.DbContextSeeders;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.Resources;
    using XAct.Services;

    public class HelpEntryResourceDbContextSeeder : XActLibDbContextSeederBase<Resource>,
                                                    IHelpEntryResourceDbContextSeeder, IHasMediumBindingPriority
    {
        public HelpEntryResourceDbContextSeeder(ITracingService tracingService) : base(tracingService)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            this.SeedInternalHelper(dbContext, true, x => new { x.Filter, x.Key, x.CultureCode });
        }

        public override void CreateEntities()
        {

            this.InternalEntities = new List<Resource>();

            this.InternalEntities.Add(
                new Resource(Guid.NewGuid(), string.Empty, "LoginForm.UserName.Label",
                             "For god's sake...Just enter your name already!", ""));
            this.InternalEntities.Add(
                new Resource(Guid.NewGuid(), string.Empty, "LoginForm", "The login form is....", ""));
            this.InternalEntities.Add(
                new Resource(Guid.NewGuid(), string.Empty, "UserName", "The username is....", ""));
            this.InternalEntities.Add(
                new Resource(Guid.NewGuid(), string.Empty, "Password", "The password  is....", ""));
            this.InternalEntities.Add(
                new Resource(Guid.NewGuid(), "SomeCategory", "HomeView", "HomeView!", string.Empty));
            this.InternalEntities.Add(
                new Resource(Guid.NewGuid(), "SomeCategory", "WelcomeView", "WelcomeView!", string.Empty));

        }
    }
}