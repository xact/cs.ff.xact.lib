﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Assistance;
    using XAct.Assistance.Initialization;
    using XAct.Assistance.Initialization.DbContextSeeders;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.Services;

    public class HelpEntryTagDbContextSeeder : XActLibDbContextSeederBase<HelpEntryTag>, IHelpEntryTagDbContextSeeder, IHasMediumBindingPriority
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public HelpEntryTagDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x => x.Name);
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<HelpEntryTag>();

            // ReSharper restore RedundantCatchClause
            this.InternalEntities.Add(new HelpEntryTag { Id = Guid.NewGuid(), Enabled = true, Name = "UI" });
            this.InternalEntities.Add(new HelpEntryTag { Id = Guid.NewGuid(), Enabled = true, Name = "Validation" });

        }

    }
}