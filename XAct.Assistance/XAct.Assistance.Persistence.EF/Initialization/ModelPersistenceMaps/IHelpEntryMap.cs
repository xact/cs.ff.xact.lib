﻿namespace XAct.Assistance.Initialization.ModelPersistenceMaps
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Contract for an
    /// <see cref="EntityTypeConfiguration{HelpEntry}"/>
    /// based implementation 
    /// </summary>
    public interface IHelpEntryModelPersistenceMap : IHasXActLibModelPersistenceMap 
    {
        
    }
}