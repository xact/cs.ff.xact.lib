﻿namespace XAct.Assistance.Initialization.ModelPersistenceMaps
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Contract for an
    /// <see cref="EntityTypeConfiguration{HelpEntryTag}"/>
    /// based implementation 
    /// </summary>
    public interface IHelpEntryTagModelPersistenceMap : IHasXActLibModelPersistenceMap 
    {
        
    }
}