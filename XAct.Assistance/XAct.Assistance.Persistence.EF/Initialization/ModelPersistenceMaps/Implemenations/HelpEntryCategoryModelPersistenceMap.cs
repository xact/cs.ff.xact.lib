﻿namespace XAct.Assistance.Initialization.ModelPersistenceMaps.Implemenations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Defines a EF Map on how to persist a HelpEntryCategory into the database.
    /// </summary>
    public class HelpEntryCategoryModelPersistenceMap : EntityTypeConfiguration<HelpEntryCategory>, IHelpEntryCategoryModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryCategoryModelPersistenceMap" /> class.
        /// </summary>
        public HelpEntryCategoryModelPersistenceMap()
        {

            this.ToXActLibTable("HelpEntryCategory");

            this.HasKey(m => m.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(m => m.Name)
                .DefineRequired64CharName(colOrder++)
                ;
            this.Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;
        }
    }
}
