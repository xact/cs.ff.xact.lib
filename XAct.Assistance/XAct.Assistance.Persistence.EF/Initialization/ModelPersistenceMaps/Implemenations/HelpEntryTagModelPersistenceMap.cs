﻿namespace XAct.Assistance.Initialization.ModelPersistenceMaps.Implemenations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Defines a EF Map on how to persist a HelpEntryTag into the database.
    /// </summary>
    public class HelpEntryTagModelPersistenceMap : EntityTypeConfiguration<HelpEntryTag>, IHelpEntryTagModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryTagModelPersistenceMap" /> class.
        /// </summary>
        public HelpEntryTagModelPersistenceMap()
        {

            this.ToXActLibTable("HelpEntryTag");

            this.HasKey(m => m.Id);

            int colOrder = 0;

            this.Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);



            this.Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this.Property(m => m.Order)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this.Property(m => m.Name)
                .HasMaxLength(64)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;


        }
    }
}
