﻿namespace XAct.Assistance.Initialization.ModelPersistenceMaps.Implemenations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Defines a EF Map on how to persist a HelpEntry into the database.
    /// </summary>
    public class HelpEntryModelPersistenceMap : EntityTypeConfiguration<HelpEntry>, IHelpEntryModelPersistenceMap
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryModelPersistenceMap" /> class.
        /// </summary>
        public HelpEntryModelPersistenceMap()
        {

            this.ToXActLibTable("HelpEntry");
            this
                .HasKey(m => m.Id);

            int colOrder = 0;
            int indexMember = 1; //1 based.

            this
                .Property(m => m.Id)
                .DefineRequiredGuidId(colOrder++);

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);



            this
                .Property(m => m.ParentFK)
                .IsOptional()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Enabled)
                .DefineRequiredEnabled(colOrder++);

            this
                .Property(m => m.Key)
                .IsRequired()
                .HasMaxLength(256) //Not 64!
                .HasColumnOrder(colOrder++)
                .HasColumnAnnotation(
                    "Index",
                    new IndexAnnotation(
                        new IndexAttribute("IX_Key", indexMember++) {IsUnique = true}))
                ;

            this
                .Property(m => m.ResourceFilter)
                .DefineOptional256CharResourceFilter(colOrder++)
                ;

            this
                
                .Property(m => m.Title)
                .DefineOptional1024CharText(colOrder++)
                ;

            this
                .Property(m => m.Text)
                //(50lines x 80chars) Should be sufficient. Any longer and you're waffling. They want Help Not Tolstoy. 
                .DefineOptional4000CharText(colOrder++)
                ;

            this
                .Property(m => m.CategoryFK)
                .IsRequired()
                .HasColumnOrder(colOrder++)
                ;

            this
                .Property(m => m.Description)
                .DefineOptional4000CharDescription(colOrder++)
                ;



            //Association between Entry and Category
            this
                .HasRequired(m => m.Category)
                .WithMany(x=>x.HelpEntries)
                .HasForeignKey(m => m.CategoryFK);

            //Self-referential
            this
                .HasMany(m => m.Children)
                .WithOptional(m => m.Parent)
                .HasForeignKey(m => m.ParentFK);

            //Many-to-many to Tags
            this
                .HasMany(x => x.Tags)
                .WithMany(x => x.HelpEntries)
                .Map(x =>
                {
                    x.ToXActLibTable("HelpEntry_HelpEntryTag");
                    x.MapLeftKey("HelpEntryId");
                    x.MapRightKey("HelpTagId");
                });


        }
    }
}
