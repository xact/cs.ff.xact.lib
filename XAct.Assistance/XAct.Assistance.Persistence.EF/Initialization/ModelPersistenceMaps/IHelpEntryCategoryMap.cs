﻿namespace XAct.Assistance.Initialization.ModelPersistenceMaps
{
    using System.Data.Entity.ModelConfiguration;

    /// <summary>
    /// Contract for an
    /// <see cref="EntityTypeConfiguration{HelpEntryCategory}"/>
    /// based implementation 
    /// </summary>
    public interface IHelpEntryCategoryModelPersistenceMap : IHasXActLibModelPersistenceMap 
    {
        
    }
}