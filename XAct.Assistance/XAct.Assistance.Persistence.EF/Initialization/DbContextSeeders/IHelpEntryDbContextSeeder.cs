namespace XAct.Assistance.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{HelpEntry}"/>
    /// to seed the Help tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IHelpEntryDbContextSeeder : IHasXActLibDbContextSeeder<HelpEntry>
    {

    }
}