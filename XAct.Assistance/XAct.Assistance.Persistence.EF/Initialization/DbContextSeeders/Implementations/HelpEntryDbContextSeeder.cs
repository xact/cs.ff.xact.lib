﻿namespace XAct.Assistance.Initialization.DbContextSeeders.Implementations
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    /// <summary>
    /// A default implementation of the <see cref="IHelpEntryDbContextSeeder"/> contract
    /// to seed the Help tables with default data.
    /// </summary>
    /// <internal>
    /// Binding Priority is Low, so that UnitTests or Applications can replace the default Seeder with anything they care to.
    /// </internal>
    public class HelpEntryDbContextSeeder : XActLibDbContextSeederBase<HelpEntry>, IHelpEntryDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="helpEntryResourceDbContextSeeder">The help entry resource database context seeder.</param>
        /// <param name="helpEntryCategoryDbContextSeeder">The help entry category database context seeder.</param>
        /// <param name="helpEntryTagDbContextSeeder">The help entry tag database context seeder.</param>
        public HelpEntryDbContextSeeder(ITracingService tracingService,
            IHelpEntryResourceDbContextSeeder helpEntryResourceDbContextSeeder,
            IHelpEntryCategoryDbContextSeeder helpEntryCategoryDbContextSeeder,
            IHelpEntryTagDbContextSeeder helpEntryTagDbContextSeeder
            ): base(
            tracingService,
            helpEntryResourceDbContextSeeder,
            helpEntryCategoryDbContextSeeder,
            helpEntryTagDbContextSeeder
            )
        {
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }
    }
}
