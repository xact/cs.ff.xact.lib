﻿namespace XAct.Assistance.Initialization.DbContextSeeders.Implementations
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Resources;

    /// <summary>
    /// 
    /// </summary>
    public class HelpEntryResourceDbContextSeeder : XActLibDbContextSeederBase<Resource>, IHelpEntryResourceDbContextSeeder
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryResourceDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public HelpEntryResourceDbContextSeeder(ITracingService tracingService):base(tracingService)
        {
        }


        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }
    }
}