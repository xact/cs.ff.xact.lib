namespace XAct.Assistance.Initialization.Implementation
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Assistance.Initialization.ModelPersistenceMaps;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class HelpDbModelBuilder : IHelpDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public HelpDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<HelpEntryCategory>)XAct.DependencyResolver.Current.GetInstance<IHelpEntryCategoryModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<HelpEntryTag>)XAct.DependencyResolver.Current.GetInstance<IHelpEntryTagModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<HelpEntry>)XAct.DependencyResolver.Current.GetInstance<IHelpEntryModelPersistenceMap>());

        }
    }
}