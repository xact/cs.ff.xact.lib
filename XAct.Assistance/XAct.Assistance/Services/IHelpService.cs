﻿namespace XAct.Assistance
{
    using System.Globalization;
    using XAct.Assistance;

    /// <summary>
    /// Contract for a service that returns hierarchical Help records.
    /// </summary>
    public interface IHelpService : IHasXActLibService
    {

        /// <summary>
        /// Gets the singleton configuration object that 
        /// is injected into the implementation of <see cref="IHelpService"/>.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        IHelpServiceConfiguration Configuration { get; }

        ///// <summary>
        ///// Gets the Help Entry Resouce string by unique HelpEntry key.
        ///// </summary>
        ///// <param name="helpEntryId">The help entry id.</param>
        ///// <param name="includeChildrenIfAny">if set to <c>true</c> [include children if any].</param>
        ///// <param name="cultureInfo">The culture info.</param>
        ///// <returns></returns>
        //HelpEntrySummary GetById(int helpEntryId, bool includeChildrenIfAny = true, CultureInfo cultureInfo = null);

        /// <summary>
        /// Gets the Help Entry Resouce string by unique HelpEntry key.
        /// </summary>
        /// <param name="helpEntryKey">The key.</param>
        /// <param name="includeChildrenIfAny">if set to <c>true</c> [include children if any].</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        HelpEntrySummary GetByKey(string helpEntryKey, bool includeChildrenIfAny = true, CultureInfo cultureInfo = null);

    }
}


