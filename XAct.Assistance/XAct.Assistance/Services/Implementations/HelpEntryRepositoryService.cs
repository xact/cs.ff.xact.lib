﻿namespace XAct.Assistance.Implementations
{
    using System;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Services;

    /// <summary>
    /// An Entity Framework based implementation 
    /// of the <see cref="IHelpEntryRepositoryService"/>
    /// </summary>
    public class HelpEntryRepositoryService : IHelpEntryRepositoryService
    {
        private readonly ITracingService _tracingService;
        private readonly IRepositoryService _repositoryService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryRepositoryService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public HelpEntryRepositoryService(ITracingService tracingService, IRepositoryService repositoryService)
        {
            _tracingService = tracingService;
            _repositoryService = repositoryService;
        }


        /// <summary>
        /// Gets the entry by Help Entry Datastore id.
        /// <para>
        /// Note that we are not filtering out by Enabled. Nor ordering the children:
        /// </para>
        /// <para>
        /// The reason there is <see cref="GetEntryById"/> 
        /// and <see cref="GetEntryByKey"/> is that not all developers will
        /// write their apps the same way -- some applications will prefer associating Help by unt Id, 
        /// some by unique Key.
        /// </para>
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="includeChildren">if set to <c>true</c> [include children].</param>
        /// <returns></returns>
        public HelpEntry GetEntryById(Guid id, bool includeChildren=false)
        {
            if (includeChildren)
            {
                //Note that we are not filtering out by Enabled. Nor ordering the children:
                return _repositoryService.GetSingle<HelpEntry>(m => (m.Id == id), new IncludeSpecification<HelpEntry>(m => m.Children));
            }

            return _repositoryService.GetSingle<HelpEntry>(m => m.Id == id);
        }

        /// <summary>
        /// Gets the (Enabled) entry by Help Entry unique key (not the Resource Key).
        /// <para>
        /// Note that we are not filtering out by Enabled. Nor ordering the children:
        /// </para>
        /// <para>
        /// The reason there is <see cref="GetEntryById"/> 
        /// and <see cref="GetEntryByKey"/> is that not all developers will
        /// write their apps the same way -- some applications will prefer associating Help by unt Id, 
        /// some by unique Key.
        /// </para>
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="includeChildren">if set to <c>true</c> [include children].</param>
        /// <returns></returns>
        public HelpEntry GetEntryByKey(string key, bool includeChildren = false)
        {
            if (includeChildren)
            {
                return _repositoryService.GetSingle<HelpEntry>(m => (m.Key == key),
                                                               new IncludeSpecification<HelpEntry>(m => m.Children));
            }
            return _repositoryService.GetSingle<HelpEntry>(m => m.Key == key);
        }



        /// <summary>
        /// Gets the entries by tag.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        public HelpEntry[] GetEntriesByTag(string tag)
        {
            //_repositoryService.GetByFilter<HelpEntry>(h=>h.Tags.Contains())
            throw new NotImplementedException("GetEntriesByTag");
        }

    }
}