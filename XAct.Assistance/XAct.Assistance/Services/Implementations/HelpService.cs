﻿// ReSharper disable CheckNamespace
namespace XAct.Assistance.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Globalization;
    using System.Linq;
    using XAct.Caching;
    using XAct.IO;
    using XAct.IO.Transformations;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="IHelpService"/> to
    /// return hierarchical help records.
    /// </summary>
    public class HelpService : IHelpService
    {


        


        private readonly ICultureInfoBasedCachingService _cultureInfoBasedCachingService;
        private readonly IMarkdownService _markdownService;
        private readonly IHelpEntryRepositoryService _helpEntryRepositoryService;
// ReSharper disable RedundantNameQualifier
        private readonly XAct.Resources.IResourceService _resourceService;
        private readonly IHelpServiceConfiguration _helpServiceConfiguration;
// ReSharper restore RedundantNameQualifier



        /// <summary>
        /// Gets the singleton configuration object that
        /// is injected into the implementation of <see cref="IHelpService" />.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IHelpServiceConfiguration Configuration { get { return _helpServiceConfiguration; } }


        /// <summary>
        /// Initializes a new instance of the <see cref="HelpService" /> class.
        /// </summary>
        /// <param name="cultureInfoBasedCachingService">The context info based caching service.</param>
        /// <param name="markdownService"></param>
        /// <param name="helpEntryRepositoryService">The help entry repository service.</param>
        /// <param name="resourceService">The resource service.</param>
        /// <param name="helpServiceConfiguration"></param>
// ReSharper disable RedundantNameQualifier
        public HelpService(ICultureInfoBasedCachingService cultureInfoBasedCachingService, 
            XAct.IO.Transformations.IMarkdownService markdownService,
            IHelpEntryRepositoryService helpEntryRepositoryService, XAct.Resources.IResourceService resourceService,
            IHelpServiceConfiguration helpServiceConfiguration)
// ReSharper restore RedundantNameQualifier
        {
            _cultureInfoBasedCachingService = cultureInfoBasedCachingService;
            _markdownService = markdownService;
            _helpEntryRepositoryService = helpEntryRepositoryService;
            _resourceService = resourceService;
            _helpServiceConfiguration = helpServiceConfiguration;


        
            if (_helpServiceConfiguration.PostProcessTransformationMethod == null)
            {
                _helpServiceConfiguration.PostProcessTransformationMethod = PostProcessResource;
            }
        }


        /// <summary>
        /// Gets the Help Entry Resouce string by HelpEntry unique key.
        /// </summary>
        /// <param name="helpEntryKey">The key.</param>
        /// <param name="includeChildrenIfAny">if set to <c>true</c> [include children if any].</param>
        /// <param name="cultureInfo">The culture info.</param>
        /// <returns></returns>
        public HelpEntrySummary GetByKey(string helpEntryKey, bool includeChildrenIfAny = true, CultureInfo cultureInfo = null)
        {
            if (_helpServiceConfiguration.Updated)
            {
                lock (this)
                {
                    if (_helpServiceConfiguration.Updated)
                    {
                    _cultureInfoBasedCachingService.Clear(cultureInfo);
                    _helpServiceConfiguration.Updated = false;
                    }
                }
            }

            HelpEntrySummary result;
            if (_helpServiceConfiguration.DefaultCacheTimeSpan == TimeSpan.MinValue)
            {
                result = GetResouceByHelpEntryKey(helpEntryKey, includeChildrenIfAny, cultureInfo);
                return result;
            }



            if (_cultureInfoBasedCachingService.TryGet(cultureInfo, helpEntryKey, out result,
                                                       () => GetResouceByHelpEntryKey(helpEntryKey, true, cultureInfo),
                                                       _helpServiceConfiguration.DefaultCacheTimeSpan))
            {
                if (!includeChildrenIfAny && result.Children!=null)
                {
                    result.Children.Clear();
                }
            }
            return result;

        }












//        private HelpEntrySummary GetResouceByHelpEntryId(int id, bool includeChildren = false, CultureInfo cultureInfo = null)
//        {

//// ReSharper disable RedundantArgumentDefaultValue
//            HelpEntry helpEntry = _helpEntryRepositoryService.GetEntryById(id, false);
//// ReSharper restore RedundantArgumentDefaultValue

//            return GetChildEntries(helpEntry, includeChildren, cultureInfo,format);
//        }

        private HelpEntrySummary GetResouceByHelpEntryKey(string helpEntryKey, bool includeChildren = false, CultureInfo cultureInfo = null)
        {
// ReSharper disable RedundantArgumentDefaultValue
            HelpEntry helpEntry = _helpEntryRepositoryService.GetEntryByKey(helpEntryKey,false);
// ReSharper restore RedundantArgumentDefaultValue


            var result = GetChildEntries(helpEntry, includeChildren, cultureInfo,_helpServiceConfiguration.TransformationFormat);

            return result;
        }









        private HelpEntrySummary GetChildEntries(HelpEntry helpEntry, bool includeChildren, CultureInfo cultureInfo, Format format)
        {
            if (helpEntry == null)
            {
                return null;
            }
            string resource =
                ((!helpEntry.ResourceFilter.IsNullOrEmpty())&&(!helpEntry.ResourceFilter.IsNull()))
                    ? _resourceService.GetString(helpEntry.ResourceFilter, helpEntry.Text, cultureInfo)
                    : helpEntry.Text;
                


            HelpEntrySummary result = 
                new HelpEntrySummary
                    {
                        Key = helpEntry.Key, 
                        Value =_helpServiceConfiguration.PostProcessTransformationMethod(resource, format)
                    };


            if (includeChildren)
            {
                helpEntry.Children
                    .Where(s => s.Enabled && s.Enabled).OrderBy(s => s.Order).Select(s => new HelpEntrySummary
                    {
                        Key = s.Key,
                        Value = _helpServiceConfiguration.PostProcessTransformationMethod(
                        (
                        ((!s.ResourceFilter.IsNullOrEmpty()) && (!s.Text.IsNull()))
                        ? _resourceService.GetString( s.ResourceFilter, s.Text, cultureInfo)
                        : s.Text),
                        format)
                    }).ForEach(s => result.Children.Add(s));
            }

            return result;
        }

        private string PostProcessResource(string value, Format format)
        {
            if ((value == null)||(format == Format.Undefined))
            {
                return value;
            }
            //Default Post Processor does nothing...but should be 
            //here that we could slip in a Transformation Text mechanism...
            return _markdownService.Transform(value, format);

        }

    }
}