﻿namespace XAct.Assistance
{
    using System;
    using XAct.IO;

    /// <summary>
    /// Contract for a Configuration package to be injected
    /// into the constructor of an implementation of
    /// <see cref="IHelpServiceConfiguration"/>.
    /// 
    /// <para>
    /// Intended to be configured during application bootstrapping.
    /// </para>
    /// <para>
    /// Lifespan is Singleton.
    /// </para>
    /// </summary>
    public interface IHelpServiceConfiguration : IHasXActLibServiceConfiguration
    {
        

        /// <summary>
        /// Gets or sets a value indicating whether this
        /// <see cref="IHelpServiceConfiguration" /> is updated
        /// and values should be reapplied. 
        /// <see cref="IHelpService"/> should reset it.
        /// </summary>
        /// <value>
        ///   <c>true</c> if updated; otherwise, <c>false</c>.
        /// </value>
        bool Updated { get; set; }

        /// <summary>
        /// Gets or sets the default timespan in which 
        /// to cache <see cref="HelpEntry"/>s.
        /// <para>
        /// The default value is 
        /// <see cref="XAct.Library.Settings.Caching.DefaultReferenceCachingTimeSpan"/>
        /// </para>
        /// </summary>
        /// <value>
        /// The default cache time span.
        /// </value>
        TimeSpan DefaultCacheTimeSpan { get; set; }


        /// <summary>
        /// Gets or sets the Formatting options of .
        /// </summary>
        /// <value>
        /// The transformation format.
        /// </value>
        Format TransformationFormat { get; set; }


        /// <summary>
        /// Gets or sets the post process transformation method (which can convert Resources of Markdown to Html).
        /// </summary>
        /// <value>
        /// The post process transformation method.
        /// </value>
        Func<string, Format, string> PostProcessTransformationMethod { get; set; }

    }

}
