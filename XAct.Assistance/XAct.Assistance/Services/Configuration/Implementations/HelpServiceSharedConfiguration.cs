﻿namespace XAct.Assistance.Implementations
{
    using System;
    using XAct.IO;
    using XAct.Services;

    /// <summary>
    /// Implementation of <see cref="IHelpServiceConfiguration"/>
    /// to be instantiated in a bootstrap event, in order to 
    /// configure the <see cref="IHelpService"/>
    /// </summary>
    public class HelpServiceConfiguration : IHelpServiceConfiguration, IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// Gets or sets a value indicating whether this
        /// <see cref="IHelpServiceConfiguration" /> is updated
        /// and values should be reapplied.
        /// <see cref="IHelpService" /> should reset it.
        /// </summary>
        /// <value>
        ///   <c>true</c> if updated; otherwise, <c>false</c>.
        /// </value>
        public bool Updated { get; set; }

        /// <summary>
        /// Gets or sets the default timespan in which
        /// to cache <see cref="HelpEntry" />s.
        /// <para>
        /// The default value is 
        /// <see cref="XAct.Library.Settings.Caching.DefaultReferenceCachingTimeSpan"/>
        /// </para>
        /// </summary>
        /// <value>
        /// The default cache time span.
        /// </value>
        public TimeSpan DefaultCacheTimeSpan
        {
            get { return _defaultCacheTimeSpan; }
            set
            {
                _defaultCacheTimeSpan = value;
                Updated = true;
            }
        }

        private TimeSpan _defaultCacheTimeSpan;


        /// <summary>
        /// Gets or sets the transformation format.
        /// </summary>
        /// <value>
        /// The transformation format.
        /// </value>
        public Format TransformationFormat
        {
            get { return _transformationFormat; }
            set
            {
                _transformationFormat = value;
                Updated = true;
            }
        }

        private Format _transformationFormat;



        /// <summary>
        /// Gets or sets the post process Transformation method.
        /// </summary>
        /// <value>
        /// The post process method.
        /// </value>
        public Func<string, Format, string> PostProcessTransformationMethod
        {
            get { return _postProcessTransformationMethod; }
            set
            {
                _postProcessTransformationMethod = value;
                Updated = true;
            }
        }

        private Func<string, Format, string> _postProcessTransformationMethod;


        /// <summary>
        /// Initializes a new instance of the <see cref="HelpServiceConfiguration" /> class.
        /// </summary>
        public HelpServiceConfiguration()
        {
            _transformationFormat = Format.Undefined;
            _defaultCacheTimeSpan = XAct.Library.Settings.Caching.DefaultReferenceCachingTimeSpan;
        }
    }

}