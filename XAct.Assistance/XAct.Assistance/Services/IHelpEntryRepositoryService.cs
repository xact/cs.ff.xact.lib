﻿namespace XAct.Assistance
{
    using System;

    /// <summary>
    /// RepositoryService to retrieve and persist new HelpResource
    /// </summary>
    public interface IHelpEntryRepositoryService : IHasXActLibService
    {
        /// <summary>
        /// Gets the entry by Help Entry Datastore id.
        /// <para>
        /// The reason there is <see cref="GetEntryById"/> 
        /// and <see cref="GetEntryByKey"/> is that not all developers will
        /// write their apps the same way -- some applications will prefer associating Help by unt Id, 
        /// some by unique Key.
        /// </para>
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="includeChildren">if set to <c>true</c> [include children].</param>
        /// <returns></returns>
        HelpEntry GetEntryById(Guid id, bool includeChildren = false);

        /// <summary>
        /// Gets the entry by Help Entry unique key (not the Resource Key).
        /// <para>
        /// The reason there is <see cref="GetEntryById"/> 
        /// and <see cref="GetEntryByKey"/> is that not all developers will
        /// write their apps the same way -- some applications will prefer associating Help by unt Id, 
        /// some by unique Key.
        /// </para>
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="includeChildren">if set to <c>true</c> [include children].</param>
        /// <returns></returns>
        HelpEntry GetEntryByKey(string name, bool includeChildren=false);

        /// <summary>
        /// Gets the entries by Tag associated to the Entry.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <returns></returns>
        HelpEntry[] GetEntriesByTag(string tag);


    }
}
