namespace XAct.Assistance
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// The category for the <see cref="HelpEntry"/>
    /// </summary>
    [DataContract(Name="HelpEntryCategory")]
    public class HelpEntryCategory : IHasXActLibEntity, IHasDistributedGuidId, IHasEnabled, IHasName, IHasDescription
    {
        /// <summary>
        /// Gets or sets the datastore id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember(Name = "Id")]
        public virtual Guid Id { get; set; }




        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="HelpEntryTag" /> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "Enabled")]
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember(Name = "Name")]
        public virtual string Name { get; set; }



        /// <summary>
        /// Gets or sets the description.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember(Name = "Description")]
        public virtual string Description { get; set; }

        /// <summary>
        /// Gets or sets the HelpEntries this Tag is associated to.
        /// </summary>
        /// <value>
        /// The help entries.
        /// </value>
        [DataMember(Name = "HelpEntries")]
        public virtual ICollection<HelpEntry> HelpEntries { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryCategory"/> class.
        /// </summary>
        public HelpEntryCategory()
        {
            this.GenerateDistributedId();
        }

    }
}