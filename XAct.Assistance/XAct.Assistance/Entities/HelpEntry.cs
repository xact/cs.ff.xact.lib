namespace XAct.Assistance
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    ///   An XAct.Assistance class.
    /// </summary>
    [DataContract(Name="HelpEntry")]
    public class HelpEntry :
        IHasXActLibEntity, 
        IHasDistributedGuidIdAndTimestamp,
        IHasParentFK<Guid?>, 
        IHasHierarchyCollection<HelpEntry>, 
        IHasResourceFilterReadOnly,
        IHasTextAndTitleReadOnly,
        IHasEnabled, 
        IHasOrder, 
        IHasDescription //, System.Runtime.Serialization.IDeserializationCallback
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember(Name="Id")]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember(Name = "Timestamp")]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the FK of the parent entity (if any).
        /// </summary>
        /// <value>
        /// The parent FK.
        /// </value>
        [DataMember(Name = "ParentFK")]
        public virtual Guid? ParentFK { get; set; }


        /// <summary>
        /// Gets the parent item if any.
        /// <para>Member defined in<see cref="IHasHierarchyCollection{T}"/></para>
        /// </summary>
        /// <value>The parent.</value>
        [DataMember]
        public virtual HelpEntry Parent { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "Enabled")]
        public virtual bool Enabled { get; set; }


        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [DataMember(Name = "Order")]
        public virtual int Order { get; set; }


        /// <summary>
        /// Gets or sets the unique key of this help entry
        /// (not the same as the Resource Filter/Key)
        /// <para>
        /// A common strategy is to record the Page Identity 
        /// in the <c>Filter</c> property, and the Control
        /// Name in the <c>Key</c> property.
        /// </para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember]
        public virtual string Key { get; set; }


        




        ///// <summary>
        ///// Gets or sets a value indicating whether
        ///// to use the resource filter and key
        ///// to retrieve culture specific text or not.
        ///// </summary>
        //[DataMember]
        //public virtual bool UseResourceFilterAndKey { get; set; }


        /// <summary>
        /// Gets the title.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// <para>
        /// Member defined in the <see cref="IHasTitleReadOnly" /> contract.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Title { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// <para>
        /// If <see cref="ResourceFilter"/> is set, this is a Resource Key.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual string Text { get; set; }


        /// <summary>
        /// Gets or sets the filter -- 
        /// the name/identity of the View on which 
        /// the Control (ie, use Key) is about.
        /// or null if a global resource.
        /// </summary>
        /// <value>
        /// The filter.
        /// </value>
        [DataMember(Name = "ResourceFilter")]
        public virtual string ResourceFilter { get; set; }
        

        /// <summary>
        /// Gets or sets an optional description of the Help entry.
        /// <para>Member defined in<see cref="IHasDescriptionReadOnly" /></para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember]
        public virtual string Description { get; set; }



        /// <summary>
        /// Gets or sets the FK of the  <see cref="HelpEntryCategory"/>
        /// associated to this <c>HelpEntry</c>.
        /// </summary>
        [DataMember]
        public virtual Guid CategoryFK { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="HelpEntryCategory"/>
        /// associated to this <c>HelpEntry</c>.
        /// </summary>
        /// <value>
        /// The category.
        /// </value>
        [DataMember]
        public virtual HelpEntryCategory Category { get; set; }


        /// <summary>
        /// Gets or sets the optional list of tags 
        /// associated to this <see cref="HelpEntry"/>.
        /// </summary>
        /// <value>
        /// The tags.
        /// </value>
        [DataMember]
        public virtual ICollection<HelpEntryTag> Tags { get { return _tags ?? (_tags = new Collection<HelpEntryTag>()); } }
        private ICollection<HelpEntryTag> _tags;

        /// <summary>
        /// Gets the collection of children items.
        /// <para>Member defined in<see cref="IHasHierarchyCollection{T}"/></para>
        /// </summary>
        /// <value>The children.</value>
        [DataMember]
        public virtual ICollection<HelpEntry> Children
        {
            get { return _children; }
            set { _children = value; }
        }
        private ICollection<HelpEntry> _children;



        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntry" /> class.
        /// </summary>
        public HelpEntry()
        {
            this.GenerateDistributedId();
            OnDeserializing(default(StreamingContext));
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext streamingContext)
        {
            if (_children == null){_children = new Collection<HelpEntry>();}
        }

    }
}