namespace XAct.Assistance
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// An optional tag for a <see cref="HelpEntry"/>
    /// </summary>
    [DataContract(Name = "Enabled")]
    public class HelpEntryTag : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp
    {
        /// <summary>
        /// Gets or sets the datastore id.
        /// </summary>
        /// <value>
        /// The id.
        /// </value>
        [DataMember(Name = "Id")]
        public virtual Guid Id { get; set; }



        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember(Name = "Timestamp")]
        public virtual byte[] Timestamp { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="HelpEntryTag" /> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember(Name = "Enabled")]
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets an integer hint of the item's order.
        /// <para>
        /// Member defined in <see cref="IHasOrder" />.
        /// </para>
        /// </summary>
        /// <value>
        /// The order.
        /// </value>
        [DataMember(Name = "Order")]
        public virtual int Order { get; set; }

        /// <summary>
        /// Gets the name of the object.
        /// <para>Member defined in<see cref="XAct.IHasName" /></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember(Name = "Name")]
        public virtual string Name { get; set; }


        /// <summary>
        /// Gets or sets the HelpEntries this Tag is associated to.
        /// </summary>
        /// <value>
        /// The help entries.
        /// </value>
        [DataMember(Name = "HelpEntries")]
        public virtual ICollection<HelpEntry> HelpEntries { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntryTag"/> class.
        /// </summary>
        public HelpEntryTag()
        {
            this.GenerateDistributedId();

        }

    }
}