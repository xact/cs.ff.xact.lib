﻿namespace XAct.Assistance
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "HelpEntrySummary")]
    public class HelpEntrySummary : IHasChildrenCollectionReadOnly<HelpEntrySummary>//, System.Runtime.Serialization.IDeserializationCallback
    {
        /// <summary>
        /// Gets or sets the key.
        /// <para>Member defined in<see cref="IHasKey" /></para>
        /// </summary>
        /// <value>
        /// The key.
        /// </value>
        [DataMember(Name = "Key")]
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        [DataMember(Name="Value")]
        public string Value { get; set; }


        /// <summary>
        /// Gets or sets a collection of nested child entries, if any exist.
        /// </summary>
        /// <value>
        /// The child entries.
        /// </value>
        public ICollection<HelpEntrySummary> Children { get { return _children ?? (_children = new Collection<HelpEntrySummary>()); } }
        [DataMember(Name = "Children")]
        private ICollection<HelpEntrySummary> _children;


        /// <summary>
        /// Initializes a new instance of the <see cref="HelpEntrySummary"/> class.
        /// </summary>
        public HelpEntrySummary()
        {
            OnDeserializing(default(StreamingContext));
        }

        [OnDeserializing]
        private void OnDeserializing(StreamingContext streamingContext)
        {
        }

    }
}
