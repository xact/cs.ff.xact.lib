﻿
namespace XAct.Tests
{
    using System.IO;
    using System.Reflection;
    using System.Web;
    using System.Web.SessionState;

    public class HttpContextHelper
    {
        /// <summary>
        /// Usage 
        /// <para>
        /// <code>
        /// <![CDATA[
        /// HttpContext.Current = MockHelper.FakeHttpContext();
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <returns></returns>
        /// <internal>
        /// See: http://stackoverflow.com/a/10126711/1052767
        /// </internal>
        public static HttpContext FakeHttpContext()
        {
            var httpRequest = new HttpRequest("", "http://fake.env/", "");
            var stringWriter = new StringWriter();
            var httpResponce = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponce);

            var sessionContainer = new HttpSessionStateContainer("id", new SessionStateItemCollection(),
                                                    new HttpStaticObjectsCollection(), 10, true,
                                                    HttpCookieMode.AutoDetect,
                                                    SessionStateMode.InProc, false);

            httpContext.Items["AspSession"] = typeof(HttpSessionState).GetConstructor(
                                        BindingFlags.NonPublic | BindingFlags.Instance,
                                        null, CallingConventions.Standard,
                                        new[] { typeof(HttpSessionStateContainer) },
                                        null)
                                .Invoke(new object[] { sessionContainer });

            return httpContext;
        }
    }
}
