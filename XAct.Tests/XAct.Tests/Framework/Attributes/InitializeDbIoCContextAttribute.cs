﻿using System.Diagnostics;

namespace XAct.Tests
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using XAct;
    using XAct.Domain.Repositories;
    using XAct.Domain.Repositories.Configuration;
    using XAct.Settings;
    using XAct.Tests.Implementations;


    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Assembly, AllowMultiple = true)]
    public class InitializeDbIoCContextAttribute : Attribute, ITestAction
    {
        private readonly Type _dbContextType;
        private readonly bool _allowSqlCompactEdition;
        private readonly Type[] _entityTypesToSeed;
        private readonly Type[] _seederTypesToRegister;
        private readonly string _sqlServerConnectionStringName;
        private readonly string _sqlServerCEConnectionStringName;


        /// <summary>
        /// Initializes a new instance of the <see cref="InitializeDbIoCContextAttribute" /> class.
        /// </summary>
        /// <param name="dbContextType">Type of the database context.</param>
        /// <param name="allowSqlCompactEdition">if set to <c>true</c> [allow SQL compact edition].</param>
        /// <param name="customSeederTypesToRegister">The custom seeder types automatic register.</param>
        /// <param name="entityTypesToSeed">The entity types automatic seed.</param>
        /// <param name="sqlServerConnectionStringName">Name of the SQL server connection string.</param>
        /// <param name="sqlServerCeConnectionStringName">Name of the SQL server ce connection string.</param>
        public InitializeDbIoCContextAttribute(
            Type dbContextType,
            bool allowSqlCompactEdition = true,
            Type[] customSeederTypesToRegister = null,
            Type[] entityTypesToSeed = null,
            string sqlServerConnectionStringName = "UnitTests_SQLServer",
            string sqlServerCeConnectionStringName = "UnitTests_SQLServer_CE")
        {
            _dbContextType = dbContextType;
            _allowSqlCompactEdition = allowSqlCompactEdition;
            _seederTypesToRegister = customSeederTypesToRegister;
            _entityTypesToSeed = entityTypesToSeed;
            _sqlServerConnectionStringName = sqlServerConnectionStringName;
            _sqlServerCEConnectionStringName = sqlServerCeConnectionStringName;
        }




        #region ITestAction Members

        /// <summary>
        /// Executed before each test is run
        /// <para>
        /// An implementation of NUnit's <see cref="ITestAction"/>  contract.
        /// </para>
        /// </summary>
        /// <param name="testDetails">Provides details about the test that is going to be run.</param>
        public void BeforeTest(TestDetails testDetails)
        {
            BeforeTest(_dbContextType,
                _sqlServerConnectionStringName,
                _sqlServerCEConnectionStringName,
                _allowSqlCompactEdition,
                _entityTypesToSeed,
                _seederTypesToRegister,
                testDetails);
        }

        /// <summary>
        /// Executed after each test is run
        /// <para>
        /// An implementation of NUnit's <see cref="ITestAction"/>  contract.
        /// </para>
        /// </summary>
        /// <param name="testDetails">Provides details about the test that has just been run.</param>
        public void AfterTest(TestDetails testDetails)
        {
            //Set up for next test:
            Singleton<DbIoCContext>.Instance.ResetIoC();
        }

        /// <summary>
        /// Provides the target for the action attribute
        /// <para>
        /// An implementation of NUnit's <see cref="ITestAction"/>  contract.
        /// </para>
        /// </summary>
        /// <returns>The target for the action attribute</returns>
        public ActionTargets Targets { get; private set; }

        #endregion


        /// <summary>
        /// Static method that can be invoked from any UNitTest Fixture setup.
        /// </summary>
        /// <param name="dbContextType">Type of the database context.</param>
        /// <param name="sqlServerConnectionStringName">Name of the SQL server connection string.</param>
        /// <param name="sqlServerCeConnectionStringName">Name of the SQL server ce connection string.</param>
        /// <param name="allowSqlCompactEdition">if set to <c>true</c> [allow SQL compact edition].</param>
        /// <param name="_entityTypesToSeed">The _entity types to seed.</param>
        /// <param name="_seederTypesToRegister">The _seeder types to register.</param>
        /// <param name="testDetails">The test details.</param>
        public static void BeforeTest(
            Type dbContextType,
            string sqlServerConnectionStringName = "UnitTests_SQLServer",
            string sqlServerCeConnectionStringName = "UnitTests_SQLServer_CE",
            bool allowSqlCompactEdition = true,
            Type[] _entityTypesToSeed=null,
            Type[] _seederTypesToRegister = null,
            TestDetails testDetails = null
            )
        {
            Debug("BeforeTest...");
            
            /* ORIGINAL:
            _databaseTestLifeCycleController = InitDatabaseTestLifeCycleController(_allowSqlCompactEdition);
            _databaseTestLifeCycleController.InitDatabaseBeforeTestCase();

            Singleton<DatabaseContext>.Instance.DbContext = _databaseTestLifeCycleController.CreateDbContext(); //creates db context to be used when seeding
            Singleton<DatabaseContext>.Instance.Seeders.Reset(); //resets the registered seeders to default seeder types


            Singleton<DatabaseContext>.Instance.DbContext.Dispose(); //the context is disposed so that the data is not cached and are retrieved from the database during the tests

            var dbContext = _databaseTestLifeCycleController.CreateDbContext();
            Singleton<DatabaseContext>.Instance.RegisterDbContext(dbContext);
             */

            //----------
            NConfigInitializer.EnableNConfig = true;

            //get singleton, which:
            // * invokes its Initialize:
            //    *  constructor will ensure NConfig has merged everything
            DbIoCContext dbIoCContext = Singleton<DbIoCContext>.Instance;


            //----------
            // IMPORTANT: Reset the ioc container, so that Seeders will be created afresh
            // This is important when dealing with seeders, as the Seeders add entities
            // to their array, each time they are invoked. If the context is the same, 
            // the second time the Seeder is invoked (from another test), it adds the same
            // items to the array *again*. As they often have the same Id, when the Seeeder
            // tries to add them to the db, you start seeing 'duplicate Key' errors.
            dbIoCContext.ResetIoC();
            //----------
            // Now that NConfig is complete, Are we allowed to use sqlServer?
            bool useSqlServerCe = ((allowSqlCompactEdition) && (DbIoCContext.EnableCompactEditionSetting));

            // Now that we know if using sqlserverce, we were given two ConnectionString names, so choose one:
            string connectionStringName = (useSqlServerCe)
                                          ? sqlServerCeConnectionStringName
                                          : sqlServerConnectionStringName;
            //----------
            //Create a Controller to manage the creation of a new database & its seeding before a testfixture is run.
            //Note: the creation of the Controller didn't actually create/seed the database:
            Debug("BeforeTest:Create Controller...");
            IDatabaseLifeCycleController databaseTestLifeCycleController =
                    useSqlServerCe
                    ? (IDatabaseLifeCycleController)new SqlCeResetedByCopyingDbFileDatabaseLifeCycleController(dbContextType, connectionStringName)
                    : new SqlDropInitDbAlwaysLifeCycleController(dbContextType, connectionStringName);
            //----------
            if (useSqlServerCe)
            {
                //The following creates a new DbContext, and 
                // * if we're keeping the SqlCe file between runs it 
                //    * invokes InitializeDatabaseBeforeSingleTest (which just copies backup over current), and 
                //    * sets it to a null initializer.
                // * or else it (normal behavior):
                //    * Sets initializer to a custom DropCreateDatabaseAlwaysBasedOnTheCurrentModelInitializer
                //    * forces it to Initialize, which in effect drops/rebuilds model,
                //    * finally, invokes our custom dbContext.Seed method 
                //        * TODO: that's super messy (should be able to kick off seeding a better way)
                //          but our initializer is a null...and who else would invoke it.
                //    * then backs it up so that it can copied back later with 
                //      databaseTestLifeCycleController.InitializeDatabaseBeforeSingleTest
                databaseTestLifeCycleController.InitializeDatabaseBeforeTestFixture();
            }
            //----------
            // * In the case of SqlCe, it 
            //     * copies any existing Backup file over the current Db file.
            // * In the case of SqlServer, it 
            //     * creates a new DbContext,
            //     * sets the Initializer to a DropCreateDatabaseAlwaysBasedOnTheCurrentModelInitializer, then 
            //     * forces the drop/create of the Model, 
            //     * then Seeding occurs.
            databaseTestLifeCycleController.InitializeDatabaseBeforeSingleTest();
            //----------
            //At this point, seeding is complete.
            //----------
            //Instantiate a new DbContext, and save a pointer to it in the DbIoCContext.
            //As the Constructor is called, so (in the case of SqlServerCe) Model is built and seeding occurs:
            var dbContext = databaseTestLifeCycleController.CreateDbContext();
            dbIoCContext.SetDbContext(connectionStringName, dbContext);
            //MM ORIGINAL:
            //Singleton<DbIoCContext>.Instance.Seeders.Reset(); //resets the registered seeders to default seeder types
            //if (_seederTypesToRegister != null) _seederTypesToRegister.ToList().ForEach(seederType => Singleton<DbIoCContext>.Instance.Seeders.RegisterSeeder(seederType));
            //if (_entityTypesToSeed != null) _entityTypesToSeed.ToList().ForEach(entityType => Singleton<DbIoCContext>.Instance.Seed(entityType));
            //----------
            //IMPORTANT:
            //the context is disposed so that data is not cached and is retrieved from the database during the tests
            Singleton<DbIoCContext>.Instance.DbContext.Dispose();
            //----------
            //Create and Register a new dbContext in a factory, so that it is available via IUnitOfWork:
            dbContext = databaseTestLifeCycleController.CreateDbContext();

            IUnitOfWorkServiceConfiguration x =
                XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkServiceConfiguration>();
            x.SetFactoryDelegate(() => new EntityDbContext(dbContext));
            //----------
            Debug("BeforeTest:Done.");
            Debug(".");
        }

        private static void Debug(string message)
        {
//            System.IO.File.AppendAllText("C:\\tmp\\debug.txt",
//                "{0}: {1}.{2}{3}"
//                    .FormatStringInvariantCulture(
//                        DateTime.Now.ToString("hh:mm:ss.fff tt"),
//                        System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
//                        message,
//                        Environment.NewLine));
        }


    }
}