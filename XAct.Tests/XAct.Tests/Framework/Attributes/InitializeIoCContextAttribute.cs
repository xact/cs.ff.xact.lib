﻿using System;
using NUnit.Framework;
using XAct.Settings;

namespace XAct.Tests
{
    using XAct;

    /// <summary>
    /// Implementation of the NUnit specific <see cref="ITestAction"/> 
    /// Attribute contract, to run operations before and after
    /// each test.
    /// <para>
    /// Attribute applied to each test.
    /// </para>
    /// <para>
    /// Creates a new IoC container for each test, keeping tests isolated.
    /// </para>
    /// <para>
    /// See: http://martinfowler.com/articles/nonDeterminism.html regarding
    /// keeping tests isolated so that they can be run in any order.
    /// </para>
    /// <para>
    /// Note that MF's entry is written as general advice, across all languages,
    /// wherease running Test Suites operations are run from top to bottom, deterministically.
    /// and therefore it's far cheaper to setup at the top of a Test Suite (that's why they are 
    /// called a Test *Suite* and not just *Tests* file.
    /// </para>
    /// <para>
    /// But whatever. That's what we are doing here.
    /// </para>
    /// <para>
    /// NOTE:
    /// This approach has some advantages over the more conventional way of
    /// achieving the same thing
    /// is to make unit tests inherit from a base class
    /// that has two methods, one decorated with NUnit's
    /// <see cref="NUnit.Framework.SetUpAttribute"/> and the other with the 
    /// <see cref="NUnit.Framework.TearDownAttribute"/>.
    /// The advantage is that each test can assign specific args, per test.
    /// Whereas the attributes only allow one setup per file.
    /// </para>
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class | AttributeTargets.Interface | AttributeTargets.Assembly, AllowMultiple = true)]
    public class InitializeIoCContextAttribute : Attribute, ITestAction
    {
        /// <summary>
        /// Executed before each test is run
        /// </summary>
        /// <param name="testDetails">Provides details about the test that is going to be run.</param>
        public void BeforeTest(TestDetails testDetails)
        {
            //----------
            NConfigInitializer.EnableNConfig = true;
            //Ensure we have an IoCContext singleton instance created.
#pragma warning disable 168
            var x = Singleton<IocContext>.Instance;
#pragma warning restore 168
        }

        /// <summary>
        /// Executed after each test is run
        /// </summary>
        /// <param name="testDetails">Provides details about the test that has just been run.</param>
        public void AfterTest(TestDetails testDetails)
        {
            //Get hold of the IoCContext Singleton and reset it before the next test.
            Singleton<IocContext>.Instance.ResetIoC();
        }

        /// <summary>
        /// Provides the target for the action attribute
        /// </summary>
        /// <returns>The target for the action attribute</returns>
        public ActionTargets Targets { get; private set; }
    }
}