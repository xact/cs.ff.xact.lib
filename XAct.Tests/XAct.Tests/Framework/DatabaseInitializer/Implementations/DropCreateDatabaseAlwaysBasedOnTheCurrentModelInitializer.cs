﻿namespace XAct.Tests.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Migrations;

    public class DropCreateDatabaseAlwaysBasedOnTheCurrentModelInitializer<TContext> : IDatabaseInitializer<TContext> where TContext : DbContext
    {
        private readonly string _connectionName;

        /// <summary>
        /// Initializes a new instance of the <see cref="DropCreateDatabaseAlwaysBasedOnTheCurrentModelInitializer{TContext}"/> class.
        /// </summary>
        /// <param name="connectionName">Name of the connection.</param>
        public DropCreateDatabaseAlwaysBasedOnTheCurrentModelInitializer(string connectionName)
        {
            _connectionName = connectionName;
        }

        /// <summary>
        /// Initializes the database.
        /// </summary>
        /// <param name="context">The context.</param>
        [System.Diagnostics.DebuggerNonUserCode]
        public void InitializeDatabase(TContext context)
        {
            //HACK:
            //Because AutomaticMigrationsEnabled = true
            //you'll get:
            //SetUp : System.Data.Entity.Migrations.Infrastructure.MigrationsException : Automatic migrations 
            //that affect the location of the migrations history system table (such as default schema changes) 
            //are not supported. 
            //Please use code-based migrations for operations that affect the 
            //location of the migrations history system table.
            //So:
            XAct.Library.Settings.Db.DefaultXActLibSchema = null;

            context.Database.Delete();

            var contextType = typeof(TContext);



            var dbMigrationConfiguration = new DbMigrationsConfiguration<TContext>
            {
                ContextType = contextType,
                AutomaticMigrationsEnabled = true,
                MigrationsAssembly = contextType.Assembly,
                MigrationsNamespace = contextType.Namespace,
                TargetDatabase = new DbConnectionInfo(_connectionName)
            };

            //Migrate:
            MigrateMe(dbMigrationConfiguration);

            //Set initializer:
            SetInitializer();
            //We can invoke initialize, but only after we set the initializer to a NullInitalizer:
            context.Database.Initialize(true);
        }

        [System.Diagnostics.DebuggerHidden]
        [System.Diagnostics.DebuggerStepThrough]
        [System.Diagnostics.DebuggerNonUserCode]
        private void SetInitializer()
        {
            Database.SetInitializer(new NullDatabaseInitializer<TContext>());
        }

        [System.Diagnostics.DebuggerHidden]
        [System.Diagnostics.DebuggerStepThrough]
        [System.Diagnostics.DebuggerNonUserCode]
        private void MigrateMe(DbMigrationsConfiguration<TContext> dbMigrationConfiguration)
        {
            var migrator = new DbMigrator(dbMigrationConfiguration);
            migrator.Update();
        }
    }
}
