﻿
namespace XAct.Tests.Data.Seeders
{
    using XAct.Data.EF.CodeFirst.Seeders.Implementations;

    public abstract class UnitTestDbContextSeederBase<TEntity> : DbContextSeederBase<TEntity>
        where TEntity : class
    {



    }
}