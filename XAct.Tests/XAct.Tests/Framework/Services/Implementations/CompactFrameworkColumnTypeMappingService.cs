﻿
namespace XAct.Tests.Services.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;

    [DefaultBindingImplementation(typeof(IColumnTypeMappingService), BindingLifetimeType.Undefined, Priority.Low)]
    public class CompactFrameworkColumnTypeMappingService : IColumnTypeMappingService, IHasXActLibService
    {
        private readonly IDictionary<string, string> _columnMapping;

        public CompactFrameworkColumnTypeMappingService()
        {
            this._columnMapping = new Dictionary<string, string>
                                 {
                                     {"datetimeoffset", /*column type will be changed to */ "datetime"},
                                     {"date", /*column type will be changed to */ "datetime"},
                                     //compact edition doesn't have 'date' type
                                     {"nvarchar(max)", /*column type will be changed to */ "ntext"}
                                     //compact edition cannot do more than 4000 characters in nvarchar and serialized identity is longer
                                 };
        }

        public string GetColumnType(string columnType)
        {
            return _columnMapping[columnType];
        }
    }
}