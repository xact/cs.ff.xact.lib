﻿namespace XAct.Tests
{
    public interface IColumnTypeMappingService : IHasXActLibService
    {
        string GetColumnType(string columnType);
    }
}