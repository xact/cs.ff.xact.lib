﻿namespace XAct.Tests
{
    using System.Data.Entity;

    /// <summary>
    /// Contract for a Controller to manage the creation 
    /// of a new database, and seeding it, before a
    /// test is run.
    /// </summary>
    public interface IDatabaseLifeCycleController
    {
        //string DbContextConnectionString { get; }

        /// <summary>
        /// Creates a Test run specific 
        /// subclass of
        /// <see cref="DbContext"/>.
        /// <para>
        /// Member defined in <see cref="IDatabaseLifeCycleController"/>
        /// </para>
        /// </summary>
        /// <returns></returns>
        DbContext CreateDbContext();

        /// <summary>
        /// Initialize and Seed Database before running a
        /// suite/file of tests
        /// (decorated with <see cref="InitializeIoCContextAttribute"/>)
        /// <para>
        /// Member defined in <see cref="IDatabaseLifeCycleController"/>
        /// </para>
        /// </summary>
        void InitializeDatabaseBeforeTestFixture();

        /// <summary>
        /// Initialize and Seed Database before running an individual test
        /// (one decorated with <see cref="InitializeIoCContextAttribute"/>)
        /// <para>
        /// Member defined in <see cref="IDatabaseLifeCycleController"/>
        /// </para>
        /// </summary>
        void InitializeDatabaseBeforeSingleTest();
    }
}