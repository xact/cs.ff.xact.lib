﻿namespace XAct.Tests.Implementations
{
    using System;
    using System.Data.Entity;
    using System.Reflection;

    public static class X
    {

        public static void SetDatabaseInitializer(this DbContext dbContext, Type databaseInitializerType,
                                                  Type dbContextType = null, params object[] args)
        {
            if (dbContextType == null)
            {
                dbContextType = dbContext.GetType();
            }

            Type genericInitializerType = databaseInitializerType;
            Type genericInstanceType = genericInitializerType.MakeGenericType(dbContextType);

            object genericInstance =
                (args == null)
                    ? Activator.CreateInstance(genericInstanceType)
                    : Activator.CreateInstance(genericInstanceType, args);


            Type targetType = typeof (Database);
            var targetTypeMethodInfo =
                targetType.GetMethod("SetInitializer", BindingFlags.Public | BindingFlags.Static)
                          .MakeGenericMethod(dbContextType);
            targetTypeMethodInfo.Invoke(null, new[] {genericInstance});
        }
    }
}