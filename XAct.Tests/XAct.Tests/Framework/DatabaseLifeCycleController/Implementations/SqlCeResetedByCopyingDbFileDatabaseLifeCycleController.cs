﻿using System.Reflection;
using XAct.Data.EF.CodeFirst;
using XAct.Data.EF.CodeFirst.Seeders.Implementations;

namespace XAct.Tests.Implementations
{
    using System;
    using System.Configuration;
    using System.Data.Entity;
    using System.IO;
    using Microsoft.Practices.Unity;
    using XAct.Tests.Services.Implementations;


    public class SqlCeResetedByCopyingDbFileDatabaseLifeCycleController : IDatabaseLifeCycleController
    {
        public const string settingKeyName = "KeepSlqceDatabaseBetweenTestRuns";

        private string DbContextConnectionStringName { get; set; }
        private readonly Type _dbContextType;


        /// <summary>
        /// Gets a value indicating whether slq ce database file will be kept between test runs.
        /// </summary>
        /// <value>
        /// <c>true</c> if [keep slqce database between test runs]; otherwise, <c>false</c>.
        /// </value>
        public static bool KeepSlqceDatabaseBetweenTestRuns
        {
            get
            {

                var result = ConfigurationManager.AppSettings[settingKeyName].ToBool();
                return result;
            }
        }


        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="SqlCeResetedByCopyingDbFileDatabaseLifeCycleController" /> class.
        /// </summary>
        /// <param name="dbContextType">Type of the database context.</param>
        /// <param name="dbConnectionStringName">Name of the database connection string.</param>
        public SqlCeResetedByCopyingDbFileDatabaseLifeCycleController(Type dbContextType, string dbConnectionStringName = null)
        {

            Debug("Initializer");

            _dbContextType = dbContextType;

            if (dbConnectionStringName.IsNullOrEmpty())
            {
                dbConnectionStringName = "Tests_SQLServer_CE";
            }

            DbContextConnectionStringName = dbConnectionStringName;

        }


        #region IDatabaseTestLifeCycleController Members

        /// <summary>
        /// Creates a Test run specific
        /// subclass of
        /// <see cref="DbContext" />.
        /// <para>
        /// Member defined in <see cref="IDatabaseLifeCycleController" />
        /// </para>
        /// </summary>
        /// <returns></returns>
        public DbContext CreateDbContext()
        {
            DbContext result;
            try
            {
                Debug("CreateDbContext [1]");

                result =
                    Activator.CreateInstance(_dbContextType, new object[] {DbContextConnectionStringName}) as DbContext;
            }
            catch
            {
                Debug("CreateDbContext [2]");

                result =
                    Activator.CreateInstance(_dbContextType) as DbContext;
            }

            XAct.Library.Settings.Db.DefaultXActLibSchema = null;
            
            return result;
        }

        /// <summary>
        /// Initialize and Seed Database before running a suite/file of tests
        /// (decorated with <see cref="InitializeIoCContextAttribute" />)
        /// <para>
        /// </para>
        /// <para>
        /// Member defined in <see cref="IDatabaseLifeCycleController" />
        /// </para>
        /// </summary>
        public void InitializeDatabaseBeforeTestFixture()
        {
            Debug("InitializeDatabaseBeforeTestFixture");

            IUnityContainer unityContainer =
                XAct.DependencyResolver.DependencyInjectionContainer as IUnityContainer;
            
            unityContainer.RegisterInstance<IColumnTypeMappingService>(new CompactFrameworkColumnTypeMappingService());



            //The default is to not keep the same database
            //tweak to spead up the test runs - 
            //the file needs to be deleted from the bin 
            //everytime the db model or seeding is changed
            if (KeepSlqceDatabaseBetweenTestRuns)
            {
                if (File.Exists(GetTestRunDbBackupFileName()))
                {
                    Debug(
                        "InitializeDatabaseBeforeTestFixture:File Exists...so invoking InitializeDatabaseBeforeSingleTest");

                    //Copy Backup over current (that's all it does):
                    InitializeDatabaseBeforeSingleTest();

                    //This one, when Initialize is called, will drop, create, and seed:
                    //dbContext.SetDatabaseInitializer(typeof(DropCreateDatabaseIfModelChanges<>), _dbContextType, DbContextConnectionStringName);
                    //This one, when Initialize is called, will drop, create, but not seed:
                    Debug("InitializeDatabaseBeforeTestFixture:Creating DbContext...");

                        Debug("InitializeDatabaseBeforeTestFixture:Setting initializer");
                        SetInitializer(typeof(NullDatabaseInitializer<>), _dbContextType, new object[]{DbContextConnectionStringName});

                        using (DbContext dbContext = CreateDbContext())
                        {
                            //Not sure if this actually does anything if Null initializer used:
                        Debug("InitializeDatabaseBeforeTestFixture:Invoking Initialize");
                        dbContext.Database.Initialize(false);

                        Debug("InitializeDatabaseBeforeTestFixture:Dispose and get out...");
                        dbContext.Dispose();
                    }
                    return;
                }
            }

            //When:
            //* not saving Db between tests, 
            //* OR the file does not yet exist (ie, first run):
            //we dropdrop/create/and seed,
            //and save a copy:
            if (File.Exists(GetTestRunDbFileName()))
            {
                Debug("InitializeDatabaseBeforeTestFixture:Deleting file.");
                try
                {
                    File.Delete(GetTestRunDbFileName());
                }
                catch
                {
                    //File may be locked/accessed from another
                    using (DbContext dbContext = CreateDbContext())
                    {
                        dbContext.Database.Delete();
                    }
                }
            }

            Debug("InitializeDatabaseBeforeTestFixture:File should not exist....Exists?:" +
                  File.Exists(GetTestRunDbFileName()));

            Debug(
                "InitializeDatabaseBeforeTestFixture:Deleting Setting initializer to DropCreateDatabaseAlwaysBasedOnTheCurrentModelInitializer.");
            SetInitializer(
                typeof (DropCreateDatabaseAlwaysBasedOnTheCurrentModelInitializer<>), 
                _dbContextType, 
                new object[]{DbContextConnectionStringName});

            using (DbContext dbContext = CreateDbContext())
            {

                Debug("InitializeDatabaseBeforeTestFixture:File should not exist....Exists?:" +
                      File.Exists(GetTestRunDbFileName()));

                //Force initialization now, which should drop file if migrations are different:
                Debug("InitializeDatabaseBeforeTestFixture:Forcing Initializing ...");

                dbContext.Database.Initialize(true);

                Debug("InitializeDatabaseBeforeTestFixture:File should exist....Exists?:" +
                      File.Exists(GetTestRunDbFileName()));

                //Seed using Extension method to in turn invoke IDbContextSeedingService
                Debug("InitializeDatabaseBeforeTestFixture:Forcing Seeding ...");
                DbContextSeederContext.ClearCache(); 
                dbContext.AutoSeed<IDbContextSeeder>(true);

                Debug("InitializeDatabaseBeforeTestFixture:File should exist....Exists?:" +
                      File.Exists(GetTestRunDbFileName()));

                //create db backup so that it doesn't have to be initialized again
                Debug("InitializeDatabaseBeforeTestFixture:Save Backup for first time.");
                File.Copy(GetTestRunDbFileName(), GetTestRunDbBackupFileName(), true);
                dbContext.Dispose();

            }
        }

        /// <summary>
        /// Initialize and Seed Database before running an individual test
        /// (one decorated with <see cref="InitializeIoCContextAttribute" />)
        /// <para>
        /// Member defined in <see cref="IDatabaseLifeCycleController" />
        /// </para>
        /// </summary>
        public void InitializeDatabaseBeforeSingleTest()
        {
            //restore backup file
            string src = GetTestRunDbBackupFileName();
            src = Path.GetFullPath(src);
            
            string target = GetTestRunDbFileName();
            target = Path.GetFullPath(target);

            if (File.Exists(src))
            {
                Debug("InitializeDatabaseBeforeSingleTest:Copy backup over current target...");
                //Overwrite the existing database:
                File.Copy(src, target, true);
            }

            //Create a new DbContext, 
            //which invokes constructor, which invokes seeding:
            Debug("InitializeDatabaseBeforeSingleTest:Create context...");
            CreateDbContext();
            Debug("InitializeDatabaseBeforeSingleTest:Done.");
        }

        #endregion

        /// <summary>
        /// Gets the name of the test run database file.
        /// </summary>
        /// <returns></returns>
        private string GetTestRunDbFileName()
        {
            var connectionString = ConfigurationManager.ConnectionStrings[DbContextConnectionStringName];

            var fileName = connectionString.ConnectionString.Split('=')[1];

            return fileName;
        }

        /// <summary>
        /// Gets the name of the test run database backup file.
        /// </summary>
        /// <returns></returns>
        private string GetTestRunDbBackupFileName()
        {
            var fileName = string.Format("{0}.TestRunDbBackup", GetTestRunDbFileName());
            return fileName;
        }

        private static void Debug(string message)
        {
//#if DEBUG
//            System.IO.File.AppendAllText("C:\\tmp\\debug.txt",
//                "{0}: {1}.{2}{3}"
//                    .FormatStringInvariantCulture(
//                        DateTime.Now.ToString("hh:mm:ss.fff tt"),
//                        System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
//                        message,
//                        Environment.NewLine));
//#endif
        }


        void SetInitializer(Type databaseInitializerType, Type dbContextType,object[] args)
        {
            Type genericInitializerType = databaseInitializerType;
            Type genericInstanceType = genericInitializerType.MakeGenericType(dbContextType);

            object genericInstance =
                (args == null)
                    ? Activator.CreateInstance(genericInstanceType)
                    : Activator.CreateInstance(genericInstanceType, args);


            Type targetType = typeof(Database);
            var targetTypeMethodInfo =
                targetType.GetMethod("SetInitializer", BindingFlags.Public | BindingFlags.Static)
                          .MakeGenericMethod(dbContextType);
            targetTypeMethodInfo.Invoke(null, new[] { genericInstance });

        }

    }
}