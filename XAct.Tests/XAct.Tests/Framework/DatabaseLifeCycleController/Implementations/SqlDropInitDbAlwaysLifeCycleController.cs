﻿
using XAct.Data.EF.CodeFirst;

namespace XAct.Tests.Implementations
{
    using System;
    using System.Data.Entity;

    /// <summary>
    /// 
    /// </summary>
    public class SqlDropInitDbAlwaysLifeCycleController : IDatabaseLifeCycleController
    {
        private Type _dbContextType;
        private string DbContextConnectionStringName { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlDropInitDbAlwaysLifeCycleController"/> class.
        /// </summary>
        /// <param name="dbContextType">Type of the database <see cref="DbContext"/>.</param>
        /// <param name="dbConnectionStringName">Name of the database connection string (in the config file).</param>
        public SqlDropInitDbAlwaysLifeCycleController(Type dbContextType, string dbConnectionStringName=null)
        {

            dbContextType.ValidateIsNotDefault("dbContextType");

            _dbContextType = dbContextType;

            if (dbConnectionStringName.IsNullOrEmpty())
            {
                dbConnectionStringName = "Tests_SQLServer";
            }

            DbContextConnectionStringName = dbConnectionStringName;
        }

        
        #region IDatabaseTestLifeCycleController Members

        /// <summary>
        /// Creates a Test run specific
        /// subclass of
        /// <see cref="DbContext" />.
        /// <para>
        /// Member defined in <see cref="IDatabaseLifeCycleController" />
        /// </para>
        /// </summary>
        /// <returns></returns>
        public DbContext CreateDbContext()
        {
            try {
                return Activator.CreateInstance(_dbContextType, new object[] { DbContextConnectionStringName }) as DbContext;
            }
            catch
            {
                return
                    Activator.CreateInstance(_dbContextType) as DbContext;
            }
        }

        /// <summary>
        /// Initialize and Seed Database before running a
        /// suite/file of tests
        /// (decorated with <see cref="InitializeIoCContextAttribute" />)
        /// <para>
        /// Member defined in <see cref="IDatabaseLifeCycleController" />
        /// </para>
        /// </summary>
        public void InitializeDatabaseBeforeTestFixture()
        {
            InitializeAndSeedDatabase();
        }

        /// <summary>
        /// Initialize and Seed Database before running an individual test
        /// (one decorated with <see cref="InitializeIoCContextAttribute" />)
        /// <para>
        /// Member defined in <see cref="IDatabaseLifeCycleController" />
        /// </para>
        /// </summary>
        public void InitializeDatabaseBeforeSingleTest()
        {
            InitializeAndSeedDatabase();
        }

        #endregion


        private void InitializeAndSeedDatabase()
        {
            using (DbContext dbContext = CreateDbContext())
            {
                dbContext.SetDatabaseInitializer(
                    typeof(DropCreateDatabaseAlwaysBasedOnTheCurrentModelInitializer<>), 
                    _dbContextType, 
                    DbContextConnectionStringName);

                //Initialize the application (ie, it's Model). Notice the *force* flag is set:
                dbContext.Database.Initialize(true);

                //Use Extension method to in turn invoke IDbContextSeedingService
                //We need true in the case of Unit Testing because
                //dbContext in a unit test is in XAct.Tests -- not the app's directory...
                //Might have to revisit with a static flag, saying we are in unit test in XActLib,
                //to distinguish from unit test in app...
                dbContext.AutoSeed<IDbContextSeeder>(true,null);
            }
        }


//        private static void Debug(string message)
//        {
////#if DEBUG
////            System.IO.File.AppendAllText("C:\\tmp\\debug.txt",
////                "{0}: {1}.{2}{3}"
////                    .FormatStringInvariantCulture(
////                        DateTime.Now.ToString("hh:mm:ss.fff tt"),
////                        System.Reflection.MethodInfo.GetCurrentMethod().DeclaringType.Name,
////                        message,
////                        Environment.NewLine));
////#endif
//        }


    }
}