﻿
namespace XAct.Tests
{
    using System;
    using System.Configuration;
    using System.Data.Entity;
    using Microsoft.Practices.Unity;
    using XAct.Settings;
    using XAct.Tests.Implementations;


    /// <summary>
    /// A specialization of <see cref="IocContext"/>
    /// that adds Database lifecylce management.
    /// <para>
    /// Initialized by <see cref="InitializeDbIoCContextAttribute"/>
    /// before every <c>Test</c>.
    /// </para>
    /// </summary>
    public class DbIoCContext : IocContext
    {
        public string ConnectionStringName { get; set; }
        
        public static bool EnableCompactEditionSetting
        {
            get
            {
                const string settingKeyName = "EnableSQLCompactEdition";
                bool enableCompactEdition;
                bool.TryParse(ConfigurationManager.AppSettings[settingKeyName], out enableCompactEdition);
                return enableCompactEdition;
            }
        }


        
        public DbContext DbContext
        {
            get { return _dbContext; }
            private set { 
                _dbContext = value;
            }
        }

        public void SetDbContext(string connectionStringName, DbContext dbContext)
        {
            ConnectionStringName = connectionStringName;
            DbContext = dbContext;
        }

        private DbContext _dbContext;
        /// <summary>
        /// Initializes a new instance of the <see cref="DbIoCContext"/> class.
        /// </summary>
        public DbIoCContext()
        {

            //By the time we get here, 
            //base has already initialized settings:
            XAct.DependencyResolver.Current.GetInstance<IAppSettingsHostSettingsService>().Configuration.Initialize(); //to merge the nconfig early                       

        }


        public DbIoCContext Seed<TEntity>() where TEntity : class
        {
            var result = this.Seed(typeof (TEntity));
            return result;
        }

        public DbIoCContext Seed(Type entityType)
        {
            //TODO:IDbContextSeeder seeder = GetRegisteredSeeder(entityType);

            //TODO:Seeders.Seed(entityType, DbContext);

            //For Fluent:
            return this;
        }

        //TODO:
        //public void RegisterDbContext<TDbContext>(TDbContext dbContext)
        //    where TDbContext :DbContext 
        //{
        //    dbContext.Configuration.LazyLoadingEnabled = false;
        //    var unitOfWorkManagementService = XAct.DependencyResolver.Current.GetInstance<IUnitOfWorkService>();

        //    unitOfWorkManagementService.GetCurrent().DbContext = dbContext; //to make library dependencies on this service work
        //    DbContext = dbContext; //to be able to access the context from the test classes
        //}

        public override void RegisterAdditionalTypes(IUnityContainer ioc)
        {
            base.RegisterAdditionalTypes(ioc);

            //ioc.RegisterType<ITestUnitOfWorkService, TestUnitOfWorkManagementService>(new ContainerControlledLifetimeManager()); //we need this service in the container for the database to work
            //ioc.RegisterType<IUnitOfWorkService, TestUnitOfWorkManagementService>(new ContainerControlledLifetimeManager());
        }
    }
}