﻿namespace XAct.Tests
{
    using Microsoft.Practices.Unity;
    using Moq;
    using XAct.Services;
    using XAct.Services.IoC;

    using NUnit.Framework;
    using XAct;

    /// <summary>
    /// A singleton.
    /// <para>
    /// Invoked by <see cref="InitializeIoCContextAttribute"/>
    /// </para>
    /// </summary>
    public class IocContext : Singleton<IocContext>
    {

        /// <summary>
        /// Gets the Unity Container that was initialized.
        /// 
        /// </summary>
        /// <value>
        /// The io c.
        /// </value>
        public IUnityContainer IoC
        {
            get { return XAct.DependencyResolver.DependencyInjectionContainer as IUnityContainer; }
        }    


        public IocContext()
        {
            BootStrap();            
        }

        private IBindingDescriptorBase[] CachedBindingDescriptors { get; set; }

        /// <summary>
        /// Bootstrap the IoC.
        /// <para>
        /// Invoked by Constructor, this means that
        /// the IoC is guranteed to have been Bootstrapped
        /// before every test (via the <see cref="InitializeIoCContextAttribute"/>)
        /// </para>
        /// </summary>
        public void BootStrap()
        {
            UnityBootstrapper.Initialize(this.IoC);

            //NO LONGER NEDED AS FLAG
            //IS NOW WITHIN
            //MigrationsAppDbContext
            //RegisterAllServicesInIoCInitializationStep.Initialized = true; 
            
            //hack for the migrator that bootstraps if this is not set to false
            //we don't want to boostrap again as it overrides our custom types registered for testing

            var bindingDescriptors = XAct.DependencyResolver.BindingResults.ServiceBindingsRegistered;

            //Cache thee services found,
            //so we can reinitialize the IoC later without 
            //having to lose time rescanning the assemblies:
            var copy = new IBindingDescriptorBase[bindingDescriptors.Length];
            bindingDescriptors.CopyTo(copy, 0);
            this.CachedBindingDescriptors = copy;

            RegisterAdditionalTypes(IoC);
        }

        /// <summary>
        /// Resets the dependency injection container using pre-cached bindings (no dll scanning)
        /// <para>
        /// Invoked after every Unit Test  by the
        ///  <see cref="InitializeIoCContextAttribute"/> implementation of 
        /// the NUnit <see cref="ITestAction"/> contract.
        /// </para>
        /// </summary>
        public void ResetIoC()
        {
            UnityBootstrapper.Initialize(
                this.IoC, 
                false, 
                null, 
                null, 
                CachedBindingDescriptors);

            RegisterAdditionalTypes(IoC);
        }



        /// <summary>
        /// Method to register into the IoC 
        /// Mock implementations of 
        /// an service interface.
        /// </summary>
        /// <typeparam name="TInterfaceType">The type of the interface type.</typeparam>
        /// <returns></returns>
        public Mock<TInterfaceType> RegisterMock<TInterfaceType>() where TInterfaceType : class
        {
            var mock = new Mock<TInterfaceType>();
            //Register mock (what if a previous implementation is already implemented?)
            IoC.RegisterInstance(mock.Object);

            return mock;
        }



        public virtual void RegisterAdditionalTypes(IUnityContainer container)
        {
            //just for derived typess
        }


    }    
}