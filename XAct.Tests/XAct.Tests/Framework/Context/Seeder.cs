﻿//using System;
//using System.Linq;
//using App.Core.Back.Integration.Tests.Framework.Seeders.DefaultImplementations;
//using App.Core.Infrastructure.Data;
//using StructureMap;
//using XAct.Data.EF.CodeFirst;

//namespace App.Core.Back.IntegrationTests.Framework.Seeders
//{
//    public class SeederContainer
//    {
//        private IContainer _container;

//        public SeederContainer()
//        {
//            _container = Scan();
//        }

//        public IRecursiveSeeder<TEntity> GetRegisteredSeeder<TEntity>() where TEntity : class
//        {
//            var seeder = _container.GetInstance<IRecursiveSeeder<TEntity>>();
//            if (seeder == null) throw new Exception(string.Format("No seeders registered for {0} entity type", typeof(TEntity).Name));
//            return seeder;
//        }

//        public IDbContextSeeder GetRegisteredSeeder(Type entityType)
//        {
//            var seederGenericinterfaceType = typeof(IRecursiveSeeder<>).MakeGenericType(entityType);
//            var seeder = _container.GetInstance(seederGenericinterfaceType) as IDbContextSeeder;
//            if (seeder == null) throw new Exception(string.Format("No seeders registered for {0} entity type", entityType.Name));
//            return seeder;
//        }

//        public void Reset()
//        {
//            _container.Dispose();
//            _container = Scan();
//        }

//        public void Seed<TEntityType>(AppDbContext context) where TEntityType : class
//        {
//            var seeder = GetRegisteredSeeder<TEntityType>();
//            seeder.Seed(context);
//        }

//        public void Seed(Type entityType, AppDbContext context)
//        {
//            var seeder = GetRegisteredSeeder(entityType);
//            seeder.Seed(context);
//        }

//        public void RegisterSeeder<TSeederInterface, TSeederClass>() where TSeederClass : TSeederInterface
//        {
//            _container.Configure(configure => configure.ForSingletonOf<TSeederInterface>().Use<TSeederClass>()); //plug in a new one
//        }

//        public void RegisterSeeder(Type seederInterface, Type seederType)
//        {
//            _container.Configure(configure => configure.For(seederInterface).Singleton().Use(seederType));
//        }

//        public void RegisterSeeder(Type seederType)
//        {
//            var seederInterface = seederType.GetInterface(typeof(IRecursiveSeeder<>).Name);

//            if (seederInterface == null)
//                throw new Exception(string.Format("Invalid seeder type {0}. Seeder has to implement {1} interface.", seederType.Name, typeof(IRecursiveSeeder<>).Name));

//            var entityType = seederInterface.GenericTypeArguments.First();
//            var seederGenericinterfaceType = typeof(IRecursiveSeeder<>).MakeGenericType(entityType);
//            RegisterSeeder(seederGenericinterfaceType, seederType);
//        }

//        private IContainer Scan()
//        {
//            return new Container(
//                config => config.Scan
//                              (
//                                  scanner =>
//                                  {
//                                      scanner.Assembly(GetType().Assembly); //From this assembly
//                                      scanner.IncludeNamespaceContainingType<DefaultStudentSeeder>(); //register types from this namespace
//                                      scanner.ConnectImplementationsToTypesClosing(typeof(IRecursiveSeeder<>))
//                                          .OnAddedPluginTypes(expression => expression.Singleton()); //that implement this generic interface as singletons
//                                  }
//                              )
//                );
//        }
//    }
//}