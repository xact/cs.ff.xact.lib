namespace XAct.Commerce.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Commerce.Initialization.ModelPersistenceMaps;
    using XAct.Commerce.Products.Entities.Products;
    using XAct.Diagnostics;

    /// <summary>
    /// 
    /// </summary>
    public class ProductDbModelBuilder : IProductDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public ProductDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<Product>)XAct.DependencyResolver.Current.GetInstance<IProductModelPersistenceMap>());

        }
    }
}