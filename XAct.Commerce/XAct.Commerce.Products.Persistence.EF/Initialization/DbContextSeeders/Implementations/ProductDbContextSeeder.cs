namespace XAct.Commerce.Initialization.DbContextSeeders.Implementations
{
    using System.Data.Entity;
    using XAct.Commerce.Products.Entities.Products;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;

#pragma warning disable 1591
    public class ProductDbContextSeeder : XActLibDbContextSeederBase<Product>, IProductDbContextSeeder
#pragma warning restore 1591
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="environmentService">The environment service.</param>
        public ProductDbContextSeeder(ITracingService tracingService, IEnvironmentService environmentService)
            : base(tracingService)
        {
            _environmentService = environmentService;
        }

        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }



    }


}

