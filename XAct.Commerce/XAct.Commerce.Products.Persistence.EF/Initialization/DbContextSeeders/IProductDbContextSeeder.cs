namespace XAct.Commerce.Initialization.DbContextSeeders
{
    using XAct.Commerce.Products.Entities.Products;
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Product}"/>
    /// to seed the Tip tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface IProductDbContextSeeder : IHasXActLibDbContextSeeder<Product>
    {

    }

}

