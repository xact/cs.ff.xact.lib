namespace XAct.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Commerce.Products.Entities.Products;

    public class ProductModelPersistenceMap : EntityTypeConfiguration<Product>,
                                                        IProductModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public ProductModelPersistenceMap()
        {

            this.ToXActLibTable("Product");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);
        }
    }
}
