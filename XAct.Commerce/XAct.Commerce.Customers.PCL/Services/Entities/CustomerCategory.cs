namespace XAct.Users.Services
{
    using System.Runtime.Serialization;
    using XAct.Entities;
    using XAct.Messages;


    /// <summary>
    /// Category under which to persist <see cref="Customer"/>
    /// </summary>
    [DataContract]
    public class CustomerCategory : ReferenceDataGuidIdBase
    {
        
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerCategory"/> class.
        /// </summary>
        public CustomerCategory() : base()
        {
            //Will Generate Id.
        }
    }
}