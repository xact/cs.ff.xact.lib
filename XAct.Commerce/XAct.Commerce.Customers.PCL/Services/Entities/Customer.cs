namespace XAct.Users.Services
{
    using System;
    using System.Runtime.Serialization;
    using XAct.Commerce.Services.Entities;
    using XAct.Users.Services.Entities.Customers;


    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Customer :
        XAct.Identity.IdentityWithAddressesBase<Customer, CustomerAlias, CustomerClaim, CustomerAddress>,
        IHasXActLibEntity
    {

        /// <summary>
        /// Gets or sets a unique customer code.
        /// </summary>
        [DataMember]
        public virtual string Code { get; set; }


        /// <summary>
        /// Gets or sets the FK of the <see cref="CustomerCategory"/>.
        /// </summary>
        [DataMember]
        public virtual Guid CategoryFK { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="CustomerCategory"/>.
        /// </summary>
        [DataMember]
        public virtual CustomerCategory Category { get; set; }


    }
}
