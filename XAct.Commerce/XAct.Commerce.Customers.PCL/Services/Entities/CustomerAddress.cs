﻿namespace XAct.Users.Services.Entities.Customers
{
    using System.Runtime.Serialization;
    using XAct.Identity;

    /// <summary>
    /// The Customer's Address.
    /// </summary>
    [DataContract]
    public class CustomerAddress : IdentityAddressBase, IHasXActLibEntity
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerAddress"/> class.
        /// </summary>
        public CustomerAddress():base()
        {
            //Generates Id
        }


    }
}
