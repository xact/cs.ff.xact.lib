﻿namespace XAct.Commerce.Services.Entities
{
    using System.Runtime.Serialization;
    using XAct.Identity;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CustomerAlias : IdentityAliasBase ,IHasXActLibEntity
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerAlias"/> class.
        /// </summary>
        public CustomerAlias() : base()
        {
         //Generates Id   
        }
    }
}
