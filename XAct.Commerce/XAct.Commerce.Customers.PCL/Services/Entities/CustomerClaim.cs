﻿
namespace XAct.Commerce.Services.Entities
{
    using System.Runtime.Serialization;
    using XAct.Claims;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class CustomerClaim : PersistableClaimBase, IHasXActLibEntity
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerClaim"/> class.
        /// </summary>
        public CustomerClaim() : base()
        {
            //generates Id.
        }

    }
}
