﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XAct.Users.Services
{
    /// <summary>
    /// Contract to manage <see cref="Customer"/> entities.
    /// </summary>
    public interface ICustomerManagementService :IHasXActLibService
    {

        /// <summary>
        /// Get the <see cref="Customer"/> by it's <see cref="Customer.Code"/>.
        /// </summary>
        /// <param name="customerCode"></param>
        Customer GetByCode(string customerCode);
        

    }
}
