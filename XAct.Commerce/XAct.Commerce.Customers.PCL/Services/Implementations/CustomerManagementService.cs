﻿namespace XAct.Users.Services
{
    using System;
    using System.Linq.Expressions;
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Identity;
    using XAct.Services;
    using XAct.Users.Services.Entities.Customers;

    /// <summary>
    /// IMplementation of the <see cref="ICustomerManagementService"/> contract.
    /// </summary>
    public class CustomerManagementService : DistributedGuidIdRepositoryServiceBase<Customer>, ICustomerManagementService
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerManagementService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public CustomerManagementService(ITracingService tracingService, IRepositoryService repositoryService):base(tracingService,repositoryService)
        {
        }

        /// <summary>
        /// Identifiers the equality.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        protected override Expression<Func<Customer, bool>> IdEquality(Guid id)
        {
            Expression<Func<Customer, bool>> result = (x) => x.Id == id;
            return result;
        }


        /// <summary>
        /// Get the <see cref="Customer"/> by it's <see cref="Customer.Code"/>.
        /// </summary>
        /// <param name="customerCode"></param>
        public Customer GetByCode(string customerCode)
        {
            return base.Get(x => x.Code == customerCode);
        }

        /// <summary>
        /// Ensures that the given <see cref="CustomerAddress"/> 
        /// is referenced from <see cref="Customer"/> PreferredBillingAddressFK
        /// and PreferredShippingAddressFK
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="customerAddress"></param>
        public void SetPreferredAddress(Customer customer, CustomerAddress customerAddress)
        {
            if (customer.Addresses == null)
            {
                throw new ArgumentException("Address does not belong to Customer's Addresses.");
            }
            if (!customer.Addresses.Contains(customerAddress))
            {
                throw new ArgumentException("Address does not belong to Customer's Addresses.");
            }

            AddressType type = customerAddress.Type;

            if (type == AddressType.Billing)
            {
                customer.PreferredBillingAddressFK = customerAddress.Id;
            }
            else
            {
                customer.PreferredShippingAddressFK = customerAddress.Id;
                
            }
        }
    }
}