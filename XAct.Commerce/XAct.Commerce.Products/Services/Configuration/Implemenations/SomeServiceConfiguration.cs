namespace XAct.Commerce.Services
{
    using XAct;
    using XAct.Services;

    [DefaultBindingImplementation(typeof(ISomeServiceConfiguration), BindingLifetimeType.SingletonScope, Priority.Low)]
    public class SomeServiceConfiguration : ISomeServiceConfiguration
    {
    }
}