﻿namespace XAct.Commerce.Products
{
    using XAct.Commerce.Products.Entities.Products;

    /// <summary>
    /// Contract for a service to manage <see cref="Product"/>
    /// entities.
    /// </summary>
    public interface IProductManagementService : IHasXActLibService
    {
        /// <summary>
        /// Get the <see cref="Product"/> by it's <see cref="Product.Code"/>.
        /// </summary>
        /// <param name="productCode"></param>
        Product GetByCode(string productCode);

    }
}