﻿namespace XAct.Commerce.Products.Entities.Products
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A note that can be attached to a <see cref="Product"/>
    /// </summary>
    public class ProductNote : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasValue<string>, IHasProductFK
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }



        /// <summary>
        /// The FK of the <see cref="Product"/> to which this note refers to.
        /// </summary>
        [DataMember]
        public virtual Guid ProductFK { get; set; }



        /// <summary>
        /// Gets or sets the FK to the <see cref="ProductNoteType"/>.
        /// </summary>
        /// <value>
        /// The note type fk.
        /// </value>
        [DataMember]
        public virtual Guid NoteTypeFK { get; set; }


        /// <summary>
        /// Gets or sets the <see cref="ProductNoteType"/>
        /// (Url, Instructions, etc.)
        /// </summary>
        [DataMember]
        public virtual ProductNoteType NoteType { get; set; }


        /// <summary>
        /// The note's value (url, text, etc.)
        /// </summary>
        [DataMember]
        public virtual string Value { get; set; }

        /// <summary>
        /// An optional description of the value (might be useful if the value is an url)
        /// </summary>
        [DataMember]
        public virtual string Description { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductNote"/> class.
        /// </summary>
        public ProductNote()
        {
            this.GenerateDistributedId();
        }
    }
}