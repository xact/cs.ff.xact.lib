﻿namespace XAct.Commerce.Products.Entities.Products
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A promotion available to a product.
    /// </summary>
    [DataContract]
    public class ProductPromotion : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasEnabled, IHasCode, IHasNullableStartDateTimeEndDateTimeUtc, IHasProduct
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        [DataMember]
        public virtual Guid Id { get; set; }


        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// Gets or sets a value indicating whether this object is enabled.
        /// <para>Member defined in<see cref="IHasEnabled" /></para>
        /// </summary>
        [DataMember]
        public virtual bool Enabled { get; set; }

        /// <summary>
        /// Gets or sets the foreign key to the Product
        /// that this promotion pertains to.
        /// </summary>
        [DataMember]
        public virtual Guid ProductFK { get; set; }


        /// <summary>
        /// Gets or sets the Product 
        /// this promotion pertains to.
        /// </summary>
        [DataMember]
        public virtual Product Product { get; set; }


        /// <summary>
        /// The promotion's code.
        /// </summary>
        [DataMember]
        public virtual string Code { get; set; }


        /// <summary>
        /// Gets or sets the optional start date (UTC).
        /// </summary>
        /// <value>
        /// The start date UTC.
        /// </value>
        public virtual DateTime? StartDateTimeUtc { get; set; }


        /// <summary>
        /// Gets or sets the optional end date (UTC).
        /// </summary>
        public virtual DateTime? EndDateTimeUtc { get; set; }
    }
}