﻿namespace XAct.Commerce.Products.Entities.Products
{
    using System;
    using System.Runtime.Serialization;


    /// <summary>
    /// Contract for an entity that has an FK back to a <see cref="Product"/>
    /// </summary>
    public interface IHasProduct : IHasProductFK
    {


        /// <summary>
        /// Gets or sets the Product 
        /// this entity pertains to.
        /// </summary>
        [DataMember]
        Product Product { get; set; }

    }
}