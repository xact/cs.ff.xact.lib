﻿namespace XAct.Commerce.Products.Entities.Products
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Contract for an entity that has an FK back to a <see cref="Product"/>
    /// </summary>
    public interface IHasProductFK
    {
        /// <summary>
        /// Gets or sets the foreign key to the Product
        /// that this entity pertains to.
        /// </summary>
        [DataMember]
        Guid ProductFK { get; set; }

    }
}