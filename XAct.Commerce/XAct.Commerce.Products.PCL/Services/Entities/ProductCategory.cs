﻿namespace XAct.Commerce.Products.Entities.Products
{
    using System.Runtime.Serialization;
    using XAct.Entities;
    using XAct.Messages;

    /// <summary>
    /// The Category under which a <see cref="Product"/> can be classified.
    /// </summary>
    [DataContract]
    public class ProductCategory : ReferenceDataGuidIdBase 
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="ProductCategory"/> class.
        /// </summary>
        public ProductCategory()
        {
            this.GenerateDistributedId();
        }


    }
}