﻿using System;
using System.Collections.Generic;

namespace XAct.Commerce.Products.Entities.Products
{
    using System.Collections.ObjectModel;
    using System.Runtime.Serialization;


    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class Product : IHasXActLibEntity, IHasDistributedGuidIdAndTimestamp, IHasCode, IHasPrice 
    {

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        [DataMember]
        public virtual Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the datastore concurrency check timestamp.
        /// <para>
        /// Note that this is filled in when persisted in the db --
        /// so it's usable to determine whether to generate the
        /// Guid <c>Id</c>.
        /// </para>
        /// </summary>
        [DataMember]
        public virtual byte[] Timestamp { get; set; }


        /// <summary>
        /// The FK of the <see cref="Category"/> to which this product belongs.
        /// </summary>
        [DataMember]
        public virtual Guid CategoryFK { get; set; }


        /// <summary>
        /// The category to which this product belongs
        /// </summary>
        [DataMember]
        public virtual ProductCategory Category { get; set; }


        /// <summary>
        /// The vendors unique code for this product.
        /// </summary>
        [DataMember]
        public virtual string Code { get; set; }


        /// <summary>
        /// The list price of this product.
        /// </summary>
        [DataMember]
        public virtual decimal Price { get; set; }


        /// <summary>
        /// Gets a collection of any optional notes attached to this <see cref="Product"/>.
        /// </summary>
        public virtual ICollection<ProductNote> Notes { get { return _notes ?? (_notes = new Collection<ProductNote>()); } }
        [DataMember]
        private ICollection<ProductNote> _notes;



        /// <summary>
        /// Gets a collection of any promotions pertinent to this <see cref="Product"/>.
        /// </summary>
        public virtual ICollection<ProductPromotion> Promotions { get { return _promotions ?? (_promotions = new Collection<ProductPromotion>()); } }
        [DataMember]
        private ICollection<ProductPromotion> _promotions;

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        public Product()
        {
            this.GenerateDistributedId();
        }
    }
}
