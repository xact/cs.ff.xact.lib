﻿namespace XAct.Commerce.Products.Entities.Products
{
    using System.Runtime.Serialization;
    using XAct.Entities;
    using XAct.Messages;

    /// <summary>
    /// The Type of the Note (Url, Warning, Construction note, etc.)
    /// </summary>
    [DataContract]
    public class ProductNoteType : ReferenceDataGuidIdBase
    {


        /// <summary>
        /// Initializes a new instance of the <see cref="ProductNoteType"/> class.
        /// </summary>
        public ProductNoteType()
        {
            this.GenerateDistributedId();
        }
        
    }
}