﻿namespace XAct.Commerce.Products
{
    using XAct.Data.Repositories.Implementations;
    using XAct.Diagnostics;
    using XAct.Domain.Repositories;
    using XAct.Services;
    using XAct.Commerce.Products.Entities.Products;

    /// <summary>
    /// Implementation of the <see cref="IProductManagementService"/>
    /// contract for a service to manage <see cref="Product"/>
    /// entities.
    /// </summary>
    public class ProductManagementService : DistributedGuidIdRepositoryServiceBase<Product>, IProductManagementService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProductManagementService"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="repositoryService">The repository service.</param>
        public ProductManagementService(ITracingService tracingService, IRepositoryService repositoryService) : base(tracingService, repositoryService)
        {
        }


        /// <summary>
        /// Get the <see cref="Product"/> by it's <see cref="Product.Code"/>.
        /// </summary>
        /// <param name="productCode"></param>
        public Product GetByCode(string productCode)
        {
            return base.Get(x => x.Code == productCode);
        }

    }
}