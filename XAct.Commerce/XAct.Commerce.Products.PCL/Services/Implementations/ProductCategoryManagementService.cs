﻿namespace XAct.Commerce.Products
{
    using XAct.Services;
    using XAct.Commerce.Products.Entities.Products;

    /// <summary>
    /// An implementation of the <see cref="IProductCategoryManagementService"/>
    /// in order to manage the set of <see cref="ProductCategory"/> used to 
    /// categorize <see cref="Product"/> entities.
    /// </summary>
    public class ProductCategoryManagementService : IProductCategoryManagementService
    {

    }
}