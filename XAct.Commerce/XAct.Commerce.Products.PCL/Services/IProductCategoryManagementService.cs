﻿namespace XAct.Commerce.Products
{
    using XAct.Commerce.Products.Entities.Products;

    /// <summary>
    /// A contract for a server to
    /// manage the set of <see cref="ProductCategory"/> used to 
    /// categorize <see cref="Product"/> entities.
    /// </summary>
    public interface IProductCategoryManagementService : IHasXActLibService
    {
        
    }
}