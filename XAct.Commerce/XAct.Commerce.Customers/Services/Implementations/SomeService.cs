namespace XAct.Commerce.Services
{
    using XAct;
    using XAct.Services;

    [DefaultBindingImplementation(typeof(ISomeService),BindingLifetimeType.Undefined,Priority.Low)]
    public class SomeService : ISomeService
    {
    }
}