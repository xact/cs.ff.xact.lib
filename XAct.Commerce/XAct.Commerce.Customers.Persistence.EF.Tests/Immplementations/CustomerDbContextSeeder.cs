﻿namespace XAct.Tests
{
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using XAct.Commerce.Initialization.DbContextSeeders;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.Services;
    using XAct.Users.Services;

    public class CustomerDbContextSeeder : XActLibDbContextSeederBase<Customer>, ICustomerDbContextSeeder, IHasMediumBindingPriority
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerAddressDbContextSeeder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="customerAliasDbContextSeeder">The customer alias database context seeder.</param>
        /// <param name="customerClaimDbContextSeeder">The customer claim database context seeder.</param>
        /// <param name="customerAddressDbContextSeeder">The customer address database context seeder.</param>
        public CustomerDbContextSeeder(ITracingService tracingService, 
            ICustomerAliasDbContextSeeder customerAliasDbContextSeeder,
                ICustomerClaimDbContextSeeder customerClaimDbContextSeeder,
                ICustomerAddressDbContextSeeder customerAddressDbContextSeeder
                )
            : base(
                tracingService,
            customerAliasDbContextSeeder,
            customerClaimDbContextSeeder,
            customerAddressDbContextSeeder
                )
        {
        }

        public override void CreateEntities() 
        {
        }


    }
}