﻿namespace XAct.Tests
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using XAct;
    using XAct.Commerce.Initialization.DbContextSeeders;
    using XAct.Commerce.Services.Entities;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    
    public class CustomerClaimDbContextSeeder : XActLibDbContextSeederBase<CustomerClaim>, ICustomerClaimDbContextSeeder, IHasMediumBindingPriority
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerClaimDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public CustomerClaimDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        /// <param name="dbContext">The context.</param>
        public override void SeedInternal(DbContext dbContext) 
        {
            SeedInternalHelper(dbContext,true,x=>x.Id);
        }


        public override void CreateEntities()
        {
            this.InternalEntities = new List<CustomerClaim>();

            this.InternalEntities.Add(new CustomerClaim { Id = 1.ToGuid(), Type = "/", Value = "Default", Authority = "/auth:none" });

        }
    }
}
