﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using XAct;
    using XAct.Commerce.Initialization.DbContextSeeders;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.Services;
    using XAct.Users.Services;

    public class CustomerCategoryDbContextSeeder : XActLibDbContextSeederBase<CustomerCategory>,
                                                   ICustomerCategoryDbContextSeeder, IHasMediumBindingPriority
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerCategoryDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public CustomerCategoryDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        /// <param name="dbContext">The context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            this.SeedInternalHelper(dbContext, true, x => x.Id);
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        public override void CreateEntities()
        {
            this.InternalEntities = new List<CustomerCategory>();

            this.InternalEntities.Add(new CustomerCategory
                {
                    Id = 1.ToGuid(),
                    Enabled = true,
                    Text = "Default",
                    Description = "Default category"
                });

        }


    }
}
