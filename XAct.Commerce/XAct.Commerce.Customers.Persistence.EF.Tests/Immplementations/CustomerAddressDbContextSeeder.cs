﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using XAct;
    using XAct.Commerce.Initialization.DbContextSeeders;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Identity;
    using XAct.Users.Services.Entities.Customers;

    public class CustomerAddressDbContextSeeder : XActLibDbContextSeederBase<CustomerAddress>, ICustomerAddressDbContextSeeder, IHasMediumBindingPriority
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerAddressDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public CustomerAddressDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        /// <param name="dbContext">The context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            this.SeedInternalHelper(dbContext,true,x=>x.Id);
        }

        public override void CreateEntities()
        {
            this.InternalEntities = new List<CustomerAddress>();

            this.InternalEntities.Add(new CustomerAddress
                {
                    Id = 1.ToGuid(),
                    Type = AddressType.Billing,
                    Name = "Whatever",
                    Email = "em@com",
                    Phone = "1234553",
                    PhoneExt = "x123",
                    StreetLine1 = "Street1...",
                    StreetLine2 = "Street2...",
                    Neighbourhood = "NB...",
                    City = "City...",
                    PostalCode = "XY123",
                    Country = "Country...",
                });

            this.InternalEntities.Add(new CustomerAddress
                {
                    Id = 2.ToGuid(),
                    Type = AddressType.Billing,
                    Name = "Whatever",
                    Email = "em@com",
                    Phone = "1234553",
                    PhoneExt = "x123",
                    Instructions = "Leave it on the front doorstep",
                    StreetLine1 = "Street1...",
                    StreetLine2 = "Street2...",
                    Neighbourhood = "NB...",
                    City = "City...",
                    PostalCode = "XY123",
                    Country = "Country...",
                });

        }
    }
}
