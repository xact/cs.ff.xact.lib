﻿namespace XAct.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using XAct;
    using XAct.Commerce.Initialization.DbContextSeeders;
    using XAct.Commerce.Services.Entities;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Library.Settings;
    using XAct.Services;
    using XAct.Users.Services;

    public class CustomerAliasDbContextSeeder : XActLibDbContextSeederBase<CustomerAlias>, ICustomerAliasDbContextSeeder, IHasMediumBindingPriority
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerClaimDbContextSeeder"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public CustomerAliasDbContextSeeder(ITracingService tracingService)
            : base(tracingService)
        {
        }

        /// <summary>s
        /// Seeds the specified context.
        /// </summary>
        /// <param name="dbContext">The context.</param>
        public override void SeedInternal(DbContext dbContext)
        {
            SeedInternalHelper(dbContext, true, x => x.Id);
        }


        public override void CreateEntities()
        {
            this.InternalEntities = new List<CustomerAlias>();

            // ReSharper restore RedundantCatchClause
            this.InternalEntities.Add(new CustomerAlias { Id = 1.ToGuid(), Title = "Mr.", FirstName = "John", MoreNames = "P.", SurName = "Smith" });

        }
    }
}
