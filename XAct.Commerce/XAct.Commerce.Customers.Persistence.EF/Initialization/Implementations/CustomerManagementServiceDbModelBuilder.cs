namespace XAct.Commerce.Initialization.Implementations
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Commerce.Initialization.ModelPersistenceMaps;
    using XAct.Commerce.Services.Entities;
    using XAct.Diagnostics;
    using XAct.Users.Services;
    using XAct.Users.Services.Entities.Customers;

    /// <summary>
    /// 
    /// </summary>
    public class CustomerManagementServiceDbModelBuilder : ICustomerManagementServiceDbModelBuilder
    {
        private readonly string _typeName;
        private readonly ITracingService _tracingService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerManagementServiceDbModelBuilder" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        public CustomerManagementServiceDbModelBuilder(ITracingService tracingService)
        {
            _typeName = this.GetType().Name;

            _tracingService = tracingService;
        }

        /// <summary>
        /// Called when the schema of the DbContext needs to be built up.
        /// </summary>
        /// <param name="modelBuilder">The model builder.</param>
        public void OnModelCreating(DbModelBuilder modelBuilder)
        {

            _tracingService.Trace(TraceLevel.Verbose, "{0}.{1}:: Defining Db Model.", _typeName, "OnModelCreateing");

            modelBuilder.Configurations.Add((EntityTypeConfiguration<Customer>)XAct.DependencyResolver.Current.GetInstance<ICustomerModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<CustomerAddress>)XAct.DependencyResolver.Current.GetInstance<ICustomerAddressModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<CustomerClaim>)XAct.DependencyResolver.Current.GetInstance<ICustomerClaimModelPersistenceMap>());
            modelBuilder.Configurations.Add((EntityTypeConfiguration<CustomerAlias>)XAct.DependencyResolver.Current.GetInstance<ICustomerAliasModelPersistenceMap>());

        }
    }
}