namespace XAct.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using XAct.Data;
    using XAct.Users.Services;

    /// <summary>
    /// Defines a EF Map on how to persist a <see cref="CustomerCategory" /> into the database.
    /// </summary>
    public class CustomerCategoryModelPersistenceMap : ReferenceDataPersistenceMapBase<CustomerCategory, Guid>, ICustomerCategoryModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public CustomerCategoryModelPersistenceMap()
            : base("CustomerCategory", DatabaseGeneratedOption.None)
        {
        }
    }
}
