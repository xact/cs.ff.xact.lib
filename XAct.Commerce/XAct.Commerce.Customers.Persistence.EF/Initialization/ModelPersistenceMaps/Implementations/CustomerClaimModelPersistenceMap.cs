namespace XAct.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Commerce.Services.Entities;

    public class CustomerClaimModelPersistenceMap : EntityTypeConfiguration<CustomerClaim>,
                                                        ICustomerClaimModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public CustomerClaimModelPersistenceMap()
        {

            this.ToXActLibTable("CustomerClaim");


            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.OwnerFK)
                .IsRequired()
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Type)
                .IsRequired()
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Value)
                .IsRequired()
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Authority)
                .IsRequired()
                .HasColumnOrder(colOrder++);

        }
    }
}
