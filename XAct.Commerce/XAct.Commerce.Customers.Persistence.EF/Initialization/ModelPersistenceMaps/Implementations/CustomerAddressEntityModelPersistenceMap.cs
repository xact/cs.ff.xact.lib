namespace XAct.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Users.Services.Entities.Customers;

    public class CustomerAddressModelPersistenceMap : EntityTypeConfiguration<CustomerAddress>,
                                                        ICustomerAddressModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public CustomerAddressModelPersistenceMap()
        {

            this.ToXActLibTable("CustomerAddress");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.Email)
                            .HasMaxLength(128)
                            .HasColumnOrder(colOrder++);

            this.Property(x => x.Phone)
                .HasMaxLength(32) //+123 (123) 1234 123 123...
                .HasColumnOrder(colOrder++);

            this.Property(x => x.PhoneExt)
                .HasMaxLength(10)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Instructions)
                .HasMaxLength(10)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.StreetLine1)
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.StreetLine2)
                .HasMaxLength(128)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Neighbourhood)
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.City)
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Region)
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.PostalCode)
                .HasMaxLength(32)
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Country)
                .HasMaxLength(64)
                .HasColumnOrder(colOrder++);

        }
    }
}
