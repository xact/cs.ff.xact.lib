namespace XAct.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Commerce.Services.Entities;

    public class CustomerAliasModelPersistenceMap : EntityTypeConfiguration<CustomerAlias>,
                                                        ICustomerAliasModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public CustomerAliasModelPersistenceMap()
        {

            this.ToXActLibTable("CustomerAlias");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);

            this.Property(x => x.Preferred)
                .IsRequired()
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Title)
                .IsOptional()
                .HasColumnOrder(colOrder++);


            this.Property(x => x.FirstName)
                .IsOptional()
                .HasColumnOrder(colOrder++);

            this.Property(x => x.MoreNames)
                .IsOptional()
                .HasColumnOrder(colOrder++);

            this.Property(x => x.SurName)
                .IsOptional()
                .HasColumnOrder(colOrder++);

        
        
        }
    }
}
