namespace XAct.Commerce.Initialization.ModelPersistenceMaps.Implementations
{
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.ModelConfiguration;
    using XAct.Users.Services;

    public class CustomerModelPersistenceMap : EntityTypeConfiguration<Customer>,
                                               ICustomerModelPersistenceMap
    {
        /// <summary>
        /// Tips the definition map.
        /// </summary>
        public CustomerModelPersistenceMap()
        {
            this.ToXActLibTable("Customer");

            this
                .HasKey(m => new {m.Id});


            int colOrder = 0;

            //No need for a tennant identifier.

            this.Property(x => x.Id)
                .DefineRequiredGuidId(colOrder++);


            this.Property(x => x.Timestamp)
                .DefineRequiredTimestamp(colOrder++);


            this.Ignore(x => x.Parent);

            this.Property(x => x.ParentFK)
                .IsRequired()
                .HasColumnOrder(colOrder++);

            this.Property(x => x.Code)
                .DefineRequired64CharCode(colOrder++);

            this.Property(x => x.CategoryFK)
                .IsOptional()
                .HasColumnOrder(colOrder++);



            this.Ignore(x => x.PreferredAliasFK);
            this.Ignore(x => x.PreferredAlias);
            this.Ignore(x => x.PreferredBillingAddressFK);
            this.Ignore(x => x.PreferredBillingAddress);
            this.Ignore(x => x.PreferredBillingAddressFK);
            this.Ignore(x => x.PreferredBillingAddress);




            //Category
            //Aliases
            //Claims
            //Addresses

            this.Property(x => x.CreatedBy)
                .DefineRequired64CharCreatedBy(colOrder++);

            this.Property(x => x.CreatedOnUtc)
                .DefineRequiredCreatedOnUtc(colOrder++);

            this.Property(x => x.LastModifiedBy)
                .DefineRequired64CharLastModifiedBy(colOrder++);
            this.Property(x => x.LastModifiedOnUtc)
                .DefineRequiredLastModifiedOnUtc(colOrder++);

        }
    }
}
