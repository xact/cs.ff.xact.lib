namespace XAct.Commerce.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Initialization;
    using XAct.Users.Services;

    /// <summary>
    /// Contract for a specialization of <see cref="IHasXActLibDbContextSeeder{Customer}"/>
    /// to seed the Tip tables with default data.
    /// </summary>
    //[Initializer("XActLib", "", InitializationStage.S03_Integration)]
    public interface ICustomerDbContextSeeder : IHasXActLibDbContextSeeder<Customer>
    {

    }
}

