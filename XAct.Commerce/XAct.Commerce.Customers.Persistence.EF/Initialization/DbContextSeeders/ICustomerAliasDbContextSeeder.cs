namespace XAct.Commerce.Initialization.DbContextSeeders
{
    using XAct.Commerce.Services.Entities;
    using XAct.Data.EF.CodeFirst;

    public interface ICustomerAliasDbContextSeeder : IHasXActLibDbContextSeeder<CustomerAlias>
    {
        
    }
}