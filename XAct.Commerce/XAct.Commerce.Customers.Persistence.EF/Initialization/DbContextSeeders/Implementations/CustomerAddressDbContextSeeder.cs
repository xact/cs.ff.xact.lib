namespace XAct.Commerce.Initialization.DbContextSeeders.Implementations
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Users.Services.Entities.Customers;

    public class CustomerAddressDbContextSeeder : XActLibDbContextSeederBase<CustomerAddress>, ICustomerAddressDbContextSeeder
    {

        public CustomerAddressDbContextSeeder(
            ITracingService tracingService
            )
            : base(
                tracingService)
        {
        }
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }
    }
}