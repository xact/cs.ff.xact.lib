namespace XAct.Commerce.Initialization.DbContextSeeders.Implementations
{
    using System.Data.Entity;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;
    using XAct.Environment;
    using XAct.Users.Services;

    public class CustomerDbContextSeeder : XActLibDbContextSeederBase<Customer>, ICustomerDbContextSeeder
    {
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomerDbContextSeeder" /> class.
        /// </summary>
        /// <param name="environmentService">The environment service.</param>
        /// <param name="customerCategoryDbContextSeeder">The customer category database context seeder.</param>
        /// <param name="customerAliasDbContextSeeder">The customer alias context seeder.</param>
        /// <param name="customerClaimDbContextSeeder">The customer claim context seeder.</param>
        /// <param name="customerAddressDbContextSeeder">The customer address context seeder.</param>
        public CustomerDbContextSeeder(
            IEnvironmentService environmentService,
            ICustomerCategoryDbContextSeeder customerCategoryDbContextSeeder,
            ICustomerAliasDbContextSeeder customerAliasDbContextSeeder,
            ICustomerClaimDbContextSeeder customerClaimDbContextSeeder,
            ICustomerAddressDbContextSeeder customerAddressDbContextSeeder
            )
            : base(
             customerCategoryDbContextSeeder,
                customerAliasDbContextSeeder, customerClaimDbContextSeeder, customerAddressDbContextSeeder)
        {
            _environmentService = environmentService;
        }

        
        /// <summary>
        /// Invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// in order to create the -- unpersisted -- <see cref="IDbContextSeeder{TEntity}.Entities"/>
        /// (in some scenarios, as per the instructions of <see cref="IDbContextSeeder{TEntity}.EntityCount"/>).
        /// <para>
        /// Important: only invoked by <see cref="IDbContextSeeder.SeedInternal"/>
        /// if <see cref="IDbContextSeeder{TEntity}.Entities"/> is still empty.
        /// </para>
        /// </summary>
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }


    }
}

