namespace XAct.Commerce.Initialization.DbContextSeeders.Implementations
{
    using XAct.Commerce.Services.Entities;
    using XAct.Data.EF.CodeFirst;
    using XAct.Diagnostics;

    public class CustomerAliasDbContextSeeder : XActLibDbContextSeederBase<CustomerAlias>, ICustomerAliasDbContextSeeder
    {

        public CustomerAliasDbContextSeeder(
            ITracingService tracingService
            )
            : base(
                tracingService)
        {
        }
        public override void CreateEntities()
        {
            // Default library implementation is to not create anything,
            // and let applications (and unit tests) provide a seeder 
            // that has a higher binding priority
        }
    }
}