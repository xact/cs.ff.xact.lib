namespace XAct.Commerce.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Users.Services;

    public interface ICustomerCategoryDbContextSeeder : IHasXActLibDbContextSeeder<CustomerCategory>
    {

    }
}