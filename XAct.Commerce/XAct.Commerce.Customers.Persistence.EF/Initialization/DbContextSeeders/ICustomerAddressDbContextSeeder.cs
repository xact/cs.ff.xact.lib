namespace XAct.Commerce.Initialization.DbContextSeeders
{
    using XAct.Data.EF.CodeFirst;
    using XAct.Users.Services.Entities.Customers;

    public interface ICustomerAddressDbContextSeeder : IHasXActLibDbContextSeeder<CustomerAddress>
    {
        
    }
}