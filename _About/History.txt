NuGet Cyclic References:

* XAct.Data.Db
* XAct.Data.EF
* XAct.IO.TemplateEngines.StringTemplate
* XAct.IO.Compression.ZipPackage
* XAct.IO.Compression.DotNetZip
* XAct.IO.FileSystem.Watcher
* XAct.IO.FS
* XACt.IO.TemplateEngines.NVelocity
* XACt.IO.TemplateEngines.StringTemplate
* XAct.IO.Resources.Db
* XAct.IO.Web.Mvc
* XAct.IO.Windows
'NVelocity 1.0.3 => XAct.Collections 0.0.220 => XAct.Core 0.0.220 => XAct.IO 0.0.220 => XAct.Core 0.0.220'.
* XAct.UI.Web
