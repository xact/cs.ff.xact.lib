﻿namespace XAct.Web.Environment.Services.Implementations
{
    using XAct.Environment.Services.Implementations;
    using XAct.Services;

    /// <summary>
    /// Implementation of 
    /// <see cref="IWebClientEnvironmentService"/>
    /// to return information as to the remote web
    /// UserAgent.
    /// </summary>
    [DefaultBindingImplementation(typeof(IWebClientEnvironmentService),BindingLifetimeType.Undefined,Priority.High)]
    public class WebClientEnvironmentService: ClientEnvironmentService, IWebClientEnvironmentService
    {
        /// <summary>
        /// Gets or sets the IP of the remote User Agent/Browser
        /// <para>
        /// returns null when the HttpContext.Current is not available.
        /// </para>.
        /// </summary>
        /// <value>
        /// The client ip.
        /// </value>
        public override string ClientIP
        {
            get
            {



                return System.Web.HttpContext.Current.Request.UserHostAddress;


                //Richer? :

                //return (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ??
                //  System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();
            }
        }
    }
}