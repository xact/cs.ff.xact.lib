﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Web.Environment
{
    using XAct.Environment;

    /// <summary>
    /// 
    /// </summary>
    public interface IWebClientEnvironmentService :IClientEnvironmentService, IHasXActLibService 
    {
    }
}
