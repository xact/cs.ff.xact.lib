﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;
using XAct.Diagnostics;
using XAct.Environment;

namespace XAct.Caching.Implementations
{
    /// <summary>
    /// Implementation of <see cref="IHttpContextCachingService"/>
    /// </summary>
    public class HttpContextCachingService : IHttpContextCachingService , IHasMediumBindingPriority
    {
        private readonly ITracingService _tracingService;
        private readonly IDateTimeService _dateTimeService;
        private readonly IEnvironmentService _environmentService;

        /// <summary>
        /// Initializes a new instance of the <see cref="HttpContextCachingService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="environmentService">The environment service.</param>
        public HttpContextCachingService(ITracingService tracingService,
            IDateTimeService dateTimeService,
            IEnvironmentService environmentService)
        {
            _tracingService = tracingService;
            _dateTimeService = dateTimeService;
            _environmentService = environmentService;
        }



        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="isValid">The is valid.</param>
        public void Set<TData>(string key, TData objectToCache, Func<bool> isValid)
        {
            key.ValidateIsNotDefault("key");
            isValid.ValidateIsNotDefault("isValid");

            _tracingService.Trace(TraceLevel.Verbose, "TracinService.Add({0},{1})".FormatStringCurrentCulture(key, objectToCache));

            //The default behaviour is to save just the value.
            //We do it differently: we save a whole object, that
            //has a delegate backing an IsValid property:
            FuncCacheItem<TData> funcCacheItem =
                new FuncCacheItem<TData>(() => (objectToCache), isValid);


            Insert(key, funcCacheItem);
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="objectToCache">The object to cache.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        public void Set<TData>(string key, TData objectToCache, TimeSpan expiryTimespan)
        {
            key.ValidateIsNotDefault("key");

            //Do not raise exception if expiryTimespan = emtpy.
            _tracingService.Trace(TraceLevel.Verbose, "TracinService.Add({0},{1},{2})".FormatStringCurrentCulture(key, objectToCache, expiryTimespan));

            //Calc expiry date:
            DateTime futureDateTimeUtc = _dateTimeService.NowUTC.Add(expiryTimespan);

            //Build a FuncCacheItem, 
            //that has a delegate for when checked to see if it is still valid.
            //Note that I had to use a ServiceLocator, rather than reference to a current
            //instance, as we are crossing threads, and the original 'this.EnvironmentService' might
            //be no longer available at that time.
            FuncCacheItem<TData> funcCacheItem =
                new FuncCacheItem<TData>(
                    () => (objectToCache),
                    () => (XAct.DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC < futureDateTimeUtc)
                    );

            //Call private:
            Insert(key, funcCacheItem);
        }

        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="dataFactory">The data factory</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        public void Set<TData>(string key, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true)
        {
            key.ValidateIsNotDefault("key");
            dataFactory.ValidateIsNotDefault("dataFactory");


            //Do not raise exception if expiryTimespan = emtpy.
            _tracingService.Trace(TraceLevel.Verbose, "HttpContextService.Add({0})".FormatStringCurrentCulture(key));

            DateTime futureUtcDateTime = _dateTimeService.NowUTC.Add(expiryTimespan);

            //Instead of using a FuncCacheItem which will only encapsulate a bool,
            //use a SelfUpdatingCacheItem which also encapsulates TimeSpan, 
            //so that when expired, can relaunch itself if need be:
            SelfUpdatingCacheItem<TData> funcCacheItem =
                new SelfUpdatingCacheItem<TData>(
                    dataFactory,
                    expiryTimespan,
                    futureUtcDateTime,
                    selfUpdating);

            Insert(key, funcCacheItem);
        }

        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// </summary>
        /// <value></value>
        /// <typeparam name="TData">The type of the data.</typeparam>
        public bool TryGet<TData>(string key, out TData result)
        {
            key.ValidateIsNotDefault("key");

            _tracingService.Trace(TraceLevel.Verbose, "HttpContextService.TryGet({0})".FormatStringCurrentCulture(key));

            return InternalTryGet(key, out result,
                //passing null:
                           null, TimeSpan.Zero, false);
        }



        /// <summary>
        /// Gets the <see cref="System.Object"/> with the specified key.
        /// If not found, registers the datafactory, and returns its results.
        /// <para>
        /// Note: basically combines TryGet and Add and into one operation.
        /// </para>
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="result">The result.</param>
        /// <param name="dataFactory">The data factory.</param>
        /// <param name="expiryTimespan">The expiry timespan.</param>
        /// <param name="selfUpdating">if set to <c>true</c> [self updating].</param>
        /// <returns></returns>
        public bool TryGet<TData>(string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating = true)
        {
            key.ValidateIsNotDefault("key");
            dataFactory.ValidateIsNotDefault("dataFactory");
            //Do not raise exception if expiryTimespan = emtpy.


            _tracingService.Trace(TraceLevel.Verbose, "HttpContextService.Add({0})".FormatStringCurrentCulture(key));

            return InternalTryGet(key, out result, dataFactory, expiryTimespan, selfUpdating);
        }



        private bool InternalTryGet<TData>(string key, out TData result, Func<TData> dataFactory, TimeSpan expiryTimespan, bool selfUpdating)
        {

            //Get the cached object:
            object unTypedObject = HttpContext.Current.Cache.Get(key);

            if (unTypedObject == null)
            {
                //nothing found, so maybe nothing else to do:
                if (dataFactory != null)
                {
                    //Set the dataFactory:
                    this.Set(key, dataFactory, expiryTimespan, selfUpdating);

                    //And then try to get it, which will set the key:
                    //return TryGet(key, out result);
                    return TryGet(key, out result, dataFactory, expiryTimespan, selfUpdating);
                }

                //It was null, but there was no data factory,
                //so just return default/null value:
                result = default(TData);
                return false;
            }

            //There was something in cache, 
            //and it theoretically should be one of our fancy ICacheItem<T>
            //objects.

            //Try to convert it to one of our objects:
            ICacheItem<TData> cacheItem = unTypedObject as ICacheItem<TData>;

            if (cacheItem == null)
            {
                //I guess someone else added something, they now want, through us
                //Sure doesn't seem to have been added by us, as it would have
                //been a derivative of ICacheItem:
                result = (TData)unTypedObject;

                //Say we found something even if it wasn't us who put it in in the first place:
                return true;
            }

            //It was added by us, but is no longer valid:
            if (!cacheItem.Valid)
            {

                //We know it's a FuncCacheItem<T>
                //but is it Self Updating?

                SelfUpdatingCacheItem<TData> tmp = cacheItem as SelfUpdatingCacheItem<TData>;


                if ((tmp == null) || (!tmp.SelfUpdating))
                {
                    //It wasn't valid, and 
                    //It's wasn't a SelfUpdatingCacheItem<T> (it was a FuncCacheItem<T>)
                    //and it's not self updating (atleast the original insert wasn't)
                    //But do we have enough info (now, this time, compared to when first set)
                    //as to turn it self updatingCacheItem?

                    //It wasn't valid, and it's not self updating...so remove:
                    Remove(key);

                    if (!selfUpdating)
                    {
                        //And we still are not self-updating:
                        result = default(TData);
                        return false;
                    }


                    //which means that now that it is clear/Removed, 
                    //we can now teach it how to get the value, and be on our merry way:

                    //Set the dataFactory:
                    this.Set(key, dataFactory, expiryTimespan, selfUpdating);
                    //And then try to get it, which will set the key:
                    return TryGet(key, out result);

                }


                //Was self updating, so calc a new future date:
                DateTime futureUtcDateTime = _dateTimeService.NowUTC.Add(tmp.TimeSpan);

                //Reset with new Future Date:
                tmp.Reset(() => (XAct.DependencyResolver.Current.GetInstance<IDateTimeService>().NowUTC < futureUtcDateTime));


                //BUt that doesn't get the current/future value value:
                //so result would be 'true', but not valid, 
                //Hence why, below, we requery the Value property, which
                //will 
            }

            //Get Value. Note that if just RReinvoke, to refresh data:
            result = cacheItem.Value;
            return true;
        }

        /// <summary>
        /// Removes the cached item that has the given key name.
        /// </summary>
        /// <param name="key">The key.</param>
        public void Remove(string key)
        {
            key.ValidateIsNotDefault("key");

            _tracingService.Trace(TraceLevel.Verbose, "HttpContextService.Remove({0})".FormatStringCurrentCulture(key));

            if (key.IsNullOrEmpty())
            {
                throw new ArgumentNullException("key");
            }
            HttpContext.Current.Cache.Remove(key);
        }

        /// <summary>
        /// Clears out all cached items.
        /// </summary>
        public void Clear(string keyprefix = null)
        {
            _tracingService.Trace(TraceLevel.Verbose, "HttpContextService.Clear()".FormatStringCurrentCulture());
            lock (this)
            {

                //Not going to work
                //As Context is not a generic dictionary 
                //List<string> keys = (from KeyValuePair<string, object> kvp in HttpContext.Current.Cache select kvp.Key).ToList();

                IDictionaryEnumerator dictionaryEnumerator = HttpContext.Current.Cache.GetEnumerator();

                List<string> keys = new List<string>();

                while (dictionaryEnumerator.MoveNext())
                {
                    string key = dictionaryEnumerator.Key as string;
                    if ((keyprefix == null) || (key.StartsWith(keyprefix)))
                    {
                        keys.Add(key);
                    }
                }

                foreach (string key in keys)
                {
                    if (keyprefix.IsNullOrEmpty() || key.StartsWith(keyprefix))
                    {
                        Remove(key);
                    }
                }

            }
        }



        /// <summary>
        /// Caches the specified item.
        /// </summary>
        /// <typeparam name="TData">The type of the data.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="cacheItem">The cache item.</param>
        /// <internal><para>5/11/2011: Sky</para></internal>
        private void Insert<TData>(string key, ICacheItem<TData> cacheItem)
        {

            if (cacheItem.Valid)
            {
                //THis is to allow for when timespan is set to 0...maybe due to someone forgetting to enter a time in AppSettings.
                _tracingService.Trace(TraceLevel.Verbose, "Adding {0} to cache even if already invalid".FormatStringCurrentCulture(key));
            }

            //IMPORTANT: Found out that in .NET45 (and maybe this was always the case)
            //that Add does not add to the cache if there was already one (!) 
            //But that it also doesn't complain if you request to Remove an object from 
            //cache, even if the item was not yet in cache.
            //So, you can either Remove it first
            //HttpContext.Current.Cache.Remove(key);
            //Or instead Insert.
            //I Prefer Insert.

            //Notice that we are not using the default anemic solution, 
            //and storing not just the value, but the whole cache item,
            //with a very future date.
            //Expiration is checked not against the Cache system, 
            //but against the ICacheItem delegate.
            //If expired, and not self updating, it removes itself:
            //HttpContext.Current.Cache.Add(key, cacheItem, null, DateTime.MaxValue, new TimeSpan(0,0,0,0),CacheItemPriority.Normal, null);

            HttpContext.Current.Cache.Insert(key, cacheItem, null, DateTime.MaxValue, new TimeSpan(0, 0, 0, 0), CacheItemPriority.Normal, null);
        }

    }
}
