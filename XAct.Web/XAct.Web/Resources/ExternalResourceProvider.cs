namespace XAct.Resources
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using System.Resources;
    using System.Web.Compilation;
    using XAct.Diagnostics;

    /// <summary>
    /// Resource provider for accessing external resources. 
    /// <para>
    /// Usage:
    /// </para>
    /// <para>
    /// <code>
    /// <![CDATA[
    ///   <compilation>
    ///     <expressionBuilders>
    ///       <add expressionPrefix="ExternalResource"
    ///            type="XAct.UI.Web.Compilation.ExternalResourceExpressionBuilder, 
    ///                  XAct.UI.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    ///     </expressionBuilders>
    ///   </compilation>
    /// ]]>
    /// </code>
    /// and
    /// <code>
    /// <![CDATA[
    /// <asp:Label runat="server" 
    ///            ID="UI_Label_Name"     
    ///            Text="<%$ ExternalResource:ClassLibrary1|Properties.Resources, UI_Label_Name %>" />
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    /// <remarks>
    /// Usage:
    /// <para>
    /// <code>
    /// <![CDATA[
    ///   <compilation>
    ///     <expressionBuilders>
    ///       <add expressionPrefix="ExternalResource"
    ///            type="XAct.UI.Web.Compilation.ExternalResourceExpressionBuilder, 
    ///                  XAct.UI.Web, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"/>
    ///     </expressionBuilders>
    ///   </compilation>
    /// ]]>
    /// </code>
    /// and
    /// <code>
    /// <![CDATA[
    /// <asp:Label runat="server" 
    ///            ID="UI_Label_Name"     
    ///            Text="<%$ ExternalResource:ClassLibrary1|Properties.Resources, UI_Label_Name %>" />
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public class GlobalExternalResourceProvider : IResourceProvider
    {
        #region Services

        private readonly ITracingService _tracingService;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalExternalResourceProvider"/> class.
        /// </summary>
        public GlobalExternalResourceProvider() : this(DependencyResolver.Current.GetInstance<ITracingService>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GlobalExternalResourceProvider"/> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        public GlobalExternalResourceProvider(ITracingService tracingService)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            _tracingService = tracingService;
        }

        #endregion

        #region IResourceProvider Members

        /// <summary>
        /// Retrieves resources using a ResourceManager instance
        /// for the assembly and resource key of this provider 
        /// instance. 
        /// </summary>
        /// <param name="resourceKey">The resource key to find.</param>
        /// <param name="culture">The culture to find.</param>
        /// <returns>The resource value if found.</returns>
        public object GetObject(string resourceKey, CultureInfo culture)
        {
            _tracingService.DebugTrace(
                TraceLevel.Verbose,
                "GlobalExternalResourceProvider.GetObject({0}, {1})",
                resourceKey,
                culture);


            if (culture == null)
            {
                culture = CultureInfo.CurrentUICulture;
            }

            return ResourceManager.GetObject(resourceKey, culture);
        }

        #endregion

        #region Nested type: Constants

        /// <summary>
        /// TODO
        /// </summary>
        public class Constants
        {
            /// <summary>
            /// TODO
            /// </summary>
            public static string ErrMsg_InvalidConstructor = "Invalid Constructor (ClassKey:{0})";
        }

        #endregion

        #region Fields

        private readonly string _assemblyName;
        private readonly string _classKey;

        #endregion

        #region Properties

        private ResourceManager _resourceManager;

        /// <summary>
        /// Loads the resource assembly and creates a 
        /// ResourceManager instance to access its resources.
        /// If the assembly is already loaded, Load returns a reference
        /// to that assembly.
        /// </summary>
        private ResourceManager ResourceManager
        {
            get
            {
                if (_resourceManager != null)
                {
                    return _resourceManager;
                }

                lock (this)
                {
                    Assembly assembly;
                    try
                    {
                        assembly = Assembly.Load(_assemblyName);
                    }
                    catch (Exception e)
                    {
                        _tracingService.TraceException(TraceLevel.Error, e, "Could not load the Assembly. {0}",
                                                     _assemblyName);
                        throw;
                    }

                    Type t = assembly.GetTypeEndingWith(_classKey);

                    if (t == null)
                    {
                        ArgumentException e =
                            new ArgumentException(
                                "Could not load the Type {1} from the Assembly. {0}"
                                    .FormatStringExceptionCulture(_assemblyName, _classKey));

                        _tracingService.TraceException(TraceLevel.Error, e,
                                                     "Could not load the Type {1} from the Assembly. {0}", _assemblyName,
                                                     _classKey);
                    }
                    _resourceManager =
                        new ResourceManager(t);
                }
                return _resourceManager;
            }
        }

        /// <summary>
        /// Implicit expressions are not supported by this provider 
        /// therefore a ResourceReader need not be implemented.
        /// Throws a NotSupportedException.
        /// </summary>
        public IResourceReader ResourceReader
        {
            get
            {
                _tracingService.DebugTrace(TraceLevel.Verbose,
                                         "GlobalExternalResourceProvider.get_ResourceReader()");

                throw new NotSupportedException();
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs an instance of the provider with the specified
        /// assembly name and resource type.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <param name="classKey">The assembly name and
        /// resource type in the format
        /// <c>resourceClassType,assemblyName</c></param>
        public GlobalExternalResourceProvider(ITracingService tracingService, string assemblyName, string classKey)
        {
            tracingService.ValidateIsNotDefault("tracingService");
            _tracingService = tracingService;

            _tracingService.DebugTrace(
                TraceLevel.Verbose,
                "GlobalExternalResourceProvider(classKey:{0})",
                classKey);


            _assemblyName = assemblyName;
            _classKey = classKey;
        }

        #endregion
    }
}