namespace XAct.Resources
{
    using System;
    using System.Linq;
    using System.Web.Compilation;
    using XAct.Diagnostics;

    /// <summary>
    /// Provider factory implementation for external resources. 
    /// <para>
    /// Only supports global resources. 
    /// </para>
    /// </summary>
    public class ExternalResourceProviderFactory : ResourceProviderFactory
    {
        private readonly ITracingService _tracingService;

        #region Consstructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalResourceProviderFactory"/> class.
        /// </summary>
        public ExternalResourceProviderFactory()
            : this(DependencyResolver.Current.GetInstance<ITracingService>())
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalResourceProviderFactory"/> class.
        /// </summary>
        /// <param name="tracingService">The logging service.</param>
        public ExternalResourceProviderFactory(ITracingService tracingService)
        {
            _tracingService = tracingService;
        }

        #endregion

        /// <summary>
        /// Creates a global resource provider.
        /// </summary>
        /// <param name="classKey">The name of the resource class.</param>
        /// <returns>
        /// An <see cref="T:System.Web.Compilation.IResourceProvider"/>.
        /// </returns>
        public override IResourceProvider CreateGlobalResourceProvider(string classKey)
        {
            _tracingService.DebugTrace(TraceLevel.Verbose,
                                     "ExternalResourceProviderFactory.CreateGlobalResourceProvider(classKey:{0})",
                                     classKey);

            string[] stuff = classKey.Split(',', ':', '|').Select(s => s.Trim()).ToArray();
            return new GlobalExternalResourceProvider(_tracingService, stuff[1], stuff[0]);
        }

        /// <summary>
        /// Creates a local resource provider.
        /// </summary>
        /// <param name="virtualPath">The path to a resource file.</param>
        /// <returns>
        /// An <see cref="T:System.Web.Compilation.IResourceProvider"/>.
        /// </returns>
        public override IResourceProvider CreateLocalResourceProvider(string virtualPath)
        {
            throw new NotSupportedException(
                Constants.ErrMsg_LocalResourcesNotSupported.FormatStringCurrentUICulture(
                    "ExternalResourceProviderFactory")
                );
        }

        #region Nested type: Constants

        private class Constants
        {
            public static string ErrMsg_LocalResourcesNotSupported = "LocalResources not supported (Class:{0})";
        }

        #endregion
    }
}