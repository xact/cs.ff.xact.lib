﻿namespace XAct
{
    using System.Web.SessionState;

    /// <summary>
    /// Extensions for the <see cref="HttpSessionState"/> object.
    /// </summary>
    public static class HttpSessionStateExtensions
    {
        /// <summary>
        /// Removes items from the httpSessionState, without
        /// throwing an exception if <paramref name="httpSessionState"/>
        /// was null, or the item didn't exist.
        /// </summary>
        /// <param name="httpSessionState">State of the HTTP session.</param>
        /// <param name="key">The key.</param>
        public static void RemoveSafely(this HttpSessionState httpSessionState, string key)
        {
            if (httpSessionState == null)
            {
                return;
            }

            try
            {
                httpSessionState.Remove(key);
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {

            }
        }
    }
}