﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct
{
    using System;
    using System.Security.Principal;
    using System.Text;
    using System.Web;
    using XAct.Net.Messaging;

#endif

    /// <summary>
    /// Extensions to the HttpContext object.
    /// </summary>
    public static class HttpContextExtensions
    {
        /// <summary>
        /// Gets the identity of the underlying AppPool, 
        /// or impersonated user if Impersonation is active.
        /// <para>
        /// If HttpWorkerRequest is null, returns "UNKNOWN".
        /// </para>
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public static string AppPoolIdentity(this HttpContext httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentException("httpContext");
            }

            IServiceProvider serviceProvider = httpContext;

            HttpWorkerRequest httpWorkerRequest =
                serviceProvider.GetService(typeof (HttpWorkerRequest)) as HttpWorkerRequest;

            //Get the identity of the AppPool:

            if (httpWorkerRequest == null)
            {
                return "UNKNOWN";
            }

            IntPtr ptrUserToken = httpWorkerRequest.GetUserToken();
            WindowsIdentity handlerWinIdentity = new WindowsIdentity(ptrUserToken);

            return handlerWinIdentity.Name;
        }








        /// <summary>
        /// Transmits the PersistedFile back to a UserAgent.
        /// </summary>
        /// <param name="httpContext">The HTTP context.</param>
        /// <param name="persistedFile">The persisted file.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <param name="setFileDownloadCookie">if set to <c>true</c> [set file download cookie].</param>
        /// <param name="cookieName">Name of the cookie.</param>
        /// <param name="cookiePath">The cookie path.</param>
        /// <param name="changeFileStreamEncoding">if set to <c>true</c> [change file stream encoding].</param>
        /// <param name="sourceFileStreamEncoding">The source file stream encoding.</param>
        /// <param name="targetFileStreamEncoding">The target file stream encoding.</param>
        public static void TransmitFile(this HttpContext httpContext, PersistedFile persistedFile, bool flush = false,
                                        bool setFileDownloadCookie = true, string cookieName = "fileDownload",
                                        string cookiePath = "/", bool changeFileStreamEncoding = false,
                                        Encoding sourceFileStreamEncoding = null,
                                        Encoding targetFileStreamEncoding = null)
        {


            var httpResponse = httpContext.Response;

            httpResponse.TransmitFile(persistedFile, flush, setFileDownloadCookie, cookieName, cookiePath,
                                      changeFileStreamEncoding, sourceFileStreamEncoding, targetFileStreamEncoding);

        }

    }


#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
