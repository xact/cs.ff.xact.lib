﻿using System;
using System.Web;

namespace XAct
{
    using System.Linq;
    using System.Net;
    using XAct.State;

    /// <summary>
    /// Extensions to the <see cref="HttpRequest"/> object.
    /// </summary>
    public static class HttpRequestExtensions
    {

        /// <summary>
        /// Determines whether the incoming request was secure (ie HTTPS), 
        /// and optionally suppresses REsponse Content (using <see cref="HttpResponse.SuppressContent"/>.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="suppressResponse">if set to <c>true</c> [suppress response].</param>
        /// <returns>
        ///   <c>true</c> if [is secure connection] [the specified request]; otherwise, <c>false</c>.
        /// </returns>
        /// <internal>
        /// http://stackoverflow.com/a/325681/1052767
        /// </internal>
        public static bool IsSecureConnection(this System.Web.HttpRequest request, bool suppressResponse = false)
        {

            bool isSecure =request.IsSecureConnection;
            
            if ((!isSecure)&&(suppressResponse))
            {
                System.Web.HttpContext.Current.Response.SuppressContent = true;
            }
            return isSecure;
        }

        /// <summary>
        /// Determines whether the incoming request was secure (ie HTTPS), 
        /// and optionally redirects user to a page that explains something about needing SSL...etc.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="redirectUri">The redirect URI.</param>
        /// <returns>
        ///   <c>true</c> if [is secure connection] [the specified request]; otherwise, <c>false</c>.
        /// </returns>
        /// <internal>
        /// http://stackoverflow.com/a/325681/1052767
        /// </internal>
        public static bool IsSecureConnection(this System.Web.HttpRequest request, Uri redirectUri = null)
        {

            bool isSecure = request.IsSecureConnection;

            if ((!isSecure) && (redirectUri!=null))
            {
                System.Web.HttpContext.Current.Response.Redirect(redirectUri.ToString());
            }
            return isSecure;
        }


        /// <summary>
        /// Maps the Headers in the given HttpRequest to the given <see cref="WebClient"/>.
        /// </summary>
        /// <param name="httpRequest"></param>
        /// <param name="webClient"></param>
        /// <param name="headersToSkip"></param>
        public static void MapHeadersFromRequest(this HttpRequest httpRequest, WebClient webClient, params string[] headersToSkip)
        {
            //Pass headers through:

            for (int i = 0; i < httpRequest.Headers.Count; ++i)
            {
                string headerName = httpRequest.Headers.GetKey(i);

                if (headersToSkip.Any(x => string.Compare(x, headerName, true) == 0))
                {
                    continue;
                }

                foreach (string value in httpRequest.Headers.GetValues(i))
                {
                    webClient.Headers.Add(headerName, value);
                }
            }
        }

        /// <summary>
        /// Reads the cookie value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="httpRequest">The HTTP request.</param>
        /// <param name="cookieName">Name of the cookie.</param>
        /// <returns></returns>
        public static T ReadCookieValue<T>(this HttpRequest httpRequest, string cookieName)
        {
            var requestCookie = httpRequest.Cookies[cookieName];
            T result = (requestCookie == null) ? default(T) : requestCookie.Value.ConvertTo<T>();
            return result;
        }

        /// <summary>
        /// Helper method to find incoming cookies, and transfer their values to the current State, before deleting the cookie.
        /// <para>
        /// The use case for this is when using a temporary cookie on a PGR (POST GET REDIRECT), and putting a cookie
        /// on the 302, that is sent back with the subsuquent GET, where this method is used to extract the value
        /// and delete the cookie.
        /// </para>
        /// <para>
        /// Any subsequent GET or POST will have no cookie (and therefore neither will their request context state).
        /// </para>
        /// <para>
        /// This is useful for displaying a message on the GET page that will expire with subsequent requests.
        /// Proxy code would be:
        /// <code>
        /// <![CDATA[
        /// if (context[X]!=null){ then 
        ///    render banner with text "Undo the last operation by clicking here").
        ///    //Note cookie X contains the Id of record to undelete.
        /// }
        /// ]]>
        /// </code>
        /// </para>
        /// </summary>
        /// <param name="httpRequest"></param>
        /// <param name="wildCardCookieNamePrefix"></param>
        /// <param name="contextStateService"></param>
        public static void SetRequestContextFromCookiesAndExpireCookies(this HttpRequest httpRequest,
                                                                        string wildCardCookieNamePrefix,
                                                                        XAct.State.IContextStateService
                                                                            contextStateService = null)
        {
            if (contextStateService == null)
            {
                contextStateService = XAct.DependencyResolver.Current.GetInstance<IContextStateService>();
            }
            foreach (HttpCookie cookie in httpRequest.Cookies)
            {
                if (!cookie.Name.CompareWildcard(wildCardCookieNamePrefix, true))
                {
                    continue;
                }

                contextStateService.Items[cookie.Name] = cookie.Value;
                //Ensure the cookie is expired:
                HttpContext.Current.Response.ExpireCookie(cookie.Name);
            }
        }
    }
}
