﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY

namespace XAct
{
    using System;
    using System.Net;
    using System.Text;
    using System.Web;
    using System.Web.Security;
    using XAct.Net.Messaging;


#endif

    /// <summary>
    /// Extensions tothe HttpResponse object
    /// </summary>
    public static class HttpReponseExtensions
    {

        /// <summary>
        /// Prevents the client caching.
        /// </summary>
        /// <param name="httpResponse">The HTTP response.</param>
        /// <internal><para>7/17/2011: Sky</para></internal>
        public static void PreventClientCaching(this HttpResponse httpResponse)
        {
            if (HttpContext.Current.Request.RequestType != "GET")
            {
                return;
            }


            //Src: http://www.352media.com/rantingandraving/rave.aspx?raveid=325&id=212
            //http://stackoverflow.com/questions/5063338/prevent-scripts-from-being-cached-programmatically

            httpResponse.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            httpResponse.Cache.SetValidUntilExpires(false);
            httpResponse.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            //Note: The HttpCacheability.NoCache has an issue in IE (not sure about FireFox) 
            //when you are on a secure (https) page and are trying to send a file by setting the headers. 
            //It causes the file download to not work properly. 
            httpResponse.Cache.SetCacheability(HttpCacheability.NoCache);
            //Required for FF?
            httpResponse.Cache.SetNoStore();

            //HttpContext.Current.Response.Cache.SetAllowResponseInBrowserHistory(false);
        }

        /// <summary>
        /// Clears the FormsAuthentication cookie -- completely (preventing playback).
        /// </summary>
        /// <param name="httpResponse">The HTTP response.</param>
        public static void ClearFormsAuthenticationCookie(this HttpResponse httpResponse)
        {
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Path = FormsAuthentication.FormsCookiePath;
            cookie1.Domain = FormsAuthentication.CookieDomain;
            cookie1.HttpOnly = true;
            cookie1.Expires = DateTime.Now.AddYears(-1);
            httpResponse.Cookies.Add(cookie1);
        }

        /// <summary>
        /// Clears the session cookie.
        /// </summary>
        /// <param name="httpResponse">The HTTP response.</param>
        /// <param name="invokeSessionAbandon">if set to <c>true</c> [invoke session abandon].</param>
        public static void ClearSessionCookie(this HttpResponse httpResponse, bool invokeSessionAbandon = false)
        {
            if (invokeSessionAbandon)
            {
                System.Web.HttpContext.Current.Session.Abandon();
            }
            //Or do it better:
            //clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
            HttpCookie cookie = new HttpCookie("ASP.NET_SessionId", "");
            cookie.Expires = DateTime.Now.AddYears(-1);
            httpResponse.Cookies.Add(cookie);

        }

        /// <summary>
        /// Helper method to set a cookie into the response.
        /// <para>
        /// Note that cookieHttpOnly is defaulted to true, or front end SPA's will fail.
        /// Requiring CookieSecure is left at false: control it from a AppSetting, or by investigating the current
        /// request's protocol, and setting it if it is https:
        /// </para>
        /// </summary>
        /// <param name="httpResponse"></param>
        /// <param name="cookieName"></param>
        /// <param name="cookieValue"></param>
        /// <param name="cookiePath"></param>
        /// <param name="cookieHttpOnly"></param>
        /// <param name="cookieSecure"></param>
        public static void SetCookie(this HttpResponse httpResponse, string cookieName, string cookieValue,  string cookiePath="/", bool cookieHttpOnly=true, bool cookieSecure=false)
        {
            //Doesn't crash if it is missing:
            httpResponse.Cookies.Remove(cookieName);

            //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
            //IMPORTANT:You have t mark it as HttpOnly or front end javascript (ie SPA) will fail:
            //NOTE: Havn't set secure... it might get picked up in a security audit.
            httpResponse.SetCookie(new HttpCookie(cookieName, cookieValue)
            {
                Path = cookiePath,
                HttpOnly = cookieHttpOnly,
                Secure = cookieSecure
            });

        }

        /// <summary>
        /// Expires a cookie by ensuring the Response cookie's va
        /// </summary>
        /// <param name="httpResponse"></param>
        /// <param name="cookieName"></param>
        public static void ExpireCookie(this HttpResponse httpResponse, string cookieName)
        {
            //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
            if (HttpContext.Current != null)
            {
                //HttpRequest httpRequest = HttpContext.Current.Request;
                //HttpCookie requestCookie = httpRequest.Cookies[cookieName];
                //if (requestCookie != null)
                //{
                    HttpCookie responseCookie = httpResponse.Cookies[cookieName];
                    if (responseCookie != null)
                    {
                        responseCookie.Expires = DateTime.Now.AddYears(-1);
                    }
                //}
            }

        }




        /// <summary>
        /// Transmits the PersistedFile back to a UserAgent.
        /// <para>
        /// Note: the operation sets a cookie as needed by JQuery's download extension.
        /// </para>
        /// </summary>
        /// <param name="httpResponse">The HTTP response.</param>
        /// <param name="persistedFile">The persisted file.</param>
        /// <param name="flush">if set to <c>true</c> [flush].</param>
        /// <param name="setFileDownloadCookie">if set to <c>true</c> [set file download cookie].</param>
        /// <param name="cookieName">Name of the cookie.</param>
        /// <param name="cookiePath">The cookie path.</param>
        /// <param name="changeFileStreamEncoding">if set to <c>true</c> [change file stream encoding].</param>
        /// <param name="sourceFileStreamEncoding">The source file stream encoding.</param>
        /// <param name="targetFileStreamEncoding">The target file stream encoding.</param>
        public static void TransmitFile(this HttpResponse httpResponse, PersistedFile persistedFile, bool flush = false,
                                        bool setFileDownloadCookie = true, string cookieName = "fileDownload",
                                        string cookiePath = "/", bool changeFileStreamEncoding = false,
                                        System.Text.Encoding sourceFileStreamEncoding = null,
                                        System.Text.Encoding targetFileStreamEncoding = null)
        {

            if (cookieName == null)
            {
                cookieName = "fileDownload";
            }
            if (sourceFileStreamEncoding == null)
            {
                sourceFileStreamEncoding = System.Text.Encoding.UTF8;
            }
            if (targetFileStreamEncoding == null)
            {
                targetFileStreamEncoding = System.Text.Encoding.UTF8;
            }


            if (setFileDownloadCookie)
            {
                bool cookieHttpOnly = false;
                //Allows usage from Ajax to HttpHandlers under Https when safe is turned on.
                bool cookieSecure = false;

                SetCookieForFileDownload(httpResponse, true, cookieName, cookiePath, cookieHttpOnly, cookieSecure);
            }

            httpResponse.ContentType = persistedFile.ContentType;
            httpResponse.AddHeader("Content-Disposition", "attachment; filename=" + persistedFile.Name);
            httpResponse.AddHeader("Content-Length", persistedFile.Size.ToString());


            //int checkBufferSize = persistedFile.Value.Length;

            byte[] outgoingBuffer;
            byte[] srcBytes = persistedFile.Value;

            outgoingBuffer = srcBytes;

            if (changeFileStreamEncoding && !sourceFileStreamEncoding.Equals(targetFileStreamEncoding))
            {
                //encoding = Encoding.GetEncoding("ISO-8859-1");
                outgoingBuffer = Encoding.Convert(sourceFileStreamEncoding, targetFileStreamEncoding, srcBytes);
            }


            //string msg = iso.GetString(isoBytes);


            int bufferSize = outgoingBuffer.Length;

            httpResponse.OutputStream.Write(outgoingBuffer, 0, bufferSize);

            if (flush)
            {
                httpResponse.Output.Flush();
            }
        }

        //The JQuery Download tool is capable of using an IFrame to work around missing functionality in IE.
        //But it needs a cookie.
        private static void SetCookieForFileDownload(HttpResponse httpResponse, bool hasFile, string cookieName,
                                                     string cookiePath = "/", bool cookieHttpOnly = false,
                                                     bool cookieSecure = false)
        {
            // Set the 'download' cookie as approprate, for the jQuery download tool that uses this
            if (hasFile)
            {
                
                //jquery.fileDownload uses this cookie to determine that a file download has completed successfully
                //IMPORTANT:You have t mark it as HttpOnly or front end javascript (ie SPA) will fail:
                //NOTE: Havn't set secure... it might get picked up in a security audit.
                httpResponse.SetCookie(cookieName,"true", cookiePath,cookieHttpOnly,cookieSecure);
            }
            else
            {
                //ensure that the cookie is removed in case someone did a file download without using jquery.fileDownload
                httpResponse.ExpireCookie(cookieName);
            }
        }




        /// <summary>
        /// Maps the Headers from the given <see cref="WebClient" />
        /// to the HttpContext.Response.
        /// </summary>
        /// <param name="httpResponse">The HTTP response.</param>
        /// <param name="webClient">The web client.</param>
        public static void MapHeadersToResponse(this HttpResponse httpResponse, WebClient webClient)
        {
            //Pass headers through:
            for (int i = 0; i < webClient.ResponseHeaders.Count; ++i)
            {
                string headerName = webClient.ResponseHeaders.GetKey(i);

                foreach (string headerValue in webClient.ResponseHeaders.GetValues(i))
                {
                    httpResponse.AddHeader(headerName, headerValue);
                }
            }
        }


    }



#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
