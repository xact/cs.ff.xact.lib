﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct
{
    using System;
    using System.IO;
    using System.Text;
    using System.Web;

#endif

    
    /// <summary>
    /// String extensions for web development.
    /// </summary>
    /// <internal><para>7/21/2011: Sky</para></internal>
    public static class StringExtensions
    {
        /// <summary>
        /// Decodes an encoded url string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>7/21/2011: Sky</para></internal>
        public static string UrlDecode(this string text)
        {
            return HttpUtility.UrlDecode(text);
        }

        /// <summary>
        /// Decodes an Html encoded string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>7/21/2011: Sky</para></internal>
        public static string HtmlDecode(this string text)
        {
            return HttpUtility.HtmlDecode(text);
        }

        /// <summary>
        /// Converts a string into a Unicode string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        [Obsolete("Obsolete in .NET45",true)]
        public static string UrlEncodeUnicode(this string text)
        {
            throw new NotImplementedException();
            //return HttpUtility.UrlEncodeUnicode(text);
        }

        /// <summary>
        /// Encodes the path portion of an Url string for reliable http tranmission from Server to Client.
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string UrlPathEncode(this string text)
        {
            return HttpUtility.UrlPathEncode(text);
        }

        /// <summary>
        /// Decodes an encoded url string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="useLegacy">if set to <c>true</c> [use legacy].</param>
        /// <returns></returns>
        /// <internal>7/21/2011: Sky</internal>
        public static string HtmlEncode(this string text, bool useLegacy)
        {
            if (useLegacy)
            {
                return HttpUtility.HtmlEncode(text);
            }
            return Microsoft.Security.Application.Encoder.HtmlEncode(text);
        }


        /// <summary>
        /// Minimally converts a string into an HTML encoded string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="useLegacy">if set to <c>true</c> [use legacy].</param>
        /// <returns></returns>
        /// <internal>7/21/2011: Sky</internal>
        public static string HtmlAttributeEncode(this string text, bool useLegacy)
        {
            if (useLegacy)
            {
                return HttpUtility.HtmlAttributeEncode(text);
            }
            return Microsoft.Security.Application.Encoder.HtmlAttributeEncode(text);
        }


        /// <summary>
        /// Encodes an Url string.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="useLegacy">if set to <c>true</c> [use legacy].</param>
        /// <returns></returns>
        /// <internal>7/21/2011: Sky</internal>
        public static string UrlEncode(this string text, bool useLegacy)
        {
            if (useLegacy)
            {
                return HttpUtility.UrlEncode(text);
            }
            return Microsoft.Security.Application.Encoder.UrlEncode(text);
        }

        /// <summary>
        /// ENcodes input strings used in CSS element values.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>7/21/2011: Sky</para></internal>
        public static string CssEncode(this string text)
        {
            return Microsoft.Security.Application.Encoder.CssEncode(text);
        }

        /// <summary>
        /// Encodes input strings for use in application/x-www-form-urlencoded form submissions
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>7/21/2011: Sky</para></internal>
        public static string HtmlFormUrlEncode(this string text)
        {
            return Microsoft.Security.Application.Encoder.HtmlFormUrlEncode(text);
        }

        /// <summary>
        /// Encodes input string for use in Javasscript
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string JavaScriptEncode(this string text)
        {
            return Microsoft.Security.Application.Encoder.JavaScriptEncode(text);
        }


        /// <summary>
        /// Encodes input strings for use in Xml.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>7/21/2011: Sky</para></internal>
        public static string XmlEncode(this string text)
        {
            return Microsoft.Security.Application.Encoder.XmlEncode(text);
        }

        /// <summary>
        /// Encodes input strings for use in Xml attributes.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        /// <internal><para>7/21/2011: Sky</para></internal>
        public static string XmlAttributeEncode(this string text)
        {
            return Microsoft.Security.Application.Encoder.XmlAttributeEncode(text);
        }



        /// <summary>
        /// Returns a MemoryStream for a given string.
        /// <para>
        /// Default Encoding is UTF8.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public static MemoryStream GenerateStreamFromString(this string value, Encoding encoding = null)
        {
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }

            return new MemoryStream(encoding.GetBytes(value ?? ""));
        }
    }

#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
}
#endif
