﻿//namespace XAct.UI.Web
//{
//    using System;
//    using System.Web;

//    /// <summary>
//    /// Service based httphandler.
//    /// <para>
//    /// Usage:
//    /// <code>
//    /// <![CDATA[
//    ///     public class MyHttpHandlerFactory : ServiceLocatorHttpHandlerFactoryBase<IMyHttpHandler>    {}
//    /// ]]>
//    /// </code>
//    /// will instantiate a:
//    /// <code>
//    /// <![CDATA[
//    /// [DefaultBindingImplementation(typeof(IMyHttpHandler))]
//    /// public class MyHttpHandler : IMyHttpHandler {
//    ///   public MyHttpHandler(ISomeInjectedService yayDependencyInjectionWorksEvenHereNow){
//    ///    ....
//    ///   }
//    /// }
//    /// ]]>
//    /// </code>
//    /// </para>
//    /// </summary>
//    /// <typeparam name="THttpHandler">The type of the HTTP handler.</typeparam>
//    public abstract class ServiceLocatorHttpHandlerFactoryBase<THttpHandler> : IHttpHandlerFactory
//        where THttpHandler : IHttpHandler
//    {


//        /// <summary>
//        /// Returns an instance of a class that implements the <see cref="T:System.Web.IHttpHandler" /> interface.
//        /// </summary>
//        /// <param name="context">An instance of the <see cref="T:System.Web.HttpContext" /> class that provides references to intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
//        /// <param name="requestType">The HTTP data transfer method (GET or POST) that the client uses.</param>
//        /// <param name="url">The <see cref="P:System.Web.HttpRequest.RawUrl" /> of the requested resource.</param>
//        /// <param name="pathTranslated">The <see cref="P:System.Web.HttpRequest.PhysicalApplicationPath" /> to the requested resource.</param>
//        /// <returns>
//        /// A new <see cref="T:System.Web.IHttpHandler" /> object that processes the request.
//        /// </returns>
//        public IHttpHandler GetHandler(HttpContext context, string requestType, String url, String pathTranslated)
//        {
//            //IHttpHandler handlerToReturn;

//            //string verb = context.Request.RequestType.ToLower();

//            Type handlerInterfaceType = typeof(THttpHandler);

//            //TODO: CHECK WHAT THIS IS FOR
//            //switch (verb)
//            //{
//            //    case "get":
//            //        handlerInterfaceType = typeof(IPOXAPIHttpHandler);
//            //        break;
//            //    default:
//            //        handlerInterfaceType = null;
//            //        break;
//            //}

//            return handlerInterfaceType != null
//                       ? XAct.Shortcuts.ServiceLocate(handlerInterfaceType) as IHttpHandler
//                       : null;
//        }

//        /// <summary>
//        /// Enables a factory to reuse an existing handler instance.
//        /// </summary>
//        /// <param name="handler">The <see cref="T:System.Web.IHttpHandler" /> object to reuse.</param>
//        public void ReleaseHandler(IHttpHandler handler)
//        {

//        }

//        /// <summary>
//        /// Gets a value indicating whether this factory is reusable.
//        /// </summary>
//        /// <value>
//        ///   <c>true</c> if [is reusable]; otherwise, <c>false</c>.
//        /// </value>
//        public bool IsReusable
//        {
//            get
//            {
//                return false;
//            }
//        }
//    }
//}