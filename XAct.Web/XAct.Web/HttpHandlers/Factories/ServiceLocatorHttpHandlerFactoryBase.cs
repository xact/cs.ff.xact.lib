﻿namespace XAct.Web.HttpHandlers
{
    using System;
    using System.Web;

    /// <summary>
    /// A service locator based 
    /// <see cref="IHttpHandler"/> 
    /// implementation factory.
    /// <para>
    /// Usage is:
    /// </para>
    /// <para>
    /// Config file points to a subclass of this factory, that
    /// indicates the type of the Handler to return.
    /// </para>
    /// <para>
    /// Usage:
    /// <code>
    /// <![CDATA[
    ///     public class MyHttpHandlerFactory : ServiceLocatorHttpHandlerFactoryBase<IMyHttpHandler>    {}
    /// ]]>
    /// </code>
    /// will instantiate a:
    /// <code>
    /// <![CDATA[
    /// [DefaultBindingImplementation(typeof(IMyHttpHandler))]
    /// public class MyHttpHandler : IMyHttpHandler {
    ///   public MyHttpHandler(ISomeInjectedService yayDependencyInjectionWorksEvenHereNow){
    ///    ....
    ///   }
    /// }
    /// ]]>
    /// </code>
    /// </para>
    /// </summary>
    /// <typeparam name="THttpHandler">The type of the HTTP handler.</typeparam>
    public abstract class ServiceLocatorHttpHandlerFactoryBase<THttpHandler> : IHttpHandlerFactory
        where THttpHandler : IHttpHandler
    {


        /// <summary>
        /// Gets a value indicating whether [is reusable].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is reusable]; otherwise, <c>false</c>.
        /// </value>
        public bool IsReusable
        {
            get
            {
                return _isReuseable;
            }
        }
        private bool _isReuseable;



        /// <summary>
        /// Services the locator HTTP handler factory base.
        /// </summary>
        /// <param name="isReuseable">if set to <c>true</c> [is reuseable].</param>
        /// <returns></returns>
        protected ServiceLocatorHttpHandlerFactoryBase(bool isReuseable)
        {
            _isReuseable = isReuseable;
        }

        /// <summary>
        /// Returns an instance of a class that implements the <see cref="T:System.Web.IHttpHandler" /> interface.
        /// </summary>
        /// <param name="context">An instance of the <see cref="T:System.Web.HttpContext" /> class that provides references to intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
        /// <param name="requestType">The HTTP data transfer method (GET or POST) that the client uses.</param>
        /// <param name="url">The <see cref="P:System.Web.HttpRequest.RawUrl" /> of the requested resource.</param>
        /// <param name="pathTranslated">The <see cref="P:System.Web.HttpRequest.PhysicalApplicationPath" /> to the requested resource.</param>
        /// <returns>
        /// A new <see cref="T:System.Web.IHttpHandler" /> object that processes the request.
        /// </returns>
        public virtual IHttpHandler GetHandler(HttpContext context, string requestType, String url, String pathTranslated)
        {
            //IHttpHandler handlerToReturn;


            Type handlerInterfaceType = typeof(THttpHandler);

            //TODO: CHECK WHAT THIS IS FOR
            //string verb = context.Request.RequestType.ToLower();
            //switch (verb)
            //{
            //    case "get":
            //        handlerInterfaceType = typeof(IPOXAPIHttpHandler);
            //        break;
            //    default:
            //        handlerInterfaceType = null;
            //        break;
            //}

            return XAct.Shortcuts.ServiceLocate(handlerInterfaceType) as IHttpHandler;
        }

        /// <summary>
        /// Enables a factory to reuse an existing handler instance.
        /// </summary>
        /// <param name="handler">The <see cref="T:System.Web.IHttpHandler" /> object to reuse.</param>
        public void ReleaseHandler(IHttpHandler handler)
        {

        }

    }
}