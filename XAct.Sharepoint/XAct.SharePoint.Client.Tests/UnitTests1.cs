
namespace XAct.SharePoint.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using NUnit.Framework;
    using XAct.SharePoint.Implementations;
    using XAct.Tests;

    /// <summary>
    /// 
    /// </summary>
    public class TestListManagementServiceConfiguration : ListManagementServiceConfiguration, IHasXActLibServiceConfiguration
    {
        public TestListManagementServiceConfiguration()
        {
            base.SiteUrl = "http://sp.258spikes.jqdev.local/";
        }
    }

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class IListManagementServiceTests
    {
        private bool _accessible;

        private IListManagementService _sharepointListService;

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            _accessible = Test("http://sp.258spikes.jqdev.local/");

                        Singleton<IocContext>.Instance.ResetIoC();


            XAct.DependencyResolver.Current.RegisterServiceBindingInIoC<
                IListManagementServiceConfiguration, 
                TestListManagementServiceConfiguration>();


            _sharepointListService =
                DependencyResolver.Current.GetInstance<IListManagementService>();

#pragma warning disable 168
            string check = _sharepointListService.Configuration.SiteUrl;
#pragma warning restore 168


            WebProxy webProxy = new WebProxy("dnzwgpx2", 80);
            //The DefaultCredentials automically get username and password.
            webProxy.Credentials = CredentialCache.DefaultCredentials;
            WebRequest.DefaultWebProxy = webProxy;


        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }


        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest01()
        {
            if (!_accessible)
            {
                Assert.Ignore("Sharepoint site not accessible.");
                return;
            }
            Assert.IsNotNullOrEmpty(_sharepointListService.Configuration.SiteUrl );
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void UnitTest02()
        {
            if (!_accessible)
            {
                Assert.Ignore("Sharepoint site not accessible.");
                return;
            }

            IEnumerable<Microsoft.SharePoint.Client.List> r = _sharepointListService.GetLists();
            foreach (Microsoft.SharePoint.Client.List list in r)
            {
#pragma warning disable 168
                string name = list.Title;
#pragma warning restore 168
            }
            Assert.IsTrue(r.Count()>0);
        }
        [Test]
        public void UnitTest03()
        {
            if (!_accessible)
            {
                Assert.Ignore("Sharepoint site not accessible.");
                return;
            }
            
            IEnumerable<Microsoft.SharePoint.Client.List> r = _sharepointListService.GetLists();
            foreach (Microsoft.SharePoint.Client.List list in r)
            {
#pragma warning disable 168
                string name = list.Title;
#pragma warning restore 168
            }
            Assert.IsTrue(r.Count() > 0);
        }
        [Test]
        public void UnitTest04()
        {
            if (!_accessible)
            {
                Assert.Ignore("Sharepoint site not accessible.");
                return;
            }
            
            
            IEnumerable<Microsoft.SharePoint.Client.List> r = _sharepointListService.GetLists(ListTemplateType.Tasks);
            foreach (Microsoft.SharePoint.Client.List list in r)
            {
#pragma warning disable 168
                string name = list.Title;
#pragma warning restore 168
            }
            Assert.IsTrue(r.Count() > 0);
        }


        bool Test(string url)
        {
            // using MyClient from linked post
            using (var client = new MyClient())
            {
                client.HeadOnly = true;
                // fine, no content downloaded
                try
                {
#pragma warning disable 168
                    string s1 = client.DownloadString(url);
#pragma warning restore 168
                    return true;
                }
                catch
                {
                }
                return false;
            }
        }
    }

    class MyClient : WebClient
    {
        public bool HeadOnly { get; set; }
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest req = base.GetWebRequest(address);
            if (HeadOnly && req.Method == "GET")
            {
                req.Method = "HEAD";
            }
            return req;
        }
    }
}


