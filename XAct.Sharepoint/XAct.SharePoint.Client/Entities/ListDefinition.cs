﻿
namespace XAct.SharePoint
{
    using System.Runtime.Serialization;

    /// <summary>
    /// An argument package to use with 
    /// <see cref="IListManagementService.CreateList(ListCreationInformation)"/> and 
    /// <see cref="IListManagementService.UpdateList(ListCreationInformation)"/>
    /// </summary>
    public class ListCreationInformation : IHasName
    {

        /// <summary>
        /// Gets or sets the list template type.
        /// </summary>
        /// <value>
        /// The type of the list template.
        /// </value>
        public ListTemplateType ListTemplateType { get; set; }

        /// <summary>
        /// Gets the Title of the SharePoint List.
        /// <para>Member defined in<see cref="XAct.IHasName"/></para>
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        /// <internal>8/13/2011: Sky</internal>
        [DataMember(Name="Name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the list description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        [DataMember(Name="Description")]
        public string Description { get; set; }


        //newListInfo.DocumentTemplateType =
        //newListInfo.TemplateType=
        //newListInfo.TemplateFeatureId = null;
        //newListInfo.TypeId = null;
        //newListInfo.Url = null;

    }
}
