﻿
namespace XAct
{
    using Microsoft.SharePoint.Client;

    /// <summary>
    /// Extensions to the <see cref="XAct.SharePoint.ListTemplateType"/> 
    /// enum
    /// </summary>
    public static class ListTemplateTypeExtensions
    {

        /// <summary>
        /// Maps to.
        /// </summary>
        /// <param name="listTemplateType">Type of the list template.</param>
        /// <returns></returns>
        public static ListTemplateType MapTo(this SharePoint.ListTemplateType listTemplateType)
        {
            ListTemplateType result =
                (ListTemplateType) (int) listTemplateType;

            return result;
        }
    }
}
