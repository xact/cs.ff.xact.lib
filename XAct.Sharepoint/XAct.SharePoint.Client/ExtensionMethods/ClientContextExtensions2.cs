﻿namespace XAct
{
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using Microsoft.SharePoint.Client;
    using XAct.Utils;

    /// <summary>
    /// 
    /// </summary>
    public static class ClientContextExtensions2
    {
        /// <summary>
        /// Creates the new ListItem, and adds it to List,
        /// to be later saved.
        /// </summary>
        /// <param name="targetList">The target list.</param>
        /// <param name="keyValues">The key values.</param>
        /// <param name="itemCreateInfo">The item create info.</param>
        /// <returns></returns>
        public static ListItem CreateAndAddItemToList(this List targetList, Dictionary<string, object> keyValues, ListItemCreationInformation itemCreateInfo = null)
        {

            if (itemCreateInfo == null)
            {
                itemCreateInfo = new ListItemCreationInformation();
            }


            ListItem newListItem = targetList.AddItem(itemCreateInfo);

            foreach (KeyValuePair<string, object> keyValuePair in keyValues)
            {
                //newListItem["Title"] = "New Announcement";
                //newListItem["Body"] = "Hello World!";


                newListItem[keyValuePair.Key] = keyValuePair.Value;
            }

            newListItem.Update();

            return newListItem;

        }

        /// <summary>
        /// Adds the item to list.
        /// </summary>
        /// <param name="clientContext">The client context.</param>
        /// <param name="list">The list.</param>
        /// <param name="keyValues">The key values.</param>
        public static void AddItemToList(ClientContext clientContext, List list, Dictionary<string, object> keyValues)
        {

            //List targetList = site.Lists.GetByTitle("Announcements");

            ListItem newListItem = list.CreateAndAddItemToList(keyValues);

            clientContext.Load(newListItem);

            clientContext.ExecuteQuery();

            //Console.WriteLine("Announcement created! \n\n"
            //   + "ID: " + newListItem.Id + "\nTitle: " + newListItem["Title"]);
        }


        /// <summary>
        /// Uploads the document.
        /// </summary>
        /// <param name="clientContext">The client context.</param>
        /// <param name="documentsList">The documents list.</param>
        /// <param name="documentListName">Name of the document list.</param>
        /// <param name="documentListURL">The document list URL.</param>
        /// <param name="documentName">Name of the document.</param>
        /// <param name="documentStream">The document stream.</param>
        /// <param name="metaData">The meta data.</param>
        /// <param name="fileCreationInformation">The file creation information.</param>
        public static void UploadDocument(
            this ClientContext clientContext,
            List documentsList,
            string documentListName,
            string documentListURL,
            string documentName,
            byte[] documentStream,
            Dictionary<string,object> metaData,
            FileCreationInformation fileCreationInformation = null)
        {


            if (fileCreationInformation == null)
            {
                fileCreationInformation = new FileCreationInformation();
            }

            //Assign to content byte[] i.e. documentStream
            fileCreationInformation.Content = documentStream;


            //Allow owerwrite of document
            fileCreationInformation.Overwrite = true;

            //Upload URL

            fileCreationInformation.Url = clientContext.Site.Url + documentListURL + documentName;

            Microsoft.SharePoint.Client.File uploadFile =
                documentsList.RootFolder.Files.Add(fileCreationInformation);

            //Update the metadata for a field having name "DocType"
            if (metaData != null)
            {
                foreach (KeyValuePair<string, object> keyValuePair in metaData)
                {
                    uploadFile.ListItemAllFields[keyValuePair.Key] = keyValuePair.Value;
                }
            }
            uploadFile.ListItemAllFields.Update();

            clientContext.ExecuteQuery();




        }


        /// <summary>
        /// Downloads the document.
        /// </summary>
        /// <param name="siteURL">The site URL.</param>
        /// <param name="documentName">Name of the document.</param>
        /// <returns></returns>
        public static Stream DownloadDocument(string siteURL, string documentName)
        {

            ListItemCollection listItems = GetListItemCollectionFromSP("FileLeafRef",
                                                                       documentName, "Text", 1);


            ListItem item = (listItems != null && listItems.Count == 1) ? listItems[0] : null;


            if (item == null)
            {
                return null;
            }

            using (ClientContext clientContext = new ClientContext(siteURL))
            {
                FileInformation fInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext,
                                                                                          item["FileRef"].ToString());

                return fInfo.Stream;
            }



        }


        private static ListItemCollection GetListItemCollectionFromSP( string name,
   string value, string type, int rowLimit)
        {
            //Update siteURL and DocumentListName with as per your site
            string siteURL = "URL of the Site";
            string documentListName = "DocumentList";
            ListItemCollection listItems = null;

            using (ClientContext clientContext = new ClientContext(siteURL))
            {
                List documentsList = clientContext.Web.Lists.GetByTitle(documentListName);

                CamlQuery camlQuery = new CamlQuery();

                camlQuery.ViewXml =
                    @"<View>
  <Query>
    <Where>
      <Eq>
        <FieldRef Name='" + name +
                    @"'/>
        <Value Type='" +
                    type + "'>" + value + @"</Value>
      </Eq>
    </Where>
    <RowLimit>" + rowLimit +
                    @"</RowLimit>
  </Query>
</View>";

                listItems = documentsList.GetItems(camlQuery);

                clientContext.Load(documentsList);
                clientContext.Load(listItems);

                clientContext.ExecuteQuery();
            }
            return listItems;

        }

        /// <summary>
        /// Sets the values of the given <see cref="ListItem"/>.
        /// </summary>
        /// <param name="listItem">The list item.</param>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        public static void SetValues(this ListItem listItem, Dictionary<string, object> values)
        {
            foreach (KeyValuePair<string, object> keyValuePair in values)
            {
                listItem[keyValuePair.Key] = keyValuePair.Value;
            }
            
        }


        /// <summary>
        /// Uploads the specified context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="listName">Name of the list.</param>
        /// <param name="listfolder">The listfolder.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="data">The data.</param>
        /// <param name="overwrite">if set to <c>true</c> [overwrite].</param>
        public static void Upload(this ClientContext context, string listName, string listfolder, string fileName, byte[] data, bool overwrite = true) 
        {

            //Contrast this with: http://blogs.msdn.com/b/sridhara/archive/2010/03/12/uploading-files-using-client-object-model-in-sharepoint-2010.aspx

            context.AuthenticationMode = ClientAuthenticationMode.Default;
            List list = context.Web.Lists.GetByTitle(listName);
            context.Load(list);
            context.Load(list.RootFolder);
            context.ExecuteQuery();

            //Create a site specific relative Url:
            
            string url = Uri.Combine(
                context.Site.Url,
                Uri.Combine(
                  list.RootFolder.ServerRelativeUrl,
                  Uri.Combine(listfolder,fileName)));

            FileCreationInformation fci = new FileCreationInformation();
            fci.Content = data;
            fci.Overwrite = overwrite;
            fci.Url = url;
            Microsoft.SharePoint.Client.File file = list.RootFolder.Files.Add(fci);
            context.ExecuteQuery();
        }


        /// <summary>
        /// Uploads the document.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <param name="authtype">The authtype.</param>
        /// <param name="listname">The listname.</param>
        /// <param name="listfolder">The listfolder.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="data">The data.</param>
        /// <param name="overwrite">if set to <c>true</c> [overwrite].</param>
        /// <returns></returns>
        public static string UploadDocument(this ClientContext context, string username, string password, string authtype, string listname, string listfolder, string fileName, byte[] data, bool overwrite=true)
        {
            //Contrast this with: http://blogs.msdn.com/b/sridhara/archive/2010/03/12/uploading-files-using-client-object-model-in-sharepoint-2010.aspx

            context.AuthenticationMode = ClientAuthenticationMode.Default;
            if (string.IsNullOrEmpty(username))
            {

            }
            else
            {
                context.Credentials = BuildCredentials(context.Site.Url, username, password, authtype);
            }

            List list = context.Web.Lists.GetByTitle(listname);

            context.Load(list);
            context.Load(list.RootFolder);
            context.ExecuteQuery();


            string url = Uri.Combine(
                context.Site.Url,
                Uri.Combine(
                    list.RootFolder.ServerRelativeUrl,
                    Uri.Combine(listfolder, fileName)));


            FileCreationInformation fci = new FileCreationInformation();
            fci.Content = data;
            fci.Overwrite = overwrite;
            fci.Url = url;
            Microsoft.SharePoint.Client.File file = list.RootFolder.Files.Add(fci);
            context.ExecuteQuery();
            return url;
        }


        private static ICredentials BuildCredentials(string siteurl, string username, string password, string authtype)
        {
            NetworkCredential cred;
            if (username.Contains(@"\"))
            {
                string domain = username.Substring(0, username.IndexOf(@"\"));
                username = username.Substring(username.IndexOf(@"\") + 1);
                cred = new NetworkCredential(username, password, domain);
            }
            else
            {
                cred = new NetworkCredential(username, password);
            }
            CredentialCache cache = new CredentialCache();
            if (authtype.Contains(":"))
            {
                authtype = authtype.Substring(authtype.IndexOf(":") + 1); //remove the TMG: prefix
            }
            cache.Add(new System.Uri(siteurl), authtype, cred);
            return cache;
        }
    }
}
