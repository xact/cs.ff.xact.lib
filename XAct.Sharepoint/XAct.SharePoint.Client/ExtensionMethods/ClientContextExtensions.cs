﻿using SP = Microsoft.SharePoint.Client;

namespace XAct
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SP;

    /// <summary>
    /// Extensions to the SharePoint <see cref="SP.ClientContext"/> object.
    /// </summary>
    public static class ClientContextExtensions
    {

        private static void X(this SP.ClientContext clientContext)
        {
            //A Site is a SiteCollection:
            Site site = clientContext.Site;
            //Returns top-level site of a SiteCollection:
            Web webSite = clientContext.Site.RootWeb;
            //Gets the version of the current SharePoint Server.
            Version version = clientContext.ServerVersion;
            //Executes the current set of data retrieval queries and method invocations.
            //clientContext.ExecuteQuery();
            //Gets or sets the login information for the forms authentication mode of the client context.
            //clientContext.FormsAuthenticationLoginInfo;
            //Gets the flag that indicates whether the client context has the ClientRequest to be sent to the server.
            bool check=clientContext.HasPendingRequest;
            //Gets the ClientRequest object that is sent to the server when the next ExecuteQuery is executed.
            ClientRequest clientRequest = clientContext.PendingRequest;
            //Gets the build version of Microsoft.SharePoint.Client.ServerRuntime.dll on the server.
            Version version2 = clientContext.ServerLibraryVersion;
            //Gets the schema version of Microsoft.SharePoint.Client.ServerRuntime.dll on the server.
            Version version3 = clientContext.ServerSchemaVersion;


            //Gets the server-relative URL of the root Web site in the site collection.
            string uri = clientContext.Site.ServerRelativeUrl;

            //clientContext.Site.

        }

        /// <summary>
        /// Gets all the sharepoint <see cref="List"/> objects.
        /// </summary>
        /// <param name="clientContext">The context.</param>
        /// <returns></returns>
        public static IEnumerable<SP.List> GetAllLists(this SP.ClientContext clientContext)
        {
            clientContext.ValidateIsNotDefault("clientContext");

            ListCollection allLists = clientContext.Web.Lists;
            
            clientContext.Load(allLists);
            clientContext.ExecuteQuery();

            return allLists;
        }


        /// <summary>
        /// Gets the tasks lists (only).
        /// </summary>
        /// <param name="clientContext">The context.</param>
        /// <param name="listTemplateType">Type of the list template.</param>
        /// <returns></returns>
        public static IEnumerable<List> GetLists(this ClientContext clientContext, SharePoint.ListTemplateType listTemplateType)
        {


            clientContext.ValidateIsNotDefault("clientContext");
            if (listTemplateType == SharePoint.ListTemplateType.InvalidType)
            {
                throw new ArgumentOutOfRangeException("listTemplateType");
            }

            ListCollection listCollection = clientContext.Web.Lists;

            int x = (int) listTemplateType.MapTo();
            IQueryable<List> resultCollection = listCollection.Where(l => true);


            //IEnumerable<SP.List> resultCollection = clientContext.LoadQuery(
            // listCollection.Include(
            //     list => list.Title,
            //     list => list.Id).Where(
            //         list => list.ItemCount != 0
            //             && list.Hidden != true));

            //IEnumerable<List> resultCollection = 
            //    clientContext
            //      .LoadQuery(query);


            //clientContext.ExecuteQuery();

            clientContext.LoadQuery(resultCollection);
            List<List> y = resultCollection.ToList();
            return resultCollection;

        }

        /// <summary>
        /// Creates the specified List in the current context.
        /// </summary>
        /// <param name="clientContext">The client context.</param>
        /// <param name="listCreationInformation">The list creation information.</param>
        /// <returns></returns>
        public static List CreateList(this ClientContext clientContext, SharePoint.ListCreationInformation listCreationInformation)
        {
            clientContext.ValidateIsNotDefault("clientContext");
            listCreationInformation.ValidateIsNotDefault("listCreationInformation");

            Microsoft.SharePoint.Client.ListCreationInformation newListInfo =
                new Microsoft.SharePoint.Client.ListCreationInformation(); 
            newListInfo.Title = listCreationInformation.Name;
            newListInfo.Description = listCreationInformation.Description;


            //newListInfo.DocumentTemplateType =
            //newListInfo.TemplateType=
            //newListInfo.TemplateFeatureId = null;
            //newListInfo.TypeId = null;
            //newListInfo.Url = null;



            //Map it our custom enum to the MS Enum:
            Microsoft.SharePoint.Client.ListTemplateType listTemplateType2 = listCreationInformation.ListTemplateType.MapTo();


            newListInfo.TemplateType = (int)listTemplateType2; //Although values are the same, don't take a chance of code-creep. 

            List list = clientContext.Web.Lists.Add(newListInfo); 
            
            clientContext.ExecuteQuery(); 

            return list;
        }

        /// <summary>
        /// Gets the specified list.
        /// </summary>
        /// <param name="clientContext">The context.</param>
        /// <param name="listTitle">The list title.</param>
        /// <returns></returns>
        public static List GetList(this ClientContext clientContext, string listTitle)
        {
            clientContext.ValidateIsNotDefault("clientContext");
            listTitle.ValidateIsNotNullOrEmpty("listTitle");

            List list = clientContext.Web.Lists.GetByTitle(listTitle); 
            list.OnQuickLaunch = true; list.Update();

            clientContext.ExecuteQuery();

            return list;
        }

        /// <summary>
        /// Deletes the specified list.
        /// </summary>
        /// <param name="clientContext">The context.</param>
        /// <param name="listTitle">The list title.</param>
        public static void DeleteList(this ClientContext clientContext, string listTitle)
        {
            clientContext.ValidateIsNotDefault("clientContext");
            listTitle.ValidateIsNotNullOrEmpty("listTitle");

            List list = clientContext.Web.Lists.GetByTitle(listTitle);
            list.DeleteObject();
            clientContext.ExecuteQuery();


            //SPWeb mySite = SPContext.Current.Web;
            //SPListCollection lists = mySite.Lists;

            //SPList list = lists[TextBox1.Text];
            //System.Guid listGuid = list.ID;

            //lists.Delete(listGuid);

        }


        /// <summary>
        /// Gets the list template.
        /// </summary>
        /// <param name="clientContext">The client context.</param>
        /// <param name="listTemplateTitle">The list template title.</param>
        public static void GetListTemplate(this ClientContext clientContext, string listTemplateTitle)
        {

            ListTemplateCollection listTemplateCollection = clientContext.Web.ListTemplates;
            //listTemplateCollection.

//            listTemplateCollection[listTemplateTitle];


//            SPListTemplate template = mySite.ListTemplates["Decisions"];
//mySite.Lists.Add("My Decisions", "This is a list of decisions", 
//   template);

//SPWeb mySite = SPContext.Current.Web;

//SPListTemplate template = mySite.ListTemplates["Decisions"];
//mySite.Lists.Add("My Decisions", "This is a list of decisions",
//   template);
        }
    }
}
