﻿namespace XAct
{
    using Microsoft.SharePoint.Client;

    /// <summary>
    /// Extensions to the <c>SiteCollection</c> object 
    /// (<see cref="Microsoft.SharePoint.Client.Site"/>)
    /// </summary>
    /// <remarks>
    /// <para>
    /// A SiteCollection is a Hierarchical collection of websites (<see cref="Microsoft.SharePoint.Client.Web"/>)
    /// </para>
    /// </remarks>
    public static class SiteExtensions
    {
        private static void X(this Site siteCollection)
        {
            //Gets Root WebSite:
            Web web = siteCollection.RootWeb;

            //Specifies the server-relative URL or web-relative URL of the site to open. If strurl is empty, the top-level site must be opened.
            //Web web = siteCollection.OpenWeb();

            string s = siteCollection.ServerRelativeUrl;
            bool b = siteCollection.ShowUrlStructure;

            //Gets or sets a value that specifies whether the Visual Upgrade UI of this site collection is displayed.
            //siteCollection.UIVersionConfigurationEnabled;

            //Gets the full URL to the root Web site of the site collection, including host name, port number, and path.
            //siteCollection.Url;

        }
        
    }
}
