﻿namespace XAct.SharePoint
{
    /// <summary>
    /// A proxy to ensure that other assemblies don't have to
    /// reference Microsoft.Sharepoint.Client.dll
    /// </summary>
    public enum ListTemplateType
    {
        /// <summary>
        /// 
        /// </summary>
        InvalidType = -1,
        /// <summary>
        /// 
        /// </summary>
        NoListTemplate = 0,
        /// <summary>
        /// 
        /// </summary>
        GenericList = 100,
        /// <summary>
        /// 
        /// </summary>
        DocumentLibrary = 101,
        /// <summary>
        /// 
        /// </summary>
        Survey = 102,
        /// <summary>
        /// 
        /// </summary>
        Links = 103,
        /// <summary>
        /// 
        /// </summary>
        Announcements = 104,
        /// <summary>
        /// 
        /// </summary>
        Contacts = 105,
        /// <summary>
        /// 
        /// </summary>
        Events = 106,
        /// <summary>
        /// 
        /// </summary>
        Tasks = 107,
        /// <summary>
        /// 
        /// </summary>
        DiscussionBoard = 108,
        /// <summary>
        /// 
        /// </summary>
        PictureLibrary = 109,
        /// <summary>
        /// 
        /// </summary>
        DataSources = 110,
        /// <summary>
        /// 
        /// </summary>
        WebTemplateCatalog = 111,
        /// <summary>
        /// 
        /// </summary>
        UserInformation = 112,
        /// <summary>
        /// 
        /// </summary>
        WebPartCatalog = 113,
        /// <summary>
        /// 
        /// </summary>
        ListTemplateCatalog = 114,
        /// <summary>
        /// 
        /// </summary>
        XMLForm = 115,
        /// <summary>
        /// 
        /// </summary>
        MasterPageCatalog = 116,
        /// <summary>
        /// 
        /// </summary>
        NoCodeWorkflows = 117,
        /// <summary>
        /// 
        /// </summary>
        WorkflowProcess = 118,
        /// <summary>
        /// 
        /// </summary>
        WebPageLibrary = 119,
        /// <summary>
        /// 
        /// </summary>
        CustomGrid = 120,
        /// <summary>
        /// 
        /// </summary>
        SolutionCatalog = 121,
        /// <summary>
        /// 
        /// </summary>
        NoCodePublic = 122,
        /// <summary>
        /// 
        /// </summary>
        ThemeCatalog = 123,
        /// <summary>
        /// 
        /// </summary>
        DataConnectionLibrary = 130,
        /// <summary>
        /// 
        /// </summary>
        WorkflowHistory = 140,
        /// <summary>
        /// 
        /// </summary>
        GanttTasks = 150,
        /// <summary>
        /// 
        /// </summary>
        Meetings = 200,
        /// <summary>
        /// 
        /// </summary>
        Agenda = 201,
        /// <summary>
        /// 
        /// </summary>
        MeetingUser = 202,
        /// <summary>
        /// 
        /// </summary>
        Decision = 204,
        /// <summary>
        /// 
        /// </summary>
        MeetingObjective = 207,
        /// <summary>
        /// 
        /// </summary>
        TextBox = 210,
        /// <summary>
        /// 
        /// </summary>
        ThingsToBring = 211,
        /// <summary>
        /// 
        /// </summary>
        HomePageLibrary = 212,
        /// <summary>
        /// 
        /// </summary>
        Posts = 301,
        /// <summary>
        /// 
        /// </summary>
        Comments = 302,
        /// <summary>
        /// 
        /// </summary>
        Categories = 303,
        /// <summary>
        /// 
        /// </summary>
        Facility = 402,
        /// <summary>
        /// 
        /// </summary>
        Whereabouts = 403,
        /// <summary>
        /// 
        /// </summary>
        CallTrack = 404,
        /// <summary>
        /// 
        /// </summary>
        Circulation = 405,
        /// <summary>
        /// 
        /// </summary>
        Timecard = 420,
        /// <summary>
        /// 
        /// </summary>
        Holidays = 421,
        /// <summary>
        /// 
        /// </summary>
        IMEDic = 499,
        /// <summary>
        /// 
        /// </summary>
        ExternalList = 600,
        /// <summary>
        /// 
        /// </summary>
        IssueTracking = 1100,
        /// <summary>
        /// 
        /// </summary>
        AdminTasks = 1200,
        /// <summary>
        /// 
        /// </summary>
        HealthRules = 1220,
        /// <summary>
        /// 
        /// </summary>
        HealthReports = 1221,
    }
}
