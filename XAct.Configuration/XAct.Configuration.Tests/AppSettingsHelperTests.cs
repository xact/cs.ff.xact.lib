namespace XAct.Configuration.Tests
{
    using System;
    using NUnit.Framework;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class AppSettingsHelperTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();

        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //Singleton<IocContext>.Instance.ResetIoC();
        }

        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void AppSetingsHelperContainsMethod()
        {

            Assert.IsNotNull(XAct.Configuration.AppSettingsHelper.Contains("KeyA"));
        }

        [Test]
        public void AppSetingsHelperContainsMethodCaseInsensitive()
        {

            Assert.IsNotNull(XAct.Configuration.AppSettingsHelper.Contains("keya"));
        }
    }
}


