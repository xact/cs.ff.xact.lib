﻿namespace XAct.Configuration
{
    using System.Configuration;
    using System.Linq;

    /// <summary>
    /// A Terrible class -- AppSettings should be avoided like the plague...
    /// </summary>
    public static class AppSettingsHelper
    {
        /// <summary>
        /// Determines whether AppSettings contains the specified key.
        /// <para>
        /// Note: watch out as the operation is not optimized to cache the keys
        /// so each invocation of the method will iterate through all the items...
        /// </para>
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        /// <internal><para>5/16/2011: Sky</para></internal>
        public static bool Contains(string key)
        {
            //Values read from the appSettings element of the Web.config file 
            //are always of type String. If the specified key does not exist 
            //in the Web.config file, no error occurs. Instead, an empty string is returned.

            return ConfigurationManager.AppSettings.AllKeys.Contains(key);
        }

        ///// <summary> 
        ///// Checks of the specified key (setting) exists 
        ///// </summary> 
        ///// <param name="key">setting to check</param> 
        ///// <returns>true if exists, false if not</returns> 
        //public static bool HasConfigSetting(string key)
        //{
        //    return (new ArrayList(ConfigurationManager.AppSettings.AllKeys).Contains(key));
        //}
    }
}