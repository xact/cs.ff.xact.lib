namespace XAct.XLib.Configuration
{
    using System.Configuration;

    ///<summary>
    ///</summary>
    ///<remarks>
    ///  <para>
    ///    This is a base class that one enherits from. An example is shown below:
    ///    <code>
    ///      <![CDATA[
    /// public class ProviderConfigurationSection : XAct.Configuration.BaseConfigurationSection {
    ///		private readonly static ConfigurationProperty _providers = new ConfigurationProperty("providers", typeof(ProviderSettingsCollection), null, ConfigurationPropertyOptions.None);
    /// 
    ///		//Provide a DefaultProvider 
    ///		[ConfigurationProperty("defaultProvider", DefaultValue="SqliSightProvider"), StringValidator(MinLength=1)]
    ///		public override string DefaultProvider {
    ///			get { return ((string)base["defaultProvider"]); }
    ///			set { base["defaultProvider"] = value; }
    ///		}
    /// 
    ///		//Make a quick wrapper, using this Config Section's name, in order
    ///		//to make installation drop-dead easy:
    ///		public static bool EnsureConfigSection(string userName, string domain, string password) {
    ///			return EnsureConfigSection("ProviderConfigurationSection", userName, domain, password);
    ///		}
    ///	}
    /// ]]>
    ///    </code>
    ///  </para>
    ///  <para>
    ///    As with all ConfigurationSections, it must be registered first:
    ///    <code>
    ///      <![CDATA[
    /// <configSections>
    ///		<sectionGroup name="system.web">
    ///			<section name="ProviderConfigurationSection" type="XAct.IS.ProviderConfigurationSection, App_Code" />
    ///		</sectionGroup>
    /// </configSections>
    /// ]]>
    ///    </code>
    ///    Notice that the name is of your choosing, but the type must match the class that you create to enherit
    ///    from this base class.
    /// </para>
    ///</remarks>
    public class ConfigurationSectionBase : ConfigurationSection, IHasEnabled
    {
        //These define the sub properties of the xml node - as strings, or as groups of nodes:

        private readonly ConfigurationPropertyCollection _csProperties = new ConfigurationPropertyCollection();

        private readonly ConfigurationProperty _csSettings = new ConfigurationProperty("settings",
                                                                                       typeof (
                                                                                           ProviderSettingsCollection),
                                                                                       null);


        /// <summary>
        ///   Constructor
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     <![CDATA[
        /// <sectionName enabled="true" defaultProvider="">
        ///     <providers>
        ///         <add name="" type="" />
        ///         <add name="" type="" />
        ///         <add name="" type="" />
        ///     </providers>
        /// </sectionName>
        /// ]]>
        ///   </para>
        /// </remarks>
        public ConfigurationSectionBase()
        {
            _csProperties.Add(_csEnabled);
            _csProperties.Add(_csDefaultProvider);
            _csProperties.Add(_csProviders);
            _csProperties.Add(_csSettings);

            //Check to see that only one is enabled:
        }

        #region Public Properties

        private readonly ConfigurationProperty _csDefaultProvider = new ConfigurationProperty("defaultProvider",
                                                                                              typeof (string), null);

        private readonly ConfigurationProperty _csEnabled = new ConfigurationProperty("enabled", typeof (bool), true);

        private readonly ConfigurationProperty _csProviders = new ConfigurationProperty("providers",
                                                                                        typeof (
                                                                                            ProviderSettingsCollection),
                                                                                        null,
                                                                                        ConfigurationPropertyOptions.
                                                                                            None);

        /// <summary>
        ///   Get/Set whether this ConfigurationSection is enabled.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     This is a quick way to turn off an installed provider without deleting any configuration
        ///     that may be needed again later.
        ///   </para>
        ///   <para>
        ///     Important: the attribute is case-sensitive: 'enabled'.
        ///   </para>
        ///   <para>
        ///     Attribute not required - default value is 'True'.
        ///   </para>
        ///   <para>
        ///     See Constructor for examples of config file entry.
        ///   </para>
        /// </remarks>
        [ConfigurationProperty("enabled", DefaultValue = true)]
        public bool Enabled
        {
            get { return ((bool) base[_csEnabled]); }
            set { base[_csEnabled] = value; }
        }


        /// <summary>
        ///   Get/Set the name of the Default Provider to use from the list of 'providers' given. See Remarks.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     If no value given, will take the first one of the providers listed.
        ///   </para>
        ///   <para>
        ///     Important: the attribute is case-sensitive: 'defaultProvider'.
        ///   </para>
        ///   <para>
        ///     See Constructor for examples of config file entry.
        ///   </para>
        /// </remarks>
        [
            ConfigurationProperty("defaultProvider", DefaultValue = ""),
            StringValidator(MinLength = 1, InvalidCharacters = "#@"),
        ]
        public virtual string DefaultProvider
        {
            get { return ((string) base[_csDefaultProvider]); }
            set { base[_csDefaultProvider] = value; }
        }


        /// <summary>
        ///   Get a ProviderSettingsCollection of Providers, one of which must be marked as the default provider with the 
        /// defaultProvider property.
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     A provider configuartion always needs to offer atleast one providers, one of which is marked as the one to use.
        ///   </para>
        ///   <para>
        ///     See Constructor for examples of config file entry.
        ///   </para>
        /// </remarks>
        [ConfigurationProperty("providers")]
        public ProviderSettingsCollection Providers
        {
            get { return (ProviderSettingsCollection) base[_csProviders]; }
        }

        /// <summary>
        ///   Get a ProviderSettingsCollection of Settings (see Remarks).
        /// </summary>
        /// <remarks>
        ///   <para>
        ///     See Constructor for examples of config file entry.
        ///   </para>
        /// </remarks>
        [ConfigurationProperty("settings")]
        public ProviderSettingsCollection Settings
        {
            get { return (ProviderSettingsCollection) base[_csSettings]; }
        }

        /// <summary>
        ///   Get a collection of Properties.
        /// </summary>
        protected override ConfigurationPropertyCollection Properties
        {
            get { return _csProperties; }
        }

        #endregion
    }


}


