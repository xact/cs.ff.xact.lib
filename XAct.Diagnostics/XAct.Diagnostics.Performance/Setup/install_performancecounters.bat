﻿@rem ============================================================
@rem NAME: install_performancecounters.bat
@rem PURPOSE: scan app directory for xml files describing perf
@rem          counters needed by the app, and register them.
@rem ============================================================
@rem Code for this batch was built upon generic template
@rem defined here:
@rem http://skysigal.com/doku.php?id=it:ad:scripting_bat_template
@rem so it has some generic/common switches that may not apply
@rem to this app (UserName/Password, etc.) Just ignore,
@rem and stick to what's described in the about section below.
@rem ============================================================
@if %_echo%!==! echo off
setlocal
@rem ============================================================
set RootPath=%~dp0
@rem ============================================================
:CommonArgs
SET _ArgsVerbose=false
SET _ArgTestMode=false
SET _ArgAuthTypeWithQuote=
SET _ArgUserNameWithQuote=
SET _ArgPasswordWithQuote=
@rem ============================================================
:NextArgument
@rem Skipping first arg (which is name of batch file), we want 
@rem Get First Arg, get first caracter, get first 3 characters ,get rest of flag
@rem ie, "/T:Foobar" gives "/", "/T:" and "FooBar"
set _ArgCurrent=%~1
set _ArgFlagFirst=%_ArgCurrent:~0,1%
set _ArgFlag=%_ArgCurrent:~0,3%
set _ArgValue=%_ArgCurrent:~3%

rem move on to next stage when parsing gets to end of line:
if /I "%_ArgFlag%" == "" goto :PostParsing
rem if there is only a / and nothing else, then end of line:
if /I "%_ArgFlag%" == "~0,3" goto :PostParsing

if /I "%_ArgFlag%" == "/?" goto :SHORTUSAGE

if /I "%_ArgFlag%" == "/V" set _ArgsVerbose=true&goto :ArgumentOK
if /I "%_ArgFlag%" == "/T" set _ArgTestMode=true&goto :ArgumentOK
rem Examples of getting switches with values
if /I "%_ArgFlag%" == "/A:" set _ArgAuthTypeWithQuote="%_ArgValue%"&goto :ArgumentOK
if /I "%_ArgFlag%" == "/U:" set _ArgUserNameWithQuote="%_ArgValue%"&goto :ArgumentOK
if /I "%_ArgFlag%" == "/P:" set _ArgPasswordWithQuote="%_ArgValue%"&echo.ARG:Password Set.&goto :ArgumentOK

:ArgumentOK
@rem shift to next argument
shift
@rem repeat
goto :NextArgument

:PostParsing
@rem After parsing we do some cleanup and checking, before moving on to :therealdeal
@rem where the app uses the vars to do whatever it has to.


:RemoveQuotes
@rem [TODO] Remove quotes if you have to
set _ArgUserNameWithoutQuote=%_ArgUserNameWithQuote:"=%
set _ArgPasswordWithoutQuote=%_ArgPasswordWithQuote:"=%

echo ============================================================
:checkenvironment
echo Batch File Environment:
echo File: %0
echo Directory: %RootPath%
echo UserName: %userdomain%\%username%
echo ComputerName: %computername%
@rem ============================================================
@rem PRECHECK: is user is a Member has Admin Rights
@rem Note that this not the same check that user has started in Admin rights.
:checkisuseradmin
set admin=N
set domain=%USERDOMAIN%\
if /i "%domain%" EQU "%computername%\" set domain=
set user=%domain%%username%
for /f "Tokens=*" %%a in ('net localgroup administrators^|find /i "%user%"') do set admin=Y
@rem Then just test the value of %admin%
echo User is Local Admin:%admin%
if "%admin%" EQU "N" (
    echo ============================================================
    echo.
    echo ERROR: User must be a member of Local Admin
    echo.
    goto :USAGE
)
@rem ============================================================
@rem PRECHECK: is user has started in admin mode
:checkisconsoleadmin
NET SESSION >nul 2>&1
IF %ERRORLEVEL% NEQ 0 (
    echo ============================================================
    echo.
    echo ERROR: Batch must be started from Console in Admin Mode.
    echo.
    goto :USAGE
)
echo Batch being run in Admin Console: Y
@rem ============================================================
:therealdeal
echo ============================================================
@rem ...Script guts go here.
goto :end
set DefaultAssemblyName=XAct.Diagnostics.Performance.dll
if /I "%A%" == "" set A=%DefaultAssemblyName%
echo Invoking `installutil /i %A%` ...

@rem installutil /i %A%

:shortUSAGE
echo ============================================================
@rem commented out so that it goes directly to INSTRUCTIONS
@rem as both will do in this case.
@rem [TODO] goto :end

:USAGE
echo ============================================================
SET readmefile="%RootPath%readme.txt"
if not exist %readmefile% (
echo # INSTRUCTIONS
echo # ------------------------------------------------------------
echo #
echo # * Run from within a CLI [opened in Admin mode!].
echo # * Relies on finding installutil.exe, within .NET 
echo #   (eg: %windir%\Microsoft.NET\Framework64\v4.0.30319)
echo #   so there's a small chance you'll have to modify your 
echo #   machine's PATH Environment to include the .NET directory
echo #   so that it can be found from within directory, including:
echo # * Move this to, and run from within, the the AppHost's 
echo #   assembly bin dir (eg: 'App.AppHost.Web\Bin\')
echo #
echo # Syntax:
echo # * default usage:
echo #   install_performancecounters.bat
echo # * defining the assembly (should never need this):
echo #   install_performancecounters.bat /A XAct.Diagnostics.Performance.dll
goto :end
) 
echo Opening File
start notepad "%RootPath%readme.txt"
)
goto :end

:end
echo ============================================================
goto :eof