﻿
namespace XAct
{
    using XAct.Diagnostics.Performance.Services.Entities;

    /// <summary>
    /// Extensions to <see cref="System.Diagnostics.PerformanceCounterType "/>
    /// </summary>
    public static class PerformanceCounterTypeExtensions
    {

        /// <summary>
        /// Maps to.
        /// </summary>
        /// <param name="systemPerformanceCounterType">Type of the system performance counter.</param>
        /// <returns></returns>
        public static XAct.Diagnostics.Performance.Services.Entities.PerformanceCounterType MapTo(
            this System.Diagnostics.PerformanceCounterType systemPerformanceCounterType)
        {
            return (XAct.Diagnostics.Performance.Services.Entities.PerformanceCounterType) (int) systemPerformanceCounterType;
        }
    }
}
