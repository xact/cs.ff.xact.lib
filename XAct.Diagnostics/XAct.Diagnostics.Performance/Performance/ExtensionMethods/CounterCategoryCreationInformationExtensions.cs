﻿namespace XAct
{
    using System.Diagnostics;
    using XAct.Diagnostics.Performance.Services.Entities;

    /// <summary>
    /// 
    /// </summary>
    public static class CounterCategoryCreationInformationExtensions
    {

        /// <summary>
        /// Ensures the Windows System Performance Counter category exists,
        /// as well as an instance of its nested Performance Counters.
        /// </summary>
        /// <param name="counterCategoryCreationInfo">The counter category creation information.</param>
        /// <param name="performanceCounterCreationInformations">The performance counter creation informations.</param>
        /// <param name="forceRebuild">if set to <c>true</c> [force rebuild].</param>
        public static void EnsureCounterCategoryExists(
            this PerformanceCounterCategoryCreationInformation counterCategoryCreationInfo,
            PerformanceCounterCreationInformation[] performanceCounterCreationInformations,
            bool forceRebuild)
        {
            //Use the static system method to see if category exists:
            bool categoryAlreadyExists = PerformanceCounterCategory.Exists(counterCategoryCreationInfo.Name);

            if (categoryAlreadyExists)
            {
                //As category already exists, we don't really want to delete any Counters.
                //But one must if there are not enough.
                //And as far as I know, there is no way to Add a counter to a Category that already exists.

                //So loop through PerformanceCounters:
                //Each one can contain one or more raw system counters that need to be registered:
                foreach (
                    PerformanceCounterCreationInformation counterCreationInformation in
                        performanceCounterCreationInformations)
                {
                    if (PerformanceCounterCategory.CounterExists(counterCreationInformation.Name,
                                                                 counterCategoryCreationInfo.Name))
                    {
                        continue;
                    }

                    //Guess it didn't exist...so we have to create .
                    forceRebuild = true;
                    break;
                }
            }


            if (categoryAlreadyExists && !forceRebuild)
            {
                //If the category exists, and it wasn't forced (and we didn't find any missing Counters)
                //get out early:
                return;
            }

            if (categoryAlreadyExists)
            {
                //But there are not enough counters.

                //So only way to add new Counters to a PerformanceCounter Category
                //is to delete existing one if it already existed:
                PerformanceCounterCategory.Delete(counterCategoryCreationInfo.Name);
            }


            //Make New Category
            CounterCreationDataCollection counterCreationDataCollection = new CounterCreationDataCollection();


            //For each PerformanceCounter, there could be one or two system counters to create.
            //Loop through all that, making a list of what to create
            foreach (
                PerformanceCounterCreationInformation counterCreationInformation in
                    performanceCounterCreationInformations)
            {
                //...while mapping:
                counterCreationDataCollection.Add(counterCreationInformation.Map());
            }

            //We have everything we need to create it:
            PerformanceCounterCategory.Create(
                counterCategoryCreationInfo.Name,
                counterCategoryCreationInfo.Description,
                PerformanceCounterCategoryType.SingleInstance,
                counterCreationDataCollection);

            //Done.

            System.Diagnostics.PerformanceCounter.CloseSharedResources();

        }



        //public static PerformanceCounterCategory Map(  PerformanceCounterCreationInformation performanceCounterCreationInformation)
        //{
        //    //Make New Category
        //    CounterCreationDataCollection counterCreationDataCollection = new CounterCreationDataCollection();


        //    //For each PerformanceCounter, there could be one or two system counters to create.
        //    //Loop through all that, making a list of what to create
        //    foreach (
        //        PerformanceCounterCreationInformation counterCreationInformation in
        //            performanceCounterCreationInformations)
        //    {
        //        //...while mapping:
        //        counterCreationDataCollection.Add(counterCreationInformation.Map());
        //    }

        //    //We have everything we need to create it:
        //    PerformanceCounterCategory.Create(
        //        counterCategoryCreationInfo.Name,
        //        counterCategoryCreationInfo.Description,
        //        PerformanceCounterCategoryType.SingleInstance,
        //        counterCreationDataCollection);

        //    //Done.


        //}


        /// <summary>
        /// Maps to.
        /// </summary>
        /// <param name="performanceCounterCategoryCreationInformation">The performance counter category creation information.</param>
        /// <returns></returns>
        public static PerformanceCounterInstaller Map(this PerformanceCounterCategoryCreationInformation performanceCounterCategoryCreationInformation)
        {
            // Create an instance of 'PerformanceCounterInstaller'.
            PerformanceCounterInstaller performanceCounterInstaller =
                new PerformanceCounterInstaller();

            // Set the 'CategoryName' for performance counter.
            performanceCounterInstaller.CategoryName = performanceCounterCategoryCreationInformation.Name;
            performanceCounterInstaller.CategoryHelp = performanceCounterCategoryCreationInformation.Description;

            foreach (var x in performanceCounterCategoryCreationInformation.CounterCreationInformation)
            {
                performanceCounterInstaller.Counters.Add(x.Map());
            }

            return performanceCounterInstaller;
        }

    }
}