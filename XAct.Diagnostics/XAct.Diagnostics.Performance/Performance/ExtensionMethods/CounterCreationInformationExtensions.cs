﻿namespace XAct
{
    using System.Diagnostics;
    using XAct.Diagnostics;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Performance.Services.Entities;

    /// <summary>
    /// 
    /// </summary>
    public static class CounterCreationInformationExtensions
    {
        /// <summary>
        /// Maps the specified <see cref="PerformanceCounterCreationInformation"/> to a <see cref="CounterCreationData"/>
        /// </summary>
        /// <param name="counterCreationInformation">The counter creation information.</param>
        /// <returns></returns>
        public static CounterCreationData Map(this PerformanceCounterCreationInformation counterCreationInformation)
        {

            CounterCreationData results = new CounterCreationData
                {
                    CounterName = counterCreationInformation.Name,
                    CounterHelp = counterCreationInformation.Description,
                    CounterType = (System.Diagnostics.PerformanceCounterType) counterCreationInformation.Type
                };

            return results;
        }


        /// <summary>
        /// Creates the counter.
        /// </summary>
        /// <param name="counterCreationInformation">The counter creation information.</param>
        /// <param name="categoryName">Name of the category.</param>
        /// <returns></returns>
        public static ISystemPerformanceCounter CreateCounter(this PerformanceCounterCreationInformation counterCreationInformation, string categoryName)
        {


              SystemPerformanceCounterWrapper performanceCounterWrapper =
                  new SystemPerformanceCounterWrapper(
                      new PerformanceCounter(
                        categoryName,
                        counterCreationInformation.Name,
                        false));

            return performanceCounterWrapper;
        }
    }
}

