﻿namespace XAct.Diagnostics
{
    using XAct.Diagnostics.Performance;

    /// <summary>
    /// A Windows System implementation of the 
    /// <see cref="IPerformanceCounterService"/>
    /// service contract.
    /// </summary>
    public interface IWindowsSystemPerformanceCounterService : IPerformanceCounterService, IHasXActLibService
    {
                
    }
}