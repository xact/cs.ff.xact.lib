﻿namespace XAct.Diagnostics.Performance
{
    using System.Diagnostics;

    /// <summary>
    /// A wrapper for the Windows System PerformanceCounter -- which are only available of Full Framework / Server conditions.
    /// </summary>
    /// <internal>
    /// The default performanceCounter doesn't have an interface.
    /// The only way to give it one, to remain decoupled and portable
    /// to other frameworks, is to wrap it in a wrapper that does
    /// implement the desired interface.
    /// </internal>
    /// <internal><para>5/12/2011: Sky</para></internal>
    public class SystemPerformanceCounterWrapper : ISystemPerformanceCounter
    {
        private readonly PerformanceCounter _systemPerformanceCounter;

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemPerformanceCounterWrapper"/> class.
        /// </summary>
        /// <param name="performanceCounter">The performance counter.</param>
        /// <internal><para>5/12/2011: Sky</para></internal>
        public SystemPerformanceCounterWrapper(PerformanceCounter performanceCounter)
        {
            performanceCounter.ValidateIsNotDefault("performanceCounter");

            _systemPerformanceCounter = performanceCounter;
        }


        /// <summary>
        /// Gets the name of the category.
        /// </summary>
        /// <internal>5/12/2011: Sky</internal>
        public string CategoryName
        {
            get { return _systemPerformanceCounter.CategoryName; }
        }


        /// <summary>
        /// Gets the description of the PerformanceCounter.
        /// </summary>
        /// <internal>5/12/2011: Sky</internal>
        public string CategoryDescription
        {
            get { return _systemPerformanceCounter.CounterHelp; }
        }

        /// <summary>
        /// Gets or sets the raw/uncalculated value.
        /// </summary>
        /// <internal>5/12/2011: Sky</internal>
        public long RawValue
        {
            get { return _systemPerformanceCounter.RawValue; }
            set { _systemPerformanceCounter.RawValue = value; }
        }


        /// <summary>
        /// Gets the computed value of the Counter.
        /// <para>
        /// If the counter has a Base counter backing it,
        /// this method returns the sum/division/whatever of the
        /// counter -- whereas Raw would just give the value
        /// of the primary counter (not what you wanted, really).
        /// </para>
        /// </summary>
        public float ComputedValue()
        {
                return _systemPerformanceCounter.NextValue();
        }

        
        /// <summary>
        /// Updates the <see cref="RawValue" />
        /// by adding or subtracting the given offset.
        /// <para>
        /// If no value is provided, it increments the
        /// <see cref="RawValue" /> by 1.
        /// </para>
        /// </summary>
        /// <param name="offset">The offset.</param>
        public long Offset(long? offset)
        {
            return !offset.HasValue 
                ? _systemPerformanceCounter.Increment() 
                : _systemPerformanceCounter.IncrementBy(offset.Value);
        }


        /// <summary>
        /// Decrements the counter.
        /// </summary>
        /// <returns></returns>
        /// <internal>5/11/2011: Sky</internal>
        /// <internal>5/12/2011: Sky</internal>
        /// <internal><para>5/12/2011: Sky</para></internal>
        public long Decrement(long? offset)
        {
            if (offset.HasValue)
            {
                return _systemPerformanceCounter.Decrement();
            }
            else
            {
                lock (_systemPerformanceCounter)
                {
                    _systemPerformanceCounter.RawValue -= offset.Value;

                    return _systemPerformanceCounter.RawValue;
                }
            }
        }
    }
}