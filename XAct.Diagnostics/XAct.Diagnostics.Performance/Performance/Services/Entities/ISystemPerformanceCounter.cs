﻿namespace XAct.Diagnostics
{
    using XAct.Diagnostics.Performance;

    /// <summary>
    /// Specialization of the <see cref="IPerformanceCounter"/> Contract 
    /// for Windows System PerformanceCounters.
    /// </summary>
    public interface ISystemPerformanceCounter :  ICategorizedPerformanceCounter
    {
        
    }
}