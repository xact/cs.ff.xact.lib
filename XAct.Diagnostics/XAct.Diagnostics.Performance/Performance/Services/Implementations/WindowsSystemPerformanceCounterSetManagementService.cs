﻿namespace XAct.Diagnostics.Performance.Implementations
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using XAct.Diagnostics.Performance.Services.Configuration;
    using XAct.Diagnostics.Performance.Services.Entities;
    using XAct.Diagnostics.Performance.State;
    using XAct.Services;

    /// <summary>
    /// An implementation of the 
    /// <see cref="IWindowsSystemPerformanceCounterService"/>
    /// Contract to initialize and use 
    /// Windows System Performance Counters
    /// <para>
    /// Note: Windows System Performance Counters are not PCL compliant,
    /// and require full admin rights to install...
    /// </para>
    /// </summary>
    [DefaultBindingImplementation(typeof(IPerformanceCounterService), BindingLifetimeType.Undefined, Priority.Normal /*OK: An Override Priority: beats NullPerformanceCounterService*/)]
    public class WindowsSystemPerformanceCounterService : PerformanceCounterServiceBase, IWindowsSystemPerformanceCounterService
    {   
        /// <summary>
        /// Initializes a new instance of the <see cref="WindowsSystemPerformanceCounterService"/> class.
        /// </summary>
        /// <param name="performanceCounterSetManagementServiceConfiguration">The performance counter set management service configuration.</param>
        /// <param name="performanceCounterSetCache">The performance counter set cache.</param>
        public WindowsSystemPerformanceCounterService(
            IPerformanceCounterServiceConfiguration performanceCounterSetManagementServiceConfiguration, 
            IPerformanceCounterServiceState performanceCounterSetCache) : 
            base(performanceCounterSetManagementServiceConfiguration, performanceCounterSetCache)
        {
        }








        /// <summary>
        /// Ensures the category exists as exactly described.
        /// </summary>
        /// <param name="performanceCounterCategoryCreationInformation">The performance counter category creation information.</param>
        /// <param name="performanceCounterCreationInformations">The performance counter creation informations.</param>
        /// <param name="forceRebuildOfCategoryEveryTime">if set to <c>true</c> [force rebuild of category every time].</param>
        /// <returns></returns>
        protected override bool EnsureCategoryExistsAsExactlyDescribed(
            PerformanceCounterCategoryCreationInformation performanceCounterCategoryCreationInformation,
            PerformanceCounterCreationInformation[] performanceCounterCreationInformations,
            bool forceRebuildOfCategoryEveryTime)
        {
             performanceCounterCategoryCreationInformation.EnsureCounterCategoryExists(
                performanceCounterCreationInformations,
                forceRebuildOfCategoryEveryTime
                );

             
            return true;
        }









        /// <summary>
        /// Creates <see cref="IPerformanceCounter" />s,
        /// based on the information within the given
        /// <see cref="PerformanceCounterCreationInformation" />.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="flattenedPerformanceCounterCreationInformations">The flattened performance counter creation informations.</param>
        /// <returns></returns>
        protected override KeyValuePair<string, IPerformanceCounter>[] 
            CreateCounters(string categoryName, PerformanceCounterCreationInformation[] flattenedPerformanceCounterCreationInformations)
        {

            List<KeyValuePair<string, IPerformanceCounter>> results =
                new List<KeyValuePair<string, IPerformanceCounter>>();


            //Iterate through the given Counter Creation info describing individual Counters
            //and create them:
            foreach (
                PerformanceCounterCreationInformation performanceCounterCreationInformation in flattenedPerformanceCounterCreationInformations)
            {

                IPerformanceCounter performanceCounter = CreateCounterInstance(categoryName, performanceCounterCreationInformation.Name, performanceCounterCreationInformation.InstanceName, performanceCounterCreationInformation.ReadOnly);

                results.Add(new KeyValuePair<string, IPerformanceCounter>(performanceCounterCreationInformation.Name, performanceCounter));
            }


            return results.ToArray();
        }


        /// <summary>
        /// Creates the counter.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="counterName">Name of the counter.</param>
        /// <param name="instanceName">Name of the instance.</param>
        /// <param name="readOnly">if set to <c>true</c> [read only].</param>
        /// <returns></returns>
        protected override IPerformanceCounter CreateCounterInstance(string categoryName, string counterName, string instanceName, bool readOnly=false)
        {
            PerformanceCounter performanceCounter;

            performanceCounter = IntPerformanceCounter(categoryName, counterName, readOnly);

            PerformanceCounter.CloseSharedResources();

            SystemPerformanceCounterWrapper performanceCounterWrapper =
                new SystemPerformanceCounterWrapper(
                    performanceCounter);

            return performanceCounterWrapper;

        }

        //[DebuggerNonUserCode]
        //[System.Diagnostics.DebuggerHidden]
        private static PerformanceCounter IntPerformanceCounter(string categoryName, string counterName, bool readOnly)
        {
            PerformanceCounter performanceCounter;
            try
            {
                performanceCounter = InternalCreatePerformanceCounter(categoryName, counterName, readOnly);
                return performanceCounter;
            }
#pragma warning disable 168
            catch (System.InvalidOperationException e)
#pragma warning restore 168
            {
                if (readOnly == false)
                {
                    performanceCounter = InternalCreatePerformanceCounter(categoryName, counterName, true);
                    return performanceCounter;
                }
                else
                {
                    throw;
                }
            }
            catch
            {
                throw;
            }
        }

        //[DebuggerNonUserCode]
        //[System.Diagnostics.DebuggerHidden]
        private static PerformanceCounter InternalCreatePerformanceCounter(string categoryName, string counterName,
                                                                           bool readOnly)
        {
            try
            {
                PerformanceCounter performanceCounter;
                performanceCounter = new PerformanceCounter(
                    categoryName,
                    counterName,
                    readOnly);

                return performanceCounter;
            }
// ReSharper disable RedundantCatchClause
#pragma warning disable 168
            catch (System.Exception e)
#pragma warning restore 168
            {
                throw;
            }
// ReSharper restore RedundantCatchClause
        }
    }
}