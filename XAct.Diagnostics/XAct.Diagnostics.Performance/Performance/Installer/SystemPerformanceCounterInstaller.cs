﻿namespace XAct.Diagnostics.Performance.Installer
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration.Install;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;
    using XAct;
    using XAct.Diagnostics.Performance.Services.Configuration;
    using XAct.Diagnostics.Performance.Services.Configuration.Implementations;
    using XAct.Diagnostics.Performance.Services.Entities;


    /// <summary>
    /// <para>
    /// Invoked from the command line:
    /// <code>
    /// <![CDATA[
    /// installutil /i XAct.Diagnostics.Performance.dll
    /// ]]>
    /// </code>
    /// IMPORTANT: You 
    /// * MUST be Admin (or you get unauth errors)
    /// * SHOULD start from the Dev command line (or it won't find install util)
    /// * Will be looking for a file called "PerformanceCounters.xml"
    /// </para>
    /// </summary>
    /// <internal>
    /// * If yor installer directly inherits from Installer, it works. 
    ///   If inheriting from a base class that inherits from installer, no... won't see it. Don't know why.
    /// </internal>
    [RunInstaller(true)]
    public class SystemPerformanceCounterInstaller : Installer
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemPerformanceCounterInstaller"/> class.
        /// </summary>
        public SystemPerformanceCounterInstaller()
        {

            string currentDirectory = System.IO.Directory.GetCurrentDirectory();

            Console.WriteLine("Current Directory:" + currentDirectory);


            IPerformanceCounterServiceConfiguration performanceCounterServiceConfiguration =
                new PerformanceCounterServiceConfiguration();

            Log("Looking for *.PerformanceCounters.xml Files...");

            List<string> messages = new List<string>();

            performanceCounterServiceConfiguration.ScanForConfigurationFiles(
                System.IO.Directory.GetCurrentDirectory(),
                (x)=>System.IO.Directory.GetFiles(x,"*.PerformanceCounters.xml").ToList(),
                XDocument.Load,
                out messages
                );

            foreach (string s in messages){Log(s);}

#pragma warning disable 162

            Log("*.PerformanceCounters.xml Files parsed. Creating Categories.");

            if (performanceCounterServiceConfiguration.CounterCategoryCreationInformation == null)
            {
                Log("Found no *.PerformanceCounters.xml");
            }
            else
            {

                foreach (
                    PerformanceCounterCategoryCreationInformation performanceCounterCategoryCreationInformation in
                        performanceCounterServiceConfiguration.CounterCategoryCreationInformation)
                {
                    try
                    {

                        System.Diagnostics.PerformanceCounter.CloseSharedResources();
                        if (PerformanceCounterCategory.Exists(performanceCounterCategoryCreationInformation.Name))
                        {
                            PerformanceCounterCategory.Delete(performanceCounterCategoryCreationInformation.Name);
                        }
                        System.Diagnostics.PerformanceCounter.CloseSharedResources();

                        // Create an instance of 'PerformanceCounterInstaller'.
                        PerformanceCounterInstaller performanceCounterInstaller =
                            new PerformanceCounterInstaller();

                        // Set the 'CategoryName' for performance counter.
                        performanceCounterInstaller.CategoryName = performanceCounterCategoryCreationInformation.Name;
                        performanceCounterInstaller.CategoryHelp =
                            performanceCounterCategoryCreationInformation.Description;

                        foreach (PerformanceCounterCreationInformation performanceCounterCreationInformation
                            in performanceCounterCategoryCreationInformation.CounterCreationInformation)
                        {
                            performanceCounterInstaller.Counters.Add(performanceCounterCreationInformation.Map());
                        }

                        Installers.Add(performanceCounterInstaller);
                    }
                    catch
                        (Exception
                            e)
                    {
                        Log("-------------------------------------------");
                        Log("Error occured :" + e.Message);
                    }
                }
            }
#pragma warning restore 162
#pragma warning restore 162
        }

        private void Log(string text)
        {
            if (Context != null)
            {
                Context.LogMessage(text);
            }
            else
            {
                Console.WriteLine(text);
            }
        }


    }

}