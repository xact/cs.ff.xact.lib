﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace XAct.Diagnostics.Performance.Installer
//{
//    using System;
//    using System.ComponentModel;
//    using System.Configuration.Install;
//    using System.Diagnostics;



//    /// <summary>
//    /// 
//    /// </summary>
//    //[RunInstaller(true)]
//    public abstract class SystemPerformanceCounterInstallerBase : Installer
//    {
//        PerformanceCounterCategoryCreationInformation _performanceCounterCategoryCreationInformation;

//        /// <summary>
//        /// Base class for an <see cref="Installer"/>
//        /// of performance counters.
//        /// <para>
//        /// Invoked from the command line:
//        /// <code>
//        /// <![CDATA[
//        /// installutil /i MyAssembly.ContainingAnInstaller.dll
//        /// ]]>
//        /// </code>
//        /// </para>
//        /// </summary>
//        /// <param name="performanceCounterCategoryCreationInformation">The performance counter category creation information.</param>
//        protected SystemPerformanceCounterInstallerBase(
//            PerformanceCounterCategoryCreationInformation performanceCounterCategoryCreationInformation)
//        {
//            _performanceCounterCategoryCreationInformation = performanceCounterCategoryCreationInformation;
//        }


//        /// <summary>
//        /// Initializes a new instance of the <see cref="SystemPerformanceCounterInstallerBase"/> class.
//        /// </summary>
//        protected SystemPerformanceCounterInstallerBase()
//        {
//            try
//            {
//                Installers.Add(_performanceCounterCategoryCreationInformation.Map());
//            }
//            catch (Exception e)
//            {
//                if (this.Context != null)
//                {
//                    this.Context.LogMessage("-------------------------------------------");
//                    this.Context.LogMessage("Error occured :" + e.Message);
//                }
//            }
//        }
//    }
//}
