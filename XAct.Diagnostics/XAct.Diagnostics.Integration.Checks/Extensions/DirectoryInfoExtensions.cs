using System;
using System.IO;

namespace XAct.Diagnostics.Integration.Checks.Deployment
{
    /// <remarks>
    /// Internal, in order to not conflict with XAct.Core
    /// </remarks>
    internal static class DirectoryInfoExtensions
    {
        private const string AccessTestFileName = "_integrationTest.txt";

        public static string GetReadWriteTestFileName(this DirectoryInfo directoryInfo)
        {
            return AccessTestFileName;
        }

        public static void CanReadWrite(this DirectoryInfo directoryInfo, out bool directoryExists,
                                        out ResultStatus canRead, out ResultStatus canWrite)
        {
            directoryExists = true;
            canRead = ResultStatus.Fail;
            canWrite = ResultStatus.Fail;

            //IMPORTANT: Yes...I did try a more formal approach:
            //MoreFormalWayThatDoesntWork();
            //So don't go retrying it...
            //Best description as to why it won't work:
            //http://bit.ly/qJcBds


            if (!directoryInfo.Exists)
            {
                try
                {
                    directoryInfo.Create();
                }
                catch
                {
                    //If it doesn't exist, and can't be created, 
                    //not much point checking whether one has read/write access to it.
                    directoryExists = false;
                    return;
                }
            }

            FileInfo fileInfo = new FileInfo(Path.Combine(directoryInfo.FullName, AccessTestFileName));
            string fileContents = string.Format(
                "Please Ignore: this is an automated deployment test, performed at {0}", DateTime.Now);

            try
            {
                File.AppendAllText(fileInfo.FullName, fileContents);
                canWrite = ResultStatus.Success;
                //Don't return yet: havn't done read part
            }
            catch
            {
                //Can't write:
                //canWrite = ResultStatus.Fail;
                //Don't return yet: havn't done read part
            }


            if (fileInfo.Exists)
            {
                try
                {
                    if (File.ReadAllText(fileInfo.FullName).Length > 0)
                    {
                        canRead = ResultStatus.Success;
                    }
                }
                catch
                {
                    //Can't read.
                    //Don't return yet: havn't tried to cleanup
                }
                try
                {
                    fileInfo.Delete();
                }
                catch
                {
                    //Bummer
                }
                return;
            }

            //The Folder doesn't have Write Access,
            //so pretty hard to be 100% sure a file will exist:
            FileInfo[] fileNames = directoryInfo.GetFiles();
            //If one doesn't, doesn't mean we don't -- just not a success.
            if (fileNames.Length == 0)
            {
                canRead = ResultStatus.Warn;
                return;
            }

            try
            {
                using (FileStream fileStream = File.OpenRead(fileNames[0].FullName))
                {
                    //Worked!
                    canRead = ResultStatus.Success;
                    return;
                }
            }
            catch
            {
            }


            //void MoreFormalWayThatDoesntWork()
            //{
            //    DirectoryInfo directoryInfo = new DirectoryInfo(@"c:\Windows");
            //    FileIOPermission fileIOPermission =
            //        new FileIOPermission(FileIOPermissionAccess.Write, directoryInfo.FullName);

            //    if (SecurityManager.IsGranted(fileIOPermission))
            //    {
            //        "Granted?".Dump();

            //        try
            //        {
            //            AuthorizationRuleCollection collection =
            //              Directory
            //                  .GetAccessControl(directoryInfo.FullName)
            //                  .GetAccessRules(true, true, typeof(System.Security.Principal.NTAccount));

            //            foreach (FileSystemAccessRule rule in collection)
            //            {
            //                if (rule.AccessControlType == AccessControlType.Allow)
            //                {

            //                    hasWriteAccess = true.Dump();
            //                    break;
            //                }
            //            }
            //        }
            //        catch
            //        {
            //        }

            //    }

            //    if (hasWriteAccess)
            //    {
            //        File.WriteAllText(Path.Combine(directoryInfo.FullName, "test2.txt"), "Test");
            //    }

            //}
        }
    }
}