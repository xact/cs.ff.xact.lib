﻿using System.Diagnostics;
using System.Reflection;

namespace XAct
{
    /// <summary>
    /// Extensions to the Application object
    /// </summary>
    /// <remarks>
    /// Internal, in order to not conflict with XAct.Core
    /// </remarks>
    internal static class ApplicationExtensions
    {

        /// <summary>
        /// Determines whether the assembly was built in Debug mode.
        /// </summary>
        /// <param name="assembly">The assembly.</param>
        /// <returns>
        ///   <c>true</c> if [is assembly debug build] [the specified assembly]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsAssemblyDebugBuild(this Assembly assembly)
        {
            foreach (var attribute in assembly.GetCustomAttributes(false))
            {
                var debuggableAttribute = attribute as DebuggableAttribute;
                // if IgnoreSymbolStoreSequencePoints and DisableOptimizations flags are both present, the assembly was built in debug mode
                if (debuggableAttribute != null)
                {
                    return
                       ((int)debuggableAttribute.DebuggingFlags).BitIsSet((int)
                           DebuggableAttribute.DebuggingModes.IgnoreSymbolStoreSequencePoints) &&
                         ((int)debuggableAttribute.DebuggingFlags).BitIsSet((int)
                             DebuggableAttribute.DebuggingModes.DisableOptimizations);
                }
            }
            return false;
        }
    }
}
