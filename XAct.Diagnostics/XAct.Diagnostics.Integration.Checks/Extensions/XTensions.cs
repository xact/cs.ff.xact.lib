using System;
using System.IO;

namespace XAct.Diagnostics.Integration.Checks.Deployment
{
    public static class XTensions
    {
        private const string AccessTestFileName = "_integrationTest.txt";

        public static void CheckDirectoryAccess(this IIntegrationTestResult testResult, DirectoryInfo directoryInfo,
                                                bool requiersDirectoryExist, bool requiresReadAccess,
                                                bool requiresWriteAccess)
        {
            testResult.Title = string.Format("Check access rights to '{0}' directory.", directoryInfo.Name);

            bool directoryExists;
            ResultStatus canRead;
            ResultStatus canWrite;

            //First, check if directory exists:)
            directoryInfo.CanReadWrite(out directoryExists, out canRead, out canWrite);

            if (!directoryExists)
            {
                testResult.Success = ResultStatus.Fail;
                testResult.ResponseSummary = "Directory did not exist, and was unable to create it.";
                return;
            }


            //Next, test Write (have to do it in in this order, before Write...you'll see why)


            if ((requiresWriteAccess) && (canWrite != ResultStatus.Success))
            {
                testResult.Success = ResultStatus.Fail;
                testResult.ResponseSummary = string.Format("Could not Write to directory '{0}'.", directoryInfo.FullName);
                //Can return early, as no test files were created:
                return;
            }


            if (!requiresReadAccess)
            {
                testResult.Success = ResultStatus.Success;
                testResult.ResponseSummary = "Tests successful.";
                //Cannot get out early until test file is deleted:
            }
            else
            {
                switch (canRead)
                {
                    case ResultStatus.Fail:
                        testResult.Success = ResultStatus.Warn;
                        testResult.ResponseSummary = string.Format("Could not Read from directory '{0}'.",
                                                                   directoryInfo.FullName);
                        testResult.ResponseDetails = "Failed opening a readonly steam to an file in the folder.";
                        break;
                    case ResultStatus.Warn:
                        testResult.Success = ResultStatus.Warn;
                        testResult.ResponseSummary = string.Format("Could not Read from directory '{0}'.",
                                                                   directoryInfo.FullName);
                        testResult.ResponseDetails =
                            "The folder has not files within it that can be checked so test is inconclusive.";
                        break;
                    case ResultStatus.Success:
                        testResult.Success = ResultStatus.Success;
                        testResult.ResponseSummary = "Tests successful.";
                        break;
                    default:
                        break;
                }
            }

            testResult.ResponseDetails +=
                string.Format("{0}The app has the following rights: Read:{1}, Write:{2}",
                              Environment.NewLine, canRead, canWrite);
        }
    }
}