//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using XAct;
//using XAct.Diagnostics.Integration.Checks;
//using XAct.IO;
//using XAct.IO.Implementations;

//public static class IntegrationCheckResultExtensions
//    {

//        private const string AccessTestFileName = "_integrationTest.txt";

//        public static void CheckDirectoryAccess(this IIntegrationCheckResult testResult, IIOService ioService, DirectoryInfo directoryInfo,
//                                                bool requiersDirectoryExist, bool requiresReadAccess,
//                                                bool requiresWriteAccess)
//        {
//            testResult.Title = string.Format("Check access rights to '{0}' directory.", directoryInfo.Name);


//            //First, check if directory exists:)

//            DirectoryAccessibilityCheckResults results =
//            ioService.DirectoryCheckAccessibilityAsync(directoryInfo.FullName,true).WaitAndGetResult();

//            if (!results.Exists)
//            {
//                testResult.Success = ResultStatus.Fail;
//                testResult.ResponseSummary = "Directory did not exist, and was unable to create it.";
//                return;
//            }


//            //Next, test Write (have to do it in in this order, before Write...you'll see why)


//            if ((requiresWriteAccess) && (results.CanWrite != ResultStatus.Success))
//            {
//                testResult.Success = ResultStatus.Fail;
//                testResult.ResponseSummary = string.Format("Could not Write to directory '{0}'.", directoryInfo.FullName);
//                //Can return early, as no test files were created:
//                return;
//            }


//            if (!requiresReadAccess)
//            {
//                testResult.Success = ResultStatus.Success;
//                testResult.ResponseSummary = "Tests successful.";
//                //Cannot get out early until test file is deleted:
//            }
//            else
//            {
//                switch (results.CanRead)
//                {
//                    case ResultStatus.Fail:
//                        testResult.Success = ResultStatus.Warn;
//                        testResult.ResponseSummary = string.Format("Could not Read from directory '{0}'.",
//                                                                   directoryInfo.FullName);
//                        testResult.ResponseDetails = "Failed opening a readonly steam to an file in the folder.";
//                        break;
//                    case ResultStatus.Warn:
//                        testResult.Success = ResultStatus.Warn;
//                        testResult.ResponseSummary = string.Format("Could not Read from directory '{0}'.",
//                                                                   directoryInfo.FullName);
//                        testResult.ResponseDetails =
//                            "The folder has not files within it that can be checked so test is inconclusive.";
//                        break;
//                    case ResultStatus.Success:
//                        testResult.Success = ResultStatus.Success;
//                        testResult.ResponseSummary = "Tests successful.";
//                        break;
//                    default:
//                        break;
//                }
//            }

//            testResult.ResponseDetails +=
//                string.Format("{0}The app has the following rights: Read:{1}, Write:{2}",
//                              Environment.NewLine, results.CanRead, results.CanWrite);
//        }
//        /// <summary>
//        /// Returns the Math.Min of all the  the result status.
//        /// <para>
//        /// Note: if the list is empty, returns <see cref="ResultStatus.Success"/>.
//        /// </para>
//        /// </summary>
//        /// <param name="integrationTestResults">The integration test results.</param>
//        /// <returns></returns>
//        /// <internal><para>7/10/2011: Sky</para></internal>
//        public static ResultStatus MinResultStatus(this IEnumerable<IIntegrationCheckResult> integrationTestResults)
//        {
//            return integrationTestResults.MinResultStatus(ResultStatus.Success);
//        }

//        /// <summary>
//        /// Returns the Math.Min of all the  the result status.
//        /// <para>
//        /// Note: if the list is empty, returns <param name="defaultStatus"/>
//        /// </para>
//        /// </summary>
//        /// <param name="integrationTestResults">The integration test results.</param>
//        /// <param name="defaultStatus">The value to return if the collection is empty.</param>
//        /// <returns></returns>
//        /// <internal><para>7/10/2011: Sky</para></internal>
//        public static ResultStatus MinResultStatus(this IEnumerable<IIntegrationCheckResult> integrationTestResults,
//                                                   ResultStatus defaultStatus)
//        {
//            return (integrationTestResults.Count() > 0) ? integrationTestResults.Min(t => t.Success) : defaultStatus;
//        }
//    }
