﻿using System.Globalization;
using System.Threading;


namespace XAct
{
    internal static class StringExtensions
    {
        
        /// <summary>
        ///   Formats the string using string.Format.
        /// </summary>
        /// <param name = "text">The text.</param>
        /// <param name = "args">The args.</param>
        /// <returns></returns>
        public static string FormatStringInvariantCulture(this string text, params object[] args)
        {
            return string.Format(CultureInfo.InvariantCulture, text, args);
        }

        /// <summary>
        ///   Formats the string using string.Format.
        /// </summary>
        /// <param name = "text">The text.</param>
        /// <param name = "args">The args.</param>
        /// <returns></returns>
        public static string FormatStringUICulture(this string text, params object[] args)
        {
            return string.Format(Thread.CurrentThread.CurrentUICulture, text, args);
        }

    }
}
