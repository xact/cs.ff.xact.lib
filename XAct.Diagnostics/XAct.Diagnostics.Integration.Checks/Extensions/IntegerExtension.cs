﻿using System;
using System.Globalization;

//For extension methods, which are compiler syntatic sugar
//use the root namespace (see notes in Core explaining why).
// ReSharper disable CheckNamespace

namespace XAct
// ReSharper restore CheckNamespace
{
    /// <summary>
    ///   Extension Methods for Integers.
    /// </summary>
    /// <internal>
    ///   FAQ: Why Extensions? 
    ///   See Tips Folder for a Readme.txt on the subject.
    /// </internal>
    internal static class IntegerExtensions
    {
        private static readonly CultureInfo _invariantCulture = CultureInfo.InvariantCulture;

        /// <summary>
        ///   Determines whether the number is larger than the specified number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "minimumNumber">The minimum nmber.</param>
        /// <returns>
        ///   <c>true</c> if [is larger than] [the specified number]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsLargerThan(this int number, int minimumNumber)
        {
            return (number > minimumNumber);
        }

        /// <summary>
        ///   Determines whether the number is larger than or equal to the specified number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "minimumNumber">The minimum nmber.</param>
        /// <returns>
        ///   <c>true</c> if [is larger than or equal to] [the specified number]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsLargerThanOrEqualTo(this int number, int minimumNumber)
        {
            return (number >= minimumNumber);
        }

        /// <summary>
        ///   Validates that the number is larger than or equal to the specified number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "minimumNumber">The minimum number.</param>
        [Obsolete]
        public static void ValidateIsLargerThan(this int number, int minimumNumber)
        {
            if (!number.IsLargerThan(minimumNumber))
            {
                throw new ArgumentException(
                    "::Number is too small. Needs to be larger than {0}".FormatStringUICulture(minimumNumber));
            }
        }

        /// <summary>
        ///   Validates that the number the is larger than or equal to the specified number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "minimumNumber">The minimum number.</param>
        [Obsolete]
        public static void ValidateIsLargerThanOrEqualTo(this int number, int minimumNumber)
        {
            if (!number.IsLargerThanOrEqualTo(minimumNumber))
            {
                throw new ArgumentException(
                    "::Number is too small. Needs to be larger than {0}".FormatStringUICulture(minimumNumber));
            }
        }

        /// <summary>
        ///   Determines whether the number is smaller than the specified number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "maximumNumber">The maximum nmber.</param>
        /// <returns>
        ///   <c>true</c> if [is smaller than] [the specified number]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSmallerThan(this int number, int maximumNumber)
        {
            return (number > maximumNumber);
        }

        /// <summary>
        ///   Determines whether the number is smaller than or equal to the specified reference number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "maximumNumber">The maximum nmber.</param>
        /// <returns>
        ///   <c>true</c> if [is smaller than or equal to] [the specified number]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsSmallerThanOrEqualTo(this int number, int maximumNumber)
        {
            return (number >= maximumNumber);
        }

        /// <summary>
        ///   Validates that the number argument the is smaller than the specified reference number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "maximumNumber">The maximum number.</param>
        public static void ValidateIsSmallerThan(this int number, int maximumNumber)
        {
            if (!number.IsSmallerThan(maximumNumber))
            {
                throw new ArgumentException(
                    "::Number is too small. Needs to be smaller than {0}".FormatStringUICulture(maximumNumber));
            }
        }

        /// <summary>
        ///   Validates that the number argument the is smaller than or equal to the specified reference number.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "referenceNumber">The maximum number.</param>
        public static void ValidateIsSmallerThanOrEqualTo(this int number, int referenceNumber)
        {
            if (!number.IsSmallerThanOrEqualTo(referenceNumber))
            {
                throw new ArgumentException(
                    "::Number is too small. Needs to be smaller than {0}".FormatStringUICulture(referenceNumber));
            }
        }


        /// <summary>
        ///   Converts an integer to a Hex string format.
        /// </summary>
        /// <param name = "number"></param>
        /// <returns></returns>
        public static string ToHexString(this int number)
        {
            try
            {
                return number.ToString("X", _invariantCulture);
            }
            catch (Exception exception)
            {
                throw new ArgumentException("Could not Convert Int to Hex notation: {0}".FormatStringUICulture(number),
                                            exception);
            }
        }


        /// <summary>
        ///   Convert Integer to Hex Str
        /// </summary>
        /// <param name = "number"></param>
        /// <returns></returns>
        public static string ToHexString2(int number)
        {
            try
            {
                return String.Format(_invariantCulture, "{0:X}", number);
            }
            catch (Exception exception)
            {
                throw new ArgumentException("Could not convert Int to Hex: " + number, exception);
            }
        }


        /// <summary>
        ///   Pads Integer
        /// </summary>
        /// <param name = "number"></param>
        /// <param name = "length">The total number of characters needed.</param>
        /// <returns></returns>
        public static string Pad(this int number, int length)
        {
            string tResult = String.Format(_invariantCulture, "{0}", number);
            while (tResult.Length < length)
            {
                tResult = "0" + tResult;
            }
            return tResult;
        }

        /// <summary>
        ///   Returns the given number, ensuring that it within the specified range.
        /// </summary>
        /// <param name = "number">The number.</param>
        /// <param name = "min">The min.</param>
        /// <param name = "max">The max.</param>
        /// <returns></returns>
        public static int MinMax(this int number, int min, int max)
        {
            return Math.Max(Math.Min(number, max), min);
        }

        /// <summary>
        /// Checks that the number fits 
        /// </summary>
        /// <param name="number"></param>
        /// <param name="checkNumber"></param>
        /// <returns></returns>
        public static bool BitIsSet(this int number, int checkNumber)
        {
            return (number & checkNumber) == checkNumber;
        }



        /// <summary>
        /// Bits the is not set.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="flags">The flags.</param>
        /// <returns></returns>
        /// <internal><para>6/11/2011: Sky</para></internal>
        public static bool BitIsNotSet(this int number, int flags)
        {
            return (number & (~flags)) == 0;
        }

        /// <summary>
        /// Verifies the given Flags contains the value.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="flags">The flags.</param>
        /// <returns></returns>
        /// <internal><para>6/11/2011: Sky</para></internal>
        public static int BitSet(this int number, int flags)
        {
            return number | flags;
        }

        /// <summary>
        /// Clears/Removes the Bit.
        /// </summary>
        /// <param name="number">The number.</param>
        /// <param name="flags">The flags.</param>
        /// <returns></returns>
        /// <internal><para>6/11/2011: Sky</para></internal>
        public static int BitClear(this int number, int flags)
        {
            return number & (~flags);
        }
    }
}