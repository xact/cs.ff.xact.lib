//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Configuration;
//    using System.Net;
//    using XAct.Commands;
//    using XAct.Environment;

//    /// <summary>
//    /// Contract for a specialized CommandMessage
//    /// </summary>
//    /// <remarks>
//    /// <para>Pretty much only used for Type resolution by a ServiceLocator.</para>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public interface IConfigAppSettingUriValuesIntegrationCheckCommandMessage : ICommandMessage
//    {

//    }

//    /// <summary>
//    /// Implementation of 
//    /// <see cref="IConfigAppSettingUriValuesIntegrationCheckCommandMessage"/>
//    /// as required by
//    /// <see cref="ConfigAppSettingUriValuesIntegrationCheckCommandMessageHandler"/>
//    /// </summary>
//    /// <remarks>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public class ConfigAppSettingUriValuesIntegrationCheckCommandMessage :
//        IConfigAppSettingUriValuesIntegrationCheckCommandMessage
//    {
//    }

//        public class ConfigAppSettingUriValuesIntegrationCheckCommandMessageHandler :
//            IntegrationCheckCommandMessageHandlerBase<IConfigAppSettingUriValuesIntegrationCheckCommandMessage> 
//    {

//            protected override string Title
//            {
//                get { return "Check Urls defined in AppSettings"; }
//            }


//            public ConfigAppSettingUriValuesIntegrationCheckCommandMessageHandler(ITracingService tracingService, IEnvironmentService environmentService) : base(tracingService, environmentService) { }


//        protected override void PerformIndividualTestsWithinGroup(IIntegrationCheckResultSet testResults)
//        {
//            foreach (string key in ConfigurationManager.AppSettings.AllKeys)
//            {
//                string value = ConfigurationManager.AppSettings[key];
//                Uri uri;
//                if (!Uri.TryCreate(value, UriKind.RelativeOrAbsolute, out uri))
//                {
//                    continue;
//                }

//                IntegrationCheckResult testResult = new IntegrationCheckResult();
//                testResults.Results.Add(testResult);

//                testResult.Title = string.Format("AppSetting.Key = '{0}'", key);

//                // Got an URI, try hitting
//                using (WebClient webClient = new WebClient())
//                {
//                    try
//                    {
//                        webClient.DownloadString(uri);

//                        testResult.ResponseSummary = "Target Uri responded.";
//                        testResult.Success = ResultStatus.Success;
//                        //Don't return -- go on to next url
//                    }
//                    catch (Exception e)
//                    {
//                        testResult.ResponseSummary = string.Format("Could not access '{1}'.{0}{2}",
//                                                                   "<br/>", //Environment.NewLine,
//                                                                   uri,
//                                                                   e.Message);
//                        testResult.ResponseDetails = e.StackTrace;
//                    }
//                }
//            }
//        }


//    }
//}