//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System.IO;
//    using System.Linq;
//    using System.Reflection;
//    using System.Web;
//    using XAct.Commands;
//    using XAct.Environment;

//    /// <summary>
//    /// Contract for a specialized CommandMessage
//    /// </summary>
//    /// <remarks>
//    /// <para>Pretty much only used for Type resolution by a ServiceLocator.</para>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public interface IConfigSetttingsIntegrationCheckComandMessage : ICommandMessage { }


//    /// <summary>
//    /// Implementation of 
//    /// <see cref="IConfigSetttingsIntegrationCheckComandMessage"/>
//    /// as required by
//    /// <see cref="ConfigSetttingsIntegrationCheckComandMessageHandler"/>
//    /// </summary>
//    /// <remarks>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public class ConfigSetttingsIntegrationCheckComandMessage : IConfigSetttingsIntegrationCheckComandMessage
//    {
//    }

//    public class ConfigSetttingsIntegrationCheckComandMessageHandler :
//        IntegrationCheckCommandMessageHandlerBase<IConfigSetttingsIntegrationCheckComandMessage>
//    {

//        protected override string Title
//        {
//            get { return "Check App Config File Settings"; }
//        }

//        public ConfigSetttingsIntegrationCheckComandMessageHandler(ITracingService tracingService, IEnvironmentService environmentService) : base(tracingService, environmentService) { }


//        protected override void PerformIndividualTestsWithinGroup(IIntegrationCheckResultSet integrationTestResultSet)
//        {
//        }

//        private static void CheckWhetherDebugWasLeftOn()
//        {
//        }

//        private string[] DoDebugCheck()
//        {
//            //Ref: http://www.eggheadcafe.com/tutorials/aspnet/cb350027-3869-433d-90c4-d0af320409c7/aspnet-create-a-debug-build-checker-page-for-your-site.aspx

//            //HttpContext.Current.IsDebuggingEnabled;

//            string binPath = HttpContext.Current.Server.MapPath("~/") + @"\bin\";

//            DirectoryInfo di = new DirectoryInfo(binPath);
        
//            FileInfo[] fileInfos = di.GetFiles();

//            return (from fileInfo in fileInfos
//                    where new[] { ".dll", ".exe", ".config", ".manifest" }.Contains(fileInfo.Name.ToLower())
//                    let isDebug = IsAssemblyDebugBuild(Path.Combine(binPath, fileInfo.Name))
//                    where isDebug
//                    select fileInfo.Name).ToArray();
//        }

//        //[DebuggerHidden]
//        private static bool IsAssemblyDebugBuild(string filepath)
//        {
//            Assembly assembly = Assembly.LoadFile(Path.GetFullPath(filepath));
//            return assembly.IsAssemblyDebugBuild();
//        }
//    }
//}