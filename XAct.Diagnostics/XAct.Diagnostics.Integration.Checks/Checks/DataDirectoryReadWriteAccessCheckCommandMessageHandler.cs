//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System.IO;
//    using System.Web;
//    using XAct.Commands;
//    using XAct.Environment;
//    using XAct.IO;

//    /// <summary>
//    /// Contract for a specialized CommandMessage
//    /// </summary>
//    /// <remarks>
//    /// <para>Pretty much only used for Type resolution by a ServiceLocator.</para>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public interface IDataDirectoryReadWriteAccessCheckCommandMessage : ICommandMessage
//    {
//    }

//    /// <summary>
//    /// Implementation of 
//    /// <see cref="IDataDirectoryReadWriteAccessCheckCommandMessage"/>
//    /// as required by
//    /// <see cref="DataDirectoryReadWriteAccessCheckCommandMessageHandler"/>
//    /// </summary>
//    /// <remarks>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public class DataDirectoryReadWriteAccessCheckCommandMessage : IDataDirectoryReadWriteAccessCheckCommandMessage
//    {

//    }

//    public class DataDirectoryReadWriteAccessCheckCommandMessageHandler :
//    IntegrationCheckCommandMessageHandlerBase<IDataDirectoryReadWriteAccessCheckCommandMessage>
//    {
//        private readonly IIOService _ioService;

//        protected override string Title
//        {
//            get { return "Checking App's access rights to '~/Data' Directory"; }
//        }

//        public DataDirectoryReadWriteAccessCheckCommandMessageHandler(ITracingService tracingService, IEnvironmentService environmentService, IIOService ioService) : base(tracingService, environmentService)
//        {
//            _ioService = ioService;
//        }


//        protected override void PerformIndividualTestsWithinGroup(IIntegrationCheckResultSet testResults)
//        {
//            //Add before any crashes can abort:

//            testResults.Results.Add(CheckThatDataDirectoyCanBeAccessed());
//        }

//        private IIntegrationCheckResult CheckThatDataDirectoyCanBeAccessed()
//        {
//            //Try to Write to the directory:
//            string directoryFullPath = Path.GetFullPath(HttpContext.Current.Server.MapPath("~/App_Data"));

//            IIntegrationCheckResult testResult = new IntegrationCheckResult
//                                                    {
//                                                        Title =
//                                                            string.Format(
//                                                                "Checking access rights to 'AppData' directory."),
//                                                    };

//            //The extension method will set the results and message:
//            testResult.CheckDirectoryAccess(_ioService, new DirectoryInfo(directoryFullPath), true, true, true);

//            return testResult;
//        }

//    }
//}