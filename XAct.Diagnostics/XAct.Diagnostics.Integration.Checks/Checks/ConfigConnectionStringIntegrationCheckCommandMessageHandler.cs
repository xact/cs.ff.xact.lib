//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Configuration;
//    using System.Data;
//    using System.Security.Principal;
//    using System.Web;
//    using XAct.Commands;
//    using XAct.Data;
//    using XAct.Environment;

//    /// <summary>
//    /// Contract for a specialized CommandMessage
//    /// </summary>
//    /// <remarks>
//    /// <para>Pretty much only used for Type resolution by a ServiceLocator.</para>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public interface IConfigConnectionStringIntegrationCheckCommandMessage : ICommandMessage { }

//    /// <summary>
//    /// Implementation of 
//    /// <see cref="IConfigConnectionStringIntegrationCheckCommandMessage"/>
//    /// as required by
//    /// <see cref="ConfigConnectionStringIntegrationCheckCommandMessageHandler"/>
//    /// </summary>
//    /// <remarks>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public class ConfigConnectionStringIntegrationCheckCommandMessage :
//        IConfigConnectionStringIntegrationCheckCommandMessage
//    {
//    }

//        public class ConfigConnectionStringIntegrationCheckCommandMessageHandler :
//            IntegrationCheckCommandMessageHandlerBase<IConfigConnectionStringIntegrationCheckCommandMessage> 
//    {

//            protected override string Title
//            {
//                get { return "Checking Config File defined ConnectionStrings"; }
//            }

//            public ConfigConnectionStringIntegrationCheckCommandMessageHandler(ITracingService tracingService, IEnvironmentService environmentService) : base(tracingService, environmentService) { }




//        protected override void PerformIndividualTestsWithinGroup(IIntegrationCheckResultSet testResults)
//        {
//            foreach (ConnectionStringSettings connectionStringSettings in ConfigurationManager.ConnectionStrings)
//            {
//                IntegrationCheckResult testResult = new IntegrationCheckResult();
//                testResults.Results.Add(testResult);

//                testResult.Title = string.Format("ConnectionString '{0}'", connectionStringSettings.Name);


//                try
//                {
//                    using (
//                        IDbConnection connection = DbHelpers.CreateConnection(connectionStringSettings.Name))
//                    {
//                        connection.Close();
//                        testResult.Success = ResultStatus.Success;
//                    }
//                }
//                catch (Exception e)
//                {
//                    if (connectionStringSettings.Name == "LocalSqlServer")
//                    {
//                        testResult.Success = ResultStatus.Warn;
//                        testResult.Description =
//                            "The 'LocalSqlServer' is a built connectionSting that appears in the list unless you <clear/> the ConnectionString. Not sure if it was intended to be there or not, hence just a Warning.";
//                    }
//                    else
//                    {
//                        testResult.Success = ResultStatus.Fail;
//                        testResult.Description = "Could not connect to the Db.";
//                    }
//                    testResult.ResponseSummary = e.Message;

//                    IServiceProvider serviceProvider = HttpContext.Current;
//                    HttpWorkerRequest httpWorkerRequest =
//                        serviceProvider.GetService(typeof (HttpWorkerRequest)) as HttpWorkerRequest;

//                    //Get the identity of the AppPool:
//                    string threadName;
//                    if (httpWorkerRequest == null)
//                    {
//                        threadName = "UNKNOWN";
//                    }
//                    else
//                    {
//                        IntPtr ptrUserToken = httpWorkerRequest.GetUserToken();
//                        WindowsIdentity handlerWinIdentity = new WindowsIdentity(ptrUserToken);
//                        threadName = handlerWinIdentity.Name;
//                    }


//                    testResult.ResponseDetails =
//                        "Thread Identity used was '{1}'{0}ConnectionString:{2}{0}ProviderName:{3}"
//                            .FormatStringCurrentCulture(
//                                base.EnvironmentService.NewLine,
//                                threadName,
//                                connectionStringSettings.ConnectionString,
//                                connectionStringSettings.ProviderName);
//                }
//            }
//        }

//    }
//}