//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System.Configuration;
//    using System.IO;
//    using System.Web;
//    using XAct.Commands;
//    using XAct.Environment;

//    /// <summary>
//    /// Contract for a specialized CommandMessage
//    /// </summary>
//    /// <remarks>
//    /// <para>Pretty much only used for Type resolution by a ServiceLocator.</para>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public interface IConfigAppSettingFileValuesIntegrationCheckCommandMessage : ICommandMessage
//    {

//    }

//    /// <summary>
//    /// Implementation of 
//    /// <see cref="IConfigAppSettingFileValuesIntegrationCheckCommandMessage"/>
//    /// as required by
//    /// <see cref="ConfigAppSettingFileValuesIntegrationCheckCommandMessageHandler"/>
//    /// </summary>
//    /// <remarks>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public class ConfigAppSettingFileValuesIntegrationCheckCommandMessage : IConfigAppSettingFileValuesIntegrationCheckCommandMessage
//    {
//    }




//    public class ConfigAppSettingFileValuesIntegrationCheckCommandMessageHandler :
//        IntegrationCheckCommandMessageHandlerBase<IConfigAppSettingFileValuesIntegrationCheckCommandMessage>
//    {

//        protected override string Title
//        {
//            get { return "Check Files referenced in AppSettings Values"; }
//        }


//        public ConfigAppSettingFileValuesIntegrationCheckCommandMessageHandler(ITracingService tracingService, IEnvironmentService environmentService) : base(tracingService, environmentService) { }


//        #region Private Methods

//        protected override void PerformIndividualTestsWithinGroup(IIntegrationCheckResultSet testResults)
//        {

//            foreach (string appSettingKey in ConfigurationManager.AppSettings.AllKeys)
//            {
//                string value = ConfigurationManager.AppSettings[appSettingKey];

//                string fullPath = value.StartsWith("~/") ? HttpContext.Current.Server.MapPath(value) : value;

//                FileInfo fileInfo;

//                try
//                {
//                    fullPath = Path.GetFullPath(fullPath);
//                    fileInfo = new FileInfo(fullPath);
//                }
//                catch
//                {
//                    //Doesn't seem to be valid in anyway:
//                    continue;
//                }


//                IntegrationCheckResult testResult = new IntegrationCheckResult();
//                //Add before any crashes can abort:
//                testResults.Results.Add(testResult);

//                testResult.Title = string.Format("AppSetting.Key = '{0}'", appSettingKey);
//                testResult.Description = string.Empty;


//                bool dirExists = Directory.Exists(fullPath);
//                bool fileExists = fileInfo.Exists;

//                if ((dirExists) && (fileExists))
//                {
//                    testResult.Success = ResultStatus.Success; //Not fail, as LogFile coud not yet exist.
//                    testResult.ResponseSummary = "File exists.";
//                    return;
//                }

//                testResult.Success = ResultStatus.Warn; //Not fail, as LogFile coud not yet exist.
//                testResult.ResponseSummary = "File does not exist yet.";
//                testResult.ResponseDetails =
//                    string.Format("File:{1}{0}Dir Exists:{2}{0}FileExists:{3}",
//                                  "<br/>" //Environment.NewLine
//                                  , fullPath
//                                  , dirExists
//                                  , fileExists);
//            }
//        }

//        #endregion


//    }
//}