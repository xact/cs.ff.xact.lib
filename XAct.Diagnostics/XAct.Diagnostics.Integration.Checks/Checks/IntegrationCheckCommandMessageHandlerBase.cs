//namespace XAct.Diagnostics.Integration.Checks
//{
//    using XAct.Commands;
//    using XAct.Environment;

//    public abstract class IntegrationCheckCommandMessageHandlerBase<TCommandMessage> :
//    CommandMessageHandlerBase<TCommandMessage, IIntegrationCheckCommandResult>,
//    IIntegrationCheckCommandHandler<TCommandMessage>
//        where TCommandMessage : ICommandMessage
//    {
//        /// <summary>
//        /// The title of the test
//        /// </summary>
//        protected abstract string Title { get; }

//        protected ITracingService TracingService { get; private set; }
//        protected IEnvironmentService EnvironmentService { get; private set; }


//        protected  IntegrationCheckCommandMessageHandlerBase(ITracingService tracingService,IEnvironmentService environmentService)
//        {
//            TracingService = tracingService;
//            EnvironmentService = environmentService;
//        }

//        public override ICommandValidationResponse ValidateForExecution(TCommandMessage commandMessage)
//        {
//            return new CommandValidationResponse { Success = true };
//        }


//        public override void Execute(TCommandMessage commandMessage)
//        {
//            IntegrationCheckResultSet integrationCheckResultSet =
//                new IntegrationCheckResultSet
//                    {
//                        Title = this.Title,
//                    };

//            PerformIndividualTestsWithinGroup(integrationCheckResultSet);

//            //Set the Result for the whole set to the max found:
//            integrationCheckResultSet.Success = integrationCheckResultSet.MinResultStatus();

//            IntegrationCheckCommandResult commandResult = 
//                new IntegrationCheckCommandResult();

//            commandResult.Result = integrationCheckResultSet;

//            //return commandResult;
//        }




//        protected abstract void PerformIndividualTestsWithinGroup(IIntegrationCheckResultSet testResults);


//    }
//}
