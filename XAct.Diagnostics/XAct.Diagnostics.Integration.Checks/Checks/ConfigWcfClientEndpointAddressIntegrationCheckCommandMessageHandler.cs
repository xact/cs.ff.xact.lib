////using System.Web.Configuration;

//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Net;
//    using System.ServiceModel.Configuration;
//    using System.ServiceModel.Description;
//    using System.Web.Configuration;
//    using XAct.Commands;
//    using XAct.Environment;

//    /// <summary>
//    /// Contract for a specialized CommandMessage
//    /// </summary>
//    /// <remarks>
//    /// <para>Pretty much only used for Type resolution by a ServiceLocator.</para>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public interface IConfigWcfClientEndpointAddressIntegrationCheckCommandMessage : ICommandMessage
//    {
//    }

//    /// <summary>
//    /// Implementation of 
//    /// <see cref="IConfigWcfClientEndpointAddressIntegrationCheckCommandMessage"/>
//    /// as required by
//    /// <see cref="ConfigWcfClientEndpointAddressIntegrationCheckCommandMessageHandler"/>
//    /// </summary>
//    /// <remarks>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public class ConfigWcfClientEndpointAddressIntegrationCheckCommandMessage : IConfigWcfClientEndpointAddressIntegrationCheckCommandMessage
//    {

//    }

//    public class ConfigWcfClientEndpointAddressIntegrationCheckCommandMessageHandler :
//    IntegrationCheckCommandMessageHandlerBase<IConfigWcfClientEndpointAddressIntegrationCheckCommandMessage>
//    {



//        protected override string Title
//        {
//            get { return "Check Client Endpoints defined in Web.Config"; }
//        }


//        public ConfigWcfClientEndpointAddressIntegrationCheckCommandMessageHandler(ITracingService tracingService, IEnvironmentService environmentService) : base(tracingService, environmentService) { }


//        protected override void PerformIndividualTestsWithinGroup(IIntegrationCheckResultSet integrationTestResultSet)
//        {
//            System.Configuration.Configuration configuration =
//                WebConfigurationManager.OpenWebConfiguration("~");
//            //ConfigurationManager.OpenMappedExeConfiguration(
//            //    new ExeConfigurationFileMap {ExeConfigFilename = "web.config"},
//            //    ConfigurationUserLevel.None);

//            ServiceModelSectionGroup serviceModel =
//                ServiceModelSectionGroup.GetSectionGroup(configuration);


//            if (serviceModel == null)
//            {
//                return;
//            }

//            foreach (ChannelEndpointElement endpoint in serviceModel.Client.Endpoints)
//            {
//                IntegrationCheckResult integrationTestResult = new IntegrationCheckResult();
//                integrationTestResultSet.Results.Add(integrationTestResult);
//                integrationTestResult.Title = string.Format(endpoint.Name);


//                using (WebClient webClient = new WebClient())
//                {
//                    //Won't work if client config doesn't mention an endpoint of type MEX contract.
//                    //MetadataExchangeClient mexClient =
//                    //    new MetadataExchangeClient(endpoint.Binding); //, MetadataExchangeClientMode.MetadataExchange
//                    //mexClient.GetMetadata(new EndpointAddress(endpoint.Address.ToString()));
//                    //mexClient.GetMetadata(new EndpointAddress(endpoint.Address.ToString()));


//                    try
//                    {
//                        //string wsdl = webClient.DownloadString(endpoint.Address + "?wsdl");

//                        //XmlDocument xmlDocument = new XmlDocument();
//                        //xmlDocument.LoadXml(wsdl);


//                        MetadataExchangeClient mexClient =
//                            new MetadataExchangeClient(endpoint.Address, MetadataExchangeClientMode.MetadataExchange);

//#pragma warning disable 168
//                        MetadataSet metadata = mexClient.GetMetadata();
//#pragma warning restore 168


//                        integrationTestResult.ResponseSummary = "Target Uri responded (Using WS-Transfer).";
//                        integrationTestResult.Success = ResultStatus.Success;
//                    }
//                    catch (Exception e1)
//                    {
//                        string e1Message = e1.Message;
//                        string e1StackTrace = e1.StackTrace;

//                        try
//                        {
//                            MetadataExchangeClient mexClient =
//                                new MetadataExchangeClient(endpoint.Address, MetadataExchangeClientMode.HttpGet);
//#pragma warning disable 168
//                            MetadataSet metadata = mexClient.GetMetadata();
//#pragma warning restore 168

//                            integrationTestResult.ResponseSummary =
//                                string.Format("Target Uri responded (Using HttpGet for Binding Type {0}).",
//                                              endpoint.Binding);
//                            integrationTestResult.Success = ResultStatus.Success;
//                        }
//                        catch (Exception e2)
//                        {
//                            string e2Message = e2.Message;
//                            string e2StackTrace = e2.StackTrace;

//                            integrationTestResult.Success = ResultStatus.Fail;

//                            integrationTestResult.ResponseSummary =
//                                string.Format("Could not invoke MEX of '{1}' at '{2}'."
//                                // ReSharper disable FormatStringProblem
//                                              , EnvironmentService.NewLine
//                                // ReSharper restore FormatStringProblem
//                                              , endpoint.Name,
//                                              endpoint.Address);

//                            integrationTestResult.ResponseDetails =
//                                string.Format(
//                                    "WS-Transfer attempt:{0}Message:{1}{0}Stack{2}{0}HttpGet attempt:{0}Message:{3}{0}Stack:{4}"
//                                    , EnvironmentService.NewLine
//                                    , e1Message
//                                    , e1StackTrace
//                                    , e2Message
//                                    , e2StackTrace);
//                        } //~catch
//                    } //~catch
//                } //~using
//            } //~foreach
//        }
//    }
//}