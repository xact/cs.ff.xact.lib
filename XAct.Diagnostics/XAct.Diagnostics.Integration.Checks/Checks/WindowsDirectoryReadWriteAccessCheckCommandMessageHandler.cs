//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System.IO;
//    using XAct.Commands;
//    using XAct.Environment;
//    using XAct.IO;

//    /// <summary>
//    /// Contract for a specialized CommandMessage
//    /// </summary>
//    /// <remarks>
//    /// <para>Pretty much only used for Type resolution by a ServiceLocator.</para>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public interface IWindowsDirectoryReadWriteAccessCheckCommandMessage : ICommandMessage
//    {
//    }

//    /// <summary>
//    /// Implementation of 
//    /// <see cref="IWindowsDirectoryReadWriteAccessCheckCommandMessage"/>
//    /// as required by
//    /// <see cref="WindowsDirectoryReadWriteAccessCheckCommandMessageHandler"/>
//    /// </summary>
//    /// <remarks>
//    /// <para>See: <see href="http://bit.ly/vDbTkt"/></para>
//    /// </remarks>
//    public class WindowsDirectoryReadWriteAccessCheckCommandMessage : IWindowsDirectoryReadWriteAccessCheckCommandMessage
//    {

//    }


//        public class WindowsDirectoryReadWriteAccessCheckCommandMessageHandler :
//    IntegrationCheckCommandMessageHandlerBase<IWindowsDirectoryReadWriteAccessCheckCommandMessage>
//        {
//            private readonly IIOService _ioService;

//        protected override string Title
//        {
//            get { return "Check that Application can access Windows Directory"; }
//        }

//        public WindowsDirectoryReadWriteAccessCheckCommandMessageHandler(ITracingService tracingService, IEnvironmentService environmentService, IIOService ioService)
//            : base(tracingService, environmentService)
//        {
//            ioService.ValidateIsNotDefault("ioService");

//            _ioService = ioService;

//        }


//        protected override void PerformIndividualTestsWithinGroup(IIntegrationCheckResultSet testResults)
//        {
//            //Add before any crashes can abort:

//            testResults.Results.Add(CheckThatDataDirectoyCanBeAccessed());
//        }


//            private IIntegrationCheckResult CheckThatDataDirectoyCanBeAccessed()
//        {
//            //Try to Write to the directory:
//            string directoryFullPath = Path.GetFullPath("c:\\Windows");

//            IIntegrationCheckResult testResult = new IntegrationCheckResult
//                                                    {
//                                                        Title =
//                                                            string.Format("Check access rights to 'AppData' directory."),
//                                                        Description = string.Empty,
//                                                        Success = ResultStatus.Fail
//                                                    };

//            //The extension method wil set the Success flags:

//            testResult.CheckDirectoryAccess(_ioService, new DirectoryInfo(directoryFullPath), true, true, true);

//            return testResult;
//        }


//    }
//}