//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System.Collections.Generic;
//    using System.Runtime.Serialization;

//    [DataContract(Name = "IntegrationCheckResultSet", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/diagnostics/deployment/")]
//    [KnownType(typeof (IIntegrationCheckResult))]
//    [KnownType(typeof (IntegrationCheckResult))]
//    public class IntegrationCheckResultSet : IExtensibleDataObject, IIntegrationCheckResultSet
//    {
//        public IntegrationCheckResultSet()
//        {
//            Initialize(new StreamingContext());
//        }

//        #region Implementation of IExtensibleDataObject

//        /// <summary>
//        /// Gets or sets the structure that contains extra data.
//        /// </summary>
//        /// <value></value>
//        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/>
//        ///  that contains data that is not recognized as belonging to the data contract.</returns>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        public ExtensionDataObject ExtensionData { get; set; }

//        #endregion

//        /// <summary>
//        /// The title of the Test.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be something like "AppSettings Uri Tests"
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "Title")]
//        public string Title { get; set; }

//        /// <summary>
//        /// A short (single line or two) description of the test.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be something like "Test that all Uri's defined in AppSettings are reachable."
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "Description")]
//        public string Description { get; set; }

//        /// <summary>
//        /// A boolean flag indicating if the test succeeded in its entirety.
//        /// <para>
//        /// For an IntegrationCheckResultSet, by convention, if any single test fails, an IntegrationCheckResultSet.Success should be set to false.
//        /// </para>
//        /// <para>
//        /// Note: Default value is false.
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "Success", IsRequired = true)]
//        public ResultStatus Success { get; set; }

//        /// <summary>
//        /// A short (single line or two) description of the test results.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be something like "Test that all Uri's defined in AppSettings are reachable."
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "ResponseSummary")]
//        public string ResponseSummary { get; set; }

//        /// <summary>
//        /// Additional info about the the test results.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be generally empty.
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "ResponseDetails")]
//        public string ResponseDetails { get; set; }


//        /// <summary>
//        /// Iterable collection of individual
//        /// <see cref="IIntegrationCheckResult"/>
//        /// instances.
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "Results")]
//        public List<IIntegrationCheckResult> Results
//        {
//            get { return _results; }
//            set { _results = value; }
//        }

//        private List<IIntegrationCheckResult> _results;

//        #region Serialization

//        [OnDeserialized]
//        private void Initialize(StreamingContext streamingContext)
//        {
//            if (_results == null)
//            {
//                _results = new List<IIntegrationCheckResult>();
//            }
//        }

//        #endregion
//    }
//}