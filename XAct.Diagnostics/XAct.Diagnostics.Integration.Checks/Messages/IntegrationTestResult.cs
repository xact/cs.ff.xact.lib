//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System.Runtime.Serialization;

//    [DataContract(Name = "IntegrationCheckResult", Namespace = "http://xact-solutions.com/contracts/v0.1/xact/diagnostics/deployment/")]
//    public class IntegrationCheckResult : IExtensibleDataObject, IIntegrationCheckResult
//    {
//        #region Implementation of IExtensibleDataObject

//        /// <summary>
//        /// Gets or sets the structure that contains extra data.
//        /// </summary>
//        /// <value></value>
//        /// <returns>An <see cref="T:System.Runtime.Serialization.ExtensionDataObject"/>
//        ///  that contains data that is not recognized as belonging to the data contract.</returns>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        public ExtensionDataObject ExtensionData { get; set; }

//        #endregion

//        /// <summary>
//        /// The title of the Test.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be something like "AppSettings Uri Tests"
//        /// </para>
//        /// 	<para>
//        /// For an individual IntegrationCheckResult, an example would be something like "Test of whether {0} is reachable."
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "Title")]
//        public string Title { get; set; }

//        /// <summary>
//        /// A short (single line or two) description of the test.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be something like "Test that all Uri's defined in AppSettings are reachable."
//        /// </para>
//        /// 	<para>
//        /// For an individual IntegrationCheckResult, an example would be something like "A test that the uri returns 200 OK"
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "Description")]
//        public string Description { get; set; }

//        /// <summary>
//        /// A boolean flag indicating if the test succeeded in its entirety.
//        /// <para>
//        /// For an IntegrationCheckResultSet, by convention, if any single test fails, an IntegrationCheckResultSet.Success should be set to false.
//        /// </para>
//        /// 	<para>
//        /// For an individual IntegrationCheckResult, a code indicating whether the result was successful, or not.
//        /// </para>
//        /// <para>
//        /// Note: Default value is false.
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "Success")]
//        public ResultStatus Success { get; set; }

//        /// <summary>
//        /// A short (single line or two) description of the test results.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be something like "Test that all Uri's defined in AppSettings are reachable."
//        /// </para>
//        /// 	<para>
//        /// For an individual IntegrationCheckResult, an example would be something like "The test failed: a 404 was received."
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "ResponseSummary", IsRequired = true)]
//        public string ResponseSummary { get; set; }

//        /// <summary>
//        /// Additional info about the the test results.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be generally empty.
//        /// </para>
//        /// 	<para>
//        /// For a individual IntegrationCheckResult, this is generally empty unless returning an exception stack trace (for example).
//        /// </para>
//        /// </summary>
//        /// <value></value>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        [DataMember(Name = "ResponseDetails")]
//        public string ResponseDetails { get; set; }
//    }
//}