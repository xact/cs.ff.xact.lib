//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System;
//    using System.Diagnostics;
//    using System.Runtime.Serialization.Json;
//    using System.Web;
//    using XAct.Environment;

//    /// <summary>
//    /// An <see cref="IHttpHandler"/>
//    /// for handling IntegrationCheck requests.
//    /// </summary>
//    /// <internal>
//    /// SOP: This is just the HttpHandler that in turn passes the request
//    /// to the <see cref="IIntegrationCheckRunner"/> to do the actual work.
//    /// </internal>
//    public class IntegrationCheckHttpHandler : IHttpHandler
//    {
//        #region Services

//        private readonly IEnvironmentService _environmentService;
//        private readonly ITracingService _tracingService;

//        #endregion

//        #region Constructors

//        /// <summary>
//        /// Initializes a new instance of the <see cref="IntegrationCheckHandler"/> class.
//        /// </summary>
//        /// <internal><para>7/26/2011: Sky</para></internal>
//        public IntegrationCheckHttpHandler() : this(
//            DependencyResolver.Current.GetInstance<IEnvironmentService>(),
//            DependencyResolver.Current.GetInstance<ITracingService>()
//            )
//        {
//        }


//        /// <summary>
//        /// Initializes a new instance of the <see cref="IntegrationCheckHandler"/> class.
//        /// </summary>
//        /// <param name="environmentService">The environment service.</param>
//        /// <param name="tracingService">The tracing service.</param>
//        public IntegrationCheckHttpHandler(IEnvironmentService environmentService, ITracingService tracingService)
//        {
//            _environmentService = environmentService;
//            _tracingService = tracingService;
//        }

//        #endregion

//        /// <summary>
//        /// Gets a value indicating whether another request can use the <see cref="T:System.Web.IHttpHandler"/> instance.
//        /// </summary>
//        /// <returns>true if the <see cref="T:System.Web.IHttpHandler"/> instance is reusable; otherwise, false.
//        ///   </returns>
//        public bool IsReusable
//        {
//            get { return false; }
//        }

//        /// <summary>
//        /// Enables processing of HTTP Web requests.
//        /// </summary>
//        /// <param name="context">
//        /// An <see cref="T:System.Web.HttpContext"/> object that provides references 
//        /// to the intrinsic server objects 
//        /// (for example, Request, Response, Session, and Server) 
//        /// used to service HTTP requests.</param>
//        /// <internal><para>7/9/2011: Sky</para></internal>
//        public void ProcessRequest(HttpContext context)
//        {
//            int testIndex = Convert.ToInt32(context.Request.Params["testIndex"] ?? "0");


//            //Run the tests, getting back the Test Result Set Inteface:
//            IIntegrationCheckRunner integrationCheckRunner =
//                DependencyResolver.Current.GetInstance<IIntegrationCheckRunner>(false)
//                ??
//                new IntegrationCheckRunner(_environmentService,_tracingService);

//            IIntegrationCheckResultSet integrationCheckResults  = 
//                integrationCheckRunner.RunTest(testIndex);


//            //Love interfaces...until they hit serialization, 
//            //where they are not so cool...

//            //Serialization has to be of Concrete types (not Interfaces):
//            DataContractJsonSerializer dataContractJsonSerializer =
//                new DataContractJsonSerializer(typeof (IntegrationCheckResultSet));

//            //So that means that the result has to be typed back from an interface
//            //to a Concrete type:
//            IntegrationCheckResultSet concreteIntegrationCheckResultSet =
//                integrationCheckResults as IntegrationCheckResultSet;

//            Debug.Assert(concreteIntegrationCheckResultSet != null);

//            //Serialize the Concrete types:
//            context.Response.ContentType = "text/json";
//            dataContractJsonSerializer.WriteObject(context.Response.OutputStream, concreteIntegrationCheckResultSet);
//        }
//    }
//}