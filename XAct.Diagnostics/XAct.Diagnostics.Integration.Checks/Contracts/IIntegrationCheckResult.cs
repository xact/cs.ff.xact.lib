//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    //[DataContract(Name="IntegrationCheckResult",Namespace = "http://xact-solutions.com/contracts/v0.1/xact/diagnostics/deployment/")]
//    /// <summary>
//    /// <para>
//    /// <see cref="IIntegrationCheckResult"/> is one array item within
//    /// <see cref="IIntegrationCheckResultSet"/> 
//    /// </para>
//    /// </summary>
//    public interface IIntegrationCheckResult
//    {
//        /// <summary>
//        /// The title of the Test.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be something like "AppSettings Uri Tests"
//        /// </para>
//        /// <para>
//        /// For an individual IntegrationCheckResult, an example would be something like "Test of whether {0} is reachable."
//        /// </para>
//        /// </summary>
//        string Title { get; set; }

//        /// <summary>
//        /// A short (single line or two) description of the test.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be something like "Test that all Uri's defined in AppSettings are reachable."
//        /// </para>
//        /// <para>
//        /// For an individual IntegrationCheckResult, an example would be something like "A test that the uri returns 200 OK"
//        /// </para>
//        /// </summary>
//        string Description { get; set; }

//        /// <summary>
//        /// A boolean flag indicating if the test succeeded in its entirety.
//        /// <para>
//        /// For an IntegrationCheckResultSet, by convention, if any single test fails, an IntegrationCheckResultSet.Success should be set to false.
//        /// </para>
//        /// <para>
//        /// For an individual IntegrationCheckResult, a code indicating whether the result was successful, or not.
//        /// </para>
//        /// </summary>
//        ResultStatus Success { get; set; }

//        /// <summary>
//        /// A short (single line or two) description of the test results.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be something like "Test that all Uri's defined in AppSettings are reachable."
//        /// </para>
//        /// <para>
//        /// For an individual IntegrationCheckResult, an example would be something like "The test failed: a 404 was received."
//        /// </para>
//        /// </summary>
//        string ResponseSummary { get; set; }

//        /// <summary>
//        /// Additional info about the the test results.
//        /// <para>
//        /// For an IntegrationCheckResultSet, an example would be generally empty.
//        /// </para>
//        /// <para>
//        /// For a individual IntegrationCheckResult, this is generally empty unless returning an exception stack trace (for example).
//        /// </para>
//        /// </summary>
//        string ResponseDetails { get; set; }
//    }
//}