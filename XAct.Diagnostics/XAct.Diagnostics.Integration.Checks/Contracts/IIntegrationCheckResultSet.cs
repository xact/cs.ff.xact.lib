//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using System.Collections.Generic;

//    //[DataContract(Name="IntegrationCheckResultSet",Namespace = "http://xact-solutions.com/contracts/v0.1/xact/diagnostics/deployment/")]
//    /// <summary>
//    /// The contract for a set of tests run by invoking the <see cref="IIntegrationCheckRunner"/>.
//    /// <para>
//    /// An <see cref="IIntegrationCheckResultSet"/> contains a subarray of <see cref="IIntegrationCheckResult"/>
//    /// </para>
//    /// </summary>
//    public interface IIntegrationCheckResultSet : IIntegrationCheckResult
//    {

//        /// <summary>
//        /// Iterable collection of individual 
//        /// <see cref="IIntegrationCheckResult"/>
//        /// instances.
//        /// </summary>
//        List<IIntegrationCheckResult> Results { get; set; }
//    }
//}