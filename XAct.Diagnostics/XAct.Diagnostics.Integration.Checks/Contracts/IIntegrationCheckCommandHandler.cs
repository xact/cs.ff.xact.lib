
//// ReSharper disable CheckNamespace

//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    using XAct.Commands;

//    //[DataContract(Name="IntegrationCheck",Namespace = "http://xact-solutions.com/contracts/v0.1/xact/diagnostics/deployment/")]
//    /// <summary>
//    /// 
//    /// </summary>
//    /// <remarks>
//    /// A <see cref="IIntegrationCheckRunner"/>
//    /// invokes the Execute command of a 
//    /// <see cref="IIntegrationCheckCommandHandler"/>
//    /// which returns
//    /// a <see cref="IIntegrationCheckCommandResult"/>
//    /// which contains a payload of 
//    /// a <see cref="IIntegrationCheckResultSet"/>,
//    /// which contains an array of one or more
//    /// <see cref="IIntegrationCheckResult"/>
//    /// items.
//    /// </remarks>
//    public interface IIntegrationCheckCommandHandler<TCommandMessage> :
//        ICommandMessageHandler<TCommandMessage, IIntegrationCheckCommandResult>
//        where TCommandMessage : ICommandMessage
//    {
//    }
//}