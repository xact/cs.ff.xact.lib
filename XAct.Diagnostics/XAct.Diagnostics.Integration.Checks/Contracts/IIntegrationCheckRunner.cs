//// ReSharper disable CheckNamespace
//namespace XAct.Diagnostics.Integration.Checks
//// ReSharper restore CheckNamespace
//{
//    /// <summary>
//    /// Contract for an IntegrationCheck harness.
//    /// </summary>
//    /// <internal>
//    /// Intended to be invoked by a *.ashx,
//    /// separating the test running from the rendering (*.ashx)
//    /// as JSON. 
//    /// </internal>
//    /// <internal><para>7/10/2011: Sky</para></internal>
//    public interface IIntegrationCheckRunner
//    {
//        /// <summary>
//        /// Runs the specified test.
//        /// </summary>
//        /// <param name="testIndex">Index of the test.</param>
//        /// <returns></returns>
//        /// <internal><para>7/10/2011: Sky</para></internal>
//        IIntegrationCheckResultSet RunTest(int testIndex);
//    }
//}