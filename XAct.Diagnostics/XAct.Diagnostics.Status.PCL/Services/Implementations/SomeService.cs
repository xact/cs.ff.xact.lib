namespace XAct.Diagnostics.Status.Services.Implementations
{
    using XAct.Services;

    [DefaultBindingImplementation(typeof(ISomeService))]
#pragma warning disable 1591
    public class SomeService :ISomeService
#pragma warning restore 1591
    {
#pragma warning disable 1591
        public SomeServiceEntity Foo() { return null; }
#pragma warning restore 1591

    }
}
