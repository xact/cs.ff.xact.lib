﻿using System;
using System.Text;

namespace XAct.Diagnostics.Status.Tests
{
    using NUnit.Framework;
    using XAct.Diagnostics.Status.Connectors.Implementations;
    using XAct.Tests;

    [TestFixture]
    class FileSizeStatusServiceConnectorTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }




        [Test]
        public void CanPerformFileSizeStatusServiceConnectorGet()
        {

            FileSizeStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<FileSizeStatusServiceConnector>();

            connector.Configuration.Files.Add(new FileSizeStatusServiceConnectorConfigurationItem(
                title:"Some File A",
                filePath:"TestFiles\\Uno.txt",
                warningBytesSize: 40,
                errorBytesSize:60
            ));
            connector.Configuration.Files.Add(new FileSizeStatusServiceConnectorConfigurationItem(
                title:"Some File B",
                filePath:"TestFiles\\Dos.txt",
                warningBytesSize:80,
                errorBytesSize:100));

            var result = connector.Get();

            Assert.IsNotNull(result);

            string json = result.ToJSON(Encoding.UTF8);


            Console.WriteLine(json);

            Assert.IsNotNull(result);

            //The following should pass, as this test assemblies config file has some appsettings.
            Assert.AreNotEqual("[NULL]", result.Data.Rows[0].Cells[1]);

            Assert.That(result.Data.Rows[0].Cells[0], Is.EqualTo("Some File A"));
            Assert.That(result.Data.Rows[0].Cells[1], Is.EqualTo("42B"));
            Assert.That(result.Data.Rows[0].StatusText, Is.EqualTo("Warn"));

            Assert.That(result.Data.Rows[1].Cells[0], Is.EqualTo("Some File B"));
            Assert.That(result.Data.Rows[1].Cells[1], Is.EqualTo("75B"));
            Assert.That(result.Data.Rows[1].StatusText, Is.EqualTo("Success"));



            Assert.That(result.StatusText, Is.EqualTo("Warn"));
        }


    }
}
