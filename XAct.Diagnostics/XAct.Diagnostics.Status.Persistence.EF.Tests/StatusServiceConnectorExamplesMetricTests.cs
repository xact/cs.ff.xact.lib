namespace XAct.Diagnostics.Tests
{
    using System;
    using System.Threading;
    using NUnit.Framework;
    using XAct;
    using XAct.Diagnostics.Metrics.Services.Configuration;
    using XAct.Diagnostics.Metrics.Services.Configuration.Implementations;
    using XAct.Diagnostics.Performance;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class StatusServiceConnectorExamplesMetricTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();



        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetIPerformanceCounterServiceConfiguration()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>();

            Assert.IsTrue(service != null);

        }

        [Test]
        public void CanAddAConnector()
        {
            IMetricsFeedServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IMetricsFeedServiceConfiguration>();


            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>();

            configuration.IntervalTimeSpan = TimeSpan.FromMilliseconds(100);
            configuration.MetricFeedSourceConnector.Add(new PerformanceCounterMetricsFeedSetConnector(service,"Processor","% Processor Time"));

            Assert.IsTrue(configuration!=null);
        }


        [Test]
        public void CanAddAConnector2()
        {
            IMetricsFeedServiceConfiguration configuration =
                XAct.DependencyResolver.Current.GetInstance<IMetricsFeedServiceConfiguration>();


            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>();


            configuration.MetricFeedSourceConnector.Add(new PerformanceCounterMetricsFeedSetConnector(service, "Processor", "% Idle Time"));

            Thread.Sleep(1000); //should allow for 10 collections.

            //Because it was not registered
            //there's nothing to count here...
            //see other tests in 
            //StatusServiceWithRegisteredConnectorExamplesMetricTests


            Assert.IsTrue(configuration != null);
        }



    }


}


