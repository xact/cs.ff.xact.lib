﻿
namespace XAct.Diagnostics.Status.Tests
{
    using System;
    using System.Text;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Diagnostics.Status.Connectors.Implementations;
    using XAct.Diagnostics.Status.Services.Connectors.Implementations;
    using XAct.Diagnostics.Status.Services.Implementations;
    using XAct.Settings;
    using XAct.Tests;

    [TestFixture]
    public class StatusServiceWithRegisteredConnectorExamplesMetricTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            //Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetStatusService()
        {
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            Assert.IsTrue(service != null);

        }

        [Test]
        public void CanGetStatusServiceOfExpectedType()
        {
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            Assert.AreEqual(typeof (StatusService), service.GetType());
        }


        [Test]
        public void CanGetAppSettingsConnector()
        {

            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            service.Configuration.Clear();

            var connector = XAct.DependencyResolver.Current.GetInstance<AppSettingsStatusServiceConnector>();
            service.Configuration.Register(connector);

            Assert.IsNotNull(connector);
        }

        [Test]
        public void CanPerformPerformanceCounterServiceConnectorGet()
        {
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            service.Configuration.Clear();

            PerformanceCounterStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<PerformanceCounterStatusServiceConnector>();

            string catName = "XActLib.Examples";
            string counterName = "EXAMPLE_OPERATION";

            connector.Configuration.CounterSpecs.Add(new StatusServiceMetricsFeedConnectorConfigurationItem { CounterCategoryName = catName, CounterName = counterName, ReadOnly = false });

            //Now that it's registered,
            //so readings should be obtained.
            service.Configuration.Register(connector);

            Random r = new Random();

            DateTime nowUtc = DateTime.UtcNow;

            var perfService = XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>();
            while (DateTime.UtcNow < nowUtc.AddSeconds(3))
            {
                var v = r.Next(100);
                Console.WriteLine(v);
                perfService.Set(catName, counterName, null, v);
                Thread.Sleep(100);
            }

           Console.WriteLine("{0}={1}".FormatStringInvariantCulture("===>: ", perfService.GetComputedValue<double>(catName, counterName, null)));

            //Wait a bit...
            //Let's see:
            var result = connector.Get();

            Console.WriteLine(result.ToJSON(Encoding.UTF8));

            var r2 = new StatusResponseTableRow();
            r2.Cells.Add("Woohoo");
            r2.Cells.Add("Serializes too...");
            //r2.Status = ResultStatus.Success;
            r2.AdditionalInformation = "foobar....additional info...";
            result.Data.Headers.Add("X");
            result.Data.Headers.Add("Y...");
            result.Data.Rows.Add(r2);
            var result2 = result.TestWCFSerialization();


            Console.WriteLine("Should be the same:");

            Console.WriteLine(result2.ToJSON(Encoding.UTF8));

        }



    }
}

