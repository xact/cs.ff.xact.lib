﻿
namespace XAct.Diagnostics.Status.Tests
{
    using System;
    using System.Data.Entity.Infrastructure;
    using System.Text;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Diagnostics.Status.Connectors.Implementations;
    using XAct.Diagnostics.Status.Services.Connectors.Implementations;
    using XAct.Diagnostics.Status.Services.Implementations;
    using XAct.Settings;
    using XAct.Tests;

    [TestFixture]
    public class StatusServiceWithRegisteredConnectorExamplesTableTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            //Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        [Test]
        public void CanGetStatusService()
        {
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            Assert.IsTrue(service != null);

        }

        [Test]
        public void CanGetStatusServiceOfExpectedType()
        {
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            Assert.AreEqual(typeof (StatusService), service.GetType());
        }


        [Test]
        public void CanGetAppSettingsConnector()
        {

            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            service.Configuration.Clear();

            var connector = XAct.DependencyResolver.Current.GetInstance<AppSettingsStatusServiceConnector>();
            service.Configuration.Register(connector);

            Assert.IsNotNull(connector);
        }

        [Test]
        public void CanPerformAppSettingsConnectorGet()
        {
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            service.Configuration.Clear();

            var connector = XAct.DependencyResolver.Current.GetInstance<AppSettingsStatusServiceConnector>();

            service.Configuration.Register(connector);

            connector.Configuration.AppSettingDefinitions.Clear();
            connector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem ( key:"Foo", title :"Foo" ));
            connector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem ( key:"Bar", title :"Bar" ));
            connector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem ( key:"Bing", title: "Bing" ));

            var result = connector.Get();

            Assert.IsNotNull(result);
            Assert.AreNotEqual("[NULL]", result.Data.Rows[0].Cells[1]);
            Assert.AreNotEqual("[NULL]", result.Data.Rows[1].Cells[1]);
            Assert.AreEqual("[NULL]", result.Data.Rows[2].Cells[1]);

            string json = result.ToJSON(Encoding.UTF8);

            Console.WriteLine(json);
        }



        [Test]
        public void CanPerformDbAccessibilityStatusServiceConnectorGet()
        {
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            service.Configuration.Clear();


            DbAccessibilityStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<DbAccessibilityStatusServiceConnector>();

            service.Configuration.Register(connector);

            connector.Configuration.ConnectionStringName.Clear();
            //<add name="UnitTestDbContext" providerName="System.Data.SqlClient" connectionString="Data Source=localhost;Initial Catalog=XActLib;Integrated Security=SSPI;" />
            //<add name="UnitTestDbContext_CE" providerName="System.Data.SqlServerCe.4.0" connectionString="Data Source=UnitTests_CE.sdf"  />

            connector.Configuration.ConnectionStringName.Add("UnitTestDbContext");
            connector.Configuration.ConnectionStringName.Add("UnitTestDbContext_CE");
            connector.Configuration.ConnectionStringName.Add("OneThatDoesNotExist");

            var result = connector.Get();

            Assert.IsNotNull(result);
            //Not always the case -- depends on whether I have vpn'ed in.Assert.AreEqual("OK", result.Data.Rows[0].Cells[1]);
            Assert.AreEqual("OK", result.Data.Rows[1].Cells[1]);
            Assert.AreNotEqual("OK", result.Data.Rows[2].Cells[1]);


            string json = result.ToJSON(Encoding.UTF8);

            Console.WriteLine(json);
        }




        [Test]
        public void CanPerformDirectoryAccessibilityStatusServiceConnectorGet()
        {
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            service.Configuration.Clear();

            DirectoryAccessibilityStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<DirectoryAccessibilityStatusServiceConnector>();

            service.Configuration.Register(connector);

            connector.Configuration.Directories.Add(new DirectoryAccessibilityStatusServiceConnectorConfigurationItem
                    (
                    title:"TestDir",
                    directoryPath:"TestDir"
                ));
            connector.Configuration.Directories.Add(new DirectoryAccessibilityStatusServiceConnectorConfigurationItem
                (
                    title:"c:\\Windows\\TestDir",
                    directoryPath:"c:\\Windows\\TestDir"
                ));

            var result = connector.Get();

            Assert.IsNotNull(result);

            string json = result.ToJSON(Encoding.UTF8);

            Console.WriteLine(json);
        }

        [Test]
        public void CanPerformUrlAccessibilityStatusServiceConnectorGet()
        {
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            service.Configuration.Clear();

            UrlAccessibilityStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<UrlAccessibilityStatusServiceConnector>();

            service.Configuration.Register(connector);

            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem { Url = new Uri("http://google.com") });
            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem {Url = new Uri("http://stnsi01.moest.govt.nz")});
            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem {Url = new Uri("https://stnsi01.moest.govt.nz")});
            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem {Url = new Uri("http://web.st.nsi.education.org.nz")});
            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem {Url = new Uri("https://web.st.nsi.education.org.nz")});


            var result = connector.Get();

            Assert.IsNotNull(result);

            string json = result.ToJSON(Encoding.UTF8);

            Console.WriteLine(json);
        }




        [Test]
        public void CanPerformCodeFirstMigrationsStatusServiceConnectorGet()
        {
            
            IStatusService service =
                XAct.DependencyResolver.Current.GetInstance<IStatusService>();

            service.Configuration.Clear();

            CodeFirstMigrationsStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<CodeFirstMigrationsStatusServiceConnector>();

            service.Configuration.Register(connector);

            var ce = System.Configuration.ConfigurationManager.ConnectionStrings["UnitTestDbContext_CE"];

            connector.Configuration.Configure<XAct.Bootstrapper.Tests.Migrations.Configuration>(ce.ConnectionString,ce.ProviderName);



            
            
            var result = connector.Get();

            Assert.IsNotNull(result);


            string json = result.ToJSON(Encoding.UTF8);

            Console.WriteLine(json);
        }


    }
}

