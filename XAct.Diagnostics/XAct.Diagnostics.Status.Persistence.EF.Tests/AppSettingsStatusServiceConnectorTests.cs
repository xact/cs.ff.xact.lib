﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XAct.Diagnostics.Status.Tests
{
    using NUnit.Framework;
    using XAct.Diagnostics.Status.Connectors.Implementations;
    using XAct.Settings;
    using XAct.Tests;

    [TestFixture]
    public class AppSettingsStatusServiceConnectorExamplesTableTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            //Using the Db specific one as some of the connectors are db based.
            Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test]
        public void CanGetAppSettingsConnector()
        {

            AppSettingsStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<AppSettingsStatusServiceConnector>();

            Assert.IsNotNull(connector);
        }
        [Test]
        public void CanReadHostSettings()
        {
            IHostSettingsService hostSettingsService =
                XAct.DependencyResolver.Current.GetInstance<IHostSettingsService>();

            var check = hostSettingsService.Get<string>("Foo");
            Console.WriteLine("C1:" + check);
            var check2 = hostSettingsService.Get<string>("Bar");
            Console.WriteLine("C2:" + check2);
        }

        [Test]
        public void CanReadAppSettings()
        {

            var check = System.Configuration.ConfigurationManager.AppSettings.Get("Foo");
            Console.WriteLine("C1:" + check);
            var check2 = System.Configuration.ConfigurationManager.AppSettings.Get("Bar");
            Console.WriteLine("C2:" + check2);

        }


        [Test]
        public void CanPerformAppSettingsConnectorGet()
        {

            AppSettingsStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<AppSettingsStatusServiceConnector>();

            connector.Configuration.AppSettingDefinitions.Clear();
            connector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem (key:"Foo", title :"Foo Title" ));
            connector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem (key:"Bar", title :"Bar Title" ));
            connector.Configuration.AppSettingDefinitions.Add(new AppSettingsStatusServiceConnectorConfigurationItem (key:"Bing",title: "Bing Title"));

            var result = connector.Get();

            string json = result.ToJSON(Encoding.UTF8);

            Console.WriteLine(json);

            Assert.IsNotNull(result);
            //The following should pass, as this test assemblies config file has some appsettings.
            Assert.AreNotEqual("[NULL]", result.Data.Rows[0].Cells[1]);
            Assert.AreNotEqual("[NULL]", result.Data.Rows[1].Cells[1]);
            Assert.AreEqual("[NULL]", result.Data.Rows[2].Cells[1]);

            //The actual values should be:
            Assert.That(result.Data.Rows[0].Cells[1], Is.EqualTo("Yay!"));

            //And the displayed title, should be:
            Assert.That(result.Data.Rows[0].Cells[0],Is.EqualTo("Foo Title"));

        }

    }
}
