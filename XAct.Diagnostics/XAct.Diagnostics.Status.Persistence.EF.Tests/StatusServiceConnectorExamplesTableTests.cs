﻿
namespace XAct.Diagnostics.Status.Tests
{
    using System;
    using System.Text;
    using NUnit.Framework;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Diagnostics.Status.Connectors.Implementations;
    using XAct.Settings;
    using XAct.Tests;

    [TestFixture]
    public class StatusServiceConnectorExamplesTableTests
    {

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            //Using the Db specific one as some of the connectors are db based.
            InitializeUnitTestDbIoCContextAttribute.BeforeTest();

            //Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        [Test]
        public void CanPerformDbAccessibilityStatusServiceConnectorGet()
        {

            DbAccessibilityStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<DbAccessibilityStatusServiceConnector>();

            connector.Configuration.ConnectionStringName.Clear();
            //<add name="UnitTestDbContext" providerName="System.Data.SqlClient" connectionString="Data Source=localhost;Initial Catalog=XActLib;Integrated Security=SSPI;" />
            //<add name="UnitTestDbContext_CE" providerName="System.Data.SqlServerCe.4.0" connectionString="Data Source=UnitTests_CE.sdf"  />

            //We want to check the accessibility of 3 databases, 
            //so we add the following connectionstrings, to match
            //what's in Config/MachineSpecific/app.config of 
            //XAct.Bootstrapper.Tests assembly.
            connector.Configuration.ConnectionStringName.Add("UnitTestDbContext");
            connector.Configuration.ConnectionStringName.Add("UnitTestDbContext_CE");
            //This one was not in the config file (it's just made up)
            connector.Configuration.ConnectionStringName.Add("OneThatDoesNotExist");


            //Investigate status:
            StatusResponse result = connector.Get();



            Assert.IsNotNull(result);
            string json = result.ToJSON(Encoding.UTF8);
            Console.WriteLine(json);

            //First
            //Not Alwasys the case (depends if I have VPN'ed into main office)
            //Assert.AreEqual("OK", result.Data.Rows[0].Cells[1],"Sql string #1");
            Assert.AreEqual("OK", result.Data.Rows[1].Cells[1], "Sql string #2");
            Assert.AreNotEqual("OK", result.Data.Rows[2].Cells[1], "Sql string #3");


        }



        [Test]
        public void CanPerformDirectoryAccessibilityStatusServiceConnectorGet()
        {

            DirectoryAccessibilityStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<DirectoryAccessibilityStatusServiceConnector>();

            connector.Configuration.Directories.Add(new DirectoryAccessibilityStatusServiceConnectorConfigurationItem
                (
                title:"TestDir",
                    directoryPath :"TestDir"
                ));
            connector.Configuration.Directories.Add(new DirectoryAccessibilityStatusServiceConnectorConfigurationItem
                (
                    title:"c:\\Windows\\TestDir",
                    directoryPath:"c:\\Windows\\TestDir"
                ));

            var result = connector.Get();

            Assert.IsNotNull(result);

            string json = result.ToJSON(Encoding.UTF8);

            Console.WriteLine(json);
        }

        [Test]
        public void CanPerformUrlAccessibilityStatusServiceConnectorGet()
        {

            UrlAccessibilityStatusServiceConnector connector =
                XAct.DependencyResolver.Current.GetInstance<UrlAccessibilityStatusServiceConnector>();

            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem {Url = new Uri("http://google.com")});
            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem { Url = new Uri("http://stnsi01.moest.govt.nz") });
            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem { Url = new Uri("https://stnsi01.moest.govt.nz") });
            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem { Url = new Uri("http://web.st.nsi.education.org.nz") });
            connector.Configuration.Urls.Add(new UrlAccessibilityStatusServiceConnectorConfigurationItem { Url = new Uri("https://web.st.nsi.education.org.nz") });




            var result = connector.Get();

            Assert.IsNotNull(result);

            string json = result.ToJSON(Encoding.UTF8);

            Console.WriteLine(json);
        }

        [Test]
        public void CanGetPerformanceCountersGet()
        {

        }

    }
}

