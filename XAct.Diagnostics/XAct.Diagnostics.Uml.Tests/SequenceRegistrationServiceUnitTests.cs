namespace XAct.Diagnostics.Tests
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Bootstrapper.Tests;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture(Description = "Some Fixture")]
    public class SequenceRegistrationServiceUnitTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            //IoCBootStrapper.Instance =null;
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }


        [Test()]
        public void Can_Get_ISequenceRegistrationService()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISequenceRegistrationService>();
            Assert.IsNotNull(service);
        }

        [Test()]
        public void Can_Get_ISequenceRegistrationService_Of_Expected_Type()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISequenceRegistrationService>();
            Assert.AreEqual(typeof(SequenceRegistrationService),service.GetType());
        }



        [Test()]
        public void Can_Build_Simple_SequenceDiagram()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISequenceRegistrationService>();

            service.AddTitle("Hi");

            var result = service.RenderDescription();

            Console.WriteLine(result);
            Assert.IsNotNullOrEmpty(result);
            Assert.IsTrue(result.Contains("title Hi"));
        }


        [Test()]
        public void Can_Build_More_Complex_SequenceDiagram()
        {
            var service = XAct.DependencyResolver.Current.GetInstance<ISequenceRegistrationService>();

            service.AddTitle("Hi");
            service.AddComment("A Comment of some kind");

            service.AddSequence("A","B","foo()",false);
            service.BeginGroup("group Uno");
            service.AddSequence("B", "C", "bar()", false);
            service.BeginGroup("if something","alt");
            service.AddSequence("C", "B", "string", true);
            service.AddConditionalCase("or something else");
            service.AddSequence("C", "B", "error", true);
            service.EndGroup();
            service.AddSequence("B", "A", "int", true);

            var result = service.RenderDescription();

            Console.WriteLine(result);
            Assert.IsNotNullOrEmpty(result);
            Assert.IsTrue(result.Contains("title Hi"));
        }

    }


}


