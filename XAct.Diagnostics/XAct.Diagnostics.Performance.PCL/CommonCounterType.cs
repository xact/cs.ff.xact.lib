﻿namespace XAct.Diagnostics.Performance
{
    /// <summary>
    /// Types of Common Counter Types already defined within
    /// <c>PerformanceCounterCreationInformationFactory</c>
    /// </summary>
    public enum CommonCounterType
    {
        /// <summary>
        /// The undefined (an error state)
        /// </summary>
        Undefined=0,
        /// <summary>
        /// The operations per second counter
        /// </summary>
        OperationsPerSecondCounter=1,
        /// <summary>
        /// The operations performed counter
        /// </summary>
        OperationsPerformedCounter=2,
        /// <summary>
        /// The operation elapsed time counter
        /// </summary>
        OperationElapsedTimeCounter=3,
        /// <summary>
        /// The operation average time counter pair
        /// </summary>
        OperationAverageTimeCounterPair=4,
        /// <summary>
        /// The average items processed per operation counter pair
        /// </summary>
        AverageItemsProcessedPerOperationCounterPair=5,
    }
}
