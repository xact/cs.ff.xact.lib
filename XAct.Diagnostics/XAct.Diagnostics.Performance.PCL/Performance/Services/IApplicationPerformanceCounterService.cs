namespace XAct.Diagnostics.Performance
{
    /// <summary>
    /// A specialization of the <see cref="IPerformanceCounterService"/>
    /// to manage in-mem custom PerformanceCounters.
    /// <para>
    /// The Performance Counters are not Windows System Counters, as they 
    /// are not portable to PCL or just about any other environment than 
    /// full servers, require full Admin rights to install, etc. ie....a PITA.
    /// </para>
    /// </summary>
    public interface IApplicationPerformanceCounterService : IPerformanceCounterService, IHasXActLibService
    {
    }

}