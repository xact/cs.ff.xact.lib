﻿namespace XAct.Diagnostics.Services.Implementations
{
    using System.Collections.Generic;

    /// <summary>
    /// A Queue to hold Metric readings
    /// in the form of <see cref="MetricsSerieEntry"/> items.
    /// </summary>
    public class MetricsSeriesEntryQueue : Queue<MetricsSerieEntry>, IHasName
    {

        /// <summary>
        /// Gets the unique name of the <see cref="MetricsSeriesEntryQueue"/>.
        /// <para>
        /// The Name is a displayable text identifier.
        /// </para>
        /// <para>Member defined in<see cref="XAct.IHasName" /></para>
        /// </summary>
        public string Name { get; set; }

    }
}