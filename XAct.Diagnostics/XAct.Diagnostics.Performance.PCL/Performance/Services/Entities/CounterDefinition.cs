﻿namespace XAct.Diagnostics.Performance
{
    using System.Runtime.Serialization;

    [DataContract]
#pragma warning disable 1591
    public class CounterDefinition
    {
        [DataMember]
        public string CategoryName { get; set; }
        [DataMember]
        public string CounterName { get; set; }
    }
#pragma warning restore 1591
}