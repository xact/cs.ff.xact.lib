﻿namespace XAct.Diagnostics.Performance
{
    using XAct.Diagnostics.Performance.Services.Entities;

    /// <summary>
    /// Inteface for an Application or System PerformanceCounter.
    /// <para>
    /// Basis of <see cref="ICategorizedPerformanceCounter"/>, which is the basis 
    /// of <c>ISystemPerformanceCounter</c>.
    /// </para>
    /// <para>
    /// Why are we not using .NET PerformanceCounter directly?
    /// Because it's not available on all platforms. On those...have to use alternate solutions,
    /// mapped back to this common contract.
    /// </para>
    /// <para>
    /// The relationship between <see cref="IPerformanceCounter"/> and 
    /// <see cref="PerformanceCounterCreationInformation"/>
    /// is that <see cref="PerformanceCounterCategoryCreationInformation"/> and <see cref="PerformanceCounterCreationInformation"/>
    /// are needed to describe to create entry types. 
    /// Only when the system knows about the types of Counters that can exist can you create one
    /// of that type....ie, <see cref="IPerformanceCounter"/>
    /// </para>
    /// </summary>
    public interface IPerformanceCounter 
    {

        /// <summary>
        /// Gets or sets the raw/uncalculated value.
        /// <para>
        /// In most cases you would invoke NextSample()
        /// instead of this property.
        /// </para>
        /// </summary>
        /// <value>The raw value.</value>
        /// <internal><para>5/12/2011: Sky</para></internal>
        long RawValue { get; set; }


        /// <summary>
        /// Updates the <see cref="RawValue"/>
        /// by adding or subtracting the given offset.
        /// <para>
        /// If no value is provided, it increments the 
        /// <see cref="RawValue"/> by 1.
        /// </para>
        /// </summary>
        /// <param name="offset">The offset.</param>
        /// <returns></returns>
        long Offset(long? offset);


        /// <summary>
        /// Gets the computed value of the Counter.
        /// <para>
        /// If the counter has a Base counter backing it, 
        /// this method returns the sum/division/whatever of the
        /// counter -- whereas Raw would just give the value
        /// of the primary counter (not what you wanted, really).
        /// </para>
        /// </summary>
        /// <value>
        /// The computed value.
        /// </value>
        float ComputedValue();
    }
}