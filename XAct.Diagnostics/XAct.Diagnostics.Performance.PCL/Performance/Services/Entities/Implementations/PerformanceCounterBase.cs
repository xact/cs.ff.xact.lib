﻿namespace XAct.Diagnostics.Performance
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class PerformanceCounterBase : IPerformanceCounter
    {

        
        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceCounterBase" /> class.
        /// </summary>
        /// <param name="performanceCounterSetName">Name of the performance counter set.</param>
        /// <param name="performanceCounterDescription">The performance counter description.</param>
        protected PerformanceCounterBase(string performanceCounterSetName,string performanceCounterDescription=null)
        {
            _name = performanceCounterSetName;

            if (performanceCounterDescription.IsNullOrEmpty())
            {
                performanceCounterDescription = performanceCounterSetName;
            }

            _description = performanceCounterDescription;
        }


        /// <summary>
        /// Initializes the specified performance counter raws.
        /// </summary>
        /// <param name="performanceCounter">The performance counter raws.</param>
        public virtual void Initialize(IPerformanceCounter performanceCounter)
        {
            PerformanceCounter = performanceCounter;
            Initialized = true;
        }

        /// <summary>
        /// Gets a value indicating whether [initialized].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [initialized]; otherwise, <c>false</c>.
        /// </value>
        protected bool Initialized { get; private set; }


        /// <summary>
        /// The cached <see cref="IPerformanceCounter"/>s that this set manages.
        /// </summary>
        protected IPerformanceCounter PerformanceCounter;

        /// <summary>
        /// The name of the <see cref="IPerformanceCounterPair"/>
        /// </summary>
        public virtual string Name { get { return _name; } }
        private readonly string _name;

        /// <summary>
        /// Gets or sets the description.
        /// <para>
        /// Member defined in the <see cref="IHasNameAndDescriptionReadOnly" /> contract.
        /// </para>
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public virtual string Description { get { return _description; } }
        private readonly string _description;



        /// <summary>
        /// Gets or sets the Counter's value
        /// (or Nominator (top) counter's value, if within a <see cref="IPerformanceCounterPair" />)
        /// value directly.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public virtual long RawValue
        {
            get { return PerformanceCounter.RawValue; }
            set { PerformanceCounter.RawValue = value; }
        }


        /// <summary>
        /// Increment/Decrement the Counter raw value
        /// (or Nominator (top) counter's raw value, if within a <see cref="IPerformanceCounterPair"/>)
        ///  by a specific amount.
        /// <para>
        /// If no value is provided, updates counter by 1.
        /// </para>
        /// </summary>
        public long Offset(long? amount = null)
        {
            IPerformanceCounter performanceCounterRaw = PerformanceCounter;

            return  performanceCounterRaw.Offset(amount);
        }



        /// <summary>
        /// Gets the computed value of the Counter.
        /// <para>
        /// If the counter has a Base counter backing it,
        /// this method returns the sum/division/whatever of the
        /// counter -- whereas Raw would just give the value
        /// of the primary counter (not what you wanted, really).
        /// </para>
        /// <para>
        /// Use this instead of <see cref="IPerformanceCounterPair"/>.<c>RawValue</c> 
        /// which just gives the value of the primary counter).
        /// </para>
        /// </summary>
        /// <value>
        /// The computed value.
        /// </value>
        public float ComputedValue()
        {
            return PerformanceCounter.ComputedValue(); 
        }





    }
}