﻿
//namespace XAct.Diagnostics.Performance
//{
//    using System;

//    /// <summary>
//    /// 
//    /// </summary>
//    public abstract class PerformanceCounterPairBase : PerformanceCounterBase , IPerformanceCounterPair
//    {

//        /// <summary>
//        /// Initializes a new instance of the <see cref="PerformanceCounterPairBase"/> class.
//        /// </summary>
//        /// <param name="name">The name.</param>
//        /// <param name="description">The description.</param>
//        protected PerformanceCounterPairBase(string name, string description) : base(name, description)
//        {
//        }


//        private IPerformanceCounter _denominatorCounter;


//        /// <summary>
//        /// Initializes the specified raw performance counters.
//        /// </summary>
//        /// <param name="nominatorPerformanceCounter">The nominator performance counter.</param>
//        /// <param name="denominatorPerformanceCounter">The denominator performance counter.</param>
//        public void Initialize(IPerformanceCounter nominatorPerformanceCounter, IPerformanceCounter denominatorPerformanceCounter)
//        {
//            base.Initialize(nominatorPerformanceCounter);
//            _denominatorCounter = denominatorPerformanceCounter;
//        }


//        /// <summary>
//        /// Initializes the specified performance counter raws.
//        /// </summary>
//        /// <param name="performanceCounter">The performance counter raws.</param>
//        /// <exception cref="System.Exception">When a counter derives from PerformanceCounterPairBase, use the other Initialize() method.</exception>
//        public override void Initialize(IPerformanceCounter performanceCounter)
//        {
//            throw new Exception("When a counter derives from PerformanceCounterPairBase, use the other Initialize() method.");
//        }


        
//        /// <summary>
//        /// Gets or sets the denominator (the b of an a/b pair) counter's value.
//        /// </summary>
//        /// <value>
//        /// The denominator value.
//        /// </value>
//        public long DenominatorRawValue
//        {
//            get
//            {

//                return _denominatorCounter.RawValue;
//            }
//            set
//            {
//                _denominatorCounter.RawValue = value;
//            }
//        }

//        /// <summary>
//        /// Increment/Decrement the bottom counter of the <see cref="IPerformanceCounterPair"/>
//        /// </summary>
//        /// <param name="amount"></param>
//        /// <returns></returns>
//        public virtual long UpdateDenominatorBy(int? amount)
//        {

//            IPerformanceCounter performanceCounterBase = _denominatorCounter;

//            return performanceCounterBase.Increment(amount);
//        }



//    }
//}