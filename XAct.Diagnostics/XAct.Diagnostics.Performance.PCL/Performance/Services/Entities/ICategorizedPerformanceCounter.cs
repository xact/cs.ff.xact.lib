﻿namespace XAct.Diagnostics.Performance
{
    /// <summary>
    /// Specialization of the <see cref="IPerformanceCounter"/> Contract 
    /// for PerformanceCounters that are Categorized.
    /// <para>
    /// Although I don't like the Contract (an Item should probably not know it's Collection Owner)
    /// it's the basis of Windows <c>ISystemPerformanceCounter</c>, hence...
    /// </para>
    /// </summary>
    public interface ICategorizedPerformanceCounter :  IPerformanceCounter 
    {
        /// <summary>
        /// Gets the name of the category.
        /// </summary>
        /// <value>The name of the category.</value>
        /// <internal><para>5/12/2011: Sky</para></internal>
        string CategoryName { get; }


        /// <summary>
        /// Gets the description of the PerformanceCounter.
        /// </summary>
        /// <value>The category description.</value>
        /// <internal><para>5/12/2011: Sky</para></internal>
        string CategoryDescription { get; }
    }
}