﻿
// ReSharper disable CheckNamespace
namespace XAct.Diagnostics.Performance
// ReSharper restore CheckNamespace
{

    /// <summary>
    /// A <see cref="IPerformanceCounterPair"/>
    /// is a construct to encapsulate and hide the
    /// unintuitivity of dealing with pairs of <see cref="IPerformanceCounter"/>.
    /// <para>
    /// Each <see cref="IPerformanceCounterPair"/> contains
    /// one or two <see cref="IPerformanceCounter"/>, 
    /// (when the first <see cref="IPerformanceCounter"/> becomes the Nominator, 
    /// plus a second BASE counter, which acts as the Denominator) 
    /// </para>
    /// </summary>
    public interface IPerformanceCounterPair : IPerformanceCounter
    {

        /// <summary>
        /// Initializes the specified raw performance counters.
        /// </summary>
        /// <param name="denominatorPerformanceCounters">The raw performance counters.</param>
        /// <param name="nominatorPerformanceCounters">The nominator performance counters.</param>
        void Initialize(IPerformanceCounter denominatorPerformanceCounters, IPerformanceCounter nominatorPerformanceCounters);

        /// <summary>
        /// Gets or sets the denominator (the b of an a/b pair) counter's value.
        /// </summary>
        /// <value>
        /// The denominator value.
        /// </value>
        long DenominatorRawValue { get; set; }


        /// <summary>
        /// Increment/Decrement the denominator (the b of an a/b pair) counter's value.
        /// </summary>
        long OffsetDenominator(long? amount);

    }
}