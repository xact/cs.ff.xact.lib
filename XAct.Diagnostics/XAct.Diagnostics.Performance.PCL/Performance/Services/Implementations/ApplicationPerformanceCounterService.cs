//namespace XAct.Diagnostics.Performance.Implementations
//{
//    using System.Collections.Generic;
//    using XAct.Diagnostics.Performance.State;
//    using XAct.Diagnostics.Services.Configuration;
//    using XAct.Diagnostics.Services.Entities;
//    using XAct.Services;

//    /// <summary>
//    /// 
//    /// </summary>
//    [DefaultBindingImplementation(typeof(IApplicationPerformanceCounterService))]
//    [DefaultBindingImplementation(typeof(IPerformanceCounterService))]
//    public class ApplicationPerformanceCounterService : PerformanceCounterServiceBase
//    {
//        public ApplicationPerformanceCounterService(IPerformanceCounterServiceConfiguration performanceCounterServiceConfiguration, IPerformanceCounterServiceState performanceCounterSetCache) : 
//            base(performanceCounterServiceConfiguration, performanceCounterSetCache)
//        {
//        }

//        protected override bool EnsureCategoryExistsAsExactlyDescribed(
//            PerformanceCounterCategoryCreationInformation performanceCounterCategoryCreationInformation,
//            bool forceRebuildOfCategoryEveryTime)
//        {
//            throw new System.NotImplementedException();
//        }

//        protected override KeyValuePair<string, IPerformanceCounter>[] CreateCounters(PerformanceCounterCategoryCreationInformation counterCategoryCreationInformation)
//        {
//            throw new System.NotImplementedException();
//        }

//        public override Dictionary<string, IPerformanceCounter> CreateCounters(PerformanceCounterCreationInformation performanceCounterCreationInformation)
//        {
//            throw new System.NotImplementedException();
//        }
//    }
//}