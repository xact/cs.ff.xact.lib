﻿// ReSharper disable CheckNamespace
namespace XAct.Diagnostics.Performance.Implementations
// ReSharper restore CheckNamespace
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics.Performance.Services.Configuration;
    using XAct.Diagnostics.Performance.Services.Entities;
    using XAct.Diagnostics.Performance.State;


    /// <summary>
    /// Implementation of the <see cref="IPerformanceCounterService"/>
    /// to update <see cref="IPerformanceCounter"/>s.
    /// </summary>
    public abstract class PerformanceCounterServiceBase : IPerformanceCounterService
    {
        /// <summary>
        /// Static lock across instances:
        /// </summary>
        protected static object _lock = new object();
        
        /// <summary>
        /// Gets or sets the common settings.
        /// <para>
        /// The Configuration object is shared between instances of 
        /// this service, therefore should only be modified as per the application's needs
        /// during Bootstrapping, and no later.
        /// </para>
        /// </summary>
        public IPerformanceCounterServiceConfiguration Configuration
        {
            get { return _performanceCounterServiceConfiguration; }
        }

        private readonly IPerformanceCounterServiceConfiguration _performanceCounterServiceConfiguration;

        //Cache in which to keep Counters during the initialize phase:
        private readonly IPerformanceCounterServiceState _performanceCounterCreatedCache;


        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceCounterServiceBase" /> class.
        /// </summary>
        /// <param name="performanceCounterServiceConfiguration">The performance counter set management service configuration.</param>
        /// <param name="performanceCounterSetCache">The performance counter set cache.</param>
        public PerformanceCounterServiceBase(
            IPerformanceCounterServiceConfiguration performanceCounterServiceConfiguration,
            IPerformanceCounterServiceState performanceCounterSetCache)
        {
            _performanceCounterServiceConfiguration = performanceCounterServiceConfiguration;
            _performanceCounterCreatedCache = performanceCounterSetCache;

        }




        /// <summary>
        /// Ensures the Category exists, describing the Counters, and then creates an initial set of Counters
        /// from them (their instances named after the Counter creation names).
        /// </summary>
        /// <returns></returns>
        public bool Initialize()
        {

            if (!_performanceCounterServiceConfiguration.Enabled)
            {
                return false;
            }


            //Will do nothing new if already initialized:

            foreach (
                var counterCategoryCreationInformation in
                    _performanceCounterServiceConfiguration.CounterCategoryCreationInformation)
            {
                if (counterCategoryCreationInformation.Initialized)
                {
                    //If the PerformanceCounterInformation has already been processed 
                    //(ie, Category created, and PerformanceCounter instances as well, 
                    //there is no need to continue).
                    continue;
                }

                lock (counterCategoryCreationInformation)
                {

                    if (counterCategoryCreationInformation.Initialized)
                    {
                        //If the PerformanceCounterInformation has already been processed 
                        //(ie, Category created, and PerformanceCounter instances as well, 
                        //there is no need to continue).
                        continue;
                    }
                    try
                    {

                        PerformanceCounterCreationInformation[] flattenedPerformanceCounterCreationInformations
                            =
                            this.Flatten(counterCategoryCreationInformation.CounterCreationInformation);


                        //AttemptSelfInstallOfCategoryAndCountes(counterCategoryCreationInformation, flattenedPerformanceCounterCreationInformations);

                        
                        CreateInstancesOfCounter(counterCategoryCreationInformation, flattenedPerformanceCounterCreationInformations);

                        //Set flag indicating that these instances don't need to be recreated:
                        counterCategoryCreationInformation.Initialized = true;
                    }
                    catch
                    {
                        //_performanceCounterServiceConfiguration.Enabled = false;
                    }
                } //~lock

            } //~each
            return _performanceCounterCreatedCache.Any();
        }





        /// <summary>
        /// Sets the raw value of the specified performance counter.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public bool Set(string categoryName, string performanceCounterName, long amount,
                        bool raiseExceptionIfNotFound = false)
        {
            return Set(categoryName, performanceCounterName, null, amount, raiseExceptionIfNotFound);
        }












        /// <summary>
        /// Sets the raw value of the specified performance counter.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="instanceName"></param>
        /// <param name="amount">The amount.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public bool Set(string categoryName, string performanceCounterName, string instanceName, long amount, bool raiseExceptionIfNotFound = false)
        {
            if (!_performanceCounterServiceConfiguration.Enabled)
            {
                return false;
            }

            IPerformanceCounter performanceCounter = GetPerformanceCounterInstance(categoryName, performanceCounterName, instanceName);

            if (performanceCounter == null)
            {
                if (raiseExceptionIfNotFound || _performanceCounterServiceConfiguration.RaiseExceptionsIfPerformanceCounterNotFound)
                {
                    throw new ArgumentException("Could not find performanceCounter: " + performanceCounterName);
                }

                return false;
            }
            performanceCounter.RawValue = amount;

            return true;
        }


        /// <summary>
        /// Gets the performance counter's raw value .
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public TValue GetRawValue<TValue>(string categoryName, string performanceCounterName,
                                          bool raiseExceptionIfNotFound = false)
        {
            var result = GetRawValue<TValue>(categoryName, performanceCounterName, null, raiseExceptionIfNotFound);
            return result;
        }



        /// <summary>
        /// Gets the performance counter's raw value .
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="instanceName">Name of the instance.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public TValue GetRawValue<TValue>(string categoryName, string performanceCounterName, string instanceName=null, bool raiseExceptionIfNotFound = false)
        {

            if (!_performanceCounterServiceConfiguration.Enabled)
            {
                return default(TValue);
            }

            IPerformanceCounter performanceCounter = GetPerformanceCounterInstance(categoryName, performanceCounterName,instanceName);

            if (performanceCounter == null)
            {
                if (raiseExceptionIfNotFound || _performanceCounterServiceConfiguration.RaiseExceptionsIfPerformanceCounterNotFound)
                {
                    throw new ArgumentException("Could not find performanceCounter: " + performanceCounterName);
                }
                return default(TValue);
            }
            return performanceCounter.ComputedValue().ConvertTo<TValue>();
        }










        /// <summary>
        /// Gets the performance counter's computed value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public TValue GetComputedValue<TValue>(string categoryName, string performanceCounterName, bool raiseExceptionIfNotFound = false)
        {

            var result = GetComputedValue<TValue>(categoryName, performanceCounterName, null, raiseExceptionIfNotFound);
            return result;
        }



        /// <summary>
        /// Gets the performance counter's computed value.
        /// </summary>
        /// <typeparam name="TValue">The type of the value.</typeparam>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="instanceName"></param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public TValue GetComputedValue<TValue>(string categoryName, string performanceCounterName, string instanceName=null, bool raiseExceptionIfNotFound = false)
        {

            if (!_performanceCounterServiceConfiguration.Enabled)
            {
                return default(TValue);
            }

            IPerformanceCounter performanceCounter = GetPerformanceCounterInstance(categoryName, performanceCounterName,instanceName);

            if (performanceCounter == null)
            {
                if (raiseExceptionIfNotFound || _performanceCounterServiceConfiguration.RaiseExceptionsIfPerformanceCounterNotFound)
                {
                    throw new ArgumentException("Could not find performanceCounter: '{0}':'{1}'".FormatStringInvariantCulture(categoryName, performanceCounterName));
                }
                return default(TValue   );
            }
            return performanceCounter.ComputedValue().ConvertTo<TValue>();
        }










        /// <summary>
        /// Updates the value of the counter.
        /// <para>
        /// Used to update simple <see cref="IPerformanceCounter" />s that have only one counter
        /// within the set.
        /// </para>
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        /// on the counter.
        /// </para>
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="amount">The amount to increment, or decrement the counter by.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public bool Offset(string categoryName, string performanceCounterName, long amount, bool raiseExceptionIfNotFound = false)
        {
            return Offset(categoryName, performanceCounterName, null, amount, raiseExceptionIfNotFound);
        }




        /// <summary>
        /// Updates the value of the counter.
        /// <para>
        /// Used to update simple <see cref="IPerformanceCounter" />s that have only one counter
        /// within the set.
        /// </para>
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        /// on the counter.
        /// </para>
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="instanceName"></param>
        /// <param name="amount">The amount to increment, or decrement the counter by.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public bool Offset(string categoryName, string performanceCounterName, string instanceName, long amount, bool raiseExceptionIfNotFound = false)
        {
            if (!_performanceCounterServiceConfiguration.Enabled)
            {
                return false;
            }

            IPerformanceCounter performanceCounter = GetPerformanceCounterInstance(categoryName, performanceCounterName,instanceName);

            if (performanceCounter == null)
            {
                if (raiseExceptionIfNotFound||_performanceCounterServiceConfiguration.RaiseExceptionsIfPerformanceCounterNotFound)
                {
                    throw new ArgumentException("Could not find performanceCounter: " + performanceCounterName);
                }
                return false;
            }
            performanceCounter.Offset(amount);

            return true;
        }





        /// <summary>
        /// Updates the value of the bottom (denominator) counter (ie, the 'b' in an 'a/b' configuration).
        /// of <see cref="IPerformanceCounter" />.
        /// <para>
        /// </para>
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        /// on the base counter.
        /// </para>
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="amount">The amount to increment, or decrement the counter by.</param>
        /// <param name="nameExtension">The name extension.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public bool UpdateDenominator(string categoryName, string performanceCounterName, 
                                      long? amount = null, string nameExtension = " (Base)",
                                      bool raiseExceptionIfNotFound = false)
        {
            return UpdateDenominator(categoryName,performanceCounterName, null,amount, nameExtension,raiseExceptionIfNotFound);
        }







        /// <summary>
        /// Updates the value of the bottom (denominator) counter (ie, the 'b' in an 'a/b' configuration).
        /// of <see cref="IPerformanceCounter" />.
        /// <para>
        /// </para>
        /// <para>
        /// If no value is provided, this is the same as simply calling <c>Increment()</c>
        /// on the base counter.
        /// </para>
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="instanceName"></param>
        /// <param name="amount">The amount to increment, or decrement the counter by.</param>
        /// <param name="nameExtension">The name extension.</param>
        /// <param name="raiseExceptionIfNotFound">if set to <c>true</c> [raise exception if not found].</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentException">Could not find performanceCounter:  + performanceCounterName</exception>
        public bool UpdateDenominator(string categoryName, string performanceCounterName, string instanceName=null, long? amount = null, string nameExtension = " (Base)", bool raiseExceptionIfNotFound = false)
        {
            if (!_performanceCounterServiceConfiguration.Enabled)
            {
                return false;
            }

            IPerformanceCounter performanceCounter = GetPerformanceCounterInstance(categoryName, performanceCounterName + nameExtension, instanceName);

            if (performanceCounter == null)
            {
                if (raiseExceptionIfNotFound || _performanceCounterServiceConfiguration.RaiseExceptionsIfPerformanceCounterNotFound)
                {
                    throw new ArgumentException("Could not find performanceCounter: " + performanceCounterName);
                }
                return false;
            }

            IPerformanceCounterPair performanceCounterSet = performanceCounter as IPerformanceCounterPair;

            if (performanceCounterSet != null)
            {
                performanceCounterSet.OffsetDenominator(amount);
            }

            return true;
        }






















        /// <summary>
        /// Gets the performance counter set by name.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="performanceCounterName">Name of the performance counter set.</param>
        /// <param name="instanceName">Name of the instance.</param>
        /// <param name="readOnly">if set to <c>true</c> [read only].</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        private IPerformanceCounter GetPerformanceCounterInstance(string groupName, string performanceCounterName, string instanceName=null, bool readOnly=false)
        {

            IPerformanceCounter result;

            string key = groupName + ":" + performanceCounterName;

            lock (_lock)
            {
                if (!_performanceCounterCreatedCache.TryGetValue(key, out result))
                {
                    try
                    {
                        result = CreateCounterInstance(groupName, performanceCounterName, instanceName, readOnly);
                    }
                    catch
                    {
                        result = null;
                    }
                    //We store it either way, so that the app doesn't try to create a counter every single time.
                    _performanceCounterCreatedCache[key] = result;
                }
            }

            return result;
        }



        private PerformanceCounterCreationInformation[] Flatten(IEnumerable<PerformanceCounterCreationInformation> performanceCounterCreationInformations)
        {
            var results = new List<PerformanceCounterCreationInformation>();

            foreach (PerformanceCounterCreationInformation performanceCounterCreationInformation in performanceCounterCreationInformations)
            {
                results.Add(performanceCounterCreationInformation);

                if (performanceCounterCreationInformation.Base != null)
                {
                    var subResult = Flatten(new PerformanceCounterCreationInformation[] { performanceCounterCreationInformation.Base });

                    if (subResult.Length > 0)
                    {
                        results.AddRange(subResult);
                    }
                }
            }
            return results.ToArray();
        }


        private void AttemptSelfInstallOfCategoryAndCountes(
            PerformanceCounterCategoryCreationInformation counterCategoryCreationInformation,
            PerformanceCounterCreationInformation[] flattenedPerformanceCounterCreationInformations)
        {
            if (!_performanceCounterServiceConfiguration.AttemptSelfInstall)
            {
                //First ensure we have created in the system the counters Category,
                //with its *definition* of the counters (but don't create the counters):
                //Use Abstract method:
                EnsureCategoryExistsAsExactlyDescribed(
                    counterCategoryCreationInformation,
                    flattenedPerformanceCounterCreationInformations,
                    _performanceCounterServiceConfiguration.ForceCategoryRecreationIfSelfInstalling);
            }
        }




        private void CreateInstancesOfCounter(PerformanceCounterCategoryCreationInformation counterCategoryCreationInformation,
                                              PerformanceCounterCreationInformation[]
                                                  flattenedPerformanceCounterCreationInformations)
        {

            return;

            //Now that category were created (or were created earlier) ... create counters based on the info:

#pragma warning disable 162
            foreach (KeyValuePair<string, IPerformanceCounter> keyValuePair in
                CreateCounters(counterCategoryCreationInformation.Name, flattenedPerformanceCounterCreationInformations))
            {
                //Persist:
                _performanceCounterCreatedCache[counterCategoryCreationInformation.Name + ":" + keyValuePair.Key] = keyValuePair.Value;

            }
#pragma warning restore 162
        }

        





        /// <summary>
        /// Ensures the Category exists, and it contains descriptions of the
        /// Counters.
        /// <para>
        /// If the Category contains more or less Counter defintions than defined
        /// in the
        /// <see cref="PerformanceCounterCategoryCreationInformation" />,
        /// will optionally force the Category to be rebuilt.
        /// </para>
        /// </summary>
        /// <param name="performanceCounterCategoryCreationInformation">The performance counter category creation information.</param>
        /// <param name="flattenedPerformanceCounterCreationInformations">The flattened performance counter creation informations.</param>
        /// <param name="forceRebuildOfCategoryEveryTime">if set to <c>true</c> [force rebuild of category every time].</param>
        /// <returns></returns>
        protected abstract bool EnsureCategoryExistsAsExactlyDescribed(
            PerformanceCounterCategoryCreationInformation performanceCounterCategoryCreationInformation,
            PerformanceCounterCreationInformation[] flattenedPerformanceCounterCreationInformations,
            bool forceRebuildOfCategoryEveryTime);




        /// <summary>
        /// Creates <see cref="IPerformanceCounter" />s,
        /// based on the information within the given
        /// <see cref="PerformanceCounterCreationInformation" />.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="flattenedPerformanceCounterCreationInformations">The flattened performance counter creation informations.</param>
        /// <returns></returns>
        protected abstract KeyValuePair<string, IPerformanceCounter>[] CreateCounters(string categoryName, PerformanceCounterCreationInformation[] flattenedPerformanceCounterCreationInformations);



        /// <summary>
        /// Creates a single instance of a named Counted.
        /// </summary>
        /// <param name="categoryName">Name of the category.</param>
        /// <param name="counterName">Name of the counter.</param>
        /// <param name="instanceName">Name of the instance.</param>
        /// <param name="readOnly">if set to <c>true</c> [read only].</param>
        /// <returns></returns>
        protected abstract IPerformanceCounter CreateCounterInstance(string categoryName, string counterName, string instanceName, bool readOnly=false);


    }
}