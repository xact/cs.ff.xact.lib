﻿namespace XAct.Diagnostics.Performance.State.Implementations
{
    using System.Collections.Generic;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="IPerformanceCounterServiceState"/>
    /// to provide singleton state for instances of
    /// <see cref="IPerformanceCounterService"/>
    /// </summary>
    public class PerformanceCounterServiceState : Dictionary<string, IPerformanceCounter>, IPerformanceCounterServiceState
    {
    }

}