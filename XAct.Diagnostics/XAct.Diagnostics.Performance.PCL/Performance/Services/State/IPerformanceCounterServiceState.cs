﻿// ReSharper disable CheckNamespace
namespace XAct.Diagnostics.Performance.State
// ReSharper restore CheckNamespace
{
    using System.Collections.Generic;

    /// <summary>
    /// Contract for a dictionary cache of <see cref="IPerformanceCounter"/>
    /// entities.
    /// </summary>
    public interface IPerformanceCounterServiceState :IDictionary<string,IPerformanceCounter>,IHasXActLibServiceState
    {
    }
}
