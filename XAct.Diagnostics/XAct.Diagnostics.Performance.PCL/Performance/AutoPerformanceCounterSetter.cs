﻿namespace XAct.Interception.CallHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Performance.Services;
    using XAct.Diagnostics.Services;
    using XAct.Diagnostics.Performance.Services.Entities;
    using XAct.Services;

    /// <summary>
    /// 
    /// </summary>
    [DefaultBindingImplementation(typeof(IAutoPerformanceCounterSetter),BindingLifetimeType.TransientScope,Priority.Low)]
    public class AutoPerformanceCounterSetter : IAutoPerformanceCounterSetter
    {
        private readonly IContextStatePerformanceCounterService _contextStatePerformanceCounterService;
        private readonly IPerformanceCounterService _performanceCounterService;

        /// <summary>
        /// Initializes a new instance of the <see cref="AutoPerformanceCounterSetter"/> class.
        /// </summary>
        /// <param name="contextStatePerformanceCounterService">The context state performance counter service.</param>
        /// <param name="performanceCounterService">The performance counter service.</param>
        public AutoPerformanceCounterSetter(IContextStatePerformanceCounterService contextStatePerformanceCounterService, IPerformanceCounterService performanceCounterService)
        {
            _contextStatePerformanceCounterService = contextStatePerformanceCounterService;
            _performanceCounterService = performanceCounterService;
        }

        /// <summary>
        /// Helper to return the names of the performance counters needed.
        /// </summary>
        /// <param name="rootName">Name of the root.</param>
        /// <param name="tag">The tag.</param>
        /// <param name="includeType">if set to <c>true</c> [include type].</param>
        /// <returns></returns>
        public static string[] GenerateAutomaticNames(string rootName, string tag = null, bool includeType = true)
        {
            PerformanceCounterNames automaticPerformanceCounterNames =
                new PerformanceCounterNames(rootName);

            List<string> results = new List<string>();

            results.AddRange(new[]
                {
                    rootName,
                    //------
                    //Instants (6):
                    automaticPerformanceCounterNames.INSTANCE_Operation_Counter + IncludeType(includeType, PerformanceCounterType.NumberOfItems64),
                    automaticPerformanceCounterNames.INSTANCE_Operation_Duration + IncludeType(includeType, PerformanceCounterType.NumberOfItems64),
                    //------
                    //Rate (5):
                    automaticPerformanceCounterNames.RATE_Operations_PerSec + IncludeType(includeType, PerformanceCounterType.RateOfCountsPerSecond32),
                    //------
                    //Averages (12):
                    automaticPerformanceCounterNames.AVERAGE_AverageDuration + IncludeType(includeType, PerformanceCounterType.AverageTimer32),
                    automaticPerformanceCounterNames.AVERAGE_AverageDuration_Base + IncludeType(includeType, PerformanceCounterType.AverageBase),
                    //------
                    //Percentage (8):
                    automaticPerformanceCounterNames.PERCENTAGE_Of_ResponseSuccessful_To_Operation_Count  + IncludeType(includeType, PerformanceCounterType.RawFraction),
                    automaticPerformanceCounterNames.PERCENTAGE_Of_ResponseSuccessful_To_Operation_Count_Base + IncludeType(includeType, PerformanceCounterType.RawBase),
                    automaticPerformanceCounterNames.PERCENTAGE_Of_ResponseNonSuccessful_To_Operation_Count + IncludeType(includeType, PerformanceCounterType.RawFraction),
                    automaticPerformanceCounterNames.PERCENTAGE_Of_ResponseNonSuccessful_To_Operation_Count_Base + IncludeType(includeType, PerformanceCounterType.RawBase),
                    automaticPerformanceCounterNames.PERCENTAGE_Of_NoExceptionOccurred_To_Operation_Count + IncludeType(includeType, PerformanceCounterType.RawFraction),
                    automaticPerformanceCounterNames.PERCENTAGE_Of_NoExceptionOccurred_To_Operation_Count_Base + IncludeType(includeType, PerformanceCounterType.RawBase),
                    automaticPerformanceCounterNames.PERCENTAGE_Of_ExceptionOccurred_To_Operation_Count + IncludeType(includeType, PerformanceCounterType.RawFraction),
                    automaticPerformanceCounterNames.PERCENTAGE_Of_ExceptionOccurred_To_Operation_Count_Base + IncludeType(includeType, PerformanceCounterType.RawBase)

                });
            if (tag.IsNullOrEmpty() != false)
            {
                return results.ToArray();
            }


            string[] tags =
                tag.Split(new char[] { ',', '|', ';' }, StringSplitOptions.RemoveEmptyEntries)
                   .Select(x => x.Trim())
                   .ToArray();

            foreach (string subOperationName in tags)
            {
                SubOpNames subOpNames = new SubOpNames(automaticPerformanceCounterNames.RootName, subOperationName);
                //Instance:

                results.AddRange(
                    new[]
                        {
                            //Instances (2):
                            subOpNames.INSTANCE_SubOperation_Counter  + IncludeType(includeType, PerformanceCounterType.NumberOfItems64),
                            subOpNames.INSTANCE_SubOperation_Duration  + IncludeType(includeType, PerformanceCounterType.NumberOfItems64),
                            subOpNames.PERCENTAGE_Of_SubOperation_Duration_To_Operation_Duration  + IncludeType(includeType, PerformanceCounterType.NumberOfItems64),
                        });
            }

            return results.ToArray();

        }






        private static string IncludeType(bool includeType, params PerformanceCounterType[] performanceCounterTypes)
        {
            if (!includeType)
            {
                return string.Empty;
            }
            return ".[" +
                   performanceCounterTypes.Select(x => Enum.GetName(typeof(PerformanceCounterType), x)).JoinSafely(",") +
                   "]";
        }


        //Entry point from case's above:
#pragma warning disable 1591
        public void SetPerformanceCountersAutomatically(string performanceCategoryName, string performanceCounterNameRoot, string tag, TimeSpan duration,
#pragma warning restore 1591
 bool isException, bool? isSuccess, bool resetSubOperationCounters=false)
        {


            PerformanceCounterNames automaticPerformanceCounterNames = new PerformanceCounterNames(performanceCounterNameRoot);

            SetCoreOperationPerformancCounters(performanceCategoryName, automaticPerformanceCounterNames, duration, isException, isSuccess);

            SetTaggedSubOperationPerformanceCounters(performanceCategoryName, automaticPerformanceCounterNames, tag, duration, resetSubOperationCounters);
        }



        private void SetCoreOperationPerformancCounters(string performanceCategoryName, PerformanceCounterNames automaticPerformanceCounterNames, TimeSpan duration,
                                                        bool isException, bool? isSuccess)
        {

            //Increment the base counter name (without suffix of any kind):
            _performanceCounterService.Offset(performanceCategoryName, automaticPerformanceCounterNames.RootName, null, amount: 1, raiseExceptionIfNotFound: false);

            //Now do the Instance, Rate, Average, and Ratios:
            //INSTANCE:
            _performanceCounterService.Offset(performanceCategoryName, automaticPerformanceCounterNames.INSTANCE_Operation_Counter, null, amount: 1);
            //RATE
            _performanceCounterService.Offset(performanceCategoryName, automaticPerformanceCounterNames.RATE_Operations_PerSec, 1);
            //AVERAGES
            _performanceCounterService.Offset(performanceCategoryName, automaticPerformanceCounterNames.AVERAGE_AverageDuration, null, amount: duration.Ticks);
            _performanceCounterService.Offset(performanceCategoryName, automaticPerformanceCounterNames.AVERAGE_AverageDuration_Base, null, amount: 1);
            //RATIOS
            string counterName;
            //Pair:
            if (isSuccess.HasValue)
            {
                counterName = (isSuccess.Value)
                                         ? automaticPerformanceCounterNames.PERCENTAGE_Of_ResponseSuccessful_To_Operation_Count
                                         : automaticPerformanceCounterNames.PERCENTAGE_Of_ResponseNonSuccessful_To_Operation_Count;
                _performanceCounterService.Offset(performanceCategoryName, counterName, 1);
            }
            _performanceCounterService.Offset(performanceCategoryName, automaticPerformanceCounterNames.PERCENTAGE_Of_ResponseSuccessful_To_Operation_Count_Base, 1);
            _performanceCounterService.Offset(performanceCategoryName, automaticPerformanceCounterNames.PERCENTAGE_Of_ResponseNonSuccessful_To_Operation_Count_Base, 1);
            //Pair:
            counterName = (isException)
                                     ? automaticPerformanceCounterNames.PERCENTAGE_Of_ExceptionOccurred_To_Operation_Count
                                     : automaticPerformanceCounterNames.PERCENTAGE_Of_NoExceptionOccurred_To_Operation_Count;
            _performanceCounterService.Offset(performanceCategoryName, counterName, 1);
            _performanceCounterService.Offset(performanceCategoryName, automaticPerformanceCounterNames.PERCENTAGE_Of_NoExceptionOccurred_To_Operation_Count_Base, 1);
            _performanceCounterService.Offset(performanceCategoryName, automaticPerformanceCounterNames.PERCENTAGE_Of_ExceptionOccurred_To_Operation_Count_Base, 1);
        }






























        private void SetTaggedSubOperationPerformanceCounters(string performanceCategoryName, PerformanceCounterNames automaticPerformanceCounterNames, string subOperationNames, TimeSpan parentOperationTimeSpan, bool resetSubOperationCounters)
        {
            if (subOperationNames.IsNullOrEmpty())
            {
                return;
            }


            string[] subOperationNamesArray = subOperationNames.Split(new char[] { ',', '|', ';' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();

            foreach (string subOperationName in subOperationNamesArray)
            {
                SubOpNames subOpNames = new SubOpNames(automaticPerformanceCounterNames.RootName, subOperationName);


                //INSTANCE:
                _performanceCounterService.Set(performanceCategoryName, subOpNames.INSTANCE_SubOperation_Counter, null,
                                          _contextStatePerformanceCounterService.GetContextOperationCounter(subOpNames.SubOperationName));

                //PERCENTAGE:
                TimeSpan subObDuration = _contextStatePerformanceCounterService.GetContextOperationDuration(subOpNames.SubOperationName);

                float tmp = ((parentOperationTimeSpan.Ticks == 0) ? 0 : subObDuration.Ticks / (float)parentOperationTimeSpan.Ticks) * 100;
                _performanceCounterService.Set(performanceCategoryName, subOpNames.PERCENTAGE_Of_SubOperation_Duration_To_Operation_Duration, null, (long)tmp);


                if (resetSubOperationCounters)
                {
                    //Reset for next operation when within a non-stateless environment:
                    _contextStatePerformanceCounterService.ResetContextOperationCounter(subOpNames.SubOperationName);
                    _contextStatePerformanceCounterService.ResetContextOperationDuration(subOpNames.SubOperationName);
                }
            }
        }




        private static class Constants
        {
            public static string NoExceptionOccurred = ".NoExceptionOccurred";
            public static string ExceptionOccurred = ".ExceptionOccurred";
            public static string ResponseSuccessful = ".ResponseSuccessful";
            public static string ResponseNonSuccessful = ".ResponseNonSuccessful";
            public static string Count = ".Count";
            public static string PerSec = ".PerSec";
            //public static string AveragePerSec = ".AveragePerSec";
            public static string Duration = ".Duration";
            public static string Percentage = ".Percentage";
            //public static string Ratio = ".Ratio";
            public static string To = ".To";
            public static string AverageDuration = ".AverageDuration";
            //public static string Tag = "Tag";
            //public static string TagPlaceHolder = ".[TAG]";
            public static string Base = ".Base";
            public static string Of = ".Of";
            public static string Operation = ".Operation";
            public static string SubOperation = ".SubOperation";

            public static string PercentageCounterStringTemplate = "{0}" + Constants.Percentage + Constants.Of +
                                                                   "{1}" + Constants.To + "{2}";



        }

        private class PerformanceCounterNames
        {


            //ROOT:
            public readonly string RootName;
            //INSTANCES:
            public readonly string INSTANCE_Operation_Counter;
            public readonly string INSTANCE_Operation_Duration;
            //RATE:
            public readonly string RATE_Operations_PerSec;
            //AVERAGES:
            public readonly string AVERAGE_AverageDuration;
            public readonly string AVERAGE_AverageDuration_Base;
            //Percentages/Ratios
            public readonly string PERCENTAGE_Of_ResponseSuccessful_To_Operation_Count;
            public readonly string PERCENTAGE_Of_ResponseSuccessful_To_Operation_Count_Base;
            public readonly string PERCENTAGE_Of_ResponseNonSuccessful_To_Operation_Count;
            public readonly string PERCENTAGE_Of_ResponseNonSuccessful_To_Operation_Count_Base;
            public readonly string PERCENTAGE_Of_NoExceptionOccurred_To_Operation_Count;
            public readonly string PERCENTAGE_Of_NoExceptionOccurred_To_Operation_Count_Base;
            public readonly string PERCENTAGE_Of_ExceptionOccurred_To_Operation_Count;
            public readonly string PERCENTAGE_Of_ExceptionOccurred_To_Operation_Count_Base;


            public PerformanceCounterNames(string baseName)
            {
                RootName = baseName;
                //Instances
                INSTANCE_Operation_Counter = RootName + Constants.Operation + Constants.Count;
                INSTANCE_Operation_Duration = RootName + Constants.Operation + Constants.Duration;
                //Rates
                RATE_Operations_PerSec = RootName + Constants.Operation + Constants.PerSec;
                //Averages
                AVERAGE_AverageDuration = RootName + Constants.Operation + Constants.AverageDuration;
                AVERAGE_AverageDuration_Base = AVERAGE_AverageDuration + Constants.Base;
                //Percentages/Ratios
                PERCENTAGE_Of_ResponseSuccessful_To_Operation_Count = Constants.PercentageCounterStringTemplate.FormatStringInvariantCulture(RootName, Constants.ResponseSuccessful, Constants.Operation + Constants.Count);
                PERCENTAGE_Of_ResponseSuccessful_To_Operation_Count_Base = PERCENTAGE_Of_ResponseSuccessful_To_Operation_Count + Constants.Base;
                PERCENTAGE_Of_ResponseNonSuccessful_To_Operation_Count = Constants.PercentageCounterStringTemplate.FormatStringInvariantCulture(RootName, Constants.ResponseNonSuccessful, Constants.Operation + Constants.Count);
                PERCENTAGE_Of_ResponseNonSuccessful_To_Operation_Count_Base = PERCENTAGE_Of_ResponseNonSuccessful_To_Operation_Count + Constants.Base;
                PERCENTAGE_Of_NoExceptionOccurred_To_Operation_Count = Constants.PercentageCounterStringTemplate.FormatStringInvariantCulture(RootName, Constants.NoExceptionOccurred, Constants.Operation + Constants.Count);
                PERCENTAGE_Of_NoExceptionOccurred_To_Operation_Count_Base = PERCENTAGE_Of_NoExceptionOccurred_To_Operation_Count + Constants.Base;
                PERCENTAGE_Of_ExceptionOccurred_To_Operation_Count = Constants.PercentageCounterStringTemplate.FormatStringInvariantCulture(RootName, Constants.ExceptionOccurred, Constants.Operation + Constants.Count);
                PERCENTAGE_Of_ExceptionOccurred_To_Operation_Count_Base = PERCENTAGE_Of_ExceptionOccurred_To_Operation_Count + Constants.Base;
            }
        }

        /// <summary>
        /// 
        /// </summary>
#pragma warning disable 1591

        public class SubOpNames
        {
            public readonly string OperationName;
            public readonly string SubOperationName;
            public readonly string CombinedNameRoot;

            public readonly string INSTANCE_SubOperation_Counter;
            public readonly string INSTANCE_SubOperation_Duration;

            public readonly string PERCENTAGE_Of_SubOperation_Duration_To_Operation_Duration;
            /// <summary>
            /// Initializes a new instance of the <see cref="SubOpNames"/> class.
            /// </summary>
            /// <param name="operationName">Name of the operation.</param>
            /// <param name="subOpName">Name of the sub op.</param>
            public SubOpNames(string operationName, string subOpName)
            {
                OperationName = operationName;
                SubOperationName = subOpName;
                CombinedNameRoot = OperationName + "[" + SubOperationName + "]";

                INSTANCE_SubOperation_Counter = CombinedNameRoot + Constants.Operation + Constants.Count;
                INSTANCE_SubOperation_Duration = CombinedNameRoot + Constants.Operation + Constants.Duration;

                PERCENTAGE_Of_SubOperation_Duration_To_Operation_Duration = Constants.PercentageCounterStringTemplate.FormatStringInvariantCulture(CombinedNameRoot, Constants.SubOperation + Constants.Duration, Constants.Operation + Constants.Duration);
            }



        }
#pragma warning restore 1591

    }
}