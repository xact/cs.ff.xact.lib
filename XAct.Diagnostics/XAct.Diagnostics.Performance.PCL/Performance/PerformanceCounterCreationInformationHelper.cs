﻿namespace XAct.Diagnostics.Performance
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using XAct.Diagnostics.Performance.Services.Entities;

    /// <summary>
    /// 
    /// </summary>
    public static class PerformanceCounterCreationInformationFactory
    {


        /// <summary>
        /// Builds the specified common counter type.
        /// </summary>
        /// <param name="commonCounterType">Type of the common counter.</param>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        public static PerformanceCounterCreationInformation[] Build(CommonCounterType commonCounterType, string token)
        {

            switch (commonCounterType)
            {
                case CommonCounterType.OperationsPerSecondCounter:
                    return OperationsPerSecondCounter(token);
                case CommonCounterType.OperationsPerformedCounter:
                    return OperationsPerformedCounter(token);
                case CommonCounterType.OperationElapsedTimeCounter:
                    return OperationElapsedTimeCounter(token);
                case CommonCounterType.OperationAverageTimeCounterPair:
                    return OperationAverageTimeCounterPair(token);
                case CommonCounterType.AverageItemsProcessedPerOperationCounterPair:
                    return AverageItemsProcessedPerOperationCounterPair(token);
                default:
                    throw new ArgumentOutOfRangeException("commonCounterType");
            }

        }

        /// <summary>
        /// Operationses the per second counter.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <param name="nameTemplate">The name template.</param>
        /// <param name="descriptionTemplate">The description template.</param>
        /// <returns></returns>
        private static PerformanceCounterCreationInformation[] OperationsPerSecondCounter(
            string token,
            string nameTemplate = "{0} Operations Per Second",
            string descriptionTemplate = "Number of '{0}' Operations per second.")
        {

            string formattedName = string.Format(CultureInfo.InvariantCulture, nameTemplate, token);
            string formattedDescription = string.Format(CultureInfo.InvariantCulture, descriptionTemplate, token);

            List<PerformanceCounterCreationInformation> counterCreationDatas = new List
                <PerformanceCounterCreationInformation>
                {
                    new PerformanceCounterCreationInformation(
                        formattedName,
                        formattedDescription,
                        PerformanceCounterType.RateOfCountsPerSecond32)
                };

            return counterCreationDatas.ToArray();
        }






        private static PerformanceCounterCreationInformation[] OperationsPerformedCounter(
            string token,
            string nameTemplate = "{0} Operations Performed",
            string descriptionTemplate = "Number of {0} Operations performed since process started."
            )
        {
            string formattedName = string.Format(CultureInfo.InvariantCulture, nameTemplate, token);
            string formattedDescription = string.Format(CultureInfo.InvariantCulture, descriptionTemplate, token);

            List<PerformanceCounterCreationInformation> counterCreationDatas = new List
                <PerformanceCounterCreationInformation>
                {
                    new PerformanceCounterCreationInformation(
                        formattedName,
                        formattedDescription,
                        PerformanceCounterType.NumberOfItems32)
                };

            return counterCreationDatas.ToArray();
        }


        private static PerformanceCounterCreationInformation[] OperationElapsedTimeCounter(
            string token,
            string nameTemplate = "{0} Elapsed Time",
            string descriptionTemplate = "Total amount of time elapsed since process started."
            )
        {
            string formattedName = string.Format(CultureInfo.InvariantCulture, nameTemplate, token);
            string formattedDescription = string.Format(CultureInfo.InvariantCulture, descriptionTemplate, token);

            List<PerformanceCounterCreationInformation> counterCreationDatas = new List
                <PerformanceCounterCreationInformation>
                {
                    new PerformanceCounterCreationInformation(
                        formattedName,
                        formattedDescription,
                        PerformanceCounterType.ElapsedTime)
                };
            return counterCreationDatas.ToArray();
        }


        /// <summary>
        /// Gets the <see cref="PerformanceCounterCreationInformation" />
        /// necessary to register the
        /// <see cref="IPerformanceCounter" />s.
        /// </summary>
        /// <returns></returns>
        private static PerformanceCounterCreationInformation[] OperationAverageTimeCounterPair(
            string token,
            string nameTemplate = "{0} Operation Average Time",
            string descriptionTemplate = "Average time elapsed per {0} operation."
            )
        {

            string formattedName = string.Format(CultureInfo.InvariantCulture, nameTemplate, token);
            string formattedDescription = string.Format(CultureInfo.InvariantCulture, descriptionTemplate, token);

            List<PerformanceCounterCreationInformation> counterCreationDatas = new List
                <PerformanceCounterCreationInformation>
                {
                    new PerformanceCounterCreationInformation(
                        formattedName, //="XYZ Average time per operation"
                        formattedDescription, //Average duration per operation execution"
                        PerformanceCounterType.AverageTimer32),

                    new PerformanceCounterCreationInformation(
                        formattedName + " (Base)", //="XYZ Average time per operation "
                        formattedDescription + " (Base)", //Average duration per operation execution"
                        PerformanceCounterType.AverageBase)
                };


            return counterCreationDatas.ToArray();
        }





        /// <summary>
        /// Gets the <see cref="Services.Entities.PerformanceCounterCreationInformation" />
        /// necessary to register the
        /// <see cref="IPerformanceCounter" />s.
        /// </summary>
        /// <returns></returns>
        private static PerformanceCounterCreationInformation[] AverageItemsProcessedPerOperationCounterPair(
            string token,
            string nameTemplate = "{0} Average Items processed per Operation",
            string descriptionTemplate = "Average number of Items processed per {0} Operations."
            )
        {
            string formattedName = string.Format(CultureInfo.InvariantCulture, nameTemplate, token);
            string formattedDescription = string.Format(CultureInfo.InvariantCulture, descriptionTemplate, token);



            //FROM: http://msdn.microsoft.com/en-us/library/system.diagnostics.performancecountertype.aspx
            //When instrumenting applications (creating and writing custom performance counters), 
            //you might be working with performance counter types that rely on an accompanying 
            //base counter that is used in the calculations. 
            //The base counter must be immediately after its associated counter in 
            //the CounterCreationDataCollection collection your application uses. 

            List<PerformanceCounterCreationInformation> counterCreationDatas = new List
                <PerformanceCounterCreationInformation>
                {
                    new PerformanceCounterCreationInformation(
                        formattedName, //="XYZ Average time per operation"
                        formattedDescription, //Average duration per operation execution"
                        PerformanceCounterType.AverageCount64),

                    new PerformanceCounterCreationInformation(
                        formattedName + " (Base)", //="XYZ Average time per operation "
                        formattedDescription + " (Base)", //Average duration per operation execution"
                        PerformanceCounterType.AverageBase)
                };


            return counterCreationDatas.ToArray();
        }


    }
}