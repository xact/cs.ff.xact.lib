﻿namespace XAct.Interception.CallHandlers
{
    using System;

#pragma warning disable 1591
    public interface IAutoPerformanceCounterSetter
    {

        void SetPerformanceCountersAutomatically(string performanceCategoryName,
                                                 string performanceCounterNameRoot, string tag, TimeSpan duration,
                                                 bool isException, bool? isSuccess, bool resetSubOperationCounters=false);

    }
#pragma warning restore 1591
}