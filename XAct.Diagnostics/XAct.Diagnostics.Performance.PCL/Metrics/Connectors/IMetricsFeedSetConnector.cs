﻿namespace XAct.Diagnostics.Metrics.Services.Configuration
{
    using System;

    
    /// <summary>
    /// Contract for Connectors that retrieve Metrics.
    /// </summary>
    public interface IMetricsFeedSetConnector
    {
        /// <summary>
        /// Gets the unique name for this metric source.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; }

        /// <summary>
        /// Get the performance Metric
        /// </summary>
        /// <returns></returns>
        double GetValue();
    }

    //public class MetricsFeedSetConnector : IMetricsFeedSetConnector
    //{
    //    public string Name { get; private set; }
    //    public double GetValue()
    //    {
    //        throw new NotImplementedException();
    //    }
    //}
}