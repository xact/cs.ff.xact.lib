﻿namespace XAct.Diagnostics.Metrics.Services.Configuration.Implementations
{
    using System;
    using XAct.Diagnostics.Metrics.Services.Configuration;

    /// <summary>
    /// A generic implementation of <see cref="IMetricsFeedSetConnector"/>
    /// where you can define the function to your own specifications.
    /// </summary>
    public class GenericMetricsFeedSetConnector : IMetricsFeedSetConnector
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMetricsFeedSetConnector" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="funcToReadPerformance">The function to read performance.</param>
        public GenericMetricsFeedSetConnector(string name, Func<long> funcToReadPerformance)
        {
            _name = name;
            _funcToReadPerformance = funcToReadPerformance;
        }

        /// <summary>
        /// Gets the unique name for this metric source.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; private set; }

        /// <summary>
        /// Get the performance Metric
        /// </summary>
        /// <returns></returns>
        public double GetValue()
        {
            return _funcToReadPerformance.Invoke(); 
        }

        private readonly string _name;
        Func<long> _funcToReadPerformance;
    }
}