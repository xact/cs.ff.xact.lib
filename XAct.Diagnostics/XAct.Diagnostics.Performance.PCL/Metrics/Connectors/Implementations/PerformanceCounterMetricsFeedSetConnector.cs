﻿namespace XAct.Diagnostics.Metrics.Services.Configuration.Implementations
{
    using XAct.Diagnostics.Metrics.Services.Configuration;
    using XAct.Diagnostics.Performance;

    /// <summary>
    /// An implementation of <see cref="IMetricsFeedSetConnector"/>
    /// for reading performance counters.
    /// </summary>
    public class PerformanceCounterMetricsFeedSetConnector :IMetricsFeedSetConnector
    {
        private readonly IPerformanceCounterService _performanceCounterService;
        private readonly string _performanceCounterCategory;
        private readonly string _performanceCounterName;
        private readonly double _multiplier;

        /// <summary>
        /// Gets the unique name for this metric source.
        /// <para>
        /// In this case it will be 'PerformanceCategoryName:PerformanceCounterName'
        /// </para>
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Get the performance Metric
        /// </summary>
        /// <returns></returns>
        public double GetValue()
        {
            double initialResult =
                _performanceCounterService.GetComputedValue<long>(_performanceCounterCategory,
                                                                  _performanceCounterName, 
                                                                  null);

            double result = (initialResult*_multiplier);

            return result;
        }

        /// <summary>
        /// Performances the counter feed service configuration.
        /// </summary>
        /// <param name="performanceCounterService">The performance counter service.</param>
        /// <param name="performanceCounterCategory">The performance counter category.</param>
        /// <param name="performanceCounterName">Name of the performance counter.</param>
        /// <param name="multiplier">The multiplier.</param>
        public PerformanceCounterMetricsFeedSetConnector(IPerformanceCounterService performanceCounterService, string performanceCounterCategory, string performanceCounterName, double multiplier=1.00)
        {
            Name = performanceCounterCategory + ":" + performanceCounterName;

            _performanceCounterService = performanceCounterService;
            _performanceCounterCategory = performanceCounterCategory;
            _performanceCounterName = performanceCounterName;
            _multiplier = multiplier;
        }


    }
}