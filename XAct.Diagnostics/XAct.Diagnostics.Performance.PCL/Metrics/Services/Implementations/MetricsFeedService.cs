﻿namespace XAct.Diagnostics.Metrics.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using XAct.Diagnostics.Metrics.Services.Configuration;
    using XAct.Diagnostics.Metrics.Services.State;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Environment;
    using XAct.Services;




    /// <summary>
    /// Implementation of the
    /// <see cref="IMetricsFeedService" />
    /// to return the values of requested Performance Counters.
    /// </summary>
    public class MetricsFeedService : XActLibServiceBase, IMetricsFeedService
    {
        private readonly IDateTimeService _dateTimeService;
        private readonly IMetricsFeedServiceState _metricsFeedServiceState;


        /// <summary>
        /// Gets the configuration object for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        public IMetricsFeedServiceConfiguration Configuration { get; private set; }

        private Timer _timer;

        //20 minutes * 250 millisecond intervals...which is a *lot*.
        private int _maxQueueSizeAllowed = 60*20*4;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetricsFeedService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="dateTimeService">The date time service.</param>
        /// <param name="performanceCounterFeedServiceState">State of the performance counter feed service.</param>
        /// <param name="performanceCounterFeedServiceConfiguration">The performance counter feed service configuration.</param>
        public MetricsFeedService(
            ITracingService tracingService,
            IDateTimeService dateTimeService,
            IMetricsFeedServiceState performanceCounterFeedServiceState,
            IMetricsFeedServiceConfiguration performanceCounterFeedServiceConfiguration)
            : base(tracingService)
        {
            _dateTimeService = dateTimeService;
            _metricsFeedServiceState = performanceCounterFeedServiceState;
            Configuration = performanceCounterFeedServiceConfiguration;

            //See: http://stackoverflow.com/questions/12555049/timer-in-portable-library
            //See: http://msdn.microsoft.com/en-us/library/system.threading.timercallback(v=vs.110).aspx
            TimerCallback tcb = Check;

            _timer = new Timer(tcb, null, TimeSpan.Zero,
                               Configuration.IntervalTimeSpan);

        }


        private void Check(Object o)
        {
            DateTime nowUtc = _dateTimeService.NowUTC;

            //loop through each Source/Connector defined in config, 
            foreach (IMetricsFeedSetConnector metricsFeedSourceConnector in Configuration.MetricFeedSourceConnector)
            {

                //For each one, we need a queue to record we will be collecting
                MetricsSeriesEntryQueue metricsQueue;

                if (!_metricsFeedServiceState.DataPoints.TryGetValue(metricsFeedSourceConnector.Name,
                                                                               out metricsQueue))
                {
                    //Not found -- make a new one:
                    metricsQueue = new MetricsSeriesEntryQueue {Name = metricsFeedSourceConnector.Name};
                    _metricsFeedServiceState.DataPoints.Add(metricsQueue.Name, metricsQueue);
                }

                //Use the connector's function to get the value:
                double value = metricsFeedSourceConnector.GetValue();


                //If there are any notes, attach them, 
                //Note that "MyPerfCategoryName:MyPerCounterName" should be an exact match
                //But you could attach a note to all variables with "MyPerfCategoryName"
                //or "*"
                //or "" which is the same thing.
                string currentTag = null;
                foreach (KeyValuePair<string, string> kvp in _metricsFeedServiceState.CurrentTags)
                {
                    if (metricsFeedSourceConnector.Name.CompareWildcard(kvp.Key,true))
                    {
                        currentTag=kvp.Value;
                    }
                }
                
                //Once we have the Time, Value, and maybe even a tag, 
                //we record it in the queue:
                metricsQueue.Enqueue(
                    new MetricsSerieEntry
                        {
                            DateTime = nowUtc,
                            Value = value,
                            Tag = currentTag,
                        });

            }

            //Whatever the tags were, clear them.
            _metricsFeedServiceState.CurrentTags.Clear();
        }






        /// <summary>
        /// Gets the specified series.
        /// </summary>
        /// <param name="seriesNames">The series names.</param>
        /// <param name="startDateTimeUtc">The last date time UTC.</param>
        /// <param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns></returns>
        public StatusResponseMetricsSerie[] Get(string[] seriesNames, DateTime? startDateTimeUtc = null,
                                    DateTime? endDateTimeUtc = null)
        {
            //const int minuteMilleconds = 60*1000;
            if (!endDateTimeUtc.HasValue)
            {
                endDateTimeUtc = _dateTimeService.NowUTC;
            }

            //Make the delta between now and the requested begining point in time
            //no more than 20 minutes, preferring 2 minutes.
            startDateTimeUtc = CalculateValidStartDateTime(startDateTimeUtc, endDateTimeUtc.Value);

            var result = GetDataSet(seriesNames, startDateTimeUtc.Value, endDateTimeUtc.Value);

            return result;
        }

        //Want a starttime that is not too far in the past (that would make too many datapoints)
        private DateTime? CalculateValidStartDateTime(DateTime? startDateTimeUtc, DateTime endDateTimeUtc)
        {
            #region X

            //Make the delta between now and the requested begining point in time
            //no more than 20 minutes, preferring 2 minutes.
            if (!startDateTimeUtc.HasValue)
            {
                startDateTimeUtc = endDateTimeUtc.Subtract(TimeSpan.FromMinutes(2));
            }

            startDateTimeUtc = startDateTimeUtc.Value.ToUniversalTime();

            if (startDateTimeUtc < endDateTimeUtc.Subtract(TimeSpan.FromMinutes(20)))
            {
                startDateTimeUtc = endDateTimeUtc.Subtract(TimeSpan.FromMinutes(20));
            }

            #endregion

            //Calc difference between requested start and now:
            TimeSpan deltaTimeSpan = endDateTimeUtc.Subtract(startDateTimeUtc.Value);

            //20mins * 60secs * 10 = 100ms for 20 minutes = 12000
            int maxSeriesItems =
                (int) deltaTimeSpan.TotalMilliseconds
                /(int) Configuration.IntervalTimeSpan.TotalMilliseconds;



            //Request at least 1 item:
            if (maxSeriesItems < 1)
            {
                maxSeriesItems = 1;
            }

            //We also don't want to send an increadibly large amount of points.
            //This ensures that we start with a time that is no more than 12000 items back:
            //20 minutes * 60 seconds * 4 = 4800 
            if (maxSeriesItems > _maxQueueSizeAllowed)
            {
                startDateTimeUtc =
                    endDateTimeUtc.Subtract(
                        TimeSpan.FromMilliseconds(12000*
                                                  Configuration.IntervalTimeSpan
                                                                                             .TotalMilliseconds));
            }
            return startDateTimeUtc;
        }


        private StatusResponseMetricsSerie[] GetDataSet(IEnumerable<string> seriesNames, DateTime startDateTimeUtc,
                                               DateTime endDateTimeUtc)
        {

            List<StatusResponseMetricsSerie> results = new List<StatusResponseMetricsSerie>();
            foreach (string seriesName in seriesNames)
            {
                MetricsSeriesEntryQueue queue;

                if (!_metricsFeedServiceState.DataPoints.TryGetValue(seriesName, out queue))
                {

                    //No points, so just move on to next:
                    continue;
                }

                var tmp = GetDataSet(queue, startDateTimeUtc, endDateTimeUtc);

                results.Add(new StatusResponseMetricsSerie
                    {
                        Name = seriesName,
                        Items = tmp.Select(x => new MetricsSerieEntry { DateTime = x.DateTime, Value = x.Value, Tag = x.Tag }).ToArray()
                    });
            }

            return results.ToArray();
        }



        private MetricsSerieEntry[] GetDataSet(MetricsSeriesEntryQueue q, DateTime startDateTimeUtc,
                                               DateTime endDateTimeUtc)
        {

            bool foundSome = false;
            List<MetricsSerieEntry> results = new List<MetricsSerieEntry>();

            foreach (MetricsSerieEntry x in q.Reverse())
            {
                if (x.DateTime >= startDateTimeUtc && x.DateTime < endDateTimeUtc)
                {
                    results.Add(x);
                    if (!foundSome)
                    {
                        foundSome = true;
                    }
                }
                else
                {
                    if (foundSome)
                    {
                        break;
                    }
                }
            }

            //We don't want to *lock* too often.
            if (q.Count() > 2 * _maxQueueSizeAllowed)
            {
                //cleanup:
                lock (this)
                {
                    while (q.Count > _maxQueueSizeAllowed)
                    {
                        q.Dequeue();
                    }
                }
            }
            return results.ToArray();

        }

        //private PerformanceSet[] GetRandomDataSet(IEnumerable<string> seriesNames, DateTime startDateTimeUtc,
        //                                          DateTime endDateTimeUtc)
        //{


        //    //Get a list of the dateTime moments in between start and end:
        //    DateTime[] intervals = startDateTimeUtc.GetIntervalsBetweenDateTimes(endDateTimeUtc,
        //                                                                         (int)
        //                                                                         Configuration
        //                                                                             .IntervalTimeSpan.TotalMilliseconds)
        //                                           .ToArray();

        //    //Iterate through seriesNames, creating a new PerformanceSet each time.
        //    var result =
        //        seriesNames.Select(seriesName => new PerformanceSet
        //            {
        //                Name = seriesName,
        //                Items = intervals.Select(dateTime =>
        //                                         new SeriesEntry
        //                                             {
        //                                                 Key = dateTime.DateTimeToUnixTimeStamp(),
        //                                                 Value = _random.Next(100)
        //                                             }).ToArray()
        //            }).ToArray();

        //    return result;
        //}

    }





}

