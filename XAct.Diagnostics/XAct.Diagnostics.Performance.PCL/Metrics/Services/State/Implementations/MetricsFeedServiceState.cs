﻿namespace XAct.Diagnostics.Metrics.Services.State.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="IMetricsFeedServiceState"/>
    /// to return requested performanceCounter values.
    /// </summary>
    public class MetricsFeedServiceState : IMetricsFeedServiceState
    {

        //private Random _random = new Random();

        /// <summary>
        /// Gets the Set of collected datapoints.
        /// </summary>
        /// <value>
        /// The data points.
        /// </value>
        public Dictionary<string, MetricsSeriesEntryQueue> DataPoints
        {
            get { return _dataPoints; }
        }

        private Dictionary<string, MetricsSeriesEntryQueue> _dataPoints = new Dictionary<string, MetricsSeriesEntryQueue>();



        /// <summary>
        /// Dictionary of Tags that will be applied to next counters picked up.
        /// <para>
        /// Dictionary will be cleared, ready for next data collection moment.
        /// </para>
        /// </summary>
        public Dictionary<string, string> CurrentTags { get { return _tags; } }
        private Dictionary<string, string> _tags = new Dictionary<string, string>();

    }
}