﻿namespace XAct.Diagnostics.Metrics.Services.State
{
    using System.Collections.Generic;
    using XAct.Diagnostics.Services.Implementations;

    /// <summary>
    /// Contract for a singleton to contain state 
    /// for instances of 
    /// <see cref="IMetricsFeedService"/>
    /// </summary>
    public interface IMetricsFeedServiceState : IHasXActLibServiceState
    {

        /// <summary>
        /// Gets the Set of collected datapoints.
        /// </summary>
        /// <value>
        /// The data points.
        /// </value>
        Dictionary<string, MetricsSeriesEntryQueue> DataPoints { get; }


        /// <summary>
        /// Dictionary of Tags that will be applied to next counters picked up.
        /// <para>
        /// Dictionary will be cleared, ready for next data collection moment.
        /// </para>
        /// </summary>
        Dictionary<string, string> CurrentTags { get; } 
    }
}