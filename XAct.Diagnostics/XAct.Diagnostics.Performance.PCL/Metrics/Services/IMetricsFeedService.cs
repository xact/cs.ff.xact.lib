﻿namespace XAct.Diagnostics.Metrics.Services
{
    using System;
    using XAct.Diagnostics.Metrics.Services.Configuration;
    using XAct.Diagnostics.Services.Implementations;


    /// <summary>
    /// Contract for a service to return the values of requested Performance Counters.
    /// </summary>
    public interface IMetricsFeedService : IHasXActLibService
    {

        /// <summary>
        /// Gets the configuration object for this service.
        /// </summary>
        /// <value>
        /// The configuration.
        /// </value>
        IMetricsFeedServiceConfiguration Configuration { get; }

        /// <summary>
        /// Gets the specified series.
        /// </summary>
        /// <param name="seriesNames">The series names.</param>
        /// <param name="startDateTimeUtc">The last date time UTC.</param>
        /// <param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns></returns>
        StatusResponseMetricsSerie[] Get(string[] seriesNames, DateTime? startDateTimeUtc = null,
                                        DateTime? endDateTimeUtc = null);

    }
}
