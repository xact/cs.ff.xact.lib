﻿namespace XAct.Diagnostics.Metrics.Services.Configuration
{
    using System;
    using System.Collections.Generic;



    /// <summary>
    /// Contract for a configuratino package to 
    /// configure an instance of 
    /// <see cref="IMetricsFeedService"/>
    /// </summary>
    public interface IMetricsFeedServiceConfiguration : IHasXActLibServiceConfiguration
    {
        /// <summary>
        /// The interval between getting performance counter values.
        /// <para>
        /// The minimum interval is 1 second.
        /// </para>
        /// </summary>
        TimeSpan IntervalTimeSpan { get; set; }

        /// <summary>
        /// Gets or sets the maximum interval of time to keep Counter values in memory.
        /// </summary>
        /// <value>
        /// The maximum interval is 10 minutes.
        /// </value>
        TimeSpan MaxInterval { get; set; }

        /// <summary>
        /// Collection of the <see cref="IMetricsFeedSetConnector" />s
        /// to sources of metrics.
        /// </summary>
        List<IMetricsFeedSetConnector> MetricFeedSourceConnector { get; }
    }

}