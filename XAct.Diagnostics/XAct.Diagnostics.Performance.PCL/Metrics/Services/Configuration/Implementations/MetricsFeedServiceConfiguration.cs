﻿namespace XAct.Diagnostics.Metrics.Services.Configuration.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics.Metrics.Services.Configuration;
    using XAct.Services;

    /// <summary>
    /// Implementation of the <see cref="IMetricsFeedServiceConfiguration"/>
    /// contract in order to configure an implementation of 
    /// <see cref="IMetricsFeedService"/>
    /// </summary>
    public class MetricsFeedServiceConfiguration : IMetricsFeedServiceConfiguration
    {
        /// <summary>
        /// The interval between getting performance counter values.
        /// <para>
        /// Default is 1 second.
        /// </para>
        /// </summary>
        public TimeSpan IntervalTimeSpan
        {
            get { return _intervalTimeSpan; }
            set
            {
                if (value < TimeSpan.FromSeconds(1))
                {
                    value = TimeSpan.FromSeconds(1);
                }
                _intervalTimeSpan = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum interval of time to keep Counter values in memory.
        /// </summary>
        /// <value>
        /// The maximum interval is 10 minutes.
        /// </value>
        public TimeSpan MaxInterval

        {
            get { return _maxInterval; }
            set
            {
                if (value > TimeSpan.FromSeconds(10*60))
                {
                    value = TimeSpan.FromSeconds(600);
                }
                _maxInterval = value;
            }
        }

        /// <summary>
        /// Collection of the <see cref="IMetricsFeedSetConnector" />s
        /// to sources of metrics.
        /// </summary>
        public List<IMetricsFeedSetConnector> MetricFeedSourceConnector { get { return _metricFeedSourceConnector ?? (_metricFeedSourceConnector = new List<IMetricsFeedSetConnector>()); } }
        private List<IMetricsFeedSetConnector> _metricFeedSourceConnector;
        private TimeSpan _intervalTimeSpan;
        private TimeSpan _maxInterval;

        /// <summary>
        /// Initializes a new instance of the <see cref="MetricsFeedServiceConfiguration"/> class.
        /// </summary>
        public MetricsFeedServiceConfiguration()
        {
            IntervalTimeSpan = TimeSpan.FromSeconds(1);
            MaxInterval = TimeSpan.FromMinutes(5);
            _metricFeedSourceConnector = new List<IMetricsFeedSetConnector>();
        }
    }
}