﻿namespace XAct.Diagnostics.Status.Services.Connectors.Implementations
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics.Metrics.Services;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Diagnostics.Status.Connectors;
    using XAct.Diagnostics.Status.Connectors.Implementations;

    /// <summary>
    /// A configuration object for 
    /// an instance of 
    /// <see cref="PerformanceCounterStatusServiceConnector"/>
    /// </summary>
    public class PerformanceCounterStatusServiceConnectorConfiguration : IStatusServiceMetricsFeedConnectorConfiguration
    {
        /// <summary>
        /// Specifications as to the Counters to initialize with <see cref="IMetricsFeedService"/>
        /// </summary>
        public List<StatusServiceMetricsFeedConnectorConfigurationItem> CounterSpecs { get { return _counterSpecs ?? (_counterSpecs = new List<StatusServiceMetricsFeedConnectorConfigurationItem>()); } }
        private List<StatusServiceMetricsFeedConnectorConfigurationItem> _counterSpecs;

    }

    /// <summary>
    /// Connector to retrieve performance counter information.
    /// </summary>
    public class PerformanceCounterStatusServiceConnector : XActLibStatusServiceConnectorBase, IStatusServiceMetricsFeedConnector
    {
        private readonly IMetricsFeedService _metricsFeedService;


        /// <summary>
        /// The configuration package of this Connector.
        /// </summary>
        public IStatusServiceMetricsFeedConnectorConfiguration Configuration { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PerformanceCounterStatusServiceConnector" /> class.
        /// </summary>
        /// <param name="metricsFeedService">The metrics feed service.</param>
        /// <param name="configuration">The configuration.</param>
        public PerformanceCounterStatusServiceConnector(
            IMetricsFeedService metricsFeedService, 
            PerformanceCounterStatusServiceConnectorConfiguration configuration) : base()
        {
            _metricsFeedService = metricsFeedService;
            Configuration = configuration;
        }



        /// <summary>
        /// Gets the <see cref="StatusResponse" />.
        /// </summary>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startTimeUtc">The start time UTC.</param>
        /// <param name="endTimeUtc">The end time UTC.</param>
        /// <returns></returns>
        public override StatusResponse Get(object arguments=null, DateTime? startTimeUtc=null, DateTime? endTimeUtc=null)
        {
            if (arguments != null)
            {
                throw new ArgumentException("{0} accepts no Arguments.".FormatStringInvariantCulture(this.GetType().Name));
            }

            //Using the base helper ensures it gets the right Title/Description
            //in the user's culture:
            StatusResponse result = base.BuildReponseObject();

            StatusResponseMetricsSeries series = result.Series;

            MakeFeed(series, null ,startTimeUtc,endTimeUtc);

            return result;
        }




        private void MakeFeed(StatusResponseMetricsSeries statusResponseMetricsSeries, object arguments, DateTime? startTimeUtc = null, DateTime? endTimeUtc = null)
        {
            var performanceCounterSeriesNames = Configuration.CounterSpecs;

List<string> names = new List<string>();
            foreach (var performanceCounterSeriesName in performanceCounterSeriesNames)
            {
                string name = performanceCounterSeriesName.GetUniqueName();
                
                names.Add(name);

            }
            var series = _metricsFeedService.Get(names.ToArray(), startTimeUtc, endTimeUtc);
            if (series != null)
            {
                statusResponseMetricsSeries.Series.AddRange(series);
            }
        }
    }
}
