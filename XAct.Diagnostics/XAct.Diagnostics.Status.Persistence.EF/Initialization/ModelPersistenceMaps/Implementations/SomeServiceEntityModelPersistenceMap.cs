//namespace XAct.Diagnostics.Status.Services.Initialization.Maps.Implementations
//{
//    using System.ComponentModel.DataAnnotations.Schema;
//    using System.Data.Entity.ModelConfiguration;
//    using XAct;
//    using XAct.Services;

//    public class SomeServiceEntityModelPersistenceMap : EntityTypeConfiguration<SomeServiceEntity>,
//                                                        ISomeServiceEntityModelPersistenceMap
//    {
//        /// <summary>
//        /// Tips the definition map.
//        /// </summary>
//        public SomeServiceEntityModelPersistenceMap()
//        {

//            this.ToXActLibTable("SomeServiceEntity");
//            this
//                .HasKey(m => new {m.Id});


//            int colOrder = 0;

//            //No need for a tennant identifier.

//            this.Property(x => x.Id)
//                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
//                .HasColumnOrder(colOrder++);


//            this.Property(x => x.Timestamp)
//                .DefineTimestamp(colOrder++);
//        }
//    }
//}
