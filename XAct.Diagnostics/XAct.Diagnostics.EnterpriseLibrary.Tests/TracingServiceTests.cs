namespace XAct.Environment.Tests
{
    using System;
    using NUnit.Framework;
    using XAct;
    using XAct.Diagnostics;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class TracingServiceTests
    {


        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
                        Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        
        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Can_Trace_Info_Level_Without_Exception()
        {
            ITracingService tracingService = DependencyResolver.Current.GetInstance<ITracingService>();

            tracingService.Trace(TraceLevel.Info,"SOme info");

            Assert.IsTrue(true);
        }
        [Test]
        public void Can_Trace_Warning_Level_Without_Exception()
        {
            ITracingService tracingService = DependencyResolver.Current.GetInstance<ITracingService>();

            tracingService.Trace(TraceLevel.Warning, "SOme warning");

            Assert.IsTrue(true);
        }
    }


}


