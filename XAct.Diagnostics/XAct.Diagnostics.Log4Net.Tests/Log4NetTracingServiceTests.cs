namespace XAct.Diagnostics.Tests.UnitTests
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using NUnit.Framework;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class Log4NetTracingServiceTests
    {
            //matching what was defined in log4net.config
            const string outputDir = @"C:\TMP\UnitTesting\myapp";
            const string outputFile = outputDir + @"\log4net.log.txt";

        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            
            if (!Directory.Exists(outputDir))
            {
                System.IO.Directory.CreateDirectory(outputDir);
            }

            Singleton<IocContext>.Instance.ResetIoC();
        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }
        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }

        //TEST:FAILING
        [Test]
        public void CanGetITracingService()
        {
            ITracingService service = XAct.DependencyResolver.Current.GetInstance<ITracingService>();
            
            Assert.IsNotNull(service);
        }
        [Test]
        public void CanFindTraceListener()
        {

            TraceListener result = null;
            foreach (TraceListener traceListener in Trace.Listeners)
            {
                if (traceListener.Name == "myLog4NetTraceListener")
                {
                    result = traceListener;
                    break;
                }
            }
            Assert.IsNotNull(result);
        }
        [Test]
        public void CanTrace()
        {
            System.Diagnostics.Trace.Flush();


            System.Diagnostics.Trace.TraceInformation("Foo");
            System.Diagnostics.Trace.Flush();

            //Did not blow up.
            Assert.IsTrue(true);
        }

        [Test]
        public void TraceFileIsCreated()
        {
            System.Diagnostics.Trace.Flush();



            System.Diagnostics.Trace.TraceInformation("Foo");
            System.Diagnostics.Trace.Flush();

            var fileInfo = new FileInfo(outputFile);
            Assert.IsTrue(fileInfo.Exists);

        }
        [Test]
        public void TraceFileGetsBiggerWhenWeTrace()
        {
            System.Diagnostics.Trace.Flush();


            var fileInfo = new FileInfo(outputFile);
            long size1 = (fileInfo.Exists) ? fileInfo.Length : 0;

            System.Diagnostics.Trace.TraceInformation("Foo");
            System.Diagnostics.Trace.Flush();

            Assert.IsTrue(fileInfo.Exists,"File should exist");
            
            long size2 = new FileInfo(outputFile).Length;

            Assert.IsNotNull(size2 > size1);
        }

    }
}


