namespace XAct.Diagnostics
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Net.Mail;
    using log4net.Appender;
    using log4net.Core;

    /// <summary>
    /// The standard log4net SmtpAppender doesn't support SSL authentication, which is
    /// required to send email via gmail.
    /// </summary>
    /// <remarks>
    ///   <para>
    /// Uses the NET 2.0 SmtpClient settings in order to prepare the SmtpClient
    /// used to send the message.
    ///   </para>
    ///   <para>
    /// An example of usage would be to define the SmtpClient settings as follows:
    ///   <code>
    ///   <![CDATA[
    ///   <system.net>
    ///   <mailSettings>
    ///   <smtp from="skysigal@xact-solutions.com">
    ///   <network host="smtp.gmail.com" port="587"
    /// userName="me@here.com"
    /// password="yourkidding?!" />
    ///   </smtp>
    ///   </mailSettings>
    ///   </system.net>
    /// ]]>
    ///   </code>
    /// And configure the Log4Net appender as follows:
    ///   <code>
    ///   <![CDATA[
    ///   <appender name="smtpClientSmtpAppender" type="XAct.Diagnostics.SmtpClientSmtpAppender, XAct.Diagnostics.Log4Net" >
    ///   <bufferSize value="512" />
    ///   <useSsl value="true"/>
    ///   <from value="skysigal@xact-solutions.com" />
    ///   <to value="skysigal@xact-solutions.com" />
    ///   <subject value="test logging message B" />
    ///   <layout type="log4net.Layout.PatternLayout">
    ///   <conversionPattern value="%date [%thread] [%-5level] %message%newline" />
    ///   </layout>
    ///   <evaluator type="log4net.Core.LevelEvaluator">
    ///   <threshold value="ERROR"/>
    ///   </evaluator>
    ///   </appender>
    /// ]]>
    ///   </code>
    /// For an even smoother experience, remember to also install the
    ///   </para>
    /// </remarks>
    public class SmtpClientSmtpAppender : SmtpAppender
    {
        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether the useSSL.
        /// </summary>
        /// <remarks>
        /// Only required if using .NET 3.5 
        /// .Net 4 finally added an useSSL switch on system.web/mailSettings
        /// </remarks>
        /// <value><c>true</c> if [use SSL]; otherwise, <c>false</c>.</value>
        /// <internal><para>6/11/2011: Sky</para></internal>
        public bool UseSsl { get; set; }

        #endregion

        #region Method Overrides

        /// <summary>
        /// Sends the contents of the cyclic buffer as an e-mail message.
        /// </summary>
        /// <param name="events">The logging events to send.</param>
        /// <internal><para>6/11/2011: Sky</para></internal>
        protected override void SendBuffer(LoggingEvent[] events)
        {
            try
            {
                string messageText = PrepareMessageText(events);

                SendMailMessage(messageText);
            }
            catch (Exception exception)
            {
                ErrorHandler.Error("SmtpClientSmtpAppender caused an Exception when trying to send an email", exception);
            }
        }

        #endregion

        #region Helper Methods

        private string PrepareMessageText(IEnumerable<LoggingEvent> loggingEvents)
        {
            using (StringWriter writer = new StringWriter(CultureInfo.InvariantCulture))
            {
                //Render the Header:
                string headerText = Layout.Header;
                if (headerText != null)
                {
                    writer.Write(headerText);
                }

                //Render the events themselves:
                foreach (LoggingEvent loggingEvent in loggingEvents)
                {
                    // Render the event and append the text to the buffer
                    RenderLoggingEvent(writer, loggingEvent);
                }

                //Render the Footer:
                string footextText = Layout.Footer;
                if (footextText != null)
                {
                    writer.Write(footextText);
                }

                return writer.ToString();
            }
        }

        private void SendMailMessage(string messageText)
        {
            using (MailMessage mail = new MailMessage(From, To, Subject, messageText))
            {
                //A reply to is required:
                mail.ReplyToList.Add(new MailAddress(From));
                // Use SmtpClient, that will use values
                //from system.web/mailSettings
                SmtpClient client = new SmtpClient {};

                if (this.UseSsl)
                {
                    //Only set it if enabled directly on Log4Net appender configuration
                    //which will be required if using .NET 3.5 (.NET 4, use the switch
                    //on system.web/mailSettings)
                    client.EnableSsl = true;
                }
                client.Send(mail);
            }
        }

        #endregion
    }
}