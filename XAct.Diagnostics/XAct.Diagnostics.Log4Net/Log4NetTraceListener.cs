﻿//CatenaLogic

namespace XAct.Diagnostics
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using log4net;
    using log4net.Config;

    /// <summary>
    /// Trace listener that will write all trace messages to log 4 net. This listener makes sure not
    /// to break existing trace functionality.
    /// </summary>
    /// <remarks>
    /// <para>
    /// An example of the configuration required is as follows:
    /// <code>
    /// <![CDATA[
    /// <system.diagnostics>
    ///   <trace autoflush="true">
    ///     <listeners>
    ///       <add name="myLog4NetTraceListener" />
    ///     </listeners>
    ///   </trace>
    ///   <sources>
    ///     <!-- http://bit.ly/rpys5M -->
    ///     <!-- http://bit.ly/oYbgKG -->
    ///     <!-- http://bit.ly/mUr0cs -->
    ///     <source name="System.ServiceModel" switchValue="Information,ActivityTracing"
    ///             propagateActivity="true">
    ///       <listeners>
    ///         <add name="xml" />
    ///       </listeners>
    ///     </source>    
    ///     <source name="System.ServiceModel.MessageLogging">
    ///       <listeners>
    ///         <add name="myLog4NetTraceListener" />
    ///         <add name="messages"
    ///              type="System.Diagnostics.XmlWriterTraceListener"
    ///              initializeData="c:\logs\messages.svclog" />
    ///       </listeners>
    ///     </source>
    ///   </sources>
    ///   <sharedListeners>
    ///     <add name="myLog4NetTraceListener" 
    ///          type="XAct.Diagnostics.Log4NetTraceListener,XAct.Diagnostics.Log4Net"/>
    ///   </sharedListeners>
    /// </system.diagnostics>
    /// <system.serviceModel>
    ///   <diagnostics>
    ///     <messageLogging 
    ///       logEntireMessage="true" 
    ///       logMalformedMessages="false"
    ///       logMessagesAtServiceLevel="true" 
    ///       logMessagesAtTransportLevel="true"
    ///       maxSizeOfMessageToLog="2000" 
    /// />
    ///   </diagnostics>
    /// </system.serviceModel>
    /// ]]>
    /// </code>
    /// </para>
    /// <para>
    /// This will then redirect Trace.LogInformation, etc. messages to the Log4Net system 
    /// which is registered using a configSection:
    /// <code>
    /// <![CDATA[  
    /// <configSections>
    ///   <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler,Log4net"/>
    ///   </configSections>
    /// ]]>
    /// </code>
    /// that points to a <c>log4net.config</c> file that has Compile/CopyToOutput set,
    /// that contains something like the following:
    /// <code>
    /// <![CDATA[
    /// <?xml version="1.0" encoding="utf-8" ?>
    /// <log4net>
    ///    <root>
    ///      <level value="DEBUG" />
    ///      <appender-ref ref="LogFileAppender" />
    ///      <appender-ref ref="RollingFileAppender" />
    ///      <appender-ref ref="ConsoleAppender" />
    ///    </root>
    ///    <!-- The file will be appended to rather than overwritten each time the logging process starts. -->
    ///    <appender name="LogFileAppender" type="log4net.Appender.RollingFileAppender" >
    ///      <param name="File" value="c:\logfiles\log4net.log.txt" />
    ///      <param name="AppendToFile" value="true" />
    ///      <rollingStyle value="Size" />
    ///      <maxSizeRollBackups value="10" />
    ///      <maximumFileSize value="10MB" />
    ///      <staticLogFileName value="true" />
    ///      <layout type="log4net.Layout.PatternLayout">
    ///        <param name="ConversionPattern" value="YYYY %-5p%d{yyyy-MM-dd hh:mm:ss} – %m%n" />
    ///      </layout>
    ///      <filter type="log4net.Filter.LevelRangeFilter">
    ///        <levelMin value="INFO" />
    ///        <levelMax value="FATAL" />
    ///      </filter>
    ///    </appender>
    ///    <!-- The file written to will always be called log.txt because 
    ///             the StaticLogFileName param is specified. The file will be rolled based 
    ///             on a size constraint (RollingStyle). Up to 10 (MaxSizeRollBackups) 
    ///             old files of 100 KB each (MaximumFileSize) will be kept. 
    ///             These rolled files will be named: log.txt.1, log.txt.2, log.txt.3, etc... -->
    ///    
    ///     <appender name="RollingFileAppender" type="log4net.Appender.RollingFileAppender">
    ///       <file value="c:\logfiles\log4net.rollinglog.txt" />
    ///       <appendToFile value="true" />
    ///       <rollingStyle value="Size" />
    ///       <maxSizeRollBackups value="10" />
    ///       <maximumFileSize value="100KB" />
    ///       <staticLogFileName value="true" />
    ///       <layout type="log4net.Layout.PatternLayout">
    ///         <conversionPattern value="%date [%thread] %-5level %logger [%property{NDC}] - %message%newline" />
    ///       </layout>
    ///      <filter type="log4net.Filter.LevelRangeFilter">
    ///        <levelMin value="INFO" />
    ///        <levelMax value="FATAL" />
    ///      </filter>
    ///     </appender>
    ///    <!-- And to Get stuff up on the screen -->
    ///    <appender name="ConsoleAppender" type="log4net.Appender.ConsoleAppender">
    ///      <layout type="log4net.Layout.PatternLayout">
    ///        <conversionPattern value="%message%newline" />
    ///      </layout>
    ///      <!--<evaluator type="log4net.Core.LevelEvaluator">
    ///        --><!-- Use DEBUG, INFO, WARN, ERROR and FATAL --><!--
    ///        <threshold value="DEBUG"/>
    ///      </evaluator>-->
    ///      <filter type="log4net.Filter.LevelRangeFilter">
    ///        <levelMin value="INFO" />
    ///        <levelMax value="FATAL" />
    ///      </filter>
    ///    </appender>
    ///    <!--<appender name="EventLogAppender" type="log4net.Appender.EventLogAppender" >
    ///            <layout type="log4net.Layout.PatternLayout">
    ///                  <conversionPattern value="%date [%thread] %-5level %logger [%property{NDC}] - %message%newline" />
    ///            </layout>
    ///            <evaluator type="log4net.Core.LevelEvaluator">
    ///             --><!-- Use DEBUG, INFO, WARN, ERROR and FATAL --><!--
    ///             <threshold value="WARN"/>
    ///     </evaluator>-->
    ///   <!--</appender>
    ///      <appender name="SmtpAppender" type="log4net.Appender.SmtpAppender">
    ///          <to value="to@domain.com" />
    ///          <from value="from@domain.com" />
    ///          <subject value="test logging message" />
    ///          <smtpHost value="SMTPServer.domain.com" />
    ///          <bufferSize value="512" />
    ///          <lossy value="false" />
    ///          <layout type="log4net.Layout.PatternLayout">
    ///              <conversionPattern value="%newline%date [%thread] %-5level %logger [%property{NDC}] - %message%newline%newline%newline" />
    ///          </layout>
    ///          <evaluator type="log4net.Core.LevelEvaluator">
    ///             --><!-- Use DEBUG, INFO, WARN, ERROR and FATAL --><!--
    ///          <threshold value="WARN"/>
    ///      </evaluator>
    ///   </appender>-->
    /// </log4net>
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    public class Log4NetTraceListener : TraceListener
    {
        private static bool constructing;

        private const string _Log4NetConfigFileName = "log4net.config";

        #region Variables

        private readonly ILog _log; // = LogManager.GetLogger("TraceOutput");

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the trace source collection.
        /// </summary>
        /// <value>The trace source collection.</value>
        private List<TraceSource> TraceSourceCollection { get; set; }

        /// <summary>
        /// Gets or sets the active trace type.
        /// </summary>
        public TraceLevel ActiveTraceLevel { get; set; }

        #endregion

        #region Constructor & destructor

        /// <summary>
        /// Initializes a new instance of the <see cref="Log4NetTraceListener"/> class.
        /// </summary>
        public Log4NetTraceListener()
        {
            if (constructing)
            {
                return;
            }

            _log = InitializeLogger(this.GetType());


            //log4net.Appender.FileAppender f;
            //f.
            //log4net.Appender.RollingFileAppender r;
            //log4net.Appender.ColoredConsoleAppender cr;
            //log4net.Appender.SmtpAppender s;
            //s.Evaluator
            //log4net.Appender.ConsoleAppender c;


            Initialize();
        }


        #endregion

        /// <summary>
        /// Initializes the logger.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        /// <internal><para>6/11/2011: Sky</para></internal>
        private ILog InitializeLogger(Type type)
        {
            ILog log = LogManager.GetLogger(type);

            //BasicConfigurator.Configure();

            //Now configure it:

            //VERY IMPORTANT 
            //NOTE: There are important Pros and Cons with this solution.
            //If the Log4Net attribuutes are include in the app, it will
            //complain about unknown config elements and attributes.
            //But if it is in an external file, as here, it will need
            //extra rights to read the file...
            FileInfo fileInfo = new FileInfo(_Log4NetConfigFileName);

            if (fileInfo.Exists)
            {
                //This is a wierd little thingie...
                //If there is an exception in a Log4Net configuration,
                //it writes the error to the System.Diagnostics system...
                //which we just trapped...so this causes a race condition.
                //Work around is a bit lame -- but then again the hack is only
                //used when config is wrong. Sort that out, and the problem
                //goes away 
                constructing = true;
                XmlConfigurator.Configure(fileInfo);
                constructing = false;
            }
            return log;
        }

        #region MethodOverrides

        /// <summary>
        /// Gets the custom attributes supported by the trace listener.
        /// </summary>
        /// <returns>
        /// A string array naming the custom attributes supported by the trace listener, 
        /// or null if there are no custom attributes.
        /// </returns>
        /// <internal><para>6/9/2011: Sky</para></internal>
        protected override string[] GetSupportedAttributes()
        {
            if (_supportedAttributes != null)
            {
                return _supportedAttributes;
            }
            List<string> tmp = new List<string>();
            string[] baseValues = base.GetSupportedAttributes();
            if (baseValues != null)
            {
                tmp.AddRange(baseValues);
            }
            tmp.AddRange(new string[]
                             {
                             });
            _supportedAttributes = tmp.ToArray();

            return _supportedAttributes;
        }

        private string[] _supportedAttributes;

        #endregion

        #region Methods

        /// <summary>
        /// Writes trace information, a formatted array of objects and event information to the listener specific output.
        /// </summary>
        /// <param name="eventCache">A <see cref="T:System.Diagnostics.TraceEventCache"/> object that contains the current process ID, thread ID, and stack trace information.</param>
        /// <param name="source">A name used to identify the output, typically the name of the application that generated the trace event.</param>
        /// <param name="traceEventType">One of the <see cref="T:System.Diagnostics.TraceEventType"/> values specifying the type of event that has caused the trace.</param>
        /// <param name="id">A numeric identifier for the event.</param>
        /// <param name="format">A format string that contains zero or more format items, which correspond to objects in the <paramref name="args"/> array.</param>
        /// <param name="args">An object array containing zero or more objects to format.</param>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode"/>
        /// </PermissionSet>
        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType traceEventType, int id,
                                        string format, params object[] args)
        {
            // Call overload
            TraceEvent(eventCache, source, traceEventType, id, format.FormatStringCurrentCulture(args));
        }

        /// <summary>
        /// Writes trace information, a message, and event information to the listener specific output.
        /// </summary>
        /// <param name="eventCache">A <see cref="T:System.Diagnostics.TraceEventCache"/> object that contains the current process ID, thread ID, and stack trace information.</param>
        /// <param name="source">A name used to identify the output, typically the name of the application that generated the trace event.</param>
        /// <param name="traceEventType">One of the <see cref="T:System.Diagnostics.TraceEventType"/> values specifying the type of event that has caused the trace.</param>
        /// <param name="id">A numeric identifier for the event.</param>
        /// <param name="message">A message to write.</param>
        /// <PermissionSet>
        /// 	<IPermission class="System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Unrestricted="true"/>
        /// 	<IPermission class="System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.3600.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" version="1" Flags="UnmanagedCode"/>
        /// </PermissionSet>
        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType traceEventType, int id,
                                        string message)
        {
            if (!ShouldTrace(eventCache, source, traceEventType, id, message, null))
            {
                return;
            }
            // Check the type
            switch (traceEventType)
            {
                case TraceEventType.Critical:
                    _log.Fatal(message);
                    break;
                case TraceEventType.Error:
                    _log.Error(message);
                    break;
                case TraceEventType.Warning:
                    _log.Warn(message);
                    break;
                case TraceEventType.Information:
                    _log.Info(message);
                    break;
                default:
                    _log.Debug(message);
                    break;
            }
        }

        /// <summary>
        /// Writes text to the output window.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public override void Write(string message)
        {
            if (!ShouldTrace(null, null, TraceEventType.Verbose, 0, message, null))
            {
                return;
            }

            _log.Debug(message);
        }

        /// <summary>
        /// Writes a line of text to the output window.
        /// </summary>
        /// <param name="message">Message to write.</param>
        public override void WriteLine(string message)
        {
            if (constructing)
            {
                return;
            }
            if (!ShouldTrace(null, null, TraceEventType.Verbose, 0, message, null))
            {
                return;
            }
            _log.Debug(message);
        }

        #endregion


        #region Protected Helper Methods: TraceFilter

        /// <summary>
        /// Shoulds the trace.
        /// </summary>
        /// <param name="traceEventCache">The trace event cache.</param>
        /// <param name="source">The source.</param>
        /// <param name="traceEventType">Type of the trace event.</param>
        /// <param name="id">The id.</param>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected virtual bool ShouldTrace(TraceEventCache traceEventCache, string source, TraceEventType traceEventType,
                                           int id, string format, params object[] args)
        {
            return ShouldTrace(this.Filter, traceEventCache, source, traceEventType, id, format, args);
        }

        /// <summary>
        /// Shoulds the trace.
        /// </summary>
        /// <param name="traceFilter">The trace filter.</param>
        /// <param name="traceEventCache">The trace event cache.</param>
        /// <param name="source">The source.</param>
        /// <param name="traceEventType">Type of the trace event.</param>
        /// <param name="id">The id.</param>
        /// <param name="format">The format.</param>
        /// <param name="args">The args.</param>
        /// <returns></returns>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected virtual bool ShouldTrace(TraceFilter traceFilter, TraceEventCache traceEventCache, string source,
                                           TraceEventType traceEventType, int id, string format, params object[] args)
        {
            return
                (
                    (this.Filter == null)
                    ||
                    (this.Filter.ShouldTrace(traceEventCache, source, traceEventType, id, format, args, null, null))
                );
        }

        #endregion


        #region Protected Helper Methods: Config Attributes

        /// <summary>
        /// Helper method to get the typed value from the config attributes.
        /// </summary>
        /// <typeparam name="TProperty">The type of the property.</typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected TProperty GetAttributeValue<TProperty>(string key, TProperty defaultValue)
        {
            var result = this.Attributes.GetValue(key, defaultValue);
            return result;
        }

        #endregion

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected virtual void Initialize()
        {
            InitializeInternal();
        }

        //protected virtual void IntializeCustom()
        //{


        //    // Create additional trace sources list
        //    TraceSourceCollection = new List<TraceSource>();

        //    // Add additional trace sources - .NET framework
        //    //TraceSources.Add(System.Diagnostics.

        //    // Add additional trace sources - WPF
        //    TraceSourceCollection.Add(PresentationTraceSources.DataBindingSource);
        //    TraceSourceCollection.Add(PresentationTraceSources.DependencyPropertySource);
        //    TraceSourceCollection.Add(PresentationTraceSources.MarkupSource);
        //    TraceSourceCollection.Add(PresentationTraceSources.ResourceDictionarySource);

        //    // Subscribe to all trace sources
        //    foreach (TraceSource traceSource in TraceSourceCollection)
        //    {
        //        traceSource.Listeners.Add(this);
        //    }
        //}


        private void InitializeInternal()
        {
            InitializePropertiesFromConfigAttributes();
        }

        /// <summary>
        /// Initializes Properties from the Attributes.
        /// </summary>
        /// <internal><para>6/6/2011: Sky</para></internal>
        protected virtual void InitializePropertiesFromConfigAttributes()
        {
            //base.InitializePropertiesFromConfigAttributes();

            //_autoFlush = GetAttributeValue("autoFlush", true);
            //_messageFormat = GetAttributeValue("messageFormat", "{DateTime}: {ProcessName}: {Level}: {Message}");
        }
    }
}