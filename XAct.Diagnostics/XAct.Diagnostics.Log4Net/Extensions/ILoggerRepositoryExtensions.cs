﻿
#if (!REMOVENS4EXTENSIONS)
//See: http://bit.ly/snE0xY
namespace XAct {
    using System;
    using System.Linq;
    using log4net.Appender;
    using log4net.Repository;

#endif

    /// <summary>
    /// Extensions to the ILoggerRepository objects.
    /// </summary>
    /// <internal><para>8/2/2011: Sky</para></internal>
    public static class ILoggerRepositoryExtensions
    {
        /// <summary>
        /// Dynamically sets the path of a Log4Net FileListener.
        /// Useful when having to set the path according to a user setting.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="fileAppenderName">Name of the file appender.</param>
        /// <param name="newPath">The new path.</param>
        /// <internal><para>8/2/2011: Sky</para></internal>
        public static bool SetFileAppenderPath(this ILoggerRepository repository, string fileAppenderName,
                                               string newPath)
        {
            //get all of the appenders for the repository 
            IAppender[] appenders = repository.GetAppenders();
            //only change the file path on the 'FileAppenders' 
            foreach (IAppender appender in (from iAppender in appenders
                                            where iAppender is FileAppender
                                            select iAppender))
            {
                FileAppender fileAppender = ((FileAppender) appender);
                if (string.Compare(fileAppender.Name, fileAppenderName, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    fileAppender.File = newPath;
                    fileAppender.ActivateOptions();
                    return true;
                }
            }
            return false;
        }
    }

#if (!REMOVENS4EXTENSIONS)
    //See: http://bit.ly/snE0xY
}
#endif
