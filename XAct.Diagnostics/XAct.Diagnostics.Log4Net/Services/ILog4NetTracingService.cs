﻿//namespace XAct.Diagnostics
//{

//    /// <summary>
//    /// A specialization of the <see cref="ITracingService"/>
//    /// for an implementation that uses Log4Net.
//    /// <para>
//    /// The contract adds no functionality to <see cref="ITracingService"/> --
//    /// it is used only for differentiation in the DependencyInjectionContainer.
//    /// </para>
//    /// </summary>
//    public interface ILog4NetTracingService : ITracingService, IHasXActLibServiceDefinition
//    {

//    }
//}
