﻿//namespace XAct.Diagnostics.Implementations
//{
//    using System;
//    using System.IO;
//    using System.Timers;
//    using XAct.Services;
//    using log4net;
//    using log4net.Appender;
//    using log4net.Config;
//    using log4net.Repository.Hierarchy;

//    /// <summary>
//    ///   An implementation of the 
//    ///   <see cref = "ITracingService" /> contract, 
//    ///   using Log4Net internally.
//    /// </summary>
//    /// <remarks>
//    ///   <para>
//    ///     For Log4Net to work, it has to be configured first.
//    ///   </para>
//    ///   <para>
//    ///     An example of configuration:
//    ///   </para>
//    /// </remarks>
//    /// <internal>
//    ///   <para>
//    ///     You can configure Log4Net by adding the vars to the config file:
//    ///     <cpde>
//    ///       <![CDATA[
//    /// <configSections>
//    ///     <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler,Log4net"/>
//    /// </configSections>
//    /// ...
//    /// <log4net>
//    /// ...
//    /// </log4net>
//    /// ]]>
//    ///     </cpde>
//    ///     or use an external file (an external file is preferred).
//    ///     <code>
//    ///       <![CDATA[
//    /// ]]>
//    ///     </code>
//    ///     A typical app.config section or external xml file will 
//    ///     contain something like:
//    ///     <code>
//    ///       <![CDATA[
//    /// <log4net>
//    ///   <root>
//    ///     <level value="DEBUG" />
//    ///     <appender-ref ref="LogFileAppender" />
//    ///   </root>
//    ///   <!-- The file will be appended to rather than overwritten each time the logging process starts. -->
//    ///   <appender name="LogFileAppender" type="log4net.Appender.RollingFileAppender" >
//    ///     <param name="File" value="log.txt" />
//    ///     <param name="AppendToFile" value="true" />
//    ///     <rollingStyle value="Size" />
//    ///     <maxSizeRollBackups value="10" />
//    ///     <maximumFileSize value="10MB" />
//    ///     <staticLogFileName value="true" />
//    ///     <layout type="log4net.Layout.PatternLayout">
//    ///       <param name="ConversionPattern" value="%-5p%d{yyyy-MM-dd hh:mm:ss} – %m%n" />
//    ///     </layout>
//    ///   </appender>
//    ///   <!-- The file written to will always be called log.txt because 
//    ///        the StaticLogFileName param is specified. The file will be rolled based 
//    ///        on a size constraint (RollingStyle). Up to 10 (MaxSizeRollBackups) 
//    ///        old files of 100 KB each (MaximumFileSize) will be kept. 
//    ///        These rolled files will be named: log.txt.1, log.txt.2, log.txt.3, etc... -->
//    ///   <appender name="RollingFileAppender" type="log4net.Appender.RollingFileAppender">
//    ///     <file value="rollinglog.txt" />
//    ///     <appendToFile value="true" />
//    ///     <rollingStyle value="Size" />
//    ///     <maxSizeRollBackups value="10" />
//    ///     <maximumFileSize value="100KB" />
//    ///     <staticLogFileName value="true" />
//    ///     <layout type="log4net.Layout.PatternLayout">
//    ///       <conversionPattern value="%date [%thread] %-5level %logger [%property{NDC}] - %message%newline" />
//    ///     </layout>
//    ///     <evaluator type="log4net.Core.LevelEvaluator">
//    ///      <!-- Use DEBUG, INFO, WARN, ERROR and FATAL -->
//    ///      <threshold value="WARN"/>
//    ///     </evaluator>
//    ///   </appender>
//    ///   <!-- And to Get stuff up on the screen -->
//    ///   <appender name="ConsoleAppender" type="log4net.Appender.ConsoleAppender">
//    ///       <layout type="log4net.Layout.PatternLayout">
//    ///           <conversionPattern value="%message%newline" />
//    ///       </layout>
//    ///     <evaluator type="log4net.Core.LevelEvaluator">
//    ///      <!-- Use DEBUG, INFO, WARN, ERROR and FATAL -->
//    ///      <threshold value="WARN"/>
//    ///     </evaluator>
//    ///   </appender>
//    ///   <appender name="EventLogAppender" type="log4net.Appender.EventLogAppender" >
//    ///       <layout type="log4net.Layout.PatternLayout">
//    ///           <conversionPattern value="%date [%thread] %-5level %logger [%property{NDC}] - %message%newline" />
//    ///       </layout>
//    ///     <evaluator type="log4net.Core.LevelEvaluator">
//    ///      <!-- Use DEBUG, INFO, WARN, ERROR and FATAL -->
//    ///      <threshold value="WARN"/>
//    ///     </evaluator>
//    ///   </appender>
//    ///   <appender name="SmtpAppender" type="log4net.Appender.SmtpAppender">
//    ///     <to value="to@domain.com" />
//    ///     <from value="from@domain.com" />
//    ///     <subject value="test logging message" />
//    ///     <smtpHost value="SMTPServer.domain.com" />
//    ///     <bufferSize value="512" />
//    ///     <lossy value="false" />
//    ///     <layout type="log4net.Layout.PatternLayout">
//    ///       <conversionPattern value="%newline%date [%thread] %-5level %logger [%property{NDC}] - %message%newline%newline%newline" />
//    ///     </layout>
//    ///     <evaluator type="log4net.Core.LevelEvaluator">
//    ///      <!-- Use DEBUG, INFO, WARN, ERROR and FATAL -->
//    ///      <threshold value="WARN"/>
//    ///     </evaluator>
//    ///   </appender>    
//    /// </log4net>
//    /// ]]>
//    ///   </code>
//    ///   </para>
//    ///   <para>
//    ///     Note: if an external file is used consider ensuring the file
//    ///     is built and copied to the BIN directory on every build.
//    ///   </para>
//    ///   <para>
//    ///     Finally, one can configure by using an attribute:
//    ///     [assembly: log4net.Config.XmlConfigurator( ConfigFile="Log4Net.config",Watch=true )]
//    ///     I like this one less as it tightly coupled to two assemblies now (app, service assemblies).
//    ///   </para>
//    /// </internal>
//    [DefaultBindingImplementation(typeof(ITracingService),BindingLifetimeType.Undefined,Priority.Low /*OK:Second Binding*/)]
//    public class Log4NetTracingService : TracingServiceBase, ILog4NetTracingService
//    {
//        #region Constants

//        private const string _Log4NetConfigFileName = "log4net.config";

//        #endregion

//        #region Fields

//        private readonly ILog _log;

//        #endregion


//        /// <summary>
//        ///   Initializes a new instance of the <see cref = "Log4NetTracingService" /> class.
//        /// </summary>
//        /// <internal>
//        ///   Note that this Service, as part of CommonServices, can't use 
//        ///   an ICommonServices constructor arg without causing a race condition.
//        /// </internal>
//        public Log4NetTracingService() : base(DependencyResolver.Current.GetInstance<ITraceSwitchService>())
//        {
//            Type type = this.GetType();

//            _log = LogManager.GetLogger(type);

//            //Now configure it:


//            //VERY IMPORTANT 
//            //NOTE: There are important Pros and Cons with this solution.
//            //If the Log4Net attribuutes are include in the app, it will
//            //complain about unknown config elements and attributes.
//            //But if it is in an external file, as here, it will need
//            //extra rights to read the file...

//            FileInfo fileInfo = new FileInfo(_Log4NetConfigFileName);

//            if (fileInfo.Exists)
//            {
//                XmlConfigurator.Configure(new FileInfo(_Log4NetConfigFileName));
//            }
//        }


//        #region Implementation of LoggingServiceBase abstract methods

//        /// <summary>
//        /// Writes the given message to the 
//        /// specific implementation's logging system.
//        /// </summary>
//        /// <internal>
//        /// Invoked only after passing WriteLineIf.
//        /// </internal>
//        /// <param name="traceLevel">The message's tracelevel</param>
//        /// <param name="message">The already string formatted message.</param>
//        protected override void WriteLine(
//            TraceLevel traceLevel,
//            string message)
//        {
//            //See: http://sadi02.wordpress.com/2008/06/29/log4net-tutorial-in-c-net-how-can-i-show-log-in-a-file/

//            if (traceLevel == TraceLevel.Off)
//            {
//                return;
//            }


//            switch (traceLevel)
//            {
//                case TraceLevel.Verbose:
//                    _log.Debug(message);
//                    break;
//                case TraceLevel.Info:
//                    _log.Info(message);
//                    break;
//                case TraceLevel.Warning:
//                    _log.Warn(message);
//                    break;
//                case TraceLevel.Error:
//                    _log.Error(message);
//                    break;
//                default:
//                    break;
//            }
//        }


//        /// <summary>
//        /// Event handler for when the flush timer goes off.
//        /// <para>
//        /// Flushes the Logging mechanism.
//        /// </para>
//        /// </summary>
//        /// <param name="sender">The sender.</param>
//        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
//        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
//        {
//            //_log.FlushContextItems();
//            //System.Diagnostics.Trace.Flush();
//            //Not needed as goes to same place: System.Diagnostics.Debug.Flush();

//            //ILog log = LogManager.GetLogger("whatever");
//            Logger logger = _log.Logger as Logger;

//            if (logger == null)
//            {
//                return;
//            }

//            foreach (IAppender appender in logger.Appenders)
//            {
//                BufferingAppenderSkeleton buffered = appender as BufferingAppenderSkeleton;
//                if (buffered == null)
//                {
//                    continue;
//                }
//                buffered.Flush();
//            }
//        }

//        #endregion

//        /*
//        /// <summary>
//        ///   Logs the exception.
//        /// </summary>
//        /// <param name = "traceLevel">The level of this message.</param>
//        /// <param name = "exception">The exception.</param>
//        /// <param name = "message">The message.</param>
//        /// <param name = "arguments">Any optional arguments to format into the message.</param>
//        public void LogEntry(TraceLevel traceLevel, Exception exception, string message, params object[] arguments)
//        {
//            //See: http://sadi02.wordpress.com/2008/06/29/log4net-tutorial-in-c-net-how-can-i-show-log-in-a-file/

//            if (traceLevel == TraceLevel.Off)
//            {
//                return;
//            }

//            if ((arguments != null) && (arguments.Length > 0))
//            {
//                message = message.FormatString(arguments);
//            }

//            switch (traceLevel)
//            {
//                case TraceLevel.Verbose:
//                    if (exception == null)
//                    {
//                        _log.Debug(message);
//                    }
//                    else
//                    {
//                        _log.Debug(message, exception);
//                    }
//                    break;
//                case TraceLevel.Info:
//                    if (exception == null)
//                    {
//                        _log.Info(message);
//                    }
//                    else
//                    {
//                        _log.Info(message, exception);
//                    }
//                    break;
//                case TraceLevel.Warning:
//                    if (exception == null)
//                    {
//                        _log.Warn(message);
//                    }
//                    else
//                    {
//                        _log.Warn(message, exception);
//                    }
//                    break;
//                case TraceLevel.Error:
//                    if (exception == null)
//                    {
//                        _log.Error(message);
//                    }
//                    else
//                    {
//                        _log.Error(message, exception);
//                    }
//                    break;
//                default:
//                    break;
//            }
//        }

//         */
//    }
//}