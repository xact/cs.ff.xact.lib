﻿using System;
using System.Collections.Generic;
using System.Linq;
using XAct.Diagnostics.Performance.State;
using XAct.Diagnostics.Performance.State.Implementations;

namespace XAct.Performance.Tests
{
    using System.Diagnostics;
    using System.Threading;
    using NUnit.Framework;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Performance.Implementations;
    using XAct.Diagnostics.Performance.Services;
    using XAct.Diagnostics.Performance.Services.Configuration;
    using XAct.Diagnostics.Performance.Services.Configuration.Implementations;
    using XAct.Interception.CallHandlers;
    using XAct.Messages;
    using XAct.Services;
    using XAct.Services.IoC;
    using XAct.Tests;

    [TestFixture]
    public class PerformanceCounterCallHandlerTests

    {

            public class InterceptAttribute : Attribute
    {

    }

    [InterceptAttribute]
    public interface IExample:IHasService
    {
        [PerformanceCounter("XActLib.Examples", "EXAMPLE_OPERATION", true, "EXAMPLE_SUBOP", PerformanceCounterUpdateType.Automatic, true)]
        string Foo1();

        [PerformanceCounter("XActLib.Examples", "EXAMPLE_OPERATION", true, "EXAMPLE_SUBOP", PerformanceCounterUpdateType.Automatic, true)]
        string Foo2();

        [PerformanceCounter("XActLib.Examples", "EXAMPLE_OPERATION", true, "EXAMPLE_SUBOP", PerformanceCounterUpdateType.Automatic, true)]
        IResponse Foo3();

        [PerformanceCounter("XActLib.Examples", "EXAMPLE_OPERATION", true, "EXAMPLE_SUBOP", PerformanceCounterUpdateType.Automatic, true)]
        IResponse Foo4();


        [PerformanceCounter("XActLib.Examples", "EXAMPLE_OPERATION", false, "EXAMPLE_SUBOP", PerformanceCounterUpdateType.Automatic, true)]
        string WithDisabledTracing();
    }


    [InterceptAttribute]
    public class Example : IExample
    {
        private readonly IContextStatePerformanceCounterService _contextStatePerformanceCounterService;

        public Example(IContextStatePerformanceCounterService contextStatePerformanceCounterService)
        {
            _contextStatePerformanceCounterService = contextStatePerformanceCounterService;
        }

        public virtual string Foo1()
        {
            for (int i = 0; i < 2; i++)
            {
                SubFoo();
            }
                Thread.Sleep(50);
            return "Hi";

        }


        public virtual string Foo2()
        {
            for (int i = 0; i < 2; i++)
            {
                SubFoo();
            }
            Thread.Sleep(50);
            throw new Exception("Some Exception");

        }



        public virtual IResponse Foo3()
        {
            for (int i = 0; i < 2; i++)
            {
                SubFoo();
            }
            Thread.Sleep(50);

            return new Response();

        }

        public virtual IResponse Foo4()
        {
            for (int i = 0; i < 2; i++)
            {
                SubFoo();
            }
            Thread.Sleep(50);

            var r = new Response(); 
            r.AddMessage(new MessageCode(0, Severity.Error));
            return r;
        }
        
        public virtual string WithDisabledTracing()
        {
            return "Hi";
        }
        public void SubFoo()
        {
            _contextStatePerformanceCounterService.IncrementContextOperationCounter("EXAMPLE_SUBOP");
            _contextStatePerformanceCounterService.IncrementContextOperationDuration("EXAMPLE_SUBOP",TimeSpan.FromMilliseconds(10));
        }
    }



        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            //run once before any tests in this testfixture have been run...
            //...setup vars common to all the upcoming tests...

            Singleton<IocContext>.Instance.ResetIoC();

            VirtualClassInterceptor.Execute<InterceptAttribute, PerformanceCounterAttribute>(
                "MyPerformanceCounterPolicy", false, new PerformanceCounterCallHandler());

        }

        /// <summary>
        ///   Tear down after all tests in this fixture.
        /// </summary>
        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        [Test]
        public void Can_Get_IPerformanceCounterServiceState()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterServiceState>();

            Assert.IsTrue(service != null);
        }

        [Test]
        public void Can_Get_IPerformanceCounterServiceState_Of_ExpectedType()
        {
            var service =
                XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterServiceState>();

            Assert.AreEqual(typeof(PerformanceCounterServiceState), service.GetType());
        }







        [Test]
        public void Can_Get_IPerformanceCounterService()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

            Assert.IsTrue(service != null);
        }

        [Test]
        public void Can_Get_IPerformanceCounterService_Of_ExpectedType()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

            Assert.AreEqual(typeof(WindowsSystemPerformanceCounterService), service.GetType());
        }


        [Test]
        public void Scan_For_PerformanceCounterServiceConfiguration_Files_In_Current_Directory_Finding_Atleast_One()
        {

            string currentDirectory = System.IO.Directory.GetCurrentDirectory();

            Console.WriteLine("Current Directory:" + currentDirectory);

            
            IPerformanceCounterServiceConfiguration performanceCounterServiceConfiguration =
                new PerformanceCounterServiceConfiguration();

            List<string> messages = new List<string>();

            performanceCounterServiceConfiguration.ScanForConfigurationFiles(currentDirectory,out messages);

            int categories = 
                performanceCounterServiceConfiguration.CounterCategoryCreationInformation.Count();

            foreach (string message in messages)
            {
                Console.WriteLine(message);
            }
            Assert.IsTrue(categories>0);
            foreach (var c in performanceCounterServiceConfiguration.CounterCategoryCreationInformation)
            {
                Assert.IsTrue(c.CounterCreationInformation.Length > 0);
            }
        }


        /// <summary>
        ///   An Example Test.
        /// </summary>
        [Test]
        public void Can_Get_Instance_Of_Example_Class_Decorated_With_InterceptAttribute()
        {
            var example = XAct.DependencyResolver.Current.GetInstance<IExample>();


            Assert.IsNotNull(example);

            Console.WriteLine(example.GetType().Name);
            //Not equal because Type is an Proxy Intercept, not the original Type:
            Assert.AreNotEqual(example.GetType(), typeof(Example));

        }

        [Test]
        public void Invoke_An_Intercepted_Method_But_Not_Traced_A_100_Times_And_Ensure_PerfCounter_Remains_Same()
        {

            IPerformanceCounterService performanceCounterService =
                XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>();

            long check1 = performanceCounterService.GetComputedValue<long>("XActLib.Examples",
                                                                           "EXAMPLE_OPERATION.Operation.Count", null);

            //Perform operation,
            //which should get intercepted, and therefore set Performance Counters:
            var example = XAct.DependencyResolver.Current.GetInstance<IExample>();

            //Get speed without tracing:
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                example.WithDisabledTracing();
            }
            stopwatch.Stop();
            Console.WriteLine("----");


            
            Console.WriteLine("----");
            Console.WriteLine("Invoked intercepted, but non-traced, operation 100 times:");
            Console.WriteLine("Elapsed ticks: " + stopwatch.Elapsed.Ticks);
            Console.WriteLine("Elapsed ms: " + stopwatch.Elapsed.Milliseconds);
            Console.WriteLine("----");
            long check2 = performanceCounterService.GetComputedValue<long>("XActLib.Examples", "EXAMPLE_OPERATION.Operation.Count", null);
            Console.WriteLine("PerfCounter value before:" + check1);
            Console.WriteLine("PerfCounter value after:" + check2);
            //Because it was intercepted, but not traced, the number should be same:
            Console.WriteLine("Counter change:" + (check2 - check1));

            Assert.AreEqual(check1, check2);

        }


        [Test]
        public void Invoke_An_Intercepted_Method_A_100_Times_And_Ensure_PerfCounter_Changes()
        {

            IPerformanceCounterService performanceCounterService =
                XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>();

            long check1 = performanceCounterService.GetComputedValue<long>("XActLib.Examples", "EXAMPLE_OPERATION.Operation.Count", null);

            //Perform operation,
            //which should get intercepted, and therefore set Performance Counters:
            var example = XAct.DependencyResolver.Current.GetInstance<IExample>();

            //Get speed without tracing:
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            for (int i = 0; i < 100; i++)
            {
                example.WithDisabledTracing();
            }
            stopwatch.Stop();



            //Get speed with tracing:
            Stopwatch stopwatch2 = new Stopwatch();
            stopwatch2.Start();
            for (int i = 0; i < 100; i++)
            {
                example.Foo1();
            }
            stopwatch2.Stop();

            Console.WriteLine("----");
            Console.WriteLine("Invoked intercepted, but non-traced, operation 100 times:");
            Console.WriteLine("Elapsed ticks: " + stopwatch.Elapsed.Ticks);
            Console.WriteLine("Elapsed ms: " + stopwatch.Elapsed.Milliseconds);
            Console.WriteLine("----");
            Console.WriteLine("Invoked intercepted, AND traced, operation 100 times:");
            Console.WriteLine("Elapsed ticks: " + stopwatch2.Elapsed.Ticks);
            Console.WriteLine("Elapsed ms: " + stopwatch2.Elapsed.Milliseconds);
            Console.WriteLine("----");
            Console.WriteLine("Cost of invoking perfCounter traced operation, versus un-traced operations:");
            Console.WriteLine("Delta ticks: " + stopwatch2.Elapsed.Subtract(stopwatch.Elapsed).Ticks);
            Console.WriteLine("Delta ms: " + stopwatch2.Elapsed.Subtract(stopwatch.Elapsed).Milliseconds);
            Console.WriteLine("----");
            long check2 = performanceCounterService.GetComputedValue<long>("XActLib.Examples", "EXAMPLE_OPERATION.Operation.Count", null);
            Console.WriteLine("PerfCounter value before:" + check1);
            Console.WriteLine("PerfCounter value after:" + check2);
            //Because it was intercepted, but not traced, the number should be changed:
            Console.WriteLine("Counter change:" + (check2 - check1));

            Assert.AreNotEqual(check1,check2);
        }

        [Test]
        public void UnitTest03()
        {

            IPerformanceCounterService performanceCounterService =
                XAct.DependencyResolver.Current.GetInstance<IPerformanceCounterService>();

            long checkA1 = performanceCounterService.GetComputedValue<long>("XActLib.Examples", "EXAMPLE_OPERATION.Operation.Count", null);

            //Can be 0, then 200 if run again
            //the reason being is that you are looking at the PerfCounter being set.
            //Not the source (which is the ContextPerfCounter....nuance!)
#pragma warning disable 168
            long checkA2 = performanceCounterService.GetComputedValue<long>("XActLib.Examples", "EXAMPLE_OPERATION[EXAMPLE_SUBOP].Operation.Count", null);
#pragma warning restore 168


            //Perform operation,
            //which should get intercepted, and therefore set Performance Counters:
            var example = XAct.DependencyResolver.Current.GetInstance<IExample>();

            //Get speed with  tracing:
            for (int i = 0; i < 100; i++)
            {
                example.Foo1();
            }
            for (int i = 0; i < 120; i++)
            {
                try
                {
                    example.Foo2();
                }catch {}
            }
            for (int i = 0; i < 140; i++)
            {
                example.Foo3();
            }
            for (int i = 0; i < 160; i++)
            {
                example.Foo4();
            }



            long checkB1 = performanceCounterService.GetComputedValue<long>("XActLib.Examples", "EXAMPLE_OPERATION.Operation.Count", null);
            long checkB2 = performanceCounterService.GetComputedValue<long>("XActLib.Examples", "EXAMPLE_OPERATION[EXAMPLE_SUBOP].Operation.Count", null);

            Assert.AreNotEqual(checkA1,checkB1);

            //Can be equal to last time, so don't check against checkA2:
            Assert.AreNotEqual(0, checkB2);
        }

    }
}


