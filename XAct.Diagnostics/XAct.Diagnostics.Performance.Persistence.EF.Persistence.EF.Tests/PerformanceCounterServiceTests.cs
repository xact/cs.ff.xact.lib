namespace XAct.Diagnostics.Tests
{
    using System;
    using System.Threading;
    using NUnit.Framework;
    using XAct;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Performance.Implementations;
    using XAct.Tests;

    /// <summary>
    ///   NUNit Test Fixture.
    /// </summary>
    [TestFixture]
    public class PerformanceCounterServiceTests
    {
        /// <summary>
        ///   Sets up to do before any tests 
        ///   within this test fixture are run.
        /// </summary>
        [TestFixtureSetUp]
        public void MyFixtureSetup()
        {
            Singleton<IocContext>.Instance.ResetIoC();

            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

            service.Initialize();

        }

        [SetUp]
        public void MyTestSetup()
        {
            //run before every test in this testfixture...
            //...setup vars common to all the upcoming tests...
        }

        [TearDown]
        public void MyTestTearDown()
        {
            GC.Collect();
        }



        [Test]
        public void CanGetIPerformanceCounterService()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

            Assert.IsTrue(service!=null);
        }
        [Test]
        public void CanGetIPerformanceCounterServiceOfExpectedType()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

            Assert.AreEqual(typeof(WindowsSystemPerformanceCounterService), service.GetType());
        }

        [Test]
        [NUnit.Framework.ExpectedException]
        public void Incrementing_A_Counter_That_Does_Not_Exist_Causes_Exception()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

                service.Offset("CatDoesNotExist", "DoesNotExist", 1, true);

            Assert.IsTrue(true);
        }

        [Test]
        public void Incrementing_A_Counter_That_Does_Not_Exist_By_Default_Can_Be_Done_Without_Exception()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

            service.Offset("CatDoesNotExist", "DoesNotExist", 1);

            Assert.IsTrue(true);
        }



        [Test]
        public void Incrementing_Registered_Counter_Does_Not_Cause_Exception()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

            for (int i = 0; i < 100; i++)
            {
                //The counter is defined in Test.PerformanceCounters.xml
                //If it was installed from the Command line first, it will be fine.


                service.Offset("XActLib.Tests.GHI", "ABC Operations Performed", 1, true);
            }
            Assert.IsTrue(true);
        }

        [Test]
        public void Incrementing_Registered_Counter_Increments_Value()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

            long initialValue = service.GetComputedValue<long>("XActLib.Tests.GHI", "ABC Operations Performed",false);


            for (int i = 0; i < 100; i++)
            {
                //The counter is defined in Test.PerformanceCounters.xml
                //If it was installed from the Command line first, it will be fine.


                service.Offset("XActLib.Tests.GHI", "ABC Operations Performed", 1, true);
            }

            long newValue = service.GetComputedValue<long>("XActLib.Tests.GHI", "ABC Operations Performed", false);

            Assert.AreNotEqual(initialValue,newValue,"Should not be the same:" + initialValue);
        }

     
        //TEST:FAILING
        [Test]
        public void Increment_Registered_Counter()
        {
            IPerformanceCounterService service =
                XAct.DependencyResolver.Current.GetInstance<XAct.Diagnostics.Performance.IPerformanceCounterService>();

            long initialValue = service.GetComputedValue<long>("XActLib.Tests.GHI", "DEF Operations Per Second", true);

            for (int x=0; x < 5; x++)
            {
                Thread.Sleep(new Random().Next(600));
                for (int i = 0; i < 10; i++)
                {
                    service.Offset("XActLib.Tests.GHI", "DEF Operations Per Second", 1);
                }
                Thread.Sleep(400);

                for (int i = 0; i < 5; i++)
                {
                    service.Offset("XActLib.Tests.GHI", "DEF Operations Per Second", 1);
                }
                Thread.Sleep(400);
            }

            long newValue = service.GetComputedValue<long>("XActLib.Tests.GHI", "DEF Operations Per Second", true);

            Assert.AreNotEqual(initialValue, newValue);
        }

    }


}


