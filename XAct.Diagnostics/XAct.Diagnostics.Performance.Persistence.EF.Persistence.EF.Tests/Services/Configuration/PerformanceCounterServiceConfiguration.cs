﻿namespace XAct.Performance.Tests
{
    using System.Collections.Generic;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Performance.Services.Configuration;
    using XAct;
    using XAct.Diagnostics.Performance.Services.Entities;
    using XAct.Services;

    public class PerformanceCounterServiceConfiguration : IPerformanceCounterServiceConfiguration, IHasXActLibServiceConfiguration, IHasMediumBindingPriority
    {
        private bool _enabled=true;
        private bool _attemptSelfInstall=false;
        private bool _raiseExceptionsIfPerformanceCounterNotFound=false;


        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public bool AttemptSelfInstall
        {
            get { return _attemptSelfInstall; }
            set { _attemptSelfInstall = value; }
        }


        /// <summary>
        /// Gets or sets a value indicating whether to force category recreation if self re-installing.
        /// </summary>
        public bool ForceCategoryRecreationIfSelfInstalling
        {
            get { return _forceCategoryRecreationIfSelfInstalling; }
            set { _forceCategoryRecreationIfSelfInstalling = value; }
        }
        private bool _forceCategoryRecreationIfSelfInstalling = false;


        public bool RaiseExceptionsIfPerformanceCounterNotFound
        {
            get { return _raiseExceptionsIfPerformanceCounterNotFound; }
            set { _raiseExceptionsIfPerformanceCounterNotFound = value; }
        }

        public PerformanceCounterCategoryCreationInformation[] CounterCategoryCreationInformation { get; set; }

        

        public PerformanceCounterServiceConfiguration():base()
        {
            Enabled = true;

            var x = new List<PerformanceCounterCreationInformation>();

            x.AddRange(PerformanceCounterCreationInformationFactory.Build(CommonCounterType.OperationsPerformedCounter, "ABC"));
            x.AddRange(PerformanceCounterCreationInformationFactory.Build(CommonCounterType.OperationsPerSecondCounter, "DEF"));



            CounterCategoryCreationInformation =
                new PerformanceCounterCategoryCreationInformation[]
                    {
                        new PerformanceCounterCategoryCreationInformation
                            {
                        Name="XActLib Test Counters",
                        Description = "Counters made by UnitTests",
                        CounterCreationInformation = x.ToArray()

                            }
                    };




        }


    }
}
