﻿//using System;
////using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

//namespace XAct
//{
//    /// <summary>
//    /// Extension Methods to the 
//    /// <see cref="Exception"/> object,
//    /// that has references to EntLib.
//    /// </summary>
//    public static class ExceptionExtensions
//    {
//        /// <summary>
//        /// 
//        /// </summary>
//        /// <internal>
//        /// Example of use:
//        /// <code>
//        /// <![CDATA[
//        /// try {...}
//        /// catch(Exception ex)
//        /// {
//        ///   if(!ex.Handled(“MyExceptionPolicyName”)) {
//        ///     throw; //not handled or configured to throw
//        ///   }
//        /// }
//        /// ]]>
//        /// </code>
//        /// </internal>
//        /// <param name="exception"></param>
//        /// <param name="policyName"></param>
//        /// <returns></returns>
//        public static bool Handled(this Exception exception, string policyName)
//        {
//            Exception newException = null;

//            bool rethrow = ExceptionPolicy.HandleException(exception, policyName, out newException);

//            if (!rethrow)
//            {
//                return true;
//            }

//            if (newException == null)
//            {
//                return false;
//            }

//            throw newException;
//        }
//    }
//}
