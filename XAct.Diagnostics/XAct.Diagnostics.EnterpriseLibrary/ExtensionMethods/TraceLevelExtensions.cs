﻿using System;
using System.Diagnostics;

namespace XAct
{
    /// <summary>
    /// Extension Methods to the 
    /// <see cref="TraceLevel"/> Enumeration type.
    /// </summary>
    public static class TraceLevelExtensions
    {
        /// <summary>
        /// Converts a generic all purpose <see cref="TraceLevel"/>
        /// to the more 
        /// System.Diaglthe type of the trace event.
        /// </summary>
        /// <param name="traceLevel">The trace level.</param>
        /// <returns></returns>
        public static TraceEventType ToTraceEventType(this TraceLevel traceLevel)
        {
            switch (traceLevel)
            {
                case TraceLevel.Error:
                    return TraceEventType.Error;
                case TraceLevel.Warning:
                    return TraceEventType.Warning;
                case TraceLevel.Info:
                    return TraceEventType.Information;
                case TraceLevel.Verbose:
                    return TraceEventType.Verbose;
                case TraceLevel.Off:
                    throw new ArgumentOutOfRangeException("traceLevel");
            }
        }
    }
}
