﻿// ReSharper disable CheckNamespace
namespace XAct.Diagnostics.Implementations
// ReSharper restore CheckNamespace
{
    using System.Timers;
    using Microsoft.Practices.EnterpriseLibrary.Logging;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="ITracingService"/>
    /// using Enterprise Library.
    /// </summary>
    /// <remarks>
    /// <para>
    /// See: http://elegantcode.com/2009/01/20/enterprise-library-logging-101/
    /// See: 
    /// </para>
    /// <para>
    /// EntLib is rather a large mess to configure -- best to do it
    /// by downloading and installign EntLib in order to use the dedicated
    /// config file generator that comes with it.
    /// </para>
    /// <para>
    /// If you really want to do it by hand, here's a config file example:
    /// <code>
    /// <![CDATA[
    /// <?xml version="1.0" encoding="utf-8" ?>
    /// <configuration>
    /// 
    ///   <configSections>
    ///     <section name="enterpriseLibrary.ConfigurationSource" type="Microsoft.Practices.EnterpriseLibrary.Common.Configuration.ConfigurationSourceSection, Microsoft.Practices.EnterpriseLibrary.Common" requirePermission="true" />
    ///     <section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging" requirePermission="true" />
    ///   </configSections>
    /// 
    ///   <enterpriseLibrary.ConfigurationSource selectedSource="System Configuration Source">
    ///     <sources>
    ///       <add name="System Configuration Source" type="Microsoft.Practices.EnterpriseLibrary.Common.Configuration.SystemConfigurationSource, Microsoft.Practices.EnterpriseLibrary.Common" />
    ///     </sources>
    ///   </enterpriseLibrary.ConfigurationSource>
    /// 
    ///   <loggingConfiguration name="" tracingEnabled="true" defaultCategory="General">
    ///     <listeners>
    ///       <add name="Rolling Flat File Trace Listener" type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.RollingFlatFileTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging"
    ///                 listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.RollingFlatFileTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging"
    ///                 fileName="rolling2.log" formatter="Text Formatter" rollInterval="Day"
    ///                 rollSizeKB="100" traceOutputOptions="DateTime, Timestamp" />
    ///     </listeners>
    /// 
    ///     <formatters>
    ///       <add type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging"
    ///            template="Timestamp: {timestamp}{newline}&#xA;Message: {message}{newline}&#xA;Category: {category}{newline}&#xA;Priority: {priority}{newline}&#xA;EventId: {eventid}{newline}&#xA;Severity: {severity}{newline}&#xA;Title:{title}{newline}&#xA;Machine: {localMachine}{newline}&#xA;App Domain: {localAppDomain}{newline}&#xA;ProcessId: {localProcessId}{newline}&#xA;Process Name: {localProcessName}{newline}&#xA;Thread Name: {threadName}{newline}&#xA;Win32 ThreadId:{win32ThreadId}{newline}&#xA;Extended Properties: {dictionary({key} - {value}{newline})}"
    ///            name="Text Formatter" />
    ///     </formatters>
    ///           
    ///     <categorySources>
    ///        <add switchValue="All" name="General">
    ///          <listeners>
    ///            <add name="Rolling Flat File Trace Listener" />
    ///          </listeners>
    ///       </add>
    ///     </categorySources>
    /// 
    ///     <specialSources>
    ///       <allEvents switchValue="All" name="All Events">
    ///         <listeners>
    ///           <add name="Rolling Flat File Trace Listener" />
    ///         </listeners>
    ///       </allEvents>
    ///       <notProcessed switchValue="All" name="Unprocessed Category">
    ///         <listeners>
    ///           <add name="Rolling Flat File Trace Listener" />
    ///         </listeners>
    ///       </notProcessed>
    ///       <errors switchValue="All" name="Logging Errors &amp; Warnings">
    ///         <listeners>
    ///           <add name="Rolling Flat File Trace Listener" />
    ///         </listeners>
    ///       </errors>
    ///     </specialSources>
    ///   </loggingConfiguration>
    /// </configuration>
    /// ]]>
    /// </code>
    /// </para>
    /// </remarks>
    [DefaultBindingImplementation(typeof(ITracingService), BindingLifetimeType.Undefined, Priority.Normal)]
    public class EnterpriseTracingService : TracingServiceBase, IEnterpriseLoggingService, IHasMediumBindingPriority 
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EnterpriseTracingService"/> class.
        /// </summary>
        /// <internal><para>7/31/2011: Sky</para></internal>
        public EnterpriseTracingService():base(DependencyResolver.Current.GetInstance<ITraceSwitchService>()){}
        #endregion

        #region Implementation of LoggingServiceBase abstract methods

        /// <summary>
        /// Writes the given message to the 
        /// specific implementation's logging system.
        /// </summary>
        /// <param name="traceLevel">The message's tracelevel</param>
        /// <param name="message">The already string formatted message.</param>
        protected override void WriteLine(
            TraceLevel traceLevel,
            string message)
        {

            LogEntry logEntry = new LogEntry();

            //Build message:
            logEntry.Message = message;

            //Use extension method to convert level:
            logEntry.Severity = ((System.Diagnostics.TraceLevel)traceLevel).ToTraceEventType();

            //Use one of the overloads:
            Logger.Write(logEntry);


        }


        /// <summary>
        /// Event handler for when the flush timer goes off.
        /// <para>
        /// Flushes the Logging mechanism.
        /// </para>
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Timers.ElapsedEventArgs"/> instance containing the event data.</param>
        protected override void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            Logger.FlushContextItems();
            //System.Diagnostics.Trace.Flush();
            //Not needed as goes to same place: System.Diagnostics.Debug.Flush();
        }

#endregion
    }
}
