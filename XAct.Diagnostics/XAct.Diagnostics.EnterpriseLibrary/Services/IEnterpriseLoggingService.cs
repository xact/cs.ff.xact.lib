﻿namespace XAct.Diagnostics
{
    /// <summary>
    /// A specialization of 
    /// <see cref="ITracingService"/>
    /// </summary>
    public interface IEnterpriseLoggingService :ITracingService, IHasXActLibService
    {
    }
}