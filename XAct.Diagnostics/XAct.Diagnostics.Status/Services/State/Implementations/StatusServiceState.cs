﻿namespace XAct.Diagnostics.Status.Services.State.Implementations
{
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IStatusServiceState"/>
    /// </summary>
    public class StatusServiceState : IStatusServiceState
    {
    }
}