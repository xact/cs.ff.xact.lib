﻿namespace XAct.Diagnostics.Status.Services.State
{
    /// <summary>
    /// Contract for a singleton state package used by
    /// an implemention of <see cref="IStatusService"/>
    /// </summary>
    public interface IStatusServiceState : IHasXActLibServiceState
    {

    }
}