﻿namespace XAct.Diagnostics.Status
{
    using System;
    using System.Collections.Generic;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Diagnostics.Status.Configuration;

    /// <summary>
    /// Contract for a service to return 
    /// </summary>
    public interface IStatusService : IHasXActLibService
    {

        /// <summary>
        /// Gets the configuration for this service.
        /// </summary>
        IStatusServiceConfiguration Configuration { get; }





        /// <summary>
        /// Gets the specified names.
        /// </summary>
        /// <param name="names">The names.</param>
        /// <param name="startDateTimeUtc">The start date time UTC.</param>
        /// <param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns></returns>
        StatusResponse[] Get(string[] names, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null);



        /// <summary>
        /// Gets a status summary of conditions on the server.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="arguments">Optional arguments that can be dispatched to the controllers.</param>
        /// <param name="startDateTimeUtc">The start date time UTC.</param>
        /// <param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns></returns>
        StatusResponse Get(string name, object arguments = null, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null);

    }
}