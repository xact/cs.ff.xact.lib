﻿namespace XAct.Diagnostics.Status.Services.Implementations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Services.Implementations;
    using XAct.Diagnostics.Status.Configuration;
    using XAct.Diagnostics.Status.Connectors;
    using XAct.Diagnostics.Status.Services.State;
    using XAct.Services;


    /// <summary>
    /// An implementation of <see cref="IStatusServiceState"/>
    /// </summary>
    public class StatusService : XActLibServiceBase, IStatusService
    {
        /// <summary>
        /// Gets the configuration for this service.
        /// </summary>
        public IStatusServiceConfiguration Configuration { get { return _configuration; } }

        readonly IStatusServiceConfiguration _configuration;

        private readonly IStatusServiceState _StatusServiceState;

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusService" /> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="statusServiceConfiguration">The status feed service configuration.</param>
        /// <param name="StatusServiceState">State of the status feed service.</param>
        public StatusService(ITracingService tracingService, IStatusServiceConfiguration statusServiceConfiguration, IStatusServiceState StatusServiceState):base(tracingService)
        {
            _configuration = statusServiceConfiguration;
            _StatusServiceState = StatusServiceState;
        }

        /// <summary>
        /// Gets a colleciton of status summaries of conditions on the server.
        /// </summary>
        /// <param name="nameAndArguments">The name and arguments.</param>
        /// <param name="startDateTimeUtc">The start date time UTC.</param>
        /// <param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public StatusResponse[] Get(StatusRequestItem[] nameAndArguments, DateTime? startDateTimeUtc = null,
                                         DateTime? endDateTimeUtc = null)
        {
            List<StatusResponse> results = new List<StatusResponse>();

            if ((nameAndArguments == null) && (Configuration.AllowNoNames))
            {
                nameAndArguments =
                    Configuration.Connectors.Select(x => new StatusRequestItem {Name = x.Name}).ToArray();
            }


            foreach (StatusRequestItem x in nameAndArguments)
            {
                StatusResponse statusQueryResponse = this.Get(x.Name, x.Arguments, startDateTimeUtc, endDateTimeUtc);

                if (statusQueryResponse == null)
                {
                    continue;
                }

                results.Add(statusQueryResponse);
            }
            return results.ToArray();
        }


        /// <summary>
        /// Gets the specified names.
        /// </summary>
        /// <param name="names">The names.</param>
        /// <param name="startDateTimeUtc">The start date time UTC.</param>
        /// <param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns></returns>
        public StatusResponse[] Get(string[] names, DateTime? startDateTimeUtc = null,
                            DateTime? endDateTimeUtc = null)
        {
            if ((names == null) && (Configuration.AllowNoNames))
            {
                names =
                    Configuration.Connectors.Select(x => x.Name).ToArray();
            }

            return names.Select(x => Get(x, null, startDateTimeUtc, endDateTimeUtc)).ToArray();
        }


        /// <summary>
        /// Gets a status summary of conditions on the server.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="startDateTimeUtc">The start date time UTC.</param>
        /// <param name="endDateTimeUtc">The end date time UTC.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public StatusResponse Get(string name, object arguments=null, DateTime? startDateTimeUtc = null, DateTime? endDateTimeUtc = null)
        {
            IStatusServiceConnector StatusServiceConnector  =
                _configuration.Connectors.SingleOrDefault(x => x.Name == name);

            if (StatusServiceConnector == null)
            {
                return null;
            }

            StatusResponse statusQueryResponse = StatusServiceConnector.Get(arguments);

            return statusQueryResponse;
        }
    }
}