﻿namespace XAct.Diagnostics.Status.Configuration
{
    using System.Collections.Generic;
    using XAct.Diagnostics.Status.Connectors;


    /// <summary>
    /// Contract for a configuration package used by
    /// an implemention of <see cref="IStatusService"/>
    /// </summary>
    public interface IStatusServiceConfiguration : IHasXActLibServiceConfiguration
    {

        /// <summary>
        /// Gets or sets a value indicating whether Giving no names means the service
        /// should return all connector statuses.
        /// The dfault is <c>false</c>.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow no names]; otherwise, <c>false</c>.
        /// </value>
        bool AllowNoNames { get; set; }

        /// <summary>
        /// Collection of registered <see cref="IStatusServiceConnector"/>s.
        /// </summary>
        IEnumerable<IStatusServiceConnector> Connectors { get; }

        /// <summary>
        /// Register a new <see cref="IStatusServiceConnector"/>
        /// so that <see cref="IStatusService"/>
        /// can respond with information as requested.
        /// </summary>
        /// <returns></returns>
        void Register(IStatusServiceConnector connector);

        /// <summary>
        /// Clears the list of registered Connectors.
        /// <para>
        /// Only for use with Unit Tests.
        /// </para>
        /// </summary>
        void Clear();
    }
}