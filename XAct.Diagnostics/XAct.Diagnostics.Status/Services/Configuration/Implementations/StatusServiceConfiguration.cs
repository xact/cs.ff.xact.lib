﻿namespace XAct.Diagnostics.Status.Configuration.Implementations
{
    using System.Collections.Generic;
    using System.Linq;
    using XAct.Diagnostics.Metrics.Services;
    using XAct.Diagnostics.Metrics.Services.Configuration;
    using XAct.Diagnostics.Metrics.Services.Configuration.Implementations;
    using XAct.Diagnostics.Performance;
    using XAct.Diagnostics.Status.Connectors;
    using XAct.Services;

    /// <summary>
    /// An implementation of <see cref="IStatusServiceConfiguration"/>
    /// </summary>
    public class StatusServiceConfiguration : IStatusServiceConfiguration
    {
        private readonly IMetricsFeedService _metricFeedService;
        private readonly IPerformanceCounterService _performanceCounterService;


        /// <summary>
        /// Initializes a new instance of the <see cref="StatusServiceConfiguration" /> class.
        /// </summary>
        /// <param name="metricFeedService">The metric feed service.</param>
        /// <param name="performanceCounterService">The performance counter service.</param>
        public StatusServiceConfiguration(XAct.Diagnostics.Metrics.Services.IMetricsFeedService metricFeedService, IPerformanceCounterService performanceCounterService)
        {
            _metricFeedService = metricFeedService;
            _performanceCounterService = performanceCounterService;
        }


        /// <summary>
        /// Gets or sets a value indicating whether Giving no names means the service
        /// should return all connector statuses.
        /// The dfault is <c>false</c>.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [allow no names]; otherwise, <c>false</c>.
        /// </value>
        public bool AllowNoNames { get; set; }

        /// <summary>
        /// Collection of registered <see cref="IStatusServiceConnector" />s.
        /// </summary>
        public IEnumerable<IStatusServiceConnector> Connectors
        {
            get { return _connectors ?? (_connectors = new List<IStatusServiceConnector>()); }
        }
        private List<IStatusServiceConnector> _connectors;



        /// <summary>
        /// Clears the list of registered Connectors.
        /// <para>
        /// Only for use with Unit Tests.
        /// </para>
        /// </summary>
        public void Clear()
        {
            if (_connectors!=null){_connectors.Clear();}
        }

        /// <summary>
        /// Registers the <see cref="IStatusServiceConnector"/>.
        /// </summary>
        /// <param name="statusServiceConnector">The status service connector.</param>
        public void Register(IStatusServiceConnector statusServiceConnector)
        {
            if (_connectors == null)
            {
                _connectors = new List<IStatusServiceConnector>();
            }
            _connectors.Add(statusServiceConnector);

            IStatusServiceMetricsFeedConnector feedConnector =
                statusServiceConnector as IStatusServiceMetricsFeedConnector;

            if (feedConnector != null)
            {
                foreach (var specs in feedConnector.Configuration.CounterSpecs)
                {
                    string name = specs.GetUniqueName();

                    if (_metricFeedService.Configuration.MetricFeedSourceConnector.All(x => x.Name != name))
                    {
                        _metricFeedService.Configuration.MetricFeedSourceConnector.Add(

                            new PerformanceCounterMetricsFeedSetConnector(_performanceCounterService,specs.CounterCategoryName,specs.CounterName,specs.Multiplier));
                    }
                    
                }
            }



        }
    }
}