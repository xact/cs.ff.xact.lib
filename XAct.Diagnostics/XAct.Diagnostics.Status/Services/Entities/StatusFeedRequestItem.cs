﻿namespace XAct.Diagnostics.Status
{

    using System.Runtime.Serialization;
    using XAct.Diagnostics.Metrics.Services.Configuration;
    using XAct.Diagnostics.Services.Implementations;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class StatusRequestItem : IHasName
    {
        /// <summary>
        /// Gets the name of the <see cref="IMetricsFeedSetConnector"/>
        /// to request a <see cref="StatusResponse"/> from.
        /// </summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>
        /// Gets the optional argument that the 
        /// target <see cref="IMetricsFeedSetConnector"/>
        /// will know what to do with.
        /// </summary>
        [DataMember]
        public object[] Arguments { get; set; }
    }
}