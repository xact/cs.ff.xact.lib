﻿namespace XAct.Diagnostics
{
    /// <summary>
    /// Contract for a service to develop a Web Sequence Diagram textual description.
    /// <para>
    /// It's intended use is for developing diagrams of what has occurred
    /// up to the point of an exception, in order to help understanding how
    /// the application was intended to work.
    /// </para>
    /// </summary>
    public interface ISequenceRegistrationService : IHasXActLibService
    {

        /// <summary>
        /// Prepares the context for the development of a new sequence diagram.
        /// </summary>
        void Clear();

        /// <summary>
        /// Adds a title to the Sequence diagram currently being developed
        /// </summary>
        /// <param name="title"></param>
        void AddTitle(string title);

        /// <summary>
        /// Add a comment to the diagram, before the next Sequence.
        /// </summary>
        /// <param name="comment"></param>
        void AddComment(string comment);


        /// <summary>
        /// Begins a grouping.
        /// </summary>
        /// <param name="groupTitle">The group title.</param>
        /// <param name="prefix">The prefix.</param>
        void BeginGroup(string groupTitle,string prefix="group");

        /// <summary>
        /// Adds a conditional case (use for if/else/else, etc.).
        /// </summary>
        /// <param name="message">The message.</param>
        void AddConditionalCase(string message, string prefix="else");

        /// <summary>
        /// Ends the last opened group.
        /// </summary>
        /// <param name="groupTitle">The group title.</param>
        void EndGroup();


        /// <summary>
        /// Add a new Sequence to the Sequence diagram currently being developed.
        /// </summary>
        /// <param name="fromParticipant">The operation's source payer.</param>
        /// <param name="toParticipant">The operation's target Participant.</param>
        /// <param name="sequenceNote">An optional sequence note to place above the sequence line.</param>
        /// <param name="isResponse">if set to <c>true</c> this is an operation's response (dashed line).</param>
        void AddSequence(string fromParticipant, string toParticipant, string sequenceNote, bool isResponse);

        /// <summary>
        /// Add a new Sequence to the Sequence diagram currently being developed.
        /// <para>
        /// An exception will be raised if this is the first Sequence added, as there will not be a <c>fromParticipant</c> defined.</para>
        /// </summary>
        /// <param name="toParticipant">The operation's target Participant.</param>
        /// <param name="sequenceNote">An optional sequence note to place above the sequence line.</param>
        /// <param name="isResponse">if set to <c>true</c> this is an operation's response (dashed line).</param>
        void AddSequence(string toParticipant, string sequenceNote, bool isResponse);

        /// <summary>
        /// Renders the diagram's textual description.
        /// </summary>
        /// <param name="clearDiagram">if set to <c>true</c> [clear diagram].</param>
        /// <returns></returns>
        string RenderDescription(bool clearDiagram=false);
    }
}