﻿namespace XAct.Diagnostics
{
    using XAct.Diagrams.Uml;

    /// <summary>
    /// Contract for a singelton State object for the <see cref="ISequenceRegistrationService"/>
    /// </summary>
    public interface ISequenceRegistrationServiceState : IHasXActLibServiceState
    {
        /// <summary>
        /// The current Web thread context specific 
        /// <see cref="SequenceDiagram"/>
        /// used with <see cref="ISequenceRegistrationService"/>
        /// </summary>
        SequenceDiagram Current { get; }
    }
}