﻿namespace XAct.Diagnostics
{
    using XAct.Diagrams.Uml;
    using XAct.State;

    /// <summary>
    /// An implementation of the <see cref="ISequenceRegistrationServiceState"/> Contract.
    /// </summary>
    public class SequenceRegistrationServiceState :
        WebThreadSpecificContextManagementServiceBase<SequenceDiagram>,
        ISequenceRegistrationServiceState
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceRegistrationServiceState"/> class.
        /// </summary>
        /// <param name="tracingService">The tracing service.</param>
        /// <param name="contextStateService">The context state service.</param>
        public SequenceRegistrationServiceState(ITracingService tracingService, IContextStateService contextStateService)
            : base(tracingService, contextStateService)
        {
        }
        //protected override SequenceDiagram CreateNewSourceInstance()
    }
}