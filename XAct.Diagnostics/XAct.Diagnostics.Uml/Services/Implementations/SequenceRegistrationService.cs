﻿namespace XAct.Diagnostics.Services.Implementations
{

    /// <summary>
    /// An implementation of the <see cref="ISequenceRegistrationService"/> contract.
    /// </summary>
    public class SequenceRegistrationService : ISequenceRegistrationService
    {


        private readonly ISequenceRegistrationServiceState _SequenceRegistrationServiceState;

        /// <summary>
        /// Initializes a new instance of the <see cref="SequenceRegistrationService"/> class.
        /// </summary>
        /// <param name="SequenceRegistrationServiceState">State of the plant uml web sequence service.</param>
        public SequenceRegistrationService(
            ISequenceRegistrationServiceState SequenceRegistrationServiceState)
        {
            _SequenceRegistrationServiceState = SequenceRegistrationServiceState;
        }

        /// <summary>
        /// Prepares the context for the development of a new sequence diagram.
        /// </summary>
        public void Clear()
        {
            _SequenceRegistrationServiceState.Current.Clear();
        }

        /// <summary>
        /// Adds a title to the Sequence diagram currently being developed
        /// </summary>
        /// <param name="title"></param>
        public void AddTitle(string title)
        {
            _SequenceRegistrationServiceState.Current.SetTitle(title);
        }

        /// <summary>
        /// Add a comment to the diagram, before the next Sequence.
        /// </summary>
        /// <param name="comment"></param>
        public void AddComment(string comment)
        {
            _SequenceRegistrationServiceState.Current.CurrentGroup.AddComment(comment);
        }

        /// <summary>
        /// Begins a grouping.
        /// </summary>
        /// <param name="groupTitle">The group title.</param>
        /// <param name="prefix">The prefix.</param>
        public void BeginGroup(string groupTitle, string prefix = "group")
        {
            _SequenceRegistrationServiceState.Current.CurrentGroup.BeginGroup(groupTitle,prefix);
        }

        /// <summary>
        /// Adds a conditional case (use for if/else/else, etc.).
        /// </summary>
        /// <param name="message">The message.</param>
        public void AddConditionalCase(string message, string prefix = "else")
        {
            _SequenceRegistrationServiceState.Current.CurrentGroup.CreateElseGroup(message,prefix);
        }

        /// <summary>
        /// Ends the last opened group.
        /// </summary>
        public void EndGroup()
        {
            _SequenceRegistrationServiceState.Current.CurrentGroup.EndGroup();
        }


        /// <summary>
        /// Add a new Sequence to the Sequence diagram currently being developed.
        /// </summary>
        /// <param name="fromParticipant">The operation's source payer.</param>
        /// <param name="toParticipant">The operation's target Participant.</param>
        /// <param name="sequenceNote">An optional sequence note to place above the sequence line.</param>
        /// <param name="isResponse">if set to <c>true</c> this is an operation's response (dashed line).</param>
        public void AddSequence(string fromParticipant, string toParticipant, string sequenceNote, bool isResponse)
        {
            _SequenceRegistrationServiceState.Current.CurrentGroup.AddSequence(fromParticipant, toParticipant, sequenceNote,
                                                                  isResponse);

        }

        /// <summary>
        /// Add a new Sequence to the Sequence diagram currently being developed.
        /// <para>
        /// An exception will be raised if this is the first Sequence added, as there will not be a <c>fromParticipant</c> defined.</para>
        /// </summary>
        /// <param name="toParticipant">The operation's target Participant.</param>
        /// <param name="sequenceNote">An optional sequence note to place above the sequence line.</param>
        /// <param name="isResponse">if set to <c>true</c> this is an operation's response (dashed line).</param>
        public void AddSequence(string toParticipant, string sequenceNote, bool isResponse)
        {
            _SequenceRegistrationServiceState.Current.CurrentGroup.AddSequence(toParticipant, sequenceNote, isResponse);
        }

        /// <summary>
        /// Renders the diagram's textual description.
        /// </summary>
        /// <param name="clearDiagram">if set to <c>true</c> [clear diagram].</param>
        /// <returns></returns>
        public string RenderDescription(bool clearDiagram = false)
        {
            return _SequenceRegistrationServiceState.Current.ToString();
        }
    }
}
