using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("XAct.Diagnostics.Log4Net.TestConsole")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("XAct Software Solutions, Inc.")]
[assembly: AssemblyProduct("XAct.Diagnostics.Log4Net.TestConsole")]
[assembly: AssemblyCopyright("Copyright © XAct Software Solutions, Inc. 2011")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

//PLC: [assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

//PLC: [assembly: Guid("306b8aec-639a-4b02-b019-44641e7ce49e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]