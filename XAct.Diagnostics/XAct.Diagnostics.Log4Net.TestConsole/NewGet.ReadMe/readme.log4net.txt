     Use a config file that has:
     <configSections>
       <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler,Log4net"/>
       </configSections>

	   that points to a root dir log4net file that has Compile/CopyToOutput set,

	   that contains something like the following:

	 
	 
	 
     <?xml version="1.0" encoding="utf-8" ?>
      <log4net>
        <root>
          <level value="DEBUG" />
          <appender-ref ref="LogFileAppender" />
          <appender-ref ref="RollingFileAppender" />
          <appender-ref ref="ConsoleAppender" />
        </root>
		
		
		
        <!-- The file will be appended to rather than overwritten each time the logging process starts. -->
        <appender name="LogFileAppender" type="log4net.Appender.RollingFileAppender" >
          <param name="File" value="c:\logfiles\log4net.log.txt" />
          <param name="AppendToFile" value="true" />
          <rollingStyle value="Size" />
          <maxSizeRollBackups value="10" />
          <maximumFileSize value="10MB" />
          <staticLogFileName value="true" />
          <layout type="log4net.Layout.PatternLayout">
            <param name="ConversionPattern" value="YYYY %-5p%d{yyyy-MM-dd hh:mm:ss} � %m%n" />
          </layout>
          <filter type="log4net.Filter.LevelRangeFilter">
            <levelMin value="INFO" />
            <levelMax value="FATAL" />
          </filter>
        </appender>




        <!-- The file written to will always be called log.txt because 
                 the StaticLogFileName param is specified. The file will be rolled based 
                 on a size constraint (RollingStyle). Up to 10 (MaxSizeRollBackups) 
                 old files of 100 KB each (MaximumFileSize) will be kept. 
                 These rolled files will be named: log.txt.1, log.txt.2, log.txt.3, etc... -->
         <appender name="RollingFileAppender" type="log4net.Appender.RollingFileAppender">
           <file value="c:\logfiles\log4net.rollinglog.txt" />
           <appendToFile value="true" />
           <rollingStyle value="Size" />
           <maxSizeRollBackups value="10" />
           <maximumFileSize value="100KB" />
           <staticLogFileName value="true" />
           <layout type="log4net.Layout.PatternLayout">
             <conversionPattern value="%date [%thread] %-5level %logger [%property{NDC}] - %message%newline" />
           </layout>
          <filter type="log4net.Filter.LevelRangeFilter">
            <levelMin value="INFO" />
            <levelMax value="FATAL" />
          </filter>
         </appender>
		 
		 
        <!-- And to Get stuff up on the screen -->
        <appender name="ConsoleAppender" type="log4net.Appender.ConsoleAppender">
          <layout type="log4net.Layout.PatternLayout">
            <conversionPattern value="%message%newline" />
          </layout>
          <filter type="log4net.Filter.LevelRangeFilter">
            <levelMin value="INFO" />
            <levelMax value="FATAL" />
          </filter>
        </appender>


		
	   <appender name="EventLogAppender" type="log4net.Appender.EventLogAppender" >
                <layout type="log4net.Layout.PatternLayout">
                      <conversionPattern value="%date [%thread] %-5level %logger [%property{NDC}] - %message%newline" />
                </layout>
          <filter type="log4net.Filter.LevelRangeFilter">
            <levelMin value="INFO" />
            <levelMax value="FATAL" />
          </filter>
	   </appender>



	   <appender name="SmtpAppender" type="log4net.Appender.SmtpAppender">
              <to value="to@domain.com" />
              <from value="from@domain.com" />
              <subject value="test logging message" />
              <smtpHost value="SMTPServer.domain.com" />
              <bufferSize value="512" />
              <lossy value="false" />
              <layout type="log4net.Layout.PatternLayout">
                  <conversionPattern value="%newline%date [%thread] %-5level %logger [%property{NDC}] - %message%newline%newline%newline" />
              </layout>
          <filter type="log4net.Filter.LevelRangeFilter">
            <levelMin value="ERROR" />
            <levelMax value="FATAL" />
          </filter>
       </appender>
     </log4net>
