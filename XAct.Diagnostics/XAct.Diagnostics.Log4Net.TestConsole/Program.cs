﻿//using XAct.Diagnostics.Tests;

namespace XAct.Diagnostics.Log4Net.TestConsole
{
    using System.Diagnostics;
    using System.Threading;
    using XAct.Bootstrapper.Tests;
    using XAct.Environment;
    using XAct.Tests;
    using log4net;
    using log4net.Repository;

    internal class Program
    {
        private static void Main(string[] args)
        {

                                    Singleton<IocContext>.Instance.ResetIoC();

            ITracingService tracingService;
            tracingService = DependencyResolver.Current.GetInstance<ITracingService>();

            IDateTimeService dateTimeService = DependencyResolver.Current.GetInstance<IDateTimeService>();

            //Notice how Now is being offset by an amount defined in the config file...
            Trace.TraceInformation(dateTimeService.NowUTC.ToStandardStringWithMilliseconds());
            Trace.WriteLine(".");
            for (int i = 0; i < 5;i++ )
            {
                tracingService.QuickTrace(dateTimeService.NowUTC.ToStandardStringWithMilliseconds());
                //Trace.TraceInformation(environmentService.Now.ToStandardStringWithMilliseconds());
                Thread.Sleep(100);
            }

            ILoggerRepository repository = LogManager.GetRepository();
            //get all of the appenders for the repository 
            
            repository.SetFileAppenderPath("RollingFileAppender", @"c:\LogFiles\Ananas.log");

            Trace.WriteLine(".");
            for (int i = 0; i < 5; i++)
            {
                tracingService.Trace(Diagnostics.TraceLevel.Info, dateTimeService.NowUTC.ToStandardStringWithMilliseconds());
                Thread.Sleep(100);
            }

            Trace.TraceInformation("Some info....");
            Trace.TraceWarning("Some warning....");
            Trace.TraceError("Some Error....");

            
            //UnitTests1 x = new UnitTests1();
            //x.UnitTest01();
        }
    }
}